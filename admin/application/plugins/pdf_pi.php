<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once('pdf/config/lang/eng.php');
require_once('pdf/tcpdf.php');

function pdf(){
         return new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
}