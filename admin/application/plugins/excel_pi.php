<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'plugins/Excel/reader.php');

function Excel($filename,$html_text){
   Header("Content-type: application/octet-stream");
   Header("Content-Disposition: attachment; filename=$filename");
   $doc = new Document();
   $doc->parce_HTML($html_text);
   $fin = $doc->get_rtf();
   echo $fin;
}
