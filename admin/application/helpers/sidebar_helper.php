<? 	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	function construct_submenus($_menus){
		if($_menus){ 
			$str="<ul>";
			foreach($_menus as $menu){
				if($submenus=construct_submenus($menu['submenus']))
					$str.="<li><a class='gonext'><div><div><span>".$menu['name']."</span></div></div></a>".$submenus."</li>";			
				else {
					$url = ($menu['controller_url']?$menu['controller_url'].'/':'').
							($menu['controller']?$menu['controller'].'/':'').
							($menu['action']?$menu['action'].'/':'');
					$str.="<li><a href='".site_url($url)."'><div><div><span>".$menu['name']."</span></div></div></a></li>";
				}
			}
			$str.="</ul>";
			return $str;
		}
		return false;
	}
	
	function construct_submenus_peta($_menus){
		if($_menus){ 
			$str="";
			foreach($_menus as $menu){
				if($submenus=construct_submenus_peta($menu['submenus']))
					$str.="<li><a class='gonext'><div><div><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&diams;&nbsp;".$menu['name']."</span></div></div></a>".$submenus."</li>";			
				else {
					$url = ($menu['controller_url']?$menu['controller_url'].'/':'').
							($menu['controller']?$menu['controller'].'/':'').
							($menu['action']?$menu['action'].'/':'');
					$str.="<li><a href='".site_url($url)."'><div><div><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&diams;&nbsp;".$menu['name']."</span></div></div></a></li>";
				}
			}
			$str.="";
			return $str;
		}
		return false;
	}
?>