<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


function confirm($message = 'Are you sure?')
{
	$CI	=& get_instance();
	$segments = $CI->uri->segment_array();
	
	if( end($segments) != 'yes')
	{
		$referrer = $CI->input->server('HTTP_REFERER');
		if (empty($referrer))
		{
			$RTR =& load_class('Router');
			$referrer = $RTR->fetch_directory().$RTR->fetch_class();
		}

		$data['no_href']	= $referrer;
		$data['yes_href']	= implode('/', $segments).'/yes';
		$data['message']	= $message;
				
		echo $CI->template->display('template/content/confirmation', $data, 'general', TRUE);
		exit;
	}
}

function set_success($text)
{
	$CI	= & get_instance();
	$CI->session->set_flashdata('success', $text);
}

function set_error($text)
{
	$CI	= & get_instance();
	$CI->session->set_flashdata('error', $text);
}

