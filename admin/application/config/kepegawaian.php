<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


$config['site_name'] 	= 'SIMPEGA';
$config['site_email'] 	= 'fatikha@gmail.com'; 
$config['logout_confirmation']	= FALSE;


$config['default_title'] 		= "SIMPEGA | Sistem Informasi Kepegawaian";
$config['default_description'] 	= "Sistem Informasi Kepegawaian - UM";
$config['default_keywords'] 	= "simpega, sistem informasi kepegawaian, um";


$config['auth']['default_group'] = '2';
$config['auth']['salt']	= 'D9jWfyrnbRbOW7LhogYrD0oI8i4QpFGZcNKdkov58WZl0DK2CBFBb3MuHPRik8Y';


$config['auth']['email_activation'] = FALSE;


$config['auth']['mail']['mailtype']		= 'html';
$config['auth']['mail']['protocol'] 	= 'sendmail';
$config['auth']['mail']['smtp_host'] 	= ''; 
$config['auth']['mail']['smtp_user'] 	= '';
$config['auth']['mail']['smtp_pass'] 	= ''; 
$config['auth']['mail']['smtp_port'] 	= '';
$config['auth']['mail_from_email'] 		= $config['site_email'];
$config['auth']['mail_from_name'] 		= $config['site_name'];

/*
|--------------------------------------------------------------------------
| Email Subjects
|--------------------------------------------------------------------------
|
*/
$config['auth']['email_activation_subject']	= 'Member Account Activation';
$config['auth']['new_password_subject']		= 'Your New Member Password';

/*
|--------------------------------------------------------------------------
| Default Group
|--------------------------------------------------------------------------
|
| The default group your users will acquire
|
*/
$config['auth']['default_group'] = '2';


?>