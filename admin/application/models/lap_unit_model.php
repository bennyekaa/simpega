<?
class lap_unit_model extends Model {
	var $record_count;
	var $table				= 'unit_kerja';
	var $groups_table		= 'unit_kerja';
	var $primary_key 		= 'kode_unit';

	function lap_unit_model()
	{
		parent::Model();
	}
	
    function findAll($order = NULL,$start = NULL, $count = NULL)
    {
        return $this->find(NULL, NULL, $order ,$start, $count);
    }

    function findByFilter($filter_rules, $order = NULL, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL, $filter_rules, $order, $start, $count);
    }

	function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
	{
		$results = array();
		$this->_set_where($filters);

		//$this->db->from($this->table);
		$this->db->from('pegawai');
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($order);

		//$this->db->join('pegawai','pegawai.kode_unit = unit_kerja.kode_unit');

		if ($count){
			if ($start) {
				$this->db->limit($count, $start);
			}
			else {
				$this->db->limit($count);
			}
		}
		//$this->db->join('unit_kerja','unit_kerja.kode_unit = erja.FK_unit_kerja');
        
		$query = $this->db->get( $this->table );
        
        //echo ($this->db->last_query());
		if ($query->num_rows() > 0)
		{
            return $query->result_array();
        }
        else
		{
            return FALSE;
		}
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		//$this->db->join('nama_unit','unit_kerja.kode_unit = kerja.FK_unit_kerja');
		//$this->db->join('pegawai','pegawai.kode_unit = unit_kerja.kode_unit');
		$this->db->where($this->primary_key, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);

	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }

	    return $results;
	}
	
	function _set_where($filters=NULL)
	{
	    if ($filters)
	    {
			if ( is_string($filters) )
			{
				$this->db->where($filters);
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
				foreach ($filters as $field => $value)
					$this->db->where($field, $value);
				}
			}
	    }
	}

	function _set_order($order=NULL)
	{
	    if ($order)
	    {
			if ( is_string($order) )
			{
				$this->db->order_by($order . " desc");
			}
			elseif ( is_array($order) )
			{
				if ( count($order) > 0 )
				{
				   foreach ($order as $field => $value)
					$this->db->order_by($field, $value);
				}
			}
	    }
	}
}
?>