<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayat_Penghargaan_model extends Model
{
    var $record_count;
    var $table_name= 'riwayat_penghargaan';
    var $pkey = 'id_riwayat_penghargaan';

	function Riwayat_Penghargaan_model()
    {
        parent::Model();
    }

    function findAll($fields=NULL,$start = NULL, $count = NULL)
    {
        return $this->find($fields,NULL, NULL,$start, $count);
    }

    function findByFilter($filter_rules, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL,$filter_rules, NULL,$start, $count);
    }

    function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
    {
      	$results = array();
		$this->_set_where($filters);

		$this->db->from($this->table_name);
		 $this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($order);

		if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}
		//$this->db->join('master_jenis_penghargaan','master_jenis_penghargaan.kd_jenis_penghargaan=riwayat_penghargaan.FK_jenis_penghargaan','left');
		$query = $this->db->get( $this->table_name );
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
	}

	function retrieve_by_pkey($idField)
	{
		$results = array();
		
		//$this->db->join('master_jenis_penghargaan','master_jenis_penghargaan.kd_jenis_penghargaan=riwayat_penghargaan.FK_jenis_penghargaan','left');
		$this->db->where($this->pkey, $idField);
		$this->db->limit( 1 );
		$query = $this->db->get( $this->table_name );
		
		if ($query->num_rows() > 0)
		{
		   $row = $query->row_array();
		   $results		 = $row;
		}
		else
		{
		   $results = false;
		}
		return $results;
	}
   
	function retrieve_by_idpeg($idField)
	{
		//$this->db->join('master_jenis_penghargaan','master_jenis_penghargaan.kd_jenis_penghargaan=riwayat_penghargaan.FK_jenis_penghargaan','left');
		//$this->db->join('master_tingkat_penghargaan','master_tingkat_penghargaan.kd_tingkat_penghargaan=master_jenis_penghargaan.FK_tingkat_penghargaan','left');
		$this->db->where('kd_pegawai',$idField);
		$query = $this->db->get($this->table_name);
		//echo $this->db->last_query();
		$str = $query->result_array();
		return $str;
	}
	
    function add( $data )
    {
		$this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    function update($keyvalue, $data)
    {
        $this->db->where($this->pkey, $keyvalue);
        $this->db->update($this->table_name, $data);
    }

    function delete($idField)
    {
        $this->db->where($this->pkey, $idField);
        $this->db->delete($this->table_name);
        return true;
    }

    function _set_where($filters=NULL)
    {
        if ($filters)
		{
			if (is_string($filters))
			{
				$this->db->where($filters);
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
					foreach ($filters as $field => $value)
						$this->db->where($field, $value);
				}
			}
		}
    }

    function _set_order($order=NULL)
    {
        if ($order)
		{
            if (is_string($order))
			{
				$this->db->order_by($order);
			}
            elseif (is_array($order))
            {
                if (count($order) > 0)
                {
					foreach ($order as $field => $value)
                        $this->db->order_by($field, $value);
                }
			}
		}
    }
    
	function get_idpenghargaan_by_userid($userid)
	{
		$has = $this->db->get_where($this->table_name,array('kd_pegawai' => $userid));   
		$data = $has->row_array();
			
		return $data['id_riwayat_penghargaan'];
	}
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table_name."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
}
?>