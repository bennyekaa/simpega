<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mutasi_model extends Model
{

	var $record_count;
	var $table		= 'mutasi';
	var $pkey 		= 'id_mutasi';
	
	function Mutasi_model()
	{
		parent::Model();
	}
	
	function retrieve_by_idpeg($id)
	{
		$results = array();
		$this->db->select('a.no_SK, a.tgl_SK, a.tmt_SK,
						a.tgl_mutasi, a.ket_mutasi, d.jenis_mutasi,
						(select b.jabatan from master_jabatan b where b.kd_jabatan = a.FK_jabatan_asal) as jabatan_asal,
						(select b.jabatan from master_jabatan b where b.kd_jabatan = a.FK_jabatan_baru) as jabatan_baru,
						(select c.unit_kerja from unit_kerja c where c.kode_unit = a.FK_unit_kerja_asal) as unit_kerja_asal,
						(select c.unit_kerja from unit_kerja c where c.kode_unit = a.FK_unit_kerja_baru) as unit_kerja_baru'
						);
		$this->db->from('mutasi a');
		$this->db->join('jenis_mutasi d','d.jns_mutasi=a.jns_mutasi','left');
		$this->db->where('FK_pegawai', $id);
		$this->db->order_by('a.tgl_mutasi');
		$query = $this->db->get();
		//echo $this->db->last_query();
	    
	    if ($query->num_rows() > 0)
	    {
	       $results	= $query->result_array();
	    }
	    else
	    {
	       $results = false;
	    }
		return $results;
	}
	
	function findAll($jumlah = Null, $mulai = 0,$ordby = Null)
	{
		return $this->find(NULL, $jumlah, $mulai,$ordby);
	}
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->select('a.no_SK, a.tgl_SK, a.tmt_SK,
				a.tgl_mutasi, a.ket_mutasi, d.jenis_mutasi,
				(select b.jabatan from master_jabatan b where b.kd_jabatan = a.FK_jabatan_asal) as jabatan_asal,
				(select b.jabatan from master_jabatan b where b.kd_jabatan = a.FK_jabatan_baru) as jabatan_baru,
				(select c.unit_kerja from unit_kerja c where c.kode_unit = a.FK_unit_kerja_asal) as unit_kerja_asal,
				(select c.unit_kerja from unit_kerja c where c.kode_unit = a.FK_unit_kerja_baru) as unit_kerja_baru'
				);
		$this->db->from('mutasi a');
		$this->db->join('jenis_mutasi d','d.jns_mutasi=a.jns_mutasi','left');
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get();
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
	}
	
	/*function find($filters = NULL, $jumlah = -1, $mulai = NULL,$ordby = Null)
    {
		$results = array();
		$this->db->select('a.no_SK, a.tgl_SK, a.tmt_SK,
			a.tgl_mutasi, a.ket_mutasi, d.jenis_mutasi,
			(select b.jabatan from master_jabatan b where b.kd_jabatan = a.FK_jabatan_asal) as jabatan_asal,
			(select b.jabatan from master_jabatan b where b.kd_jabatan = a.FK_jabatan_baru) as jabatan_baru,
			(select c.unit_kerja from unit_kerja c where c.kode_unit = a.FK_unit_kerja_asal) as unit_kerja_asal,
			(select c.unit_kerja from unit_kerja c where c.kode_unit = a.FK_unit_kerja_baru) as unit_kerja_baru'
			);
		$this->db->from('mutasi a');
		$this->db->join('jenis_mutasi d','d.jns_mutasi=a.jns_mutasi','left');
		$this->_set_where($filters);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		
		if ($jumlah) {
		   if ($mulai) {
			  $this->db->limit( $jumlah,$mulai);
		   }
		   else {
			  $this->db->limit($jumlah);
		   }
		}
		if($ordby)
		$this->db->select('a.no_SK, a.tgl_SK, a.tmt_SK,
				a.tgl_mutasi, a.ket_mutasi, d.jenis_mutasi,
				(select b.nama_jabatan_s from jabatan_struktural b where b.id_jabatan_s = a.id_jabatan_asal) as jabatan_asal,
				(select b.nama_jabatan_s from jabatan_struktural b where b.id_jabatan_s = a.id_jabatan_baru) as jabatan_baru
				');
		$this->db->from('mutasi a');
		$this->db->join('jenis_mutasi d','d.jns_mutasi=a.jns_mutasi','left');
		$this->db->order_by($ordby."tmt_SK asc");
		$query = $this->db->get();
	
		if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else 
			{
				return FALSE;
		}
    }*/
	function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
    {
      	$results = array();
		$this->_set_where($filters);

		$this->db->from($this->table_name);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($order);

		
		if($ordby)
		$this->db->join('pegawai a','a.kd_pegawai=mutasi.kd_pegawai','left');
		//$this->db->join('jabatan_struktural','jabatan_struktural.id_jabatan_s=riwayat_jabatan_struktural.id_jabatan_s');
		$query = $this->db->get( $this->table_name );
		echo $this->db->last_query();
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
	}
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	
	function delete($id)
	{
		$this->db->delete($this->table, array($this->pkey => $id));
	}
	
	function update($id, $data)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table, $data);
	}

	function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
            }
			elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value) 
                        $this->db->where($field, $value);               
                }
			}
		}
    }
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
            if ( is_string($order) ) 
			{
                $this->db->order_by($order);
			}
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
					foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
			}
		}
    }
	
	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}

	function get_tipe_mutasi(){
		return $this->db->select("jns_mutasi, nama_mutasi")->from("jenis_mutasi")->get()->result_array();
	}

	function get_data_mutasi(){
		$sql="select mutasi.kd_pegawai,pegawai.nama_pegawai, pegawai.gelar_depan, pegawai.gelar_belakang, pegawai.NIP,golongan_pangkat.golongan, golongan_pangkat.pangkat, EXTRACT(YEAR FROM mutasi.tmt_SK) as tahun_sk
from pegawai inner join mutasi on pegawai.kd_pegawai=mutasi.kd_pegawai
inner join golongan_pangkat on golongan_pangkat.id_golpangkat=pegawai.id_golpangkat_terakhir
order by tgl_mutasi desc";

return $this->db->query($sql)->result_array();
	}

	// function get_jabatan_remun($tahun,$id_mutasi){
	// 	$sql="select jabatan_asal.nama_jabatan_uj as nama_jabatan_asal, jabatan_baru.nama_jabatan_uj as nama_jabatan_baru from mutasi
	// 			inner join jenis_jabatan_uj_remun".$tahun." as jabatan_asal on mutasi.id_jabatan_asal=jabatan_asal.id_jabatan_uj
	// 			inner join jenis_jabatan_uj_remun".$tahun." as jabatan_baru on mutasi.id_jabatan_baru=jabatan_baru.id_jabatan_uj
	// 			where mutasi.id_mutasi='".$id_mutasi."'
	// 	";

	// 	return $this->db->query($sql)->result_array();
	// }

	// 	function get_jabatan($id_mutasi){
	// 	$sql="select jabatan_asal.nama_jabatan  as nama_jabatan_asal, jabatan_baru.nama_jabatan  as nama_jabatan_baru from mutasi
	// 			inner join jabatan as jabatan_asal on mutasi.id_jabatan_asal=jabatan_asal.id_jabatan
	// 			inner join jabatan as jabatan_baru on mutasi.id_jabatan_baru=jabatan_baru.id_jabatan
	// 			where mutasi.id_mutasi='".$id_mutasi."'
	// 	";

	// 	// var_dump($sql);exit();

	// 	return $this->db->query($sql)->result_array();
	// }


function get_unit_kerja($kd_pegawai){
$sql="select unit_kerja_asal.nama_unit as nama_unit_kerja_asal, unit_kerja_baru.nama_unit as nama_unit_kerja_baru
		from mutasi inner join unit_kerja as unit_kerja_asal on mutasi.kode_unit_kerja_asal=unit_kerja_asal.kode_unit
		inner join unit_kerja as unit_kerja_baru on mutasi.kode_unit_kerja_baru=unit_kerja_baru.kode_unit
		where mutasi.kd_pegawai='".$kd_pegawai."'";

	return $this->db->query($sql)->result_array();
}

}
