<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tracker_model extends Model {

	var $table	= 'tracker';

	/**
	 * Constructor
	 *
	 * @access	public
	 */
	function Tracker_model()
	{
		parent::Model();
	}

	// --------------------------------------------------------------------

	/**
	 * Get IP Info
	 *
	 * Gets information on an ip address
	 *
	 * @access	private
	 * @return	mixed
	 */
	function get_ip_info()
	{
		$this->db->where('ip_address', $this->input->ip_address());
		$i = $this->db->get($this->table, 1, 0);

		return $var = ($i->num_rows() > 0) ? $i->row() : FALSE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Add IP
	 *
	 * Add an ip address to the tracker
	 *
	 * @access	private
	 * @return	mixed	new row
	 */
	function add_ip()
	{
		$this->db->set('ip_address', $this->input->ip_address());
		$this->db->insert($this->table);
		
		$this->db->where('tracker_id', $this->db->insert_id());
		$query = $this->db->get($this->table);
		return $query->row();
	}
	
	// --------------------------------------------------------------------

	/**
	 * Increment failure count and set last login time
	 *
	 * @access	public
	 * @return	object	user object
	 */
	function increment_failures($failed_so_far)
	{
		$now = time();
		$this->db->where('ip_address', $this->input->ip_address());
		
		if($failed_so_far < 4) // Not relevant beyond this point
		{
			$this->db->set('failed_logins', 'failed_logins + 1', FALSE);
		}
		
		$this->db->set('last_failure', $now);
		$this->db->update($this->table);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Resets login failure count
	 *
	 * @access	public
	 * @return	object	user object
	 */
	function reset_failures()
	{		
		$this->db->where('ip_address', $this->input->ip_address());
		$this->db->set('failed_logins', 0);
		
		$this->db->update($this->table);
	}

}
// END Tracker_model class

/* End of file tracker_model.php */
/* Location: ./application/models/tracker_model.php */