<?
class lap_gaji_model extends Model {
	var $record_count;
	var $table				= 'pegawai';
	var $groups_table		= 'pegawai';
	var $primary_key 		= 'kd_pegawai';

	function lap_gaji_model()
	{
		parent::Model();
	}
	
    function findAll($order = NULL,$start = NULL, $count = NULL)
    {
        return $this->find(NULL, NULL, $order ,$start, $count);
    }

    function findByFilter($filter_rules, $order = NULL, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL, $filter_rules, $order, $start, $count);
		
    }

	function getGajiPegawai($id_golpangkat,$masa_kerja)
	{
		$query =$this->db->query("SELECT gaji_pokok from acuan_gaji_pokok where id_golpangkat = '".$id_golpangkat."' 
								  and masa_kerja = '".$masa_kerja."'");
		if ($query->num_rows() > 0)
			{
				//return $query->result_array();
				$res = $query->row_array();
				return $res['gaji_pokok'];
			}
        else
			{
				$res = 0;
				return $res;
			}
	}
	
	function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
	{
		$results = array();

		$this->_set_where($filters);
		$this->_set_order($order);
		//$this->db->join('pegawai','pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat');
        
        $query = $this->db->get( $this->table );
        $this->record_count = $query->num_rows();

        $this->db->limit($start, $count);
		/*if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}*/
		//$this->db->join('nama_unit','unit_kerja.kode_unit = kerja.FK_unit_kerja');
		$this->_set_where($filters);
		$this->_set_order($order);
		//$this->db->join('pegawai','pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat');
		$query = $this->db->get( $this->table );
        //echo ($this->db->last_query());
		if ($query->num_rows() > 0)
		{
            return $query->result_array();
        }
        else
		{
            return FALSE;
		}
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		//$this->db->join('nama_unit','unit_kerja.kode_unit = kerja.FK_unit_kerja');
		//$this->db->join('pegawai','pegawai.kode_unit = unit_kerja.kode_unit');
        //$this->db->join('pegawai','pegawai.kode_unit_induk=unit_kerja.kode_unit');
		$this->db->where($this->primary_key, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);

	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }

	    return $results;
	}
	
	function _set_where($filters=NULL)
	{
	    if ($filters)
	    {
			if ( is_string($filters) )
			{
				$this->db->where($filters);
                
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
				foreach ($filters as $field => $value)
					$this->db->where($field, $value);
				}
			}
	    }
	}
    function _set_or_where($filters=NULL)
	{
	    if ($filters)
	    {
			if ( is_string($filters) )
			{
				$this->db->or_where($filters);
                
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
				foreach ($filters as $field => $value)
					$this->db->or_where($field, $value);
				}
			}
	    }
	}
	function _set_order($order=NULL)
	{
	    if ($order)
	    {
			if ( is_string($order) )
			{
				$this->db->order_by($order . " desc");
			}
			elseif ( is_array($order) )
			{
				if ( count($order) > 0 )
				{
				   foreach ($order as $field => $value)
					$this->db->order_by($field, $value);
				}
			}
	    }
	}
}
?>