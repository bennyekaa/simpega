<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai_model extends Model {
    
	var $record_count;
    var $table = 'pegawai';
	var $table_pensiun = 'pegawai_pensiun';
	var $table_absensi = 'absensi';
    var $primary_key = 'kd_pegawai';
	
    function Pegawai_model(){
		parent::Model();
    }

	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->primary_key,$field);
	}

  function rawQuery($sql){
    return $this->db->query($sql);
  }
	
  	function findAll($jumlah = Null, $mulai = 0,$ordby = Null)
	{
		return $this->find(NULL, $jumlah, $mulai,$ordby);
	}

    function findById($key_value)
    {
		return $this->find(array($this->primary_key => $key_value));
    }

    function findByFilter($filter_rules, $order = NULL, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL, $filter_rules, $order, $start, $count);
    }

    function find($filters = NULL, $jumlah = 1, $mulai = NULL)
    {
		$results = array();
		$this->_set_where($filters);

		$this->db->from($this->table);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
        
		$this->_set_order($order);

		//$this->db->join('pegawai','pegawai.kode_unit = unit_kerja.kode_unit');
        //$this->db->join('pegawai','pegawai.kode_unit_induk=unit_kerja.kode_unit');

		if ($count){
			if ($start) {
				$this->db->limit($count, $start);
			}
			else {
				$this->db->limit($count);
			}
		}
		//$this->db->join('unit_kerja','unit_kerja.kode_unit = erja.FK_unit_kerja');
        
		$query = $this->db->get( $this->table );
        
        //echo ($this->db->last_query());
		if ($query->num_rows() > 0)
		{
            return $query->result_array();
        }
        else
		{
            return FALSE;
		}
	}


   function retrieve_by_pkey($idField)
   {
		$results = array();
		$this->db->where( $this->primary_key, $idField);
		$this->db->limit( 1 );
		$query = $this->db->get( $this->table );
	  
		if ($query->num_rows() > 0){
		   $row = $query->row_array();
		   $results		 = $row;
		}
		else{
		   $results = false;
		}	  
		return $results;
   }
   
   function retrieve_by_pkey_p($idField)
   {
		$results = array();
		$this->db->where( $this->primary_key, $idField);
		$this->db->limit( 1 );
		$query = $this->db->get( $this->table_pensiun );
	  
		if ($query->num_rows() > 0){
		   $row = $query->row_array();
		   $results		 = $row;
		}
		else{
		   $results = false;
		}	  
		return $results;
   }



    function add( $data )
    {
		$this->db->insert($this->table, $data);	
		return $this->db->insert_id();
			
    }

    public function update($id, $data)
	{	
		$this->db->where("kd_pegawai", $id);
		$this->db->update('pegawai',$data);

		return ($this->db->affected_rows()>0)? true : false;
	}

    function modify($keyvalue, $data)
    {
		$this->db->where($this->primary_key, $keyvalue);
		$this->db->update($this->table, $data);
    }
	
	function modify_p($keyvalue, $data)
    {
		$this->db->where($this->primary_key, $keyvalue);
		$this->db->update($this->table_pensiun, $data);
    }
	
    function delete_by_pkey($idField)
    {
		$this->db->where($this->primary_key, $idField);
		$this->db->delete($this->table);

		return true;
    }
	
	function delete_by_pkey_p($idField)
    {
		$this->db->where($this->primary_key, $idField);
		$this->db->delete($this->table_pensiun);

		return true;
    }

    function _set_where($filters=NULL)
	{
	    if ($filters)
	    {
			if ( is_string($filters) )
			{
				$this->db->where($filters);
                
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
				foreach ($filters as $field => $value)
					$this->db->where($field, $value);
				}
			}
	    }
	}
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
            if ( is_string($order) ) 
			{
                $this->db->order_by($order);
			}
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
		   foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
			}
		}
    }
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}	
	
	function get_id_p()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table_pensiun."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function get_id_absensi()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table_absensi."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function get_all_by($field)
	{
		$results = array();
		$this->db->select('*');
		/*$this->db->join('agama b','b.kd_agama=a.kd_agama');
		$this->db->join('kelurahan c','c.kd_kelurahan=a.kd_kelurahan');
		$this->db->join('jenis_pegawai d','d.id_jns_pegawai=a.id_jns_pegawai');
		$this->db->join('unit_kerja e','e.kode_unit=a.kode_unit');
		$this->db->join('riwayat_gol_kepangkatan f','f.id_riwayat_gol=a.id_golpangkat_terakhir');
		$this->db->join('riwayat_jabatan g','g.id_riwayat_jabatan=a.id_jabatan_terakhir');
		$this->db->join('riwayat_pendidikan h','h.id_riwayat_jabatan=a.id_pendidikan_terakhir');
		*/
        
		//$this->db->where('a.aktif','1');
		$this->db->from('pegawai');
		$this->db->order_by('id_golpangkat_terakhir', desc);
		if( isset($field['kode_unit']) )
		{
			$this->db->where($field);
    }
    else
    {
			$this->db->like($field);
		}
		$query = $this->db->get();
        //echo $this->db->last_query();
		if ($query->num_rows() > 0){
		   $results	= $query->result_array();
		}
		else{
		   $results = false;
		}	  
		return $results;
	}
	function get_namapegawai_by_idpegawai($id)
	{
		$this->db->where('kd_pegawai',$id);
		$query = $this->db->get($this->table_name);   		
		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
			return $data['kd_pegawai'];
		}
		else
		{
			return FALSE;
		}
		//return $data;		
	}
	function get_active($idField)
	{
		$results = array();
		$this->db->select('aktif');
		$this->db->where( $this->primary_key, $idField);
		$this->db->limit( 1 );
		$query = $this->db->get( $this->table );
	  
		if ($query->num_rows() > 0){
		   $row = $query->row_array();
		   $results		 = $row;
		}
		else{
		   $results = false;
		}	  
		return $results;
	}
	
	
	
	function getAll($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
    {
		$results = array();
		$this->_set_where($filters);
		$this->db->from($this->table);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($order);

		if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}
		if($ordby)
		$this->db->select('pegawai.*, golongan_pangkat.golongan, golongan_pangkat.pangkat, unit_kerja.unit_kerja');
		$this->db->join('golongan_pangkat','golongan_pangkat.id_golpangkat=pegawai.id_golpangkat_terakhir');
		$this->db->join('unit_kerja c','c.kode_unit=pegawai.kode_unit');
		$query = $this->db->get( $this->table );
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
    }

    function get_list_pegawai(){
		$query=$this->db->query("	SELECT
					pegawai.kd_pegawai,
					pegawai.NIP,
					pegawai.gelar_depan,
					pegawai.nama_pegawai,
					pegawai.gelar_belakang,
					pegawai.npwp,
					pegawai.email,
					agama.agama,
					jenis_pegawai.jenis_pegawai,
					unit_kerja.nama_unit,
					golongan_pangkat.golongan
					FROM
					pegawai
					INNER JOIN agama ON agama.kd_agama = pegawai.kd_agama
					INNER JOIN jenis_pegawai ON jenis_pegawai.id_jns_pegawai = pegawai.id_jns_pegawai
					INNER JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
					INNER JOIN golongan_pangkat ON golongan_pangkat.id_golpangkat = pegawai.id_golpangkat_terakhir
					where nip not like '' and nip not like '-' and status_pegawai < 3 ORDER BY pegawai.id_jns_pegawai asc, pegawai.id_golpangkat_terakhir DESC, pegawai.NIP 
					");
	
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
    }

     function get_list_ptt(){
		$query=$this->db->query("	SELECT
					pegawai.kd_pegawai,
					pegawai.NIP,
					pegawai.gelar_depan,
					pegawai.nama_pegawai,
					pegawai.gelar_belakang,
					pegawai.npwp,
					pegawai.email,
					agama.agama,
					jenis_pegawai.jenis_pegawai,
					unit_kerja.nama_unit
					FROM
					pegawai
					INNER JOIN agama ON agama.kd_agama = pegawai.kd_agama
					INNER JOIN jenis_pegawai ON jenis_pegawai.id_jns_pegawai = pegawai.id_jns_pegawai
					INNER JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
					where nip not like '' and nip not like '-' and status_pegawai > 2 ORDER BY pegawai.id_jns_pegawai asc, pegawai.id_golpangkat_terakhir DESC, pegawai.NIP 
					");
	
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
    }

    function get_list_cpns(){
      $query=$this->db->query(" SELECT
        pegawai.kd_pegawai,
        pegawai.NIP,
        pegawai.gelar_depan,
        pegawai.nama_pegawai,
        pegawai.gelar_belakang,
        pegawai.npwp,
        pegawai.email,
        pegawai.id_golpangkat_terakhir,
        agama.agama,
        jenis_pegawai.jenis_pegawai,
        unit_kerja.nama_unit
        FROM pegawai
        INNER JOIN agama ON agama.kd_agama = pegawai.kd_agama
        INNER JOIN jenis_pegawai ON jenis_pegawai.id_jns_pegawai = pegawai.id_jns_pegawai
        INNER JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
        where nip not like '' and nip not like '-' and (status_pegawai = 1 and pegawai.id_jns_pegawai like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen')) ORDER BY pegawai.id_jns_pegawai asc, pegawai.id_golpangkat_terakhir DESC, pegawai.NIP 
        ");

      if ($query->num_rows() > 0){
        return $query->result_array();
      }
      else{
        return FALSE;
      }
    }

    function count_dosen(){
      $query=$this->db->query(" SELECT
        pegawai.NIP,
        jenis_pegawai.jenis_pegawai,
        unit_kerja.nama_unit
        FROM pegawai
        INNER JOIN jenis_pegawai ON jenis_pegawai.id_jns_pegawai = pegawai.id_jns_pegawai
        INNER JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
        where nip != '' and nip != '-' and (status_pegawai >= 1 and pegawai.id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen')) ORDER BY pegawai.id_jns_pegawai asc, pegawai.id_golpangkat_terakhir DESC, pegawai.NIP 
        ");

        return $query->num_rows();
    }

    function count_tendik(){
      $query=$this->db->query(" SELECT
        pegawai.NIP,
        jenis_pegawai.jenis_pegawai,
        unit_kerja.nama_unit
        FROM pegawai
        INNER JOIN jenis_pegawai ON jenis_pegawai.id_jns_pegawai = pegawai.id_jns_pegawai
        INNER JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
        where nip != '' and nip != '-' and ((status_pegawai IN (1,2) and pegawai.id_jns_pegawai != (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen')) OR status_pegawai IN (3, 4)) ORDER BY pegawai.id_jns_pegawai asc, pegawai.id_golpangkat_terakhir DESC, pegawai.NIP 
        ");

        return $query->num_rows();
    }


    function detail_pegawai($kd_pegawai){
    	$query=$this->db->query("	SELECT
					pegawai.kd_pegawai,
					pegawai.NIP,
					pegawai.gelar_depan,
					pegawai.nama_pegawai,
					pegawai.gelar_belakang,
					pegawai.npwp,
					pegawai.email,
					agama.agama,
          pegawai.status_pegawai,
					jabatan_struktural.nama_jabatan_s,
					unit_kerja.nama_unit,
					golongan_pangkat.golongan,
					golongan_pangkat.pangkat
					FROM
					pegawai
					LEFT JOIN agama ON agama.kd_agama = pegawai.kd_agama
					LEFT JOIN jabatan_struktural ON jabatan_struktural.id_jabatan_s = pegawai.id_jabatan_struktural
					LEFT JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
					LEFT JOIN golongan_pangkat ON golongan_pangkat.id_golpangkat = pegawai.id_golpangkat_terakhir
					where  pegawai.kd_pegawai=".$kd_pegawai."
					");
	
		if ($query->num_rows() > 0){
            return $query->row_array();
        }
        else{
            return FALSE;
		}
    }

    function detail_ptt($kd_pegawai){
    	$query=$this->db->query("	SELECT
					pegawai.kd_pegawai,
					pegawai.NIP,
					pegawai.gelar_depan,
					pegawai.nama_pegawai,
					pegawai.gelar_belakang,
					pegawai.npwp,
					pegawai.email,
					unit_kerja.nama_unit
					FROM
					pegawai
					LEFT JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
					where pegawai.kd_pegawai=".$kd_pegawai."
					");
	
		if ($query->num_rows() > 0){
            return $query->row_array();
        }
        else{
            return FALSE;
		}
    }

    function detail_cpns($kd_pegawai){
      $query=$this->db->query(" SELECT
          pegawai.kd_pegawai,
          pegawai.NIP,
          pegawai.gelar_depan,
          pegawai.nama_pegawai,
          pegawai.gelar_belakang,
          pegawai.npwp,
          pegawai.email,
          unit_kerja.nama_unit
          FROM
          pegawai
          LEFT JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
          where pegawai.kd_pegawai=".$kd_pegawai."
          ");
  
    if ($query->num_rows() > 0){
            return $query->row_array();
        }
        else{
            return FALSE;
    }
    }

	function update_status($kd_pegawai, $data)
	{
		$this->db->where('kd_pegawai', $kd_pegawai);
		$this->db->update($this->table, $data);
	}

	function info_logged_pegawai($user_id){
		$result=$this->db->query("select u.kd_pegawai,p.NIP,p.nama_pegawai,LTRIM(concat(p.gelar_depan,' ',p.nama_pegawai,p.gelar_belakang)) as namapeg from user_pegawai u,pegawai p where 
							  p.kd_pegawai = u.kd_pegawai and u.user_id='".$user_id."'")->result_array();

		return $result;
	}
	// function get_pegawai_on_tambah($nip){
	// 	$result=$this->db->select("pegawai.NIP, pegawai.nama_pegawai, pegawai.tgl_lahir, pegawai.email, ");
	// }
	
	function draft_mutasi(){
		return $this->db->query("SELECT pegawai.nama_pegawai, pegawai.NIP,datediff(curdate(),pegawai.tgl_lahir) as usia, pegawai.ket_pendidikan, datediff(curdate(),mutasi.tmt_SK) as selama FROM `mutasi` inner join pegawai on mutasi.kd_pegawai=pegawai.kd_pegawai WHERE mutasi.tmt_SK !='0000-00-00'")->result_array();
	}

	function draft_mutasi_internal(){
		return $this->db->query("SELECT pegawai.nama_pegawai, pegawai.NIP,datediff(curdate(),pegawai.tgl_lahir) as usia, pegawai.ket_pendidikan, datediff(curdate(),mutasi.tmt_SK) as selama FROM `mutasi` inner join pegawai on mutasi.kd_pegawai=pegawai.kd_pegawai WHERE mutasi.tmt_SK !='0000-00-00' and datediff(pegawai.tgl_resign,curdate())>1826 having selama between 2192 and 4380")->result_array();
	}

	function draft_mutasi_eksternal(){
		return $this->db->query("SELECT pegawai.nama_pegawai, pegawai.NIP,datediff(curdate(),pegawai.tgl_lahir) as usia, pegawai.ket_pendidikan, datediff(curdate(),mutasi.tmt_SK) as selama FROM `mutasi` inner join pegawai on mutasi.kd_pegawai=pegawai.kd_pegawai WHERE mutasi.tmt_SK !='0000-00-00' and datediff(pegawai.tgl_resign,curdate())>1826 having selama > 4380")->result_array();
	}

	function get_list_nama_pegawai(){
		// $result=$this->db->query("select pegawai.kd_pegawai, pegawai.nama_pegawai from pegawai order by nama_pegawai asc")->result();
		$result=$this->db->query("
					SELECT
					pegawai.kd_pegawai,
					pegawai.gelar_depan,
					pegawai.nama_pegawai,
					pegawai.gelar_belakang,
					pegawai.id_jabatan_uj,
					pegawai.kode_unit,
					pegawai.id_golpangkat_terakhir,
					jenis_jabatan_uj_remun2016.nama_jabatan_uj,
					unit_kerja.nama_unit,
					unit_kerja.keterangan
					FROM pegawai
					INNER JOIN jenis_jabatan_uj_remun2016 ON jenis_jabatan_uj_remun2016.id_jabatan_uj = pegawai.id_jabatan_uj
					INNER JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
					where nip not like '' and nip not like '-' ORDER BY pegawai.nama_pegawai asc
			")->result();

		return $result;
	}

	function get_jabatan_unit_kerja($kd_pegawai){

	}

}
