<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jabatan_detil_model extends Model {

	var $record_count;
    var $table		= 'jabatan_detil';
	var $pkey 		= 'id_jabatan_detil';
	var $table_jabatan		= 'jabatan';
	var $pkey_jabatan 		= 'id_jabatan';
	
	function jabatan_detil_model()
	{
		parent::Model();
	}
	
	function findAll($jumlah = Null, $mulai = 0,$ordby = Null)
	{
		return $this->find(NULL, $jumlah, $mulai,$ordby);
	}
	
	function findByFilter($filter_rules, $jumlah = 0, $mulai = 0){
		return $this->find($filter_rules, $jumlah, $mulai);
    }
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
	}
	
	function find($filters = NULL, $jumlah = -1, $mulai = NULL,$ordby = Null)
    {
		$results = array();
		$this->_set_where($filters);
		$this->db->from($this->table);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		
		if ($jumlah) {
		   if ($mulai) {
			  $this->db->limit( $jumlah,$mulai);
		   }
		   else {
			  $this->db->limit($jumlah);
		   }
		}
		if($ordby)
		$this->db->order_by($ordby." asc");
		$query = $this->db->get( $this->table );
	
		if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else 
			{
				return FALSE;
		}
    }
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	
	function delete($id)
	{
		$this->db->delete($this->table, array($this->pkey => $id));
	}
	
	function update($id, $data)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table, $data);
	}

	function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
            }
			elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value) 
                        $this->db->where($field, $value);               
                }
			}
		}
    }
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
            if ( is_string($order) ) 
			{
                $this->db->order_by($order);
			}
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
					foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
			}
		}
    }
	
	function get_assoc()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey." asc");
		$q = $this->db->get($this->table);
		return records_to_assoc4($q->result(),"id_jabatan","nama_jabatan","id_jabatan");
	}
	
	function get_assoc_id_jenis_jabatan()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey_jabatan." asc");
		$q = $this->db->get($this->table_jabatan);
		return records_to_assoc2($q->result(),"id_jabatan","id_jenis_jabatan"); /*depan kunci yg dicari, belakang hasil yg ditampilkan*/
	}
	
	function get_jabatan_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}
	
	
/*	function get_golongan_pangkat()
    {
		$this->load->helper('adapter');
		$q=$this->db->get('golongan_pangkat');
		$res=records_to_assoc($q->result(), 'id_golpangkat',  '(golongan, ' ', pangkat) as golonganpangkat');
		return $res;
    }*/
	
}