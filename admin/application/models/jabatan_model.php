<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jabatan_model extends Model {

	var $record_count;
    var $table		= 'jabatan';
	var $pkey 		= 'id_jabatan';
	var $table2		= 'jabatan_struktural';
	var $pkey2 		= 'id_jabatan_s';
	
	function jabatan_model()
	{
		parent::Model();
	}
	
	function findAll($jumlah = Null, $mulai = 0,$ordby = Null)
	{
		return $this->find(NULL, $jumlah, $mulai,$ordby);
	}
	
	function findByFilter($filter_rules, $jumlah = 0, $mulai = 0){
		return $this->find($filter_rules, $jumlah, $mulai);
    }
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
	}
	
	function find($filters = NULL, $jumlah = -1, $mulai = NULL,$ordby = Null)
    {
		$results = array();
		$this->_set_where($filters);
		$this->db->from($this->table);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		
		if ($jumlah) {
		   if ($mulai) {
			  $this->db->limit( $jumlah,$mulai);
		   }
		   else {
			  $this->db->limit($jumlah);
		   }
		}
		if($ordby)
		$this->db->order_by($ordby." asc");
		$query = $this->db->get( $this->table );
	
		if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else 
			{
				return FALSE;
		}
    }
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	
	function delete($id)
	{
		$this->db->delete($this->table, array($this->pkey => $id));
	}
	
	function update($id, $data)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table, $data);
	}

	function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
            }
			elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value) 
                        $this->db->where($field, $value);               
                }
			}
		}
    }
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
            if ( is_string($order) ) 
			{
                $this->db->order_by($order);
			}
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
					foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
			}
		}
    }
	
	function get_assoc()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey." asc");
		$q = $this->db->get($this->table);
		return records_to_assoc4($q->result(),"id_jenis_jabatan","nama_jenis_jabatan","id_jenis_jabatan");
	}
		
	function get_jabatan_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}
	
	function get_jabatan_assocs($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table2);
		return records_to_assoc2($q->result(),$this->pkey2,$field);
	}


	function get_assoc2()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey." asc");
		$q = $this->db->get($this->table);
		return records_to_assoc($q->result(),"id_jabatan","nama_jabatan");
	}
	
/*	function get_golongan_pangkat()
    {
		$this->load->helper('adapter');
		$q=$this->db->get('golongan_pangkat');
		$res=records_to_assoc($q->result(), 'id_golpangkat',  '(golongan, ' ', pangkat) as golonganpangkat');
		return $res;
    }*/

    function get_jabatan_struktural(){
    	$this->db->select('id_jabatan_uj, nama_jabatan_uj, id_jns_pegawai, struktural_fungsional');
    	$this->db->from('jenis_jabatan_uj_remun2016');
    	$this->db->like("nama_jabatan_uj", "kepala", "after");
    	// $result=$this->db->get()->result_array();

    	return $this->db->get()->result_array();
    }

    function get_jabatan_fungsional_umum(){
    	$this->db->select('id_jabatan_uj, nama_jabatan_uj, id_jns_pegawai, struktural_fungsional');
    	$this->db->from('jenis_jabatan_uj_remun2016');
    	$this->db->like("nama_jabatan_uj", "arsiparis", "after");
    	$this->db->or_like("nama_jabatan_uj", "pustakawan", "after");
    	$this->db->or_like("nama_jabatan_uj", "pranata laboratorium pendidikan", "after");
    	$this->db->or_like("nama_jabatan_uj", "pranata humas", "after");
    	$this->db->or_like("nama_jabatan_uj", "analis kepegawaian", "after");
    	$this->db->or_like("nama_jabatan_uj", "pengelola barang dan jasa", "after");
    	$this->db->or_like("nama_jabatan_uj", "dokter", "after");
    	$this->db->or_like("nama_jabatan_uj", "perawat", "after");
    	// $result=$this->db->get()->result_array();

    	return $this->db->get()->result_array();
    }

    function get_jabatan_fungsional_khusus(){
    	$this->db->select('id_jabatan_uj, nama_jabatan_uj, id_jns_pegawai, struktural_fungsional');
    	$this->db->from('jenis_jabatan_uj_remun2016');
    	$this->db->not_like("nama_jabatan_uj", "kepala", "after");;
    	$this->db->not_like("nama_jabatan_uj", "arsiparis", "after");
    	$this->db->not_like("nama_jabatan_uj", "pustakawan", "after");
    	$this->db->not_like("nama_jabatan_uj", "pranata laboratorium pendidikan", "after");
    	$this->db->not_like("nama_jabatan_uj", "pranata humas", "after");
    	$this->db->not_like("nama_jabatan_uj", "analis kepegawaian", "after");
    	$this->db->not_like("nama_jabatan_uj", "pengelola barang dan jasa", "after");
    	$this->db->not_like("nama_jabatan_uj", "dokter", "after");
    	$this->db->not_like("nama_jabatan_uj", "perawat", "after");
    	$result=$this->db->get()->result_array();

    	// $result=$this->db->query("select id_jabatan_uj, nama_jabatan_uj, id_jns_pegawai, struktural_fungsional from jenis_jabatan_uj_remun2016 where nama_jabatan_uj not like 'kepala%' and nama_jabatan_uj not like 'arsiparis%' and nama_jabatan_uj not like 'pustakawan%' and nama_jabatan_uj not like 'pranata laboratorium pendidikan%' and nama_jabatan_uj not like 'pranata humas%' and nama_jabatan_uj not like 'analis kepegawaian%' and nama_jabatan_uj not like 'pengelola barang dan jasa%' and nama_jabatan_uj not like 'dokter%' and nama_jabatan_uj not like 'perawat%'");

    	return $result;
    }

    function get_unit_kerja_otk($tahun_otk=null){
    	$result=null;

    	$this->db->select('kode_unit, nama_unit');
    	$this->db->from('unit_kerja');
    	$this->db->where("keterangan", $tahun_otk);
    	$result=$this->db->get()->result_array();
    	return $result;
    }
	
}