<?php 

/**
* 
*/
class Jabatan_remunerasi_model extends Model
{
	public $table="riwayat_jabatan_uj";
	
	function __construct()
	{
		parent::Model();

	}

	function get_jabatan_remunerasi($kd_pegawai){
		$this->db->select("riwayat_jabatan_uj.*, jenis_jabatan_uj_remun2016.nama_jabatan_uj, golongan_pangkat.golongan, golongan_pangkat.pangkat, unit_kerja.nama_unit");
		$this->db->from("riwayat_jabatan_uj");
		$this->db->join("jenis_jabatan_uj_remun2016", "riwayat_jabatan_uj.id_jabatan=jenis_jabatan_uj_remun2016.id_jabatan_uj");
		$this->db->join("golongan_pangkat", "riwayat_jabatan_uj.id_golpangkat=golongan_pangkat.id_golpangkat");
		$this->db->join("unit_kerja", "riwayat_jabatan_uj.kode_unit=unit_kerja.kode_unit");
		$this->db->where("kd_pegawai", $kd_pegawai);
		// $data=$this->db->_compile_select();
		$data=$this->db->get();

		return $data->result_array();
	}

	function getDetailJabatanRemunerasi($kd_pegawai=null, $id_riwayat_jabatan=null)
	{
		$this->db->select("riwayat_jabatan_uj.*, jenis_jabatan_uj_remun2016.nama_jabatan_uj, golongan_pangkat.golongan, golongan_pangkat.pangkat, unit_kerja.nama_unit");
		$this->db->from("riwayat_jabatan_uj");
		$this->db->join("jenis_jabatan_uj_remun2016", "riwayat_jabatan_uj.id_jabatan=jenis_jabatan_uj_remun2016.id_jabatan_uj");
		$this->db->join("golongan_pangkat", "riwayat_jabatan_uj.id_golpangkat=golongan_pangkat.id_golpangkat");
		$this->db->join("unit_kerja", "riwayat_jabatan_uj.kode_unit=unit_kerja.kode_unit");

		if($kd_pegawai!=null){
			$this->db->where("kd_pegawai", $kd_pegawai);
		}

		if($id_riwayat_jabatan!=null){
			$this->db->where("id_riwayat_jabatan", $id_riwayat_jabatan);
		}
		// $data=$this->db->_compile_select();
		$data=$this->db->get();

		return $data->row_array();
	}

	public function add($data)
	{
		$this->db->insert("riwayat_jabatan_uj", $data);

		return ($this->db->affected_rows()>0)?	true :false;
	}

	public function delete($id_riwayat_jabatan)
	{
		$this->db->delete("riwayat_jabatan_uj", array("id_riwayat_jabatan"=>$id_riwayat_jabatan));

		return ($this->db->affected_rows()>0)?true:false;
	}

	public function update($id, $data)
	{	
		$this->db->where("id_riwayat_jabatan", $id);
		$this->db->update('riwayat_jabatan_uj',$data);
		return ($this->db->affected_rows()>0)? true : false;
	}

}