<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayat_dp3_model extends Model
{
    var $record_count;
    var $table= 'riwayat_dp3';
    var $pkey = 'id_riwayat_dp3';

	function Riwayat_dp3_model()
    {       parent::Model();
    }

    function findAll($fields=NULL,$start = NULL, $count = NULL)
    {
        return $this->find($fields,NULL, NULL,$start, $count);
    }

    function findByFilter($filter_rules, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL,$filter_rules, NULL,$start, $count);
    }

    function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
    {
      	$results = array();
		$this->_set_where($filters);

		$this->db->from($this->table);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($order);

		if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}
		$query = $this->db->get( $this->table );
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
	}

	function retrieve_by_pkey($idField)
	{
		$results = array();
		$this->db->where($this->pkey, $idField);
		$this->db->limit( 1 );
		$query = $this->db->get( $this->table );
		
		if ($query->num_rows() > 0)
		{
		   $row = $query->row_array();
		   $results		 = $row;
		}
		else
		{
		   $results = false;
		}
		return $results;
	}
   
    function add( $data )
    {
		$this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function update($keyvalue, $data)
    {
        $this->db->where($this->pkey, $keyvalue);
        $this->db->update($this->table, $data);
    }

    function delete($idField)
    {
        $this->db->where($this->pkey, $idField);
        $this->db->delete($this->table);
        return true;
    }

    function _set_where($filters=NULL)
    {
        if ($filters)
		{
			if (is_string($filters))
			{
				$this->db->where($filters);
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
					foreach ($filters as $field => $value)
						$this->db->where($field, $value);
				}
			}
		}
    }

    function _set_order($order=NULL)
    {
        if ($order)
		{
            if (is_string($order))
			{
				$this->db->order_by($order, desc);
			}
            elseif (is_array($order))
            {
                if (count($order) > 0)
                {
					foreach ($order as $field => $value)
                        $this->db->order_by($field, $value);
                }
			}
		}
    }
    
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function get_max_no_urut($id)
	{
		$this->db->select_max('no_urut');
		$this->db->where('FK_pegawai',$id);
		$query = $this->db->get($this->table);
		$row = $query->row_array();
		return $row['no_urut'];
	}
	
	function get_rata2($id, $no_urut)
	{
		$this->db->where('no_urut',$no_urut);
		$this->db->where('FK_pegawai',$id);
		$query = $this->db->get($this->table);
		$row = $query->row_array();
		return $row['rata_rata'];
	}
}
?>