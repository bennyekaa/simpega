<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload_model extends Model {

	var $pkey = 'id_gambar';
	var $table = 'gambar';
	
	function Upload_model()
	{
		parent::Model();
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);

	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }

	    return $results;
	}
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	
	function delete($id)
    {
		$this->db->where($this->pkey, $id);
		$this->db->delete($this->table);
		return true;
    }
}