<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kelurahan_model extends Model
{
    var $record_count;
    var $table_name= 'kelurahan';
    var $pkey = 'kd_kelurahan';

	function Kelurahan_model()
    {
        parent::Model();
    }

    function findAll($fields=NULL,$start = NULL, $count = NULL)
    {
        return $this->find($fields,NULL, NULL,$start, $count);
    }

    function findByFilter($filter_rules, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL,$filter_rules, NULL,$start, $count);
    }


	function get_kelurahan()
	{
	    $this->db->select("kd_kelurahan, nama_kelurahan, (select nama_kecamatan from kecamatan where kd_kecamatan=kelurahan.kd_kecamatan) as kd_kecamatan");
		$q=$this->db->get($this->table_name);
		foreach ($q->result_array() as $value)
		{
			$res[strtoupper($value['kd_kecamatan'])][$value['kd_kelurahan']]=$value['nama_kelurahan'];
        }	
		return $res;
    }
	
	
    function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
    {
      	$results = array();
		$this->_set_where($filters);

		$this->db->from($this->table_name);
	   $this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($filters);

		if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}
		//$this->db->join('pegawai','pegawai.kd_pegawai=pasangan_pegawai.kd_pegawai','left');
		$this->db->join('kecamatan','kecamatan.kd_kecamatan=kelurahan.kd_kecamatan','left');
		$query = $this->db->get( $this->table_name );
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
	}

	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->from($this->table_name);
		$this->db->where($this->pkey, $id);
		$this->db->join('kecamatan','kecamatan.kd_kecamatan=kelurahan.kd_kecamatan','left');
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
	}
   
	function retrieve_by_idpeg($idField)
	{
		//$this->db->join('pegawai','pegawai.kd_pegawai=pasangan_pegawai.kd_pegawai','left');
		$this->db->join('kecamatan','kecamatan.kd_kecamatan=kelurahan.kd_kecamatan','left');
		$this->db->where('kd_pegawai',$idField);
		$query = $this->db->get($this->table_name);
		//echo $this->db->last_query();
		$str = $query->result_array();
		return $str;
	}
	
    function add( $data )
    {
		$this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    function update($keyvalue, $data)
    {
        $this->db->where($this->pkey, $keyvalue);
        $this->db->update($this->table_name, $data);
    }

    function delete($idField)
    {
        $this->db->where($this->pkey, $idField);
        $this->db->delete($this->table_name);
        return true;
    }

    function _set_where($filters=NULL)
    {
        if ($filters)
		{
			if (is_string($filters))
			{
				$this->db->where($filters);
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
					foreach ($filters as $field => $value)
						$this->db->where($field, $value);
				}
			}
		}
    }

    function _set_order($order=NULL)
    {
        if ($order)
		{
            if (is_string($order))
			{
				$this->db->order_by($order);
			}
            elseif (is_array($order))
            {
                if (count($order) > 0)
                {
					foreach ($order as $field => $value)
                        $this->db->order_by($field, $value);
                }
			}
		}
    }
    

	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table_name."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
    	
	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table_name);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}
}
?>