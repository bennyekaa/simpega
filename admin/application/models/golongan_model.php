<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Golongan_model extends Model {

	var $record_count;
    var $table		= 'golongan_pangkat';
	var $pkey 		= 'id_golpangkat';
	
	function Golongan_model()
	{
		parent::Model();
	}
	
	function findAlldesc($jumlah = Null, $mulai = 0,$ordby = Null)
	{
		return $this->find('tingkat > 0', $jumlah, $mulai,'id_golpangkat desc');
	}
	
	function findAll($jumlah = Null, $mulai = 0,$ordby = Null)
	{
		return $this->find(NULL, $jumlah, $mulai,'id_golpangkat');
	}
	
	function findByFilters($Filters, $jumlah = Null, $mulai = 0,$ordby)
	{
		return $this->find($Filters, $jumlah, $mulai,$ordby);
	}
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
	}
	
	
	
	function find($filters = NULL, $jumlah = -1, $mulai = NULL,$ordby = Null)
    {
		$results = array();
		$this->_set_where($filters);
		$this->db->from($this->table);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		
		if ($jumlah) {
		   if ($mulai) {
			  $this->db->limit( $jumlah,$mulai);
		   }
		   else {
			  $this->db->limit($jumlah);
		   }
		}
		if($ordby)
		$this->db->order_by($ordby);
		$query = $this->db->get( $this->table );
		//echo $this->db->last_query();
		
		if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else 
			{
				return FALSE;
		}
    }
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	
	function delete($id)
	{
		$this->db->delete($this->table, array($this->pkey => $id));
	}
	
	function update($id, $data)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table, $data);
	}

	function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
            }
			elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value) 
                        $this->db->where($field, $value);               
                }
			}
		}
    }
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
            if ( is_string($order) ) 
			{
                $this->db->order_by($order);
			}
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
					foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
			}
		}
    }
	
	function get_assoc()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey);
		$q = $this->db->get($this->table);
		return records_to_assoc4($q->result(),$this->pkey,"golongan","pangkat");
	}
	
	function get_gol_assoc()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey);
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,"golongan");
	}
	function get_pangkat_assoc()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey);
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,"pangkat");
	}
	
	function get_assoc2()
	{
		
		$this->load->helper('adapter');
		$this->db->select("*,(SELECT count(kd_pegawai) from pegawai where 
id_golpangkat_terakhir =golongan_pangkat.id_golpangkat) as jumlah");
		$this->db->order_by($this->pkey);
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,"golongan");
	}
	
	function get_golpangkat_assoc($param = 'name')
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
        $returnvalue = false;
        if(is_array($param))
            $returnvalue = records_to_assoc3($q->result(),$this->pkey,$param);
        else 
            $returnvalue = records_to_assoc2($q->result(),$this->pkey,$param);
		return $returnvalue;
	}
	
	
	
	
	/*function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}*/
}