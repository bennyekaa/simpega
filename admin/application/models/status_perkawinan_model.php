<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Status_perkawinan_model extends Model {

	var $table		= 'status_perkawinan';
	var $pkey 		= 'status_kawin';
	
	function Status_perkawinan_model()
	{
		parent::Model();
	}
	
	function findAll()
	{
		$this->db->order_by($this->pkey);
		$query = $this->db->get($this->table);
		return $query->result_array();
	}
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);

	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }

	    return $results;
	}
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
	}
	
	function delete($id)
	{
		$this->db->delete($this->table, array($this->pkey => $id));
	}
	
	function update($id, $field)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table, $field);
	}
		
	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}
}