<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends Model
{
	var $record_count;
	var $table				= 'user';
	var $groups_table		= 'user_group';
	var $tracker_table		= 'tracker';
	var $primary_key 		= 'user_id';

	function Users_model()
	{
		parent::Model();
	}

	function findAll($jumlah = Null, $mulai = 0,$filter=Null,$ordby = Null)
	{
	    return $this->find($filter, $jumlah, $mulai,$filter);
	}

	function findById($key_value)
	{
	    return $this->find(array($this->primary_key => $key_value));
	}

	function findByFilter($filter_rules, $jumlah = 10, $mulai = 0)
	{
	    return $this->find($filter_rules, $jumlah, $mulai);
	}

	function find($filters = NULL, $jumlah = -1, $mulai = NULL)
	{
	    $results = array();
	    $this->_set_where($filters);
		$this->db->from($this->table);
	    $this->record_count = $this->db->count_all_results();

	    $this->_set_where($filters);
	    $this->_set_order($filters);

	    if ($jumlah)
		{
			if ($mulai)
			{
				$this->db->limit( $jumlah,$mulai);
			}
	       else
			{
				$this->db->limit($jumlah);
			}
	    }

	    $this->db->join($this->groups_table,"'".$this->groups_table.".group_id = ".$this->table.".FK_group_id");
	    $query = $this->db->get( $this->table);
	    
		if ($query->num_rows() > 0)
	    {
			return $query->result_array();
	    }
	    else
	    {
			return FALSE;
	    }
	}

    function retrieve_by_pkey($idField) {
	    $results = array();

	    $this->db->where( $this->primary_key, $idField);
	    $this->db->limit( 1 );
	    $query = $this->db->get( $this->table);
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
    }


	function add( $userdata )
	{
		$auth = $this->config->item('auth');
		/* Config Variables */
		foreach($auth as $key => $value)
		{
			$conf[$key] = $value;
		}

		$hash = sha1(microtime());
		$userdata['hash'] = $hash;
		$userdata['password'] = sha1($conf['salt'] . $hash . $userdata['password']);

		$userdata['unique_id'] = sha1($conf['salt'].sha1($conf['salt'].$userdata['username']));

		//ambil data
		$this->add_user($userdata);
		$id=$this->db->insert_id();
		$this->db->where('group_id',$userdata['FK_group_id'] );
		$data_rule=$this->db->get('rule_group');
		return true;
	}

	function modify($keyvalue, $data)
	{
		if($data['password'])
		{
			$this->update_password_by_id($data['password'] ,$keyvalue);
			unset($data['password']);
		}
		$this->db->where($this->primary_key, $keyvalue);
		$this->db->update($this->table, $data);
	}

	function delete_by_pkey($idField)
	{
	    $this->db->where($this->primary_key, $idField);
	    $this->db->delete($this->table);
		return true;
	}

	function _set_where($filters=NULL)
	{
	    if ($filters)
		{
			if ( is_string($filters) )
			{
				$this->db->where($filters);
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
				foreach ($filters as $field => $value)
					$this->db->where($field, $value);
				}
			}
	    }
	}

	function _set_order($order=NULL)
	{
	    if ($order)
	    {
			if ( is_string($order) )
			{
				$this->db->order_by($order);
			}
			elseif ( is_array($order) )
			{
				if ( count($order) > 0 )
				{
					foreach ($order as $field => $value)
					$this->db->order_by($field, $value);
				}
			}
	    }
	}

	// =============================
	// = ========== GET ========== =
	// =============================

	function get_user($unique_id, $token = FALSE)
	{
		return $this->_get_user('current', $unique_id, $token);
	}

	// --------------------------------------------------------------------
	function get_user_by_name($username)
	{
		return $this->_get_user('general', $username);
	}

	function get_login_info($username)
	{
		return $this->_get_user('login', $username);
	}

	// --------------------------------------------------------------------

	function get_all_users($filter = '')
	{
		$this->db->select('user_id, join_date, name, username, email, '.$this->db->dbprefix($this->groups_table).'.title AS user_group', FALSE);
		$this->db->join($this->groups_table, $this->groups_table.'.group_id = '.$this->table.'.FK_group_id');

		switch($filter)
		{
			case 'banned'		:	$this->db->where('active', 0);
				break;
			case 'ip_banned'	:	$this->db->where('FK_tracker_id IN (SELECT tracker_id FROM '.$this->db->dbprefix($this->tracker_table).' WHERE banned = \'1\')');
				break;
		}

		$query = $this->db->get($this->table);

		return $query->result();
	}

	// --------------------------------------------------------------------


	function _get_user($purpose, $identifier, $token = FALSE)
	{
		if($purpose == 'login')
		{
			$this->db->select('password,FK_group_id, email, user_id, hash, unique_id');
		}
		else
		{
			$this->db->select($this->tracker_table.'.*');
			$this->db->select('user_id, join_date, name, username, email, '.$this->db->dbprefix($this->groups_table).'.title AS user_group', FALSE);
			$this->db->join($this->groups_table, $this->groups_table.'.group_id = '.$this->table.'.FK_group_id');
			$this->db->join($this->tracker_table, $this->tracker_table.'.tracker_id = '.$this->table.'.FK_tracker_id');
		}

		if($purpose == 'current')
		{
			$this->db->where('unique_id', $identifier);

			if($token !== FALSE)
			{
				$this->db->where('rem_timeout >', time());
				$this->db->where('rem_token', $token);
			}
		}
		else
		{
			$this->db->where('username', $identifier);
		}

		$this->db->limit(1);

		$query = $this->db->get($this->table);
		
		return ($query->num_rows() > 0) ? $query->row() : FALSE;
	}


	// =============================
	// = ========== ADD ========== =
	// =============================


	function add_user($userdata)
	{
		$this->db->set($userdata);
		$this->db->insert($this->table);
	}

	function update_password($password, $email, $change = FALSE)
	{
		$hash = sha1(microtime());
		$salt = $this->config->item('auth');
		$salt = $salt['salt'];

		$password = sha1($salt.$hash.$password);

		//$change = $change ? '1' : '0';
		$data = array(
				//'change_password'		=> $change,
				'password'				=> $password,
				'hash' 					=> $hash
		);

		$this->db->where('email', $email);
		$this->db->update($this->table, $data);

		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	function update_password_by_uid($password, $unique_id, $change = FALSE)
	{
		$hash = sha1(microtime());
		$salt = $this->config->item('auth');
		$salt = $salt['salt'];

		$password = sha1($salt.$hash.$password);

		//$change = $change ? '1' : '0';
		$data = array(
				//'change_password'		=> $change,
				'password'				=> $password,
				'hash' 					=> $hash
		);

		$this->db->where('unique_id', $unique_id);
		$this->db->update($this->table, $data);

		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
	function update_password_by_id($password, $unique_id, $change = FALSE)
	{
		$hash = sha1(microtime());
		$salt = $this->config->item('auth');
		$salt = $salt['salt'];

		$password = sha1($salt.$hash.$password);

		$change = $change ? '1' : '0';
		$data = array(
				//'change_password'		=> $change,
				'password'				=> $password,
				'hash' 					=> $hash
		);

		$this->db->where('user_id', $unique_id);
		$this->db->update($this->table, $data);

		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	// --------------------------------------------------------------------


	function update_profile()
	{
		$data = array(
			'name'		=>	$this->input->post('name'),
			'email'		=>	$this->input->post('email')
		);

		if($this->input->post('password'))
		{
			$this->update_password($this->input->post('password'), $this->user->email);
		}

		$this->db->where('email', $this->user->email);
		$this->db->update($this->table, $data);
	}

	// --------------------------------------------------------------------


	function login_update($user_id, $tracker_id)
	{
		$data = array(
			'FK_tracker_id'		=>	$tracker_id
		);

		$this->db->where('user_id', $user_id);
		$this->db->update($this->table, $data);
	}

	// --------------------------------------------------------------------

	function update_remember($unique_id, $token, $timeout)
	{
		$data = array(
			'rem_token'		=>	$token,
			'rem_timeout'	=> time() //+ $timeout
		);

		$this->db->where('unique_id', $unique_id);
		$this->db->update($this->table, $data);
	}

	// --------------------------------------------------------------------

	/**
	 * Update Banning Information
	 * @param	user/tracker id | ban type | 0 = un_ban, 1 = ban
	 */
	function update_ban($id, $direction, $type = '')
	{
		if($type == 'ip')
		{
			$this->db->set('banned', $direction);
			$this->db->where('tracker_id', $id);
			$this->db->update($this->tracker_table);
		}
		else
		{
			$this->db->set('active', intval(1 - $direction));
			$this->db->where('user_id', $id);
			$this->db->update($this->table);
		}
	}

	function update_user($id, $data)
	{
		$this->db->where($this->primary_key, $id);
		$this->db->update($this->table, $data);
	}
	
	// ================================
	// = ========== DELETE ========== =
	// ================================

	/**
	 * Delete a user
	 *
	 * @access	public
	 */
	function delete($id)
	{
		$this->db->delete($this->table, array('user_id' => $id));
	}
	
	function delete_any($idField){
		if($idField){
			foreach($idField as $id=>$value){
				if($id==0)$this->db->where($this->primary_key,$value);
				else if($value)$this->db->or_where($this->primary_key,$value);
			}
			$this->db->delete($this->table);
			return true;
		}
		else return false;
	}

	// ================================
	// = ========== OTHERS ========== =
	// ================================

	/**
	 * Checks if $value for $field is already used
	 */
	function check_unique($field, $value)
	{
		$this->db->select($field);
		$this->db->where($field, $value);
		$this->db->limit(1);

		return ($this->db->count_all_results($this->table) > 0) ? TRUE : FALSE;
	}
	
	function get_user_unique($field, $value)
	{
		$this->db->select('name','username');
		$this->db->where($field, $value);
		$this->db->limit(1);

		return ($this->db->count_all_results($this->table) > 0) ? TRUE : FALSE;
	}
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function update_last_login($user_id)
	{
		$datestring = "%Y-%m-%d %H:%i:%s";
		$time = time();
		$wkt = mdate($datestring, $time);
		$this->db->set('last_login', $wkt);
		$this->db->where($this->primary_key, $user_id);
		$this->db->update($this->table);
	}
}

