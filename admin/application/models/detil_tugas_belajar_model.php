<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class detil_tugas_belajar_model extends Model
{
    var $record_count;
    var $table_name= 'detil_riwayat_tb';
    var $pkey = 'id_detil_riwayat_tb';

	function detil_tugas_belajar_model()
    {
        parent::Model();
    }

    function findAll($fields=NULL,$start = NULL, $count = NULL)
    {
        return $this->find($fields,NULL, NULL,$start, $count);
    }

    function findByFilter($filter_rules, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL,$filter_rules, NULL,$start, $count);
    }

    function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
    {
      	$results = array();
		$this->_set_where($filters);

		$this->db->from($this->table_name);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($order);

		if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}
		//if($ordby)
		//$this->db->join('riwayat_tugas_belajar a','a.id_riwayat_tb=detil_riwayat_tb.id_riwayat_tb','left');
		//$this->db->join('golongan_pangkat b','b.id_golpangkat=riwayat_gol_kepangkatan.id_golpangkat','left');
		$query = $this->db->get( $this->table_name );
        //echo $this->db->last_query();
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
	}

	function retrieve_by_pkey($idField)
	{
		$results = array();
		$this->db->select('*');
		//$this->db->join('pegawai a','a.kd_pegawai=pegawai_rincian_ak.kd_pegawai','left');
		//$this->db->join('golongan_pangkat b','b.id_golpangkat=riwayat_gol_kepangkatan.id_golpangkat','left');
		$this->db->where($this->pkey, $idField);
		$this->db->limit( 1 );
		$query = $this->db->get( $this->table_name );
		
		if ($query->num_rows() > 0)
		{
		   $row = $query->row_array();
		   $results		 = $row;
		}
		else
		{
		   $results = false;
		}
		return $results;
	}
   
    function add( $data )
    {
		$this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    function update($keyvalue, $data)
    {
        $this->db->where($this->pkey, $keyvalue);
        $this->db->update($this->table_name, $data);
		//die($this->db->last_query());
    }

	function update_status($kd_pegawai, $data)
	{
		$this->db->where('kd_pegawai', $kd_pegawai);
		$this->db->update($this->table_name, $data);
	}
    function delete($idField)
    {
        $this->db->where($this->pkey, $idField);
        $this->db->delete($this->table_name);
        return true;
    }

    function _set_where($filters=NULL)
    {
        if ($filters)
		{
			if (is_string($filters))
			{
				$this->db->where($filters);
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
					foreach ($filters as $field => $value)
						$this->db->where($field, $value);
				}
			}
		}
    }

    function _set_order($order=NULL)
    {
        if ($order)
		{
            if (is_string($order))
			{
				$this->db->order_by($order);
			}
            elseif (is_array($order))
            {
                if (count($order) > 0)
                {
					foreach ($order as $field => $value)
                        $this->db->order_by($field, $value);
                }
			}
		}
    }
    
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table_name."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function get_max_no_urut($id)
	{
		$this->db->select_max('no_urut');
		$this->db->where('kd_pegawai',$id);
		$query = $this->db->get($this->table_name);
		$row = $query->row_array();
		return $row['no_urut'];
	}
}
?>