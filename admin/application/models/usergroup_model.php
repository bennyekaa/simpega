<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usergroup_model extends Model
{
	var $record_count;
	var $table		= 'user_group';
	var $pkey 		= 'group_id';
	
	function Usergroup_model()
	{
		parent::Model();
	}
	
	function get_group()
	{
		$this->load->helper('adapter');
		$q=$this->db->get($this->table);
		$res=records_to_assoc($q->result(), 'group_id',  'title');
		return $res;
    }
	
	function get_group_user(){	
		$q=$this->db->get_where($this->table,'group_id <> 1');
		$data = $q->result_array();
		return $data;
	}
	
	function findAll()
	{
		$this->db->order_by('group_id asc');
		$data = $this->db->get($this->table);
		if ($data->num_rows() > 0) {
			return $data->result_array();
		}
		else {	
			return false;
		}
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);

	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }

	    return $results;
	}
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	
	function delete($id)
	{
		$this->db->delete($this->table, array($this->pkey => $id));
	}
	
	function update($id, $data)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table, $data);
	}

	function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
				return true;
			}
			elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value)
						if(is_int($field))
							$this->db->where($this->pkey, $value);
						else
                        $this->db->where($field, $value);               
                }
				return true;
			}
			else return false;
		}
    }
}