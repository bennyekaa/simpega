<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayat_jabatan_model extends Model {

	var $table_name	= 'riwayat_jabatan';
	var $pkey 		= 'id_riwayat_jabatan';
	
	function Riwayat_jabatan_model()
	{
		parent::Model();
	}
	
	function findAll()
	{
		$this->db->join('jabatan','jabatan.id_jabatan=riwayat_jabatan.id_jabatan');
		$this->db->order_by($this->pkey);
		$query = $this->db->get($this->table_name);
		return $query->result_array();
	}
	
	function findAll_bykd_pegawai($userid)
	{
		$this->db->join('jabatan','jabatan.id_jabatan=riwayat_jabatan.id_jabatan');
		$this->db->where('kd_pegawai',$userid);
		$this->db->order_by($this->pkey);
		$query = $this->db->get($this->table_name);
		return $query->result_array();
	}
	
    function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
    {
      	$results = array();
		$this->_set_where($filters);

		$this->db->from($this->table_name);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($order);

		if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}
		if($ordby)
		$this->db->join('pegawai a','a.kd_pegawai=riwayat_jabatan.kd_pegawai','left');
		$this->db->join('jabatan b','b.id_jabatan=riwayat_jabatan.id_jabatan','left');
		$query = $this->db->get( $this->table_name );
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
	}

	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table_name."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->join('jabatan','jabatan.id_jabatan=riwayat_jabatan.id_jabatan');
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table_name);

	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }

	    return $results;
	}
	
	function add($data)
	{
		$this->db->insert($this->table_name,$data);
	}
	
	function delete($id)
	{
		$this->db->delete($this->table_name, array($this->pkey => $id));
	}
	
	function update($id, $data)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table_name, $data);
	}
	
	function update_status($kd_pegawai, $data)
	{
		$this->db->where('kd_pegawai', $kd_pegawai);
		$this->db->update($this->table_name, $data);
	}
	
    function _set_where($filters=NULL)
    {
        if ($filters)
		{
			if (is_string($filters))
			{
				$this->db->where($filters);
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
					foreach ($filters as $field => $value)
						$this->db->where($field, $value);
				}
			}
		}
    }

    function _set_order($order=NULL)
    {
        if ($order)
		{
            if (is_string($order))
			{
				$this->db->order_by($order);
			}
            elseif (is_array($order))
            {
                if (count($order) > 0)
                {
					foreach ($order as $field => $value)
                        $this->db->order_by($field, $value);
                }
			}
		}
    }
}