<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Proses_kenaikan_pangkat_model extends Model {

	var $record_count;
	var $table = 'pegawai';
    var $pkey = 'kd_pegawai';
	
	function Proses_kenaikan_pangkat_model()
	{
		parent::Model();
	}
	
	function findAll($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
	{
		$results = array();
		$this->_set_where($filters);
		$this->db->from($this->table);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($filters);

		if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}
		$this->db->select('pegawai.kd_pegawai, pegawai.NIP, pegawai.nama, pegawai.gelar_depan, pegawai.gelar_belakang, kerja.tmt_golongan, golongan_pangkat.golongan, golongan_pangkat.pangkat, unit_kerja.unit_kerja');
		$this->db->join('kerja','kerja.FK_pegawai=pegawai.kd_pegawai','left');
		$this->db->join('golongan_pangkat','golongan_pangkat.id_golpangkat=pegawai.id_golpangkat_terakhir');
		$this->db->join('nama_unit','unit_kerja.kode_unit=kerja.FK_unit_kerja');
		if($ordby)
		$this->db->order_by($ordby." asc");
		$query = $this->db->get( $this->table );
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
	}
	
	function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
            }
			elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value) 
                        $this->db->where($field, $value);               
                }
			}
		}
    }
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
            if ( is_string($order) ) 
			{
                $this->db->order_by($order);
			}
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
					foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
			}
		}
    }
	
	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}
}
?>