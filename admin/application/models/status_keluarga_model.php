<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Status_keluarga_model extends Model {

	var $table		= 'status_keluarga';
	var $pkey 		= 'kd_status_keluarga';
    var $vkey       = 'status_keluarga';
	
	function Status_keluarga_model()
	{
		parent::Model();
	}
	
	function findAll()
	{
		$this->db->order_by($this->pkey);
		$query = $this->db->get($this->table);
		return $query->result_array();
	}
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);

	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }

	    return $results;
	}
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
	}
	
	function delete($id)
	{
		$this->db->delete($this->table, array($this->pkey => $id));
	}
	
	function update($id, $data)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table, $data);
	}
		
	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
    }

	function get_pasangan_assoc($field,$kriteria)
	{
		$filters= "(kd_status_keluarga like (select id_var from pos_variabel where keterangan ='".$kriteria."'))";
		$this->_set_where($filters);
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
    }
	
	function get_pasangan_kd_status($field,$kriteria)
	{
		$filters= "(kd_status_keluarga like (select id_var from pos_variabel where keterangan ='".$kriteria."'))";
		$this->_set_where($filters);
		$this->load->helper('adapter');
		$query = $this->db->get($this->table);
		$data = $query->row_array();
		return $data['kd_status_keluarga'];
    }
		
	function get_anak_assoc($field)
	{
		$filters= "(kd_status_keluarga not like (select id_var from pos_variabel where keterangan ='id_istri') and  kd_status_keluarga not like (select id_var from pos_variabel where keterangan ='id_suami'))";
		$this->_set_where($filters);
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
    }
	
	
	function get_keluarga_assoc($field)
	{
		$filters= "(status_keluarga not like '%anak%' and kd_status_keluarga not like (select id_var from pos_variabel where keterangan ='id_istri') and  kd_status_keluarga not like (select id_var from pos_variabel where keterangan ='id_suami'))";
		$this->_set_where($filters);
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
    }

	
	function _set_where($filters=NULL)
	{
	    if ($filters)
	    {
			if ( is_string($filters) )
			{
				$this->db->where($filters);
                
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
				foreach ($filters as $field => $value)
					$this->db->where($field, $value);
				}
			}
	    }
	}
}