<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class rule_group_model extends Model {
    var $record_count;
    var $table_name = 'rule_group';
    var $primary_key = 'id';
    
	function rule_group_model()
    {
		parent::Model();

    }


    function findAll($jumlah = 10, $mulai = 0)
    {
		return $this->find(NULL, $jumlah, $mulai);
    }

    function findById($key_value)
    {
		return $this->find(array($this->primary_key => $key_value));
    }

    function findByFilter($filter_rules, $jumlah = 10, $mulai = 0)
    {
		
		return $this->find($filter_rules, $jumlah, $mulai);
    }

    function find($filters = NULL, $jumlah = -1, $mulai = NULL)
    {
		$results = array();
		$this->_set_where($filters);
		$this->db->from($this->table_name);
      	$this->record_count = $this->db->count_all_results();
		
		//the real result
		
		$this->_set_where($filters);
	
		if ($jumlah) {
		   if ($mulai) {
			  $this->db->limit( $jumlah,$mulai);
		   }
		   else {
			  $this->db->limit($jumlah);
		   }
		}
		$this->db->order_by("rule.action,rule.id_rule,rule_group.id");
		$this->db->join('user_group','user_group.group_id = rule_group.group_id');
		$this->db->join('rule','rule.id_rule = rule_group.id_rule');
		$query = $this->db->get( $this->table_name );
		//echo $this->db->last_query();
		if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else 
			{
				return FALSE;
		}
    }


   function retrieve_by_pkey($idField) {

	$results = array();
  
	$this->db->where( $this->primary_key, $idField);
	$this->db->limit( 1 );
	$query = $this->db->get( $this->table_name );
	if ($query->num_rows() > 0)
	{
	   $row = $query->row_array();
	   $results		 = $row;
	}
	else
	{
	   $results = false;
	}
  
	return $results;
   }

    
    function add( $data )
    {

      $this->db->insert($this->table_name, $data);

      return $this->db->insert_id();
    }

    function modify($keyvalue, $data)
    {

	$this->db->where($this->primary_key, $keyvalue);
	$this->db->update($this->table_name, $data);

    }

    function delete_by_pkey($idField)
    {
	$this->db->where($this->primary_key, $idField);
	$this->db->delete($this->table_name);

	return true;

    }

    function _set_where($filters=NULL)
    {
        if ($filters) 
	{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
            }
	    elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value) 
                        $this->db->where($field, $value);               
                }
	    }

	}
    }
    
    function _set_order($order=NULL)
    {
        if ($order) 
	{
            if ( is_string($order) ) 
	    {
                $this->db->order_by($order);
	    }
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
		   foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
	    }

	}
    }
    

}

?>
