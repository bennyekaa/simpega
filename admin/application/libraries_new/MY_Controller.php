<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends Controller {
function MY_Controller()
	{
		parent::Controller();
		/****** load inisialisasi sistem dari data base *******/
		$this->user = $this->access->get_user();
		/*$this->access->restrict();
		$this->user = $this->access->get_user();
		$this->load->helper('confirm');*/
	}
}

class Member_Controller extends MY_Controller {

	var $user;
	var $reference;

	function Member_Controller()
	{
		parent::MY_Controller();
		
		$this->access->restrict();
		$this->user = $this->access->get_user();
		$this->load->helper('confirm');
	}

	function set_reference()
	{
		$this->session->set_userdata('referrer', $this->uri->uri_string());
	}

	function get_reference()
	{
		if ( ! $this->reference)
		{
			$this->reference = $this->session->userdata('referrer');
		}
		return $this->reference;
	}

	function redirect_to_referer($default = '')
	{
		$this->reference = $this->get_reference() ? $this->reference : $default;
		redirect($this->reference);
	}
	
	function _remap($func)
	{
		$this->access->cek_hak_akses( strtolower(get_class($this)),$func);
		
		if (method_exists($this,$func)){
		    $URI =& load_class('URI');
		    call_user_func_array(array(&$this, $func), array_slice($URI->rsegments, 2));
		}
		else
		    show_404(get_class($this).'/'.$func);
	}
}


class Admin_Controller extends Member_Controller
{
    var $sub_menu;
    function Admin_Controller()
    {
        parent::Member_Controller();
		
		$this->access->restrict('Administrator');
    }
}


class Non_Staff_Controller extends Member_Controller
{
    var $sub_menu;
    function Non_Staff_Controller()
    {
        parent::Member_Controller();
		$this->access->restrict_not('Staff');
    }
}

?>
