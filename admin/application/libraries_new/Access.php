<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Access
{
	var $CI;
	var $tracker;
	var $user;

	/*
	** Constructor
	*/
	function Access()
	{
		$this->CI =& get_instance();
		$auth = $this->CI->config->item('auth');
		/* Config Variables */
		foreach($auth as $key => $value)
		{
			$this->$key = $value;
		}

		$this->CI->load->helper('cookie');
		$this->CI->load->model('users_model');
		$this->CI->load->model('tracker_model');

		$this->users_model =& $this->CI->users_model;
		$this->tracker_model =& $this->CI->tracker_model;

		$this->start_tracking();
	}

	// --------------------------------------------------------------------

	/**
	 * Track the user
	 *
	 */
	function start_tracking()
	{
		$this->tracker = $this->tracker_model->get_ip_info();

		if( ! $this->tracker)
		{
			$this->tracker = $this->tracker_model->add_ip();
		}
		else if($this->tracker->banned == 1)
		{
			exit('IP anda telah di banned.');
		}
	}

	// --------------------------------------------------------------------

	// --------------------------------------------------------------------

	function login($username, $password)
	{
		if($this->tracker->failed_logins > 3)
		{
			$now = time();
			$wait = $now - 900;

			if($this->tracker->last_failure > $wait)
			{
				return 'TIMEOUT';
			}
		}

		$result = $this->users_model->get_login_info($username);
		
		if ($result) // Result Found
		{
			/*if (empty($result->active))
			{
				return 'BANNED';
			}*/

			if ( ! empty($result->activation_code))
			{
				return 'NOT_ACTIVATED';
			}

			$password = sha1($this->salt.$result->hash.$password);
			//echo $password;
			if ($password === $result->password)
			{
				// Start session
				$this->CI->session->set_userdata('unique_id', $result->unique_id);
				$this->CI->session->set_userdata('user_id', $result->user_id);
				$this->CI->session->set_userdata('group_id', $result->FK_group_id);
				$this->tracker_model->reset_failures();
				
				// Remember me?
				if($this->CI->input->post('remember'))
				{
					$this->renew_remember($result->unique_id);
				}
				
				/*
				// Change Password?
				if($result->change_password != '0')
				{
					// Redirect to change password form
					$key = $this->CI->session->flashdata_key;
					$this->CI->session->set_flashdata('msg', 'Silakan Ganti Password Anda.');
					$this->CI->session->set_userdata($key.':old:referrer', 'staff/profile/edit');
				}
*/
				// Update user data
				$this->users_model->login_update($result->user_id, $this->tracker->tracker_id);
				
				return TRUE;
			}
		}

		$this->tracker_model->increment_failures($this->tracker->failed_logins);

		return FALSE;
	}

	function change_password($password)
	{
		$unique_id = $this->CI->session->userdata('unique_id');
		$this->CI->users_model->update_password_by_uid($password, $unique_id);
	}
	// --------------------------------------------------------------------

	/**
	 * cek apakah udah login
	 */
	function logged_in ()
	{
		return $var = ($this->CI->session->userdata('unique_id')) ? TRUE : FALSE;
	}

	// --------------------------------------------------------------------

	/**
	 * Logout
	 *
	 */
	function logout ()
	{
		delete_cookie('remember');
		$this->CI->session->unset_userdata('unique_id');
		$this->CI->session->unset_userdata('user_id');
		$this->CI->session->sess_destroy();
	}

	// --------------------------------------------------------------------

	/**
	 * Activate
	 */
	function activate ($activation_code)
	{
		$activate = $this->users_model->activate($activation_code);

		return $var = ($activate) ? TRUE : FALSE;
	}

	// --------------------------------------------------------------------

	/**
	 * Forgotten Begin
	 */
	function forgotten_password($email)
	{
		$CI =& get_instance();

		if($this->check_email($email))
		{
			$CI->load->library('email');
			$CI->email->initialize($this->mail);
			$data = $this->users_model->get_user_unique('email', $email);
			
			/* Generate new password. */
			$password = substr(sha1(microtime()), 0, 10);
			$this->users_model->update_password($password, $email, TRUE);
			
			$data['password']	= $password;
		
			$CI->load->view('template/email/body', $data);
			$message = $CI->load->view('template/email/body', $data, TRUE);

			$CI->email->from($this->mail_from_email, $this->mail_from_name);
			$CI->email->to($email);
			$CI->email->subject($this->new_password_subject);
			$CI->email->message($message);
			$CI->email->send();

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Check Username availability
	 *
	 * @access	public
	 * @param	string	username
	 * @return	bool
	 */
	function check_username($username)
	{
		return $this->users_model->check_unique('username', $username);
	}

	// --------------------------------------------------------------------


	function check_email($email)
	{
		return $this->users_model->check_unique('email', $email);
	}

	// --------------------------------------------------------------------

	/**
	 * Returns User Object
	  */
	function get_user()
	{
		$unique_id	= FALSE;
		$token		= FALSE;

		if($this->logged_in())
		{
			$unique_id = $this->CI->session->userdata('unique_id');
			$token = FALSE;
		}
		else if ($persistent = get_cookie('remember'))
		{
			list($unique_id, $token) = explode(':', $persistent);
		}
		
		if($unique_id)
		{
			if($user = $this->CI->users_model->get_user($unique_id, $token))
			{
				$user->id = $user->user_id;
				$user->join_date = strtotime($user->join_date . " GMT");	// unix timestamp
				$user->is_admin = ($user->user_group === 'Administrator') ? TRUE : FALSE;
				$user->confirm_logout = $this->CI->config->item('logout_confirmation');

				if ($token)
				{
					$this->renew_remember($unique_id);
				}
				return $user;
			}
		}
		
		$user			= new stdClass;
		$user->id		= -1;
		$user->is_admin	= FALSE;

		return $user;
	}

	// --------------------------------------------------------------------

	/**
	 * Renew Remember
	 */
	function renew_remember($unique_id) {

		$token = md5(uniqid(rand(), TRUE));
		$timeout = 60 * 60 * 24 * 7;

		$cookie = array(
						'name'		=> 'remember',
						'value'		=> $unique_id.':'.$token,
						'expire'	=> $timeout
						);

		set_cookie($cookie);
		$this->CI->session->set_userdata('unique_id', $unique_id);

		$this->users_model->update_remember($unique_id, $token, $timeout);
	}

	// --------------------------------------------------------------------

	/**
	 * Protect a controller / function
	 */
	function restrict($user_group = FALSE)
	{
		if($user_group == 'logged_out')
		{
			if ($this->logged_in())
			{
				redirect('/staff');
			}
		}
		else if( ! $this->logged_in())
		{
			$this->CI->session->set_flashdata('referrer', $this->CI->uri->uri_string());
			if (async_request())
			{
				die("<script>window.location = '".site_url('staff/login')."'</script>");
			}
			redirect('/staff/login');
		}
		else if ($user_group && $this->CI->user->user_group !== $user_group)
		{
			$this->CI->session->set_flashdata('referrer', $this->CI->uri->uri_string());
			redirect('/staff/login');
		}
	}
	
	function restrict_not($user_group = FALSE)
	{
		if($user_group == 'logged_out')
		{
			if ($this->logged_in())
			{

				redirect('/staff');
			}
		}
		else if( ! $this->logged_in())
		{
			$this->CI->session->set_flashdata('referrer', $this->CI->uri->uri_string());
			if (async_request())
			{
				die("<script>window.location = '".site_url('staff/login')."'</script>");


			}
			redirect('/staff/login');
		}
		else if ($user_group && $this->CI->user->user_group === $user_group)
		{
			$this->CI->session->set_flashdata('referrer', $this->CI->uri->uri_string());
			redirect('/staff/login');
		}
	}

	function cek_hak_akses($controller,$action){
		$this->restrict();
		
		if (!$this->CI->user)
			$this->CI->user = $this->get_user();
			
		if($this->CI->user->user_group=='Administrator')
			return true;
		
		$this->CI->db->where('rule_group.group_id',$this->CI->session->userdata('group_id'));
		$this->CI->db->where('controller',$controller);
		if(!$action=='index')
			$this->CI->db->where('action',$action);
		else
			$this->CI->db->where("( action = '$action' or action = '') ");
	
		$this->CI->db->join('rule','rule.id_rule= rule_group.id_rule');
		$this->CI->db->from('rule_group');
		
		$q=$this->CI->db->get();
		if ($q->num_rows())
			return true;
		else{
			//set_error('Maaf, anda tidak punya hak akses ke module ini');
			redirect('staff');
			return false;
		}
	}

	function get_hak_akses($controller,$is_menu=3)
	{
		//$this->restrict();
		if (!$this->logged_in()) return false;

		//$this->CI->db->select('action');
		if($this->CI->user->user_group!='Administrator' && $this->CI->session->userdata('group_id')!=1)
		{
			$this->CI->db->where('rule_group.group_id',$this->CI->session->userdata('group_id'));
			$this->CI->db->join('rule_group','rule.id_rule= rule_group.id_rule');
		}
		if($is_menu!=3)
		$this->CI->db->where('is_menu',$is_menu);
		$this->CI->db->where('lower(rule.controller)="'.strtolower($controller).'"','',FALSE);
		$this->CI->db->from('rule');
		$this->CI->db->orderby('rule_name ASC');
		$q=$this->CI->db->get();
		///echo $this->CI->db->last_query();
		return $q->result_array();
	}
	
	function menus_grouping ($id,&$array){
		$i=0;$result=array();
		if ($array) foreach($array as &$value){
			if (!$value['cek'] && $value['parent']==$id){
				$value['cek']=1;
				$has = $this->menus_grouping ($value['id_menu'],$array);
				$result[$value['id_menu']]=$value;
				$result[$value['id_menu']]['submenus']=$has;
				$i++;
			}
		}
		unset($value);
		if (!$i)
			return false;
		else return $result;
	}
	
	function get_multi_hak_akses()
	{
		if (!$this->logged_in()) return false;
		$this->CI->db->join('rule','rule.id_rule = menus.id_rule','left');
		if($this->CI->user->user_group!='Administrator' && $this->CI->session->userdata('group_id')!=1)
		{
			$this->CI->db->where('rule_group.group_id',$this->CI->session->userdata('group_id'));
			$this->CI->db->or_where('menus.id_rule',0);
			$this->CI->db->join('rule_group','rule_group.id_rule = rule.id_rule','left');
		}
		
		$this->CI->db->from('menus');
		$this->CI->db->orderby('menus.parent ASC,menus.ordering ASC');
		$q=$this->CI->db->get();
		$menus_tmp = $q->result_array();
		if($menus_tmp) foreach($menus_tmp as &$value)
			if(!$value['cek']){
				$value['cek']=1;
				$struct_menus[$value['id_menu']]=$value;
				$struct_menus[$value['id_menu']]['submenus'] = $this->menus_grouping($value['id_menu'],$menus_tmp);
			}
		unset($value);
		return $struct_menus;
	}
}
?>