<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Validation extends CI_Validation {
			
	function MY_Validation()
	{
		parent::CI_Validation();
	}
	
	function set_fields($data = '', $field = '')
	{		
		if ($data == '')
		{
			if (count($this->_fields) == 0)
			{
				return FALSE;
			}
		}
		else
		{
			if ( ! is_array($data))
			{
				$data = array($data => $field);
			}
			
			if (count($data) > 0)
			{
				$this->_fields = $data;
			}
		}
				
		foreach($this->_fields as $key => $val)
		{
			$this->$key = ( ! isset($_POST[$key])) ? '' : $this->prep_for_form($_POST[$key]);
			
			$error = $key.'_error';

			if ( ! isset($this->$error))
			{
				$this->$error = '';
			}
			else if($this->$error != '')
			{
				$this->$error = 'field_error';
			}
		}
		
		// Remove blank errors
		$this->_error_array = array_filter($this->_error_array);
	}

	function check_login()
	{
		$this->_error_messages['check_login'] = '';
		
		if(count($this->_error_array) > 0)
		{
			return FALSE;
		}
		
		$username	= $this->CI->input->post('username');
		$password	= $this->CI->input->post('password');
				
		// Try authenticating
		$login = $this->CI->access->login($username, $password);

		if($login === 'BANNED')
		{
			$this->_error_array[] = 'ID Anda Telah Berlaku Lagi.';
			return FALSE;
		}
		else if($login === 'NOT_ACTIVATED')
		{
			$this->_error_array[] = 'Account Anda Belum Diaktifasi.';
			return FALSE;
		}
		else if($login === 'TIMEOUT')
		{
			$this->_error_array[] = 'Anda telah gagal login sebanyak 3 kali. Coba lagi setelah 15 menit.';
			return FALSE;
		}
		
		if($login)
		{
			return TRUE;
		}
		else
		{
			// Wrong username/password combination
			$this->_error_array[] = 'Username atau password anda salah.';		
			return FALSE;
		}
	}
	
	function check_username($username)
	{
		if ($this->CI->access->check_username($username))
		{
			$this->_error_messages['check_username'] = 'Username sudah pernah digunakan.';		
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function check_email($email, $old = FALSE)
	{
		if ($email !== $old && $this->CI->access->check_email($email))
		{
			$this->_error_messages['check_email'] = 'Email sudah pernah digunakan.';		
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	// --------------------------------------------------------------------
	
	
	function check_password($pass)
	{
		if ($this->CI->access->login($this->CI->user->username, $pass) !== TRUE)
		{
			$this->_error_messages['check_password'] = 'Password lama tidak cocok';		
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function check_page($new, $old)
	{
		$this->CI->load->model('pages_model', 'pages');
		
		if ($new !== $old && $this->CI->pages->check_page($new))
		{
			$this->_error_messages['check_page'] = 'Url Already in Use';		
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	
}
?>