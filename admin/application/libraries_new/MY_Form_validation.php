<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {
			
	
	function MY_Form_validation()
	{
		parent::CI_Form_validation();
	}
	
	
	function check_email($email, $old = FALSE)
	{
		if ($email !== $old && $this->CI->access->check_email($email))
		{
			$this->_error_messages['check_email'] = 'Email sudah pernah digunakan.';		
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	// --------------------------------------------------------------------
	function check_password($pass)
	{
		if ($this->CI->access->check_password($this->CI->session->userdata['username'], $pass) !== TRUE)
		{
			$this->_error_messages['check_password'] = 'Password lama tidak cocok';		
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}

