<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Printtopdf
{
	var $CI;
	var $pdf;
	function Printtopdf()
	{
		$this->CI =& get_instance();
		set_time_limit(0);
		ini_set("memory_limit","256M");
		$this->CI->load->plugin('pdf');
		$this->pdf=pdf();
		$this->pdf->SetCreator(PDF_CREATOR);
	}
	
	function _setProperties($properties=array())
	{
		$this->pdf->SetAuthor('SIMPEGA pdf printer');
		$this->pdf->SetTitle($properties['title']);
		$this->pdf->SetSubject($properties['subject']);
		$this->pdf->SetKeywords($properties['keywords']);
		//$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		//$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		//set margins
		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		//$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$this->pdf->Footer();

		$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		//set some language-dependent strings
		$this->pdf->setLanguageArray($l);

		//initialize document
		$this->pdf->AliasNbPages();
		$this->pdf->SetFont('helvetica', '', 10);
	}
	function htmltopdf($properties=array(),$html='')
	{
		$this->_setProperties($properties);
		$this->pdf->AddPage($properties['paperlayout'],$properties['papersize']);
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->MultiCell(0, 2, $properties['title'], 0, 'C', 0, 1, 0 ,0, true);
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->MultiCell(0, 2, $properties['subtitle'], 0, 'C', 0, 1, 0 ,0, true);
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->writeHTML("<br/>".$html, true, 1, true, 0);	
		$this->pdf->lastPage();
		$this->pdf->Output($properties['filename'].'.pdf', 'I');
	}
	
	function vertical($properties=array(),$coloumn=array(),$data=array())
	{
		$this->_setProperties($properties);
		$j=0;$i = 0;
		$this->pdf->AddPage($properties['paperlayout'],$properties['papersize']);
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->MultiCell(0,2, strtoupper($properties['title']), 0, 'C', 0, 1, 0 ,0, true);
		$this->pdf->SetFont('helvetica', '', 10);
		$htmltable = "<br/>.<table width=\"100%\" cellpadding=\"4\" >
				<tr valign=\"middle\" bgcolor=\"#E0E0E0\">";
		foreach($coloumn as $table_hdr)
		{
			$htmltable = $htmltable . "<td ALIGN='".$table_hdr['align']."' width='".$table_hdr['width']."'>".$table_hdr['title']."</td>";
		}
		$htmltable = $htmltable . "</tr>";
		foreach ($data as $row) 
		{
			$i++;
			if (($i%2)==0) 
				{ $bgColor = "#FFFFFF"; } 
			else { $bgColor = "#F6F6F6"; }
			$no = $i;
			$htmltable=$htmltable."<tr valign=\"middle\" bgcolor=\"".$bgColor."\">";
			for($k=0;$k<count($coloumn);$k++)	
			{
				if (strtolower($coloumn[$k][field_name])=="tahun")
				$row[$coloumn[$k][field_name]]=$row[$coloumn[$k][field_name]]."/".($row[$coloumn[$k][field_name]]+1);
				$htmltable=$htmltable."<td  width='".$coloumn[$k]['width']."' align=\"".$coloumn[$k]['align']."\" nowrap=\"nowrap\">". $row[$coloumn[$k][field_name]]."</td>";
			}
			$htmltable = $htmltable ."</tr>";
		} 
		$htmltable = $htmltable."</table>";

		// output the HTML content
		$this->pdf->writeHTML($htmltable, true, 1, true, 0);
		$this->pdf->lastPage();
		$this->pdf->Output($properties['filename'].'.pdf', 'I');
	}
	
	function horisontal($properties=null, $data=null)
	{
		$this->_setProperties($properties);
		$this->pdf->AddPage($properties['paperlayout'],$properties['papersize']);
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->MultiCell(0, 2, strtoupper($properties['title']), 0, 'C', 0, 1, 0 ,0, true);
		$this->pdf->SetFont('helvetica', '', 10);
		$htmltable = "<br/><table width=\"520px\" cellpadding=\"4\" >";
		foreach ($data as $row)
		{
			$htmltable = $htmltable."<tr valign='middle'>";
			if ($row[subtitle]==1)
				$htmltable = $htmltable."<td colspan=\"3\"></td></tr><tr valign='middle'><td bgcolor=\"#F0F0F0\" colspan=\"3\" align=\"left\"><b>".$row[title]."</b></td>";
			else $htmltable=$htmltable."<td width =\"30%\" align=\"left\">".$row[title]."</td><td width=\"10%\"> : </td><td width =\"60%\" align=\"left\">".$row[data]."</td>";
			$htmltable=$htmltable."</tr>";
		}
		$htmltable = $htmltable."</table>";
		$this->pdf->writeHTML($htmltable, true, 1, true, 0);
		$this->pdf->lastPage();
		$this->pdf->Output($properties['filename'].'.pdf', 'I');
	}
	
	function horisontalvertical($properties=null,$data_hor=null,$data_ver=null)
	{
		$this->_setProperties($properties);
		$this->pdf->AddPage($properties['paperlayout'],$properties['papersize']);
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->MultiCell(0, 2, strtoupper($properties['title']), 0, 'C', 0, 1, 0 ,0, true);
		$this->pdf->SetFont('helvetica', '', 10);
		$htmltable = "<br/><table width=\"520px\" cellpadding=\"4\">";
		foreach ($data_hor as $row)
		{
			$htmltable = $htmltable."<tr valign=\"middle\">";
			if ($row[subtitle]==1)
				$htmltable = $htmltable."<td colspan=\"5\"></td></tr><tr valign=\"middle\"><td valign='middle' bgcolor=\"#F0F0F0\" align=\"left\"><b>".$row[title]."</b></td>";
			else $htmltable=$htmltable."<td width=\"100px\"> </td><td width =\"200px\" align=\"left\"><b>".$row[title]."</b></td><td align=\"center\" width=\"20px\"> : </td><td width =\"200px\" align=\"left\"><b>".$row[data]."</b></td><td width=\"50px\"> </td>";
			$htmltable=$htmltable."</tr>";
		}
		$htmltable = $htmltable."</table>";
		$this->pdf->writeHTML($htmltable, true, 1, true, 0);
		
		$htmltable = "<table  width=\"100%\" cellpadding=\"4\" >
				<tr bgcolor=\"#E0E0E0\" valign=\"middle\">";
		$data = $data_ver['data'];
		$column = $data_ver['column'];
		foreach($column as $table_hdr)
		{
			$htmltable = $htmltable . "<td VALIGN='MIDDLE' ALIGN='".$table_hdr['align']."' width='".$table_hdr['width']."'>".$table_hdr['title']."</td>";
		}
		$htmltable = $htmltable . "</tr>";
		foreach ($data as $row) 
		{
			$i++;
			if (($i%2)==0) 
				{ $bgColor = "#FFFFFF"; } 
			else { $bgColor = "#F6F6F6"; }
			$htmltable=$htmltable."<tr valign='middle' bgcolor=\"".$bgColor."\">";
			for($k=0;$k<count($column);$k++)	
			{
				if (strtolower($column[$k]['field_name'])=="tahun")
				$row[$coloumn[$k]['field_name']]=$row[$column[$k]['field_name']]."/".($row[$column[$k]['field_name']]+1);
				$htmltable=$htmltable."<td valign='middle' width='".$column[$k]['width']."' align=\"".$column[$k]['align']."\" nowrap=\"nowrap\">". $row[$column[$k]['field_name']]."</td>";
			}
			$htmltable = $htmltable ."</tr>";
		} 
		$htmltable = $htmltable."</table>";
		$this->pdf->writeHTML($htmltable, true, 1, true, 0);
		$this->pdf->lastPage();
		$this->pdf->Output($properties['filename'].'.pdf', 'I');
	}
}
?>