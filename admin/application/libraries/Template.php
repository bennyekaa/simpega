<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*

*/
class Template {

	var $CI;
	var $temp_sidebar = array();
	var $temp_userguide = array();
	var $temp_css = array();
	var $temp_js = array();
	var $temp_js_ie = array();
	var $temp_js_var = array();
	var $temp_data = array();
	var $default_template='template.php';

	/**
	 * Template Constructor
	 *
	 * @access	public
	 */
	function Template()
	{
		$this->CI =& get_instance();
		$this->set_defaults();
	}

	// ------------------------------------------------------------------------

	/**
	 * Set Default Variables value
	 *
	 * @access	public
	 */
	function set_defaults()
	{
		// Defaut meta tags
		$this->metas();

		$data['pagination'] 	= FALSE;
		$data['site_name'] 	= $this->CI->config->item('site_name');
		$data['site_email'] 	= $this->CI->config->item('site_email');
		$data['flashdata']	= 'template/content/flashdata';

		$this->sidebar('default');
		//$this->userguide();

		$this->CI->load->vars($data);
	}

	// ------------------------------------------------------------------------

	/**
	 * Set Meta Tags
	 *
	 */
	function metas($metas = array(), $value = '')
	{
		if( ! is_array($metas))
		{
			$metas = array($metas => $value);
		}

		$default_metas = array(
					'title' 		=> $this->CI->config->item('default_title'),
					'meta_description' 	=> $this->CI->config->item('default_description'),
					'meta_keywords' 	=> $this->CI->config->item('default_keywords')
				);

		$metas = array_merge($default_metas, $metas);

		$this->CI->load->vars($metas);
		return $metas;
	}

	function set_title($title='')
	{
		$this->metas('title',$title);
	}

	function set_keyword()
	{
		$this->metas('meta_keywords',$title);
	}

	// ------------------------------------------------------------------------

	/**
	 * Manage the side content
	 *
	 * @access	public
	 * @param	mixed	array of side partials
	 * @param	mixed	data needed by the side partials
	 * @param	bool	return
	 * @return	parsed content
	 */
	function sidebar($partials = '', $data = array())
	{
		$sidebar = '';
		//gabungin
		if( ! is_array($partials))
		{
			//$partials = array($partials);
			//cegah duplikasi
			if (!(in_array($partials,$this->temp_sidebar)))
			    @array_push($this->temp_sidebar,$partials);
		}else {

		    array_merge($this->temp_sidebar,$partials);
		}
		//var_dump($this->temp_sidebar);
		if($this->temp_sidebar!=NULL)
		foreach($this->temp_sidebar as $partial)
		{
			$sidebar =$this->CI->load->view('template/sidebar/'.$partial, $data, TRUE).$sidebar;
		}
		$this->CI->load->vars(array('side_content' => $sidebar));
	}

	function userguide()
	{
		$userguidex = $this->CI->load->view('template/userguide/default');

		$this->CI->load->vars(array('user_guide' => $userguidex));
	}

	function load_css($css_file,$absolute= TRUE)
	{
		if ((in_array(base_url().$css_file,$this->temp_css))) return;
		if ($absolute)
			array_push($this->temp_css,base_url().$css_file);
		else
			array_push($this->temp_css,base_url().$css_file);

	}

	function load_js($js_file,$absolute= TRUE)
	{
		if ((in_array(base_url().$js_file,$this->temp_js))) return;
		if ($absolute)
			array_push($this->temp_js ,base_url().$js_file);
		else
			array_push($this->temp_js,base_url().$js_file);
	}
	function load_js_ie($js_file,$absolute= TRUE)
	{
		if ((in_array(base_url().$js_file,$this->temp_js))) return;
		if ($absolute)
			array_push($this->temp_js_ie ,base_url().$js_file);
		else
			array_push($this->temp_js_ie,base_url().$js_file);

	}
	function load_js_var($js_var)
	{
		array_push($this->temp_js_var ,$js_var);
	}

	function show_head()
	{
		$result='';
		foreach($this->temp_css as $value)
		{
			$result=$result.'<link href="'.$value.'" rel="stylesheet" type="text/css" /> '."\n";
		}

		foreach($this->temp_js as $value)
		{
			$result=$result.'<script type="text/javascript" src="'.$value.'"></script> '."\n";
		}
		foreach($this->temp_js_ie as $value)
		{
			$result=$result.'<!--[if IE]><script type="text/javascript" src="'.$value.'"></script><![endif]-->'."\n";

		}
		if (count($this->temp_js_var)>0)
		{
			$result=$result.'<script type="text/javascript">';
			foreach($this->temp_js_var as $value)
			{
				$result=$result.$value;

			}
			$result=$result.'</script>';

		}
		return $result;

	}

	function _user_define_js()
	{
		if (count($this->temp_js_var)>0)
		{
			$result=$result.'<script type="text/javascript">';
			foreach($this->temp_js_var as $value)
			{
				$result=$result.$value;

			}
			$result=$result.'</script>';

		}
		return $result;
	}
	// ------------------------------------------------------------------------

	/**
	 * Display the final view to browser
	 *
	 * @access	public
	 * @param	string	view
	 * @param	mixed	data
	 * @param	string	content template
	 * @param	bool	return
	 * @return	parsed view
	 */
	function display($view, $data = array(), $content_template = 'general', $return = FALSE)
	{

		//cek ajax
		if (async_request())
		{
			if ($this->CI->session->flashdata('success')||$this->CI->session->flashdata('error')||$this->CI->session->flashdata('notofication'))
			{
				exit($this->CI->load->view($data['flashdata'], $data, TRUE));
			}
			else
			{
				exit($this->CI->load->view($view, $data, TRUE).$this->_user_define_js());
			}
		}

		$data['content'] = $this->CI->load->view($view, $data, TRUE);
		$data['content_template']	= "template/layout/".$content_template;

		// load data from here, bismillah
		$this->CI->load->model("pegawai_model", "pegawai");
		$data['data_pegawai']=$this->CI->pegawai->info_logged_pegawai($this->CI->user->user_id);

		return $this->CI->load->view($this->default_template, $data, $return);
	}
}
