<div class="content">
<div id="content-header">
    <div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('laporan/pelatihan')?>" title="Kembali">
		<img class="noborder" src="<?=base_url().'public/images/back.jpg'?>"></a>
	</div>
	<div class="clear"></div>
</div>	
<br/>
	<div class="content-data">
		<table class="general">
		<tr>
			<td align="left">NIP</td>
			<td> : </td>
			<td align="left">
			<?=$nip;?>
			</td>
		</tr>
		<tr>
			<td align="left">Nama Pegawai</td>
			<td> : </td>
			<td align="left">
			<?=$nama_peg?>
			</td>
		</tr>
		<tr>
			<td align="left">Nama Pelatihan</td>
			<td> : </td>
			<td align="left">
			<?=$nama_penghargaan?>
			</td>
		</tr>
		<tr>
			<td align="left">Tanggal</td>
			<td> : </td>
			<td align="left" colspan="2">
			<?= date('d M Y',strtotime($tgl_penghargaan)); ?>
			</td>
		</tr>
		<tr>
			<td align="left">Keterangan</td>
			<td> : </td>
			<td align="left">
				<?=$ket_riwayat_penghargaan?>
			</td>
		</tr>
		</table>
		<br>
		<br>

		<div class="clear"></div>
	</div>
</div>