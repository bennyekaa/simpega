<?php

?>
<div class="content">
	<div id="content-header">
		<div align="left" class="ch-title"><h3><?=$judul?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/pelatihan/printtopdf')?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
		</div>	
		<div class="clear"></div>
	</div>
	<br/>
	<div class="content-data">
		<table  class="table-list">
		<tr class="trhead">
			<th class="colEvn" width='20' align="center">No</th> 
			<th width='20'  align="center">NIPs</th>
			<th class="colEvn" width='250' class="colEvn" align="center">Nama</th>
			<th class="trhead" width='100' align="center">Tgl Pelatihan</th>
			<th class="colEvn" width='250' class="colEvn" align="center">Nama Pelatihan</th>
			<th class="trhead" width='40'>Edit</th>
		</tr>
		<?	$i = 0;
			if ($pelatihan_list!=FALSE){
				foreach ($pelatihan_list as $pelatihan) {
			$i++;
		   if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
		?>
		<tr class="<?=$class;?>">
			<td align="center" class="colEvn"><?= $i; ?></td>
			<td class="trhead" align="left" nowrap="nowrap"><?= $pelatihan['nippeg']; ?></td>
			<td class="colEvn" align="left" nowrap="nowrap" ><?= $pelatihan['nama_peg']; ?></td>
			<td class="trhead" align="center" class="colEvn" nowrap="nowrap" ><?= date('d M Y',strtotime($pelatihan['tglpenghargaan'])); ?> </td>
			<td class="colEvn" align="left" class="colEvn" nowrap="nowrap"><?= $pelatihan['nama_penghargaan']; ?></td>
			<td class="trhead" align="center" class="colEvn" >
				<a href = "<?=site_url("laporan/pelatihan/view/".$pelatihan['id_riwayat_pelatihan']); ?>" class="view action" >Lihat Detil</a>
			</td>
		</tr>
		
		<? 	} 
		}
		else {
			echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
		}?>
		</table>
		<div class="paging"><?=$page_links?></div>
	</div>
</div>	
