<?
function ucname($string) {
    $string =ucwords(strtolower($string));

    foreach (array('-', '\'') as $delimiter) {
      if (strpos($string, $delimiter)!==false) {
        $string =implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
      }
    }
    return $string;
}
?>
<div class="content">
<div id="content-header">

	<div id="module-title">
		<h3>
		Daftar Pejabat Struktural Eselon 
		
		</h3>
	</div>
	<div id="module-menu">
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/absensi/printtopdf/'.$group.'/'.$tahun.'/'.$unit_kerja)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/absensi/printtoxls/'.$group.'/'.$tahun.'/'.$unit_kerja.'/list_perunit_absensi')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
	</div>
	<div class="clear"></div>
</div>
	
<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?=site_url('laporan/eselon/browse')?>" >
				<label>Eselon : </label>
				<?=form_dropdown('eselon',$eselon_assoc,$eselon);?><br />
				<label>Keadaan : 1 </label>
				<?=form_dropdown('bulan',$bulan_assoc,$bulan);?> <?=form_dropdown('tahun',$tahun_assoc,$tahun);?>
				<input name="mulai" type="submit" value="Cari">
			</form>
			Data Eselon: <? $this->input->post('eselon_assoc', TRUE)?>
	  	</center>
		<br/>
		<?php
			if ($this->input->post('mulai')){
		?>
				
		
			<table class="table-list" width="100%">
				
					<tr class="trhead">
						<th  width="34" rowspan="3" align="center" class="trhead">No</th>
						<th width="134" rowspan="3" align="center" class="trhead"><span class="trhead">Nama, NIP<br />
						Tanggal Lahir </span></th>
						<th colspan="2" align="center" class="trhead">Pangkat</th>
						<th width="89" rowspan="3" align="center" class="trhead">Masa Kerja Keseluruhan  </th>
						<th colspan="4" align="center" class="trhead">Jabatan Saat ini </th>
						<th width="265" colspan="2" rowspan="2" align="center" class="trhead">Diklat</th>
						<th width="265" rowspan="3" align="center" class="trhead">Pendidikan Terakhir </th>
						<th width="265" rowspan="3" align="center" class="trhead">No. DUK </th>
						<th width="265" rowspan="3" align="center" class="trhead">DP3</th>
						<th width="265" rowspan="3" align="center" class="trhead">Jabatan yang pernah diduduki </th>
						<th width="265" rowspan="3" align="center" class="trhead">Tanggal Pensiun </th>
					</tr>
					<tr class="trhead">
					  <th width="31" rowspan="2" align="center" class="trhead">Gol</th>
			          <th width="65" rowspan="2" align="center" class="trhead">TMT</th>
			          <th width="68" rowspan="2" align="center" class="trhead">Struktural</th>
			          <th width="133" rowspan="2" align="center" class="trhead">Nomor SK </th>
			          <th colspan="2" align="center" class="trhead">Masa Kerja </th>
			  </tr>
					<tr class="trhead">
					  <th width="80" align="center" class="trhead">TMT Jabatan </th>
			          <th width="139" align="center" class="trhead">Eselon</th>
			          <th width="131" align="center" class="trhead">Struktural</th>
			          <th width="132" align="center" class="trhead">Fungsional/Teknis</th>
			  </tr>								
					<?php 		
							$i = $start++;
							if ($kerja_list!=FALSE)
							{
								foreach ($kerja_list as $pegawai) 
								{
									$j++;
									if (($j%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
									 
						?>
								<tr class="<?= $class; ?>">
									<td  align="center" class="colEvn"><?= $j;?>									</td>
									<td align="left" class="trhead"> <?=$pegawai['gelar_depan']." ".ucname($pegawai['nama_pegawai'])." ".$pegawai['gelar_belakang']?><br />
									  <?=$pegawai['NIP']?>  </td>
									<td align="left" class="trhead"><?=$golongan_assoc[$pegawai['id_golpangkat_terakhir']]?></td>
									<td align="left" class="trhead"><?= $pegawai['tmt_golpangkat_terakhir']?></td>
								
								  <td align="center" class="trhead"><?= $pegawai['masa_kerja']?></td>
									<td  align="center" class="trhead"></td>
									<td  align="center" class="trhead">&nbsp;</td>
									<td  align="center" class="trhead">&nbsp;</td>
									<td  align="center" class="trhead">&nbsp;</td>
								  <td align="center" class="colEvn"></td>
									<td  align="center" class="trhead">&nbsp;</td>
									<td  align="center" class="trhead"><?=$pendidikan_assoc[$pegawai['id_pendidikan_terakhir']]?></td>
									<td  align="center" class="trhead">&nbsp;</td>
									<td  align="center" class="trhead"></td>
									<td  align="center" class="trhead">&nbsp;</td>
									<td  align="center" class="trhead"><?= $pegawai['tgl_resign']?></td>
								</tr>
						<? 		}
							}
							else {
				
									echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
							}?>
			</table>
  </div>
</div>
<? } ?>