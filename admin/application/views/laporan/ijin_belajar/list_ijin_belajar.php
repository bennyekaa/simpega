<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/ijin_belajar/printtopdf/'.$group.'/'.$tahun.'/'.$unit_kerja)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/ijin_belajar/printtoxls/'.$group.'/'.$tahun.'/'.$unit_kerja.'/daftar_tb.xls')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<br/>
	<div id="content-data">
		<center>
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/ijin_belajar/browse')?>" >
				<label>Unit Kerja </label>
				<?=form_dropdown('unit_kerja',$unit_kerja_assoc,$unit_kerja);?>
				<br />
				<label> Keadaan  </label>
				<?=form_dropdown('group',$group_assoc,$group);?>
				
				<?=form_dropdown('tahun',$tahun_assoc,$tahun);?>
				<input type="submit" value=" Cari  " name="mulai">
			</form>
		</center>
		<br/>
		<?php
			if ($this->input->post('mulai')){
		?>
		<table class="table-list" width="70%">
			<tr class="trhead" align="center">	
				<th rowspan="2" align="center" class="colEvn">No</th> 
				<th class="colEvn" >Nama Pegawai</th>
				<th rowspan="2" align="center" class="trhead">Jurusan</th>
				<th rowspan="2" align="center" class="trhead">Jabatan</th>
				<th rowspan="2" align="center" class="trhead">Golongan</th>
				<th rowspan="2" align="center" class="trhead"><span class="colEvn">Bidang Studi </span></th>
				<th rowspan="2" align="center" class="trhead">Tempat Studi </th>
				<!--<th rowspan="2" >TMT Tugas Belajar </th>
				
				<th rowspan="2" nowrap class="colEvn">Batas Akhir <br />
		      Tugas Belajar </th>
				<th rowspan="2" nowrap class="colEvn">Biaya</th>-->
				<th rowspan="2" nowrap class="colEvn">Keterangan</th>
			</tr>
			<tr class="trhead" align="center">
			  <th class="colEvn" >NIP</th>
		  </tr>
			
			<?
			$i = 0;
			
			if ($tb_list!=FALSE){
			
			foreach ($tb_list as $tb) {
				//if ($pensiun['usia']=='56') {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				
				list($y, $m, $d) = explode('-', $tb['tmt_tb']);
				$str_tmt_tb = $d . "-" . $m . "-" . $y;
				$tb['tmt_tb']=$str_tmt_tb;
				
				list($y, $m, $d) = explode('-', $tb['batas_akhir_tb']);
				$str_batas_tb = $d . "-" . $m . "-" . $y;
				$tb['batas_akhir_tb']=$str_batas_tb;
				
				
			?>   
			<tr class="<?= $class; ?>">
				<td rowspan="2" align="center" class="colEvn"><?= $i; ?></td>
				<td align="left" nowrap="nowrap" class="colEvn"><?= $tb['gelar_depan']." ".$tb['nama_pegawai']." ".$tb['gelar_belakang']; ?></td>	
					<td rowspan="2" align="left" class="trhead"><?=$tb['nama_unit']?></td>
				<td rowspan="2" align="left" class="trhead"><?=$tb['nama_jabatan']?></td>
				<td rowspan="2" align="left" class="trhead"><?=$tb['golongan']?></td>
				<td rowspan="2" align="left" class="trhead"><?=$tb['bidang_studi_tb']?></td>
				<td rowspan="2" align="left" class="trhead"><?= $tb['tempat_studi_tb']?></td>
				<!--<td rowspan="2" align="center" nowrap="nowrap" class="trhead"><?=$tb['tmt_tb'];?></td>
				<td rowspan="2" align="center" nowrap class="trhead"><?=$tb['batas_akhir_tb'];?></td>
				<td rowspan="2" align="center" nowrap class="trhead"><?=$tb['biaya_tb'];?></td>-->
				<td rowspan="2" align="center" nowrap class="trhead"><?=$tb['keterangan']?></td>
			</tr>
			<tr class="<?= $class; ?>">
			  <td align="left" nowrap="nowrap" class="colEvn"><span class="trhead">
			     `<?= $tb['NIP']; ?>
			  </span></td>
		  </tr>
			
			<? } }//}
			else {
				$conspan = ($group == '') ? 7 : 6;
				echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
			}?>
	  </table>
		<div class="paging"><?=$page_links?></div>
	</div>
</div>
<? }?>