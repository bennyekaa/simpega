
<div class="content">
	<div id="content-header">
		<div id="module-title"><h3>Daftar Nominatif Masa Kerja PNS UM</h3></div>
		<div id="module-menu">
		
			<a class="icon" href="<?=site_url('laporan/mutasi/printtopdf/'.$group.'/'.$nama_seacrh)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/mutasi/printtoxls/'.$group.'/'.$nama_seacrh)?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
		<div class="clear"></div>
	</div>

<div id="content-data" style="padding:10px">
	<!-- <div style="width:90%;height:auto;padding:10px;margin:auto;text-align:center">
		<form action="" method='post'>
			Unit Kerja : <select name="unit_kerja" id="">
							<option value="">--Pilih--</option>
				<?php 
					// foreach ($unit_kerja as $data_unit_kerja) {
					// 	echo "<option value='".$data_unit_kerja['kode_unit']."'>".$data_unit_kerja['nama_unit']."</option>";
					// }
				 ?>
			</select>
			<button type="submit">Tampilkan</button>
		</form>
	</div>	 -->
	<!-- 	<h3>Daftar Nominatif Masa Kerja PNS UM</h3>
		<table class="table-list" width="100%" border="1">
		<tr>
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama</th>
			<th rowspan="2">NIP</th>
		<th colspan="2">Usia</th>
			<th colspan="2">Pendidikan</th>
			<th colspan="2">Masa Kerja Keseluruhan</th> 
			<th colspan="2">Unit Kerja Sekarang</th>
			<th colspan="2">Unit Kerja Sebelumnya</th> 
			<th rowspan="2">Detail</th>
		</tr>
		<tr>
			 <th>Tahun</th>
			<th>Bulan</th>
			<th>Jenjang</th>
			<th>Jurusan</th>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Tahun</th>
			<th>Bulan</th> 
			<th>Tahun</th>
			<th>Bulan</th>
		</tr>
		
		</table>-->

	<!-- <h3>Draft Mutasi Internal dalam Unit Kerja UM</h3> -->
		<table class="table-list" width="100%" border="1" id="data_mutasi">
		<thead>
		<tr>
			<th width="1%">No.</th>
			<th width="16%">Nama</th>
			<th>NIP</th>
			<th>Usia</th> 
			<th width="30%">Pendidikan</th>
			<!-- <th colspan="2">Masa Kerja Keseluruhan</th> -->
			<th>Unit Kerja Sekarang</th>
			<!-- <th rowspan="2">Unit Kerja Usulan</th> -->
		</tr>
		</thead>
		<tbody>
		<!-- <tr>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Tahun</th>
			<th>Bulan</th>
		</tr> -->
	
		<?php 
		$i=1;
			foreach ($draft_mutasi_internal as $mutasi_internal) {
				?>
				<tr>
					<td><?php echo $i++ ?></td>
					<td><?php echo $mutasi_internal['nama_pegawai'] ?></td>
					<td><?php echo $mutasi_internal['NIP'] ?></td>
					<td><?php echo $mutasi_internal['usia']['tahun'] ?> tahun <?php echo $mutasi_internal['usia']['bulan'] ?> bulan</td>
					<td><?php echo $mutasi_internal['ket_pendidikan'] ?></td>
					<td><?php echo $mutasi_internal['selama']['tahun'] ?> tahun <?php echo $mutasi_internal['selama']['bulan'] ?> bulan</td>
				</tr>
				<?php
			}
		 ?>
		 </tbody>
		</table>

<!-- 			<h3>Draft Mutasi Eksternal dalam Unit Kerja UM</h3>
		<table class="table-list" width="100%" border="1">
		<tr>
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama</th>
			<th rowspan="2">NIP</th>
			<th colspan="2">Usia</th>
			<th rowspan="2">Pendidikan</th>
				<th colspan="2">Masa Kerja Keseluruhan</th> 
			<th colspan="2">Unit Kerja Sekarang</th>
			<th rowspan="2">Unit Kerja Usulan</th>
		</tr>
		<tr>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Tahun</th>
			<th>Bulan</th>
		</tr>
		<?php 
		/**$i=1;
			foreach ($draft_mutasi_eksternal as $mutasi_eksternal) {
				?>
				<tr>
					<td><?php echo $i++ ?></td>
					<td><?php echo $mutasi_eksternal['nama_pegawai'] ?></td>
					<td><?php echo $mutasi_eksternal['NIP'] ?></td>
					<td><?php echo $mutasi_eksternal['usia']['tahun'] ?></td>
					<td><?php echo $mutasi_eksternal['usia']['bulan'] ?></td>
					<td><?php echo $mutasi_eksternal['ket_pendidikan'] ?></td>
					<td><?php echo $mutasi_eksternal['selama']['tahun'] ?></td>
					<td><?php echo $mutasi_eksternal['selama']['bulan'] ?></td>
				</tr>
				<?php
			}**/
		 ?>
		</table> -->
		<!-- <div class="paging">PAGE LINK</div> -->
	</div>
</di
v>