
<div class="content">
	<div id="content-header">
		<div id="module-title"><h3>Daftar Nominatif Masa Kerja PNS UM</h3></div>
		<div id="module-menu">
		
			<a class="icon" href="<?=site_url('laporan/mutasi/printtopdf/'.$group.'/'.$nama_seacrh)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/mutasi/printtoxls/'.$group.'/'.$nama_seacrh)?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
		<div class="clear"></div>
	</div>
</div>	
<div id="content-data">
	<div style="width:90%;height:auto;padding:10px;margin:auto;text-align:center">
		<form action="" method='post'>
			Unit Kerja : <select name="unit_kerja" id="">
							<option value="">--Pilih--</option>
				<?php 
					foreach ($unit_kerja as $data_unit_kerja) {
						echo "<option value='".$data_unit_kerja['kode_unit']."'>".$data_unit_kerja['nama_unit']."</option>";
					}
				 ?>
			</select>
			<button type="submit">Tampilkan</button>
		</form>
	</div>	
	<h3>Daftar Nominatif Masa Kerja PNS UM</h3>
		<table class="table-list" width="95%" border="1">
		<tr>
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama</th>
			<th rowspan="2">NIP</th>
<!-- 			<th colspan="2">Usia</th>
			<th colspan="2">Pendidikan</th>
			<th colspan="2">Masa Kerja Keseluruhan</th> -->
			<th colspan="2">Unit Kerja Sekarang</th>
			<!-- <th colspan="2">Unit Kerja Sebelumnya</th> -->
			<th rowspan="2">Detail</th>
		</tr>
		<tr>
			<!-- <th>Tahun</th>
			<th>Bulan</th>
			<th>Jenjang</th>
			<th>Jurusan</th>
			<th>Tahun</th>
			<th>Bulan</th>
			<th>Tahun</th>
			<th>Bulan</th> -->
			<th>Tahun</th>
			<th>Bulan</th>
		</tr>
		
		</table>

	<h3>Draft Mutasi Internal dalam Unit Kerja UM</h3>
		<table class="table-list" width="95%" border="1">
		<tr>
			<th>No.</th>
			<th>Nama</th>
			<th>NIP</th>
<!-- 			<th colspan="2">Usia</th>
			<th colspan="2">Pendidikan</th>
			<th colspan="2">Masa Kerja Keseluruhan</th> -->
			<th>Unit Kerja Sekarang</th>
			<!-- <th colspan="2">Unit Kerja Sebelumnya</th> -->
			<th>Unit Kerja Usulan</th>
		</tr>
	
		
		</table>

			<h3>Draft Mutasi Eksternal dalam Unit Kerja UM</h3>
		<table class="table-list" width="95%" border="1">
		<tr>
			<th>No.</th>
			<th>Nama</th>
			<th>NIP</th>
<!-- 			<th colspan="2">Usia</th>
			<th colspan="2">Pendidikan</th>
			<th colspan="2">Masa Kerja Keseluruhan</th> -->
			<th>Unit Kerja Sekarang</th>
			<!-- <th colspan="2">Unit Kerja Sebelumnya</th> -->
			<th>Unit Kerja Usulan</th>
		</tr>

		
		</table>
		<div class="paging">PAGE LINK</div>
	</div>
</div>

