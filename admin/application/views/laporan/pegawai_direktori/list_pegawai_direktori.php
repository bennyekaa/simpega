<?
$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');
?>

<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul;?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/pegawai_direktori/printtopdf/'.$group.'/'.$id_golpangkat.'/'.$id_jabatan.'/'.$id_jabatan_f)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/pegawai_direktori/printtoxls/'.$group.'/'.$id_golpangkat.'/'.$id_jabatan.'/'.$id_jabatan_f.'/buku_pegawai.xls')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	
	<div id="content-data" align="left">
		<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/pegawai_direktori/browse')?>" >
				<label>NIP/Nama Pegawai : </label>
				<input type="text" name="nama_search" size="25" value="<?=$nama_seacrh;?>">
				&nbsp;&nbsp;&nbsp;<label>Jenis Pegawai : </label><?=form_dropdown('group',$adm_akd_assoc,$group);?><br />
				<label>Jabatan Fungsional : </label><?=form_dropdown('id_jabatan_f',$jabatan_assoc, $id_jabatan_f);?>
				&nbsp;&nbsp;&nbsp;<label>Jabatan Struktural : </label><?=form_dropdown('id_jabatan',$jabatan_struktural_assoc, $id_jabatan);?><br />
				<label>Unit Kerja : </label><?=form_group_dropdown('unit_kerja',$option_unit,$unit_kerja);?>&nbsp;&nbsp;&nbsp;<label>Golongan : </label><?=form_dropdown('id_golpangkat',$golongan_assoc, $id_golpangkat);?>
				<input type="submit" value="Filter">
			</form>
		</center>
		<br/>
		
		<?
			//echo "<h3>Jenis Pegawai : ".$adm_akd_assoc[$group]." Golongan : ".$golongan_assoc[$id_golpangkat]."Jabatan :".$jabatan."Unit Kerja :".$unit."</h3>";
			$id_golpangkat = $_POST['id_golpangkat'];
			if ($id_golpangkat == 0)
			{
				$golongan = "-";
			}
			else
			{
				
				$golongan = $golongan_assoc['$id_golpangkat'];
			}
			
			if ($id_jabatan == 0)
			{
				$jabatan = "-";
			}
			else
			{
				$jabatan = $jabatan_struktural_assoc['$id_jabatan'];
			}
			
			
			if ($unit_kerja == 0)
			{
				$unit = "-";
			}
			else
			{
				$unit = $unit_kerja_assoc['$unit_kerja'];
			}
		?>
		<table class="table-list">
			<tr class="trhead">
				<th class="trhead" width="20" align="center">No</th>
				<th class="trhead" width="191" align="center">Nama</th>
				<th width="129" align="center" class="trhead">NIP</th>
				<th width="100" align="center" class="trhead">Gol/Ruang</th>
				<th class="trhead" width="350" align="center">Jabatan</th>
				<th class="trhead" width="350" align="center">Jabatan Fungsional</th>
				<th class="trhead" width="350" align="center">Subag</th>
				<th class="trhead" width="185" align="center">Unit</th>
			</tr>
			
				
			<?
			$i = $start++;
			if ($kerja_list!=FALSE){
			foreach ($kerja_list as $pegawai) {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				list($y, $m, $d) = explode('-', $pegawai['tgl_resign']);
				$str_tgl_resign = $d . "/" . $m . "/" . $y;
				$pegawai['tgl_resign']=$str_tgl_resign;
				
				list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
				$str_tgl_lahir = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tgl_lahir']=$str_tgl_lahir;
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_cpns']);
				$str_tmt_cpns = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tmt_cpns']=$str_tmt_cpns;
				if ($pegawai['tmt_cpns']=='00/00/0000')
				{
					$pegawai['tmt_cpns']='-';
				}
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_kgb']);
				$str_tmt_kgb = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tmt_kgb']=$str_tmt_kgb;
				if ($pegawai['tmt_kgb']=='00/00/0000')
				{
					$pegawai['tmt_kgb']='-';
				}
				
				if ($pegawai['eselon']=='non-eselon')
				{
					$pegawai['id_jabatan_struktural']='';
				}
				
			?>   
			<tr class="<?= $class; ?>">
				<td align="right" class="colEvn"><?= $i; ?></td>
				<!--<td align="left" class="trhead" ><?=$pegawai['NIP']?></td>-->
				<td  align="left" class="trhead"><a href="<?=site_url("pegawai/pegawai/view/".$pegawai['kd_pegawai']); ?>" title="Detil Data Pegawai" >
				<?=$pegawai['gelar_depan']." ".strtoupper($pegawai['nama_pegawai'])." ".$pegawai{'gelar_belakang'} ?> </a></td>
				<td align="left" class="colEvn"><?=$pegawai['NIP']?></td>
				<td align="center" class="colEvn"><?=$golongan_assoc[$pegawai['id_golpangkat_terakhir']]?></td>

				<td align="left" class="colEvn"><?
	
				if (($pegawai['id_jns_pegawai']== 1) or ($pegawai['id_jns_pegawai']== 5))
				{
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_jabatan_s_terakhir']);
				$str_tmt_jabatan_s_terakhir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
				
					$query = mysql_query("select nama_jabatan_s from jabatan_struktural where id_jabatan_s=(select id_jabatan_struktural from pegawai where kd_pegawai='".$pegawai['kd_pegawai']."')");					
						if ($query) {
								$data=mysql_fetch_array($query);  
								$jabatannya = $data['nama_jabatan_s'];
						if ($pegawai['tmt_jabatan_s_terakhir']=='0000-00-00'){
							$str_tmt_jabatan_s_terakhir='';
						}
									
						echo $jabatannya.'<br>'.$str_tmt_jabatan_s_terakhir;								
								}	
					
				}
				?></td>
				<td align="center" class="colEvn">
				<? 
				if (($pegawai['id_jabatan_terakhir']==27)or($pegawai['id_jabatan_terakhir']==28)or($pegawai['id_jabatan_terakhir']==29)){
				echo '-';
				}
				else
				{
					list($y, $m, $d) = explode('-', $pegawai['tmt_jabatan_terakhir']);
					if ($pegawai['tmt_jabatan_terakhir']=='0000-00-00'){
							$str_tmt_jabatan_terakhir='';
						}
					$str_tmt_jabatan_terakhir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
					echo $jabatan_assoc2[$pegawai['id_jabatan_terakhir']].'<br>'.$str_tmt_jabatan_terakhir;									
				}?></td>

				
				
				<td align="center" class="colEvn"> 
				
				<?=$unitkerja_assoc[$pegawai['kode_unit']]?></td>
				
				<?php 
			$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general, u.GD FROM unit_kerja  u 
								where  u.kode_unit='".$pegawai['kode_unit']."'");					
			if ($query) 
				{
					$data=mysql_fetch_array($query);  
					$tingkat_unit = $data['tingkat'];
					$namaunit = $data['nama_unit'];
					$kodeunit = $data['kode_unit'];
					$kodeunitg = $data['kode_unit_general'];
				}
				if ($tingkat_unit==1 and $data['GD'] != 'D') {
					$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general, u.GD FROM unit_kerja  u 
										where  u.kode_unit'".$kodeunitg."'");					
					if ($query) {
						$data=mysql_fetch_array($query);  
						$tingkat_unit = $data['tingkat'];
						$namaunit = $data['nama_unit'];
						$kodeunit = $data['kode_unit'];
						$kodeunitg = $data['kode_unit_general'];
					}
					if ($tingkat_unit==1 and $data['GD'] != 'D' ) {
						$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general, u.GD FROM unit_kerja  u 
											where  u.kode_unit='".$kodeunitg."'");					
						if ($query) {
							$data=mysql_fetch_array($query);  
							$tingkat_unit = $data['tingkat'];
							$namaunit = $data['nama_unit'];
							$kodeunit = $data['kode_unit'];
							$kodeunitg = $data['kode_unit_general'];
						}
						if ($tingkat_unit==1 and $data['GD'] != 'D') {
							$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general, u.GD FROM unit_kerja  u 
												where  u.kode_unit='".$kodeunitg."'");					
							if ($query) {
								$data=mysql_fetch_array($query);  
								$tingkat_unit = $data['tingkat'];
								$namaunit = $data['nama_unit'];
								$kodeunit = $data['kode_unit'];
								$kodeunitg = $data['kode_unit_general'];
							}
							if ($tingkat_unit==1 and $data['GD'] != 'D') {
								$namaunit = $pegawai['unit_kerja'];
							}													
						}
					}
				}					
				
		  		
		  ?>
		  
				<td align="center" class="colEvn"><?= $namaunit;?></td>
			</tr>
				

			
		  
		
		
			<? } }
			else {
				echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
			} ?>
		</table>
		<div class="paging">
		  <?=$page_links?>
		</div>
	</div>
</div>

