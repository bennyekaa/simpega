<?php 
function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}  
 ?>
<div class="content">
<div id="content-header">

	<div id="module-title"><h3>
	  Laporan Masa Kerja Unit Pegawai
	</h3>
	</div>
	<div id="module-menu">
		<div id="module-menu">
			<a href="<?=site_url('laporan/mutasi_mk/printtoxls/'.$unit_kerja.'/'.$id_jns_pegawai.'/list_perunit_mutasi_mk')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
	</div>
	<div class="clear"></div>
</div>
	
<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?=site_url('laporan/mutasi_mk/browse')?>" >		
				Jenis Pegawai:
					<?php echo form_dropdown('id_jns_pegawai', $get_jenis_pegawai_all, $id_jns_pegawai); ?>
				<label>Unit Kerja : </label>
				<?=form_group_dropdown('unit_kerja',$unitkerja_assoc,$unit_kerja);?>
				<label>NIP/Nama: </label>
				<input type="text" name="nama_search" size="25" value="<?=$nama_seacrh;?>">
				<input name="mulai" type="submit" value="Cari">
			</form>
			
	  	</center>
		<br/>
		<?php
			if ($this->input->post('mulai')){
		?>	
		
		 <?php 
		 	if ($unit_kerja == 'Semua'){
		?>
		<table class="table-list" width="100%">
			<tr class="trOdd">
				<td colspan="16"  align="center">
					<font size="+1"> <strong><?=$judul;?></strong></font>				</td>
			</tr>
			<tr class="trOdd">
				<td colspan="16"  align="center">
					<font size="+1"> <strong><?=$judul_unit;?></strong></font>				</td>
			</tr>
			<?php 
			//$query = mysql_query("select *, replace(kode_unit,'0','') as tingkatan from unit_kerja where tingkat < 3 order by kode_unit");					
			$query = mysql_query("select *, replace(kode_unit,'0','') as tingkatan from unit_kerja order by kode_unit");					
			if ($query) {
				while ($data_unit=mysql_fetch_array($query)) {
						$kd_unit = $data_unit['kode_unit'];
						$tingkatan = strlen($data_unit['tingkatan']);
						$pembanding=left($kd_unit,$tingkatan);		
			?>
			<?php
			
				if ($this->input->post('nama_search')!=''){
					$search_nama = " ((nama_pegawai like '%".$this->input->post('nama_search')."%') or (NIP like '".$this->input->post('nama_search')."'))";	
				}
				else{
					$search_nama = " (nama_pegawai like '%%')";	
				}
				if ($data_unit['tingkat'] == 1){
					$sql ="select * from pegawai where 
					(left(kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR kode_unit = '".$kd_unit."' 
					OR kode_unit_induk = '".$kd_unit."') 
					and (eselon not like 'non-eselon') and ".$search_nama."".$kriteria_jenis." order by id_golpangkat_terakhir desc";
					$nama_induk = $data_unit['nama_unit'];
					}
				else{
					$sql = "select * from pegawai where (kode_unit = '".$kd_unit."'  OR kode_unit_induk = '".$kd_unit."')
					and (eselon like 'non-eselon')  and ".$search_nama."".$kriteria_jenis." order by kode_unit,id_golpangkat_terakhir desc";							
					}	
				//echo $sql;
				$query_pegawai = mysql_query($sql);			
				$row_count = mysql_num_rows($query_pegawai);
				//echo $row_count.'__';
				$j=0;
						
				if (($query_pegawai) and $row_count>0 ){	
						$query_anak = mysql_query("select * from unit_kerja where kode_unit_general='".$kd_unit."'");
						
						if ($data_unit['tingkat'] == 1){							
							$ada_anak = '';}
						else if ($query_anak) {
							$data_anak=mysql_fetch_array($query_anak);
							if ($data_anak!=FALSE) {
								$ada_anak = $data_anak['kode_unit'];
							}
							else {
								$ada_anak = '';
								}
							}
						
				if ($ada_anak =='') {		
					echo "<tr><td colspan='12'>&nbsp;</td></tr>";
					if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='1')) {
						echo "<tr><td colspan='12'><strong> PUSAT (".$data_unit['nama_unit'].")</strong></td></tr>";}
					else if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='2')) {
						echo "<tr><td colspan='12'><strong> FAKULTAS (".$data_unit['nama_unit'].")</strong></td></tr>";}
					else {
						echo "<tr><td colspan='12'><strong>".$nama_induk." (".$data_unit['nama_unit'].")</strong></td></tr>";
					}
				?>
			<tr class="trhead">
				<th width="20" rowspan="2" align="center" class="trhead">No</th>
				<th width="153" rowspan="2" align="center" class="trhead">Nama Pegawai/NIP</th>
				<th width="53" rowspan="2" align="center" class="trhead">Usia</th>
				<th colspan="2" align="center" class="trhead">Pendidikan</th>
				<th align="center" colspan="3" class="trhead">Masa Kerja/Unit Kerja</th>
				<th width="76" rowspan="2" align="center" class="trhead">Detil</th>
			</tr>
			<tr class="trhead">
			  <th width="53" align="center" class="trhead">Jenjang</th>
				<th width="81" align="center" class="trhead">Jurusan</th>
				<th width="101" align="center" class="trhead">Keseluruhan</th>
				<th width="139" align="center" class="trhead">Unit kerja sekarang</th>
				<th width="138" align="center" class="trhead">Unit kerja lama</th>
			</tr>						
			<?php 				
				while ($data_pegawai=mysql_fetch_array($query_pegawai)) {
					$j++;
					if (($j%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				?>
			<tr class="<?= $class; ?>">
				<td  align="left" class="colEvn"><?= $j;?> </td>
				<td align="left" class="trhead">  
					<?=$data_pegawai['gelar_depan']." ".strtoupper($data_pegawai['nama_pegawai'])." 
				".$data_pegawai['gelar_belakang']?>
				<br /><font color="#0000CC">[<?=$data_pegawai['NIP']?>]</font>				</td>
				<td  align="left" class="colEvn">
					<?php 
					$a = datediff($data_pegawai['tgl_lahir'], date('Y-m-d'));
					echo $a[years].' thn '.$a[months].' bln ';
				?>				</td>
				<td  align="left" class="colEvn">
					 
				<?php 
						//buat prosedur pembacaan dari record riwayat pendidikan terbaru
						$query_pendidikan = mysql_query("select  * from riwayat_pendidikan where 
						kd_pegawai = '".$data_pegawai['kd_pegawai']."' and
						aktif = '1'");
						
						$data_pendidikan=mysql_fetch_array($query_pendidikan);
						$row_count = mysql_num_rows($query_pendidikan);
						if ($row_count>0){	
							$jurusan =$data_pendidikan['prodi'];	
							$pendidikan =$pendidikan_assoc[$data_pendidikan['id_pendidikan']];			
						echo $pendidikan;
						}
					
					?>				</td>
				<td  align="left" class="colEvn"><?=$jurusan;?></td>
				<td  align="left" class="colEvn">
					<?php 
					$mk = datediff($data_pegawai['tmt_cpns'], date('Y-m-d'));
					$mk_tambahan_th = $data_pegawai['mk_tambahan_th'];
					$mk_tambahan_bl = $data_pegawai['mk_tambahan_bl'];
					$th = $mk[years] + $mk_tambahan_th;
					$bl = $mk[months] + $mk_tambahan_bl;
					echo $th.' thn '.$bl.' bln ';
				?>				</td>
				<td  align="left" class="colEvn">
					<?php 
						//buat prosedur pembacaan dari record mutasi ke unit skrg
						$query_mutasi = mysql_query("select  * from mutasi where 
						kd_pegawai = '".$data_pegawai['kd_pegawai']."' and
						kode_unit_kerja_baru = '".$data_pegawai['kode_unit']."' order by tmt_SK desc limit 0,1");
						/*echo "select  tmt_SK from mutasi where 
						kd_pegawai = '".$data_pegawai['kd_pegawai']."' and
						kode_unit_kerja_baru = '".$data_pegawai['kode_unit']."' order by tmt_SK desc limit 0,1";*/
						$data_mutasi=mysql_fetch_array($query_mutasi);
						$row_count = mysql_num_rows($query_mutasi);
						if ($row_count>0){	
							$a = datediff(date('Y-m-d'),$data_mutasi['tmt_SK']);
							$tmt_SK_terbaru =$data_mutasi['tmt_SK'];	
							$kode_unit_kerja_sebelum_terbaru =$data_mutasi['kode_unit_kerja_asal'];			
							echo $unit_kerja_assoc[$data_pegawai['kode_unit']].'<BR><font color="#0000CC">'.$a[years].' thn '.$a[months].' bln </font>';
						}
						else{
							$kode_unit_kerja_sebelum_terbaru =$data_pegawai['kode_unit'];	//$kode_unit_kerja_sebelum_terbaru =$data_pegawai['kode_unit'];
							$tmt_SK_terbaru =date('Y-m-d');
							$a = datediff($data_pegawai['tmt_cpns'], date('Y-m-d'));
							echo $unit_kerja_assoc[$data_pegawai['kode_unit']].'<BR><font color="#0000CC">'.$a[years].' thn '.$a[months].' bln </font> ';
						}
					
					?>				</td>
				<td  align="left" class="colEvn">
					<?php 
					//buat prosedur pembacaan dari record mutasi sebelum ke unit skrg
					$query_mutasi_sebelum_terbaru = mysql_query("select  * from mutasi where 
					kd_pegawai = '".$data_pegawai['kd_pegawai']."' and 
					kode_unit_kerja_baru = '".$kode_unit_kerja_sebelum_terbaru."' order by tmt_SK desc limit 0,1");									
					//ambil data mutasi sebelum
					$data_mutasi_sebelum_terbaru=mysql_fetch_array($query_mutasi_sebelum_terbaru);
					$row_count_qms = mysql_num_rows($query_mutasi_sebelum_terbaru);
					if ($row_count_qms>0){			
							$tmt_SK_sebelum_terbaru = $data_mutasi_sebelum_terbaru['tmt_SK'];						
							$a = datediff($tmt_SK_sebelum_terbaru, $tmt_SK_terbaru);	
						
						echo $unit_kerja_assoc[$kode_unit_kerja_sebelum_terbaru].'<BR><font color="#0000CC">'.$a[years].' thn '.$a[months].' bln </font>';
					}
					else{
						$a = datediff($data_pegawai['tmt_cpns'],$tmt_SK_terbaru);
						
						if ($data_pegawai['kode_unit'] != $kode_unit_kerja_sebelum_terbaru){
						echo  $unit_kerja_assoc[$kode_unit_kerja_sebelum_terbaru].'<BR><font color="#0000CC">'.$a[years].' thn '.$a[months].' bln </font>';}
						else{
						echo  '-';
						}
						
					}
				
					?>				</td>
				<td  align="center" class="colEvn">
					<a href="<?=site_url("pegawai/riwayatmutasi/index/".$data_pegawai['kd_pegawai']); ?>" title="Detil Data Mutasi Pegawai" class="view action">detil data mutasi</a>				</td>
			</tr>
			<?php
	$nama_peg = $data_pegawai['gelar_depan']." ".strtoupper($data_pegawai['nama_pegawai'])." ".$data_pegawai['gelar_belakang'];		
					}
					
				} //penutup jika ada anak
				
						}
					$j=0;
					
					}
				
				}
			 ?>
		</table>	
<!--jika unit kerja yang dipilih adalah per unit maka;-->		  
		<?php
			}
			else {
				$query = mysql_query("select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,2)='".left($unit_kerja,2)."')");					
				if ($query) 
					{
					$data_induk=mysql_fetch_array($query);
					$nama_induk = $data_induk['nama_unit'];
					if ($nama_induk='') { 
						$nama_induk = $data_induk['nama_unit'];}
					else
						{
							$query = mysql_query("select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,1)='".left($unit_kerja,1)."')");					
							if ($query) {
								$data_induk=mysql_fetch_array($query); 
								$nama_induk = $data_induk['nama_unit'];}
						}
				}
				$query = mysql_query("select id_group,nama_unit from unit_kerja where kode_unit='".$unit_kerja."'");					
				if ($query) {
					$data_unit=mysql_fetch_array($query); 
					$nama_unit = $data_unit['nama_unit'];
					if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='1')) {
						$nama_induk ='PUSAT';}
					}	
				$nama_unit = $nama_induk.' ('.$nama_unit.')';
		?>
		<table class="table-list" width="100%">
			<tr class="trOdd"><td colspan="15"  align="center">
			<font size="+1"> <strong><?=$judul;?></strong></font>
			</td></tr>
			<tr class="trOdd"><td colspan="15"  align="left">
			<?=$nama_unit;?>
			</td></tr>
			<tr class="trhead">
				<th width="20" rowspan="2" align="center" class="trhead">No</th>
				<th align="center" rowspan="2" class="trhead">Nama Pegawai/NIP</th>
				<th align="center" rowspan="2" class="trhead">Usia</th>
				<th colspan="2" align="center" class="trhead">Pendidikan</th>
				<th align="center" colspan="3" class="trhead">Masa Kerja/Unit Kerja</th>
				<th align="center" rowspan="2" class="trhead">Detil</th>
			</tr>
			<tr class="trhead">
				<th width="53" align="center" class="trhead">Jenjang</th>
				<th width="81" align="center" class="trhead">Jurusan</th>
				<th align="center" class="trhead">Keseluruhan</th>
				<th align="center" class="trhead">Unit kerja sekarang</th>
				<th align="center" class="trhead">Unit kerja lama</th>
			</tr>			
			<?
			$i = $start++;
			if ($kerja_list!=FALSE){
			foreach ($kerja_list as $data_pegawai) {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
			
			?>   
			<tr class="<?= $class; ?>">
				<td  align="left" class="colEvn"><?= $i;?> </td>
				<td align="left" class="trhead">  
					<?=$data_pegawai['gelar_depan']." ".strtoupper($data_pegawai['nama_pegawai'])." 
				".$data_pegawai['gelar_belakang']?>
				<br /><font color="#0000CC">[<?=$data_pegawai['NIP']?>]</font>
				</td>
				<td  align="left" class="colEvn">
					<?php 
					$a = datediff($data_pegawai['tgl_lahir'], date('Y-m-d'));
					echo $a[years].' thn '.$a[months].' bln ';
				?> 
				</td>
				<td  align="left" class="colEvn">
					 
				<?php 
						//buat prosedur pembacaan dari record riwayat pendidikan terbaru
						$query_pendidikan = mysql_query("select  * from riwayat_pendidikan where 
						kd_pegawai = '".$data_pegawai['kd_pegawai']."' and
						aktif = '1'");
						
						$data_pendidikan=mysql_fetch_array($query_pendidikan);
						$row_count = mysql_num_rows($query_pendidikan);
						if ($row_count>0){	
							$jurusan =$data_pendidikan['prodi'];	
							$pendidikan =$pendidikan_assoc[$data_pendidikan['id_pendidikan']];			
						echo $pendidikan;
						}
					
					?>				</td>
				<td  align="left" class="colEvn"><?=$jurusan;?></td>
				<td  align="left" class="colEvn">
					<?php 
					$mk = datediff($data_pegawai['tmt_cpns'], date('Y-m-d'));
					$mk_tambahan_th = $data_pegawai['mk_tambahan_th'];
					$mk_tambahan_bl = $data_pegawai['mk_tambahan_bl'];
					$th = $mk[years] + $mk_tambahan_th;
					$bl = $mk[months] + $mk_tambahan_bl;
					echo $th.' thn '.$bl.' bln ';
				?>
				</td>
				<td  align="left" class="colEvn">
					<?php 
						//buat prosedur pembacaan dari record mutasi ke unit skrg
						$query_mutasi = mysql_query("select  * from mutasi where 
						kd_pegawai = '".$data_pegawai['kd_pegawai']."' and
						kode_unit_kerja_baru = '".$data_pegawai['kode_unit']."' order by tmt_SK desc limit 0,1");
						/*echo "select  tmt_SK from mutasi where 
						kd_pegawai = '".$data_pegawai['kd_pegawai']."' and
						kode_unit_kerja_baru = '".$data_pegawai['kode_unit']."' order by tmt_SK desc limit 0,1";*/
						$data_mutasi=mysql_fetch_array($query_mutasi);
						$row_count = mysql_num_rows($query_mutasi);
						if ($row_count>0){	
							$a = datediff(date('Y-m-d'),$data_mutasi['tmt_SK']);
							$tmt_SK_terbaru =$data_mutasi['tmt_SK'];	
							$kode_unit_kerja_sebelum_terbaru =$data_mutasi['kode_unit_kerja_asal'];			
							echo $unit_kerja_assoc[$data_pegawai['kode_unit']].'<BR><font color="#0000CC">'.$a[years].' thn '.$a[months].' bln </font>';
						}
						else{
							$kode_unit_kerja_sebelum_terbaru =$data_pegawai['kode_unit'];	
							$tmt_SK_terbaru =date('Y-m-d');
							$a = datediff($data_pegawai['tmt_cpns'], date('Y-m-d'));
							echo $unit_kerja_assoc[$data_pegawai['kode_unit']].'<BR><font color="#0000CC">'.$a[years].' thn '.$a[months].' bln </font> ';
						}
					
					?>
				</td>
				<td  align="left" class="colEvn">
					<?php 
					//buat prosedur pembacaan dari record mutasi sebelum ke unit skrg
					$query_mutasi_sebelum_terbaru = mysql_query("select  * from mutasi where 
					kd_pegawai = '".$data_pegawai['kd_pegawai']."' and 
					kode_unit_kerja_baru = '".$kode_unit_kerja_sebelum_terbaru."' order by tmt_SK desc limit 0,1");									
					//ambil data mutasi sebelum
					$data_mutasi_sebelum_terbaru=mysql_fetch_array($query_mutasi_sebelum_terbaru);
					$row_count_qms = mysql_num_rows($query_mutasi_sebelum_terbaru);
					if ($row_count_qms>0){			
							$tmt_SK_sebelum_terbaru = $data_mutasi_sebelum_terbaru['tmt_SK'];						
							$a = datediff($tmt_SK_sebelum_terbaru, $tmt_SK_terbaru);	
						
						echo $unit_kerja_assoc[$kode_unit_kerja_sebelum_terbaru].'<BR><font color="#0000CC">'.$a[years].' thn '.$a[months].' bln </font>';
					}
					else{
						$a = datediff($data_pegawai['tmt_cpns'],$tmt_SK_terbaru);
						if ($data_pegawai['kode_unit'] != $kode_unit_kerja_sebelum_terbaru){
						echo  $unit_kerja_assoc[$kode_unit_kerja_sebelum_terbaru].'<BR><font color="#0000CC">'.$a[years].' thn '.$a[months].' bln </font>';}
						else{
						echo  '-';
						}
					}
				
				?> 
				</td>
				<td  align="center" class="colEvn">
					<a href="<?=site_url("pegawai/riwayatmutasi/index/".$data_pegawai['kd_pegawai']); ?>" 
				title="Detil Data Mutasi Pegawai" class="view action">detil data mutasi</a>
				</td>
			</tr>
			<?php
$nama_peg = $data_pegawai['gelar_depan']." ".strtoupper($data_pegawai['nama_pegawai'])." ".$data_pegawai['gelar_belakang'];	
			 } }
				else {
					$conspan = ($group == '') ? 7 : 6;
					echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
			}?>		
		</table>	
		<? } }?>
		<br />
		<div class="paging"><?=$page_links?></div>
  </div>
</div>
