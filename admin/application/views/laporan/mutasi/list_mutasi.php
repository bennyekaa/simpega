<?
$option_group=$this->lookup->get_group_laporan(0,'jenis_mutasi','jns_mutasi','nama_mutasi');
?>
<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul?></h3></div>
		<div id="module-menu">
		
			<a class="icon" href="<?=site_url('laporan/mutasi/printtopdf/'.$group.'/'.$nama_seacrh)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/mutasi/printtoxls/'.$group.'/'.$nama_seacrh)?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
		<div class="clear"></div>
	</div>
	<br/>
	<div>
		<div class="paging" style="float:left">&nbsp;<?=$page_links?></div>
	</div>
</div>	
<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
&nbsp;		
		<table class="table-list">
		<tr class="trOdd" align="left">	
		    <td colspan="7">
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/mutasi/browse')?>" >
				NIP/Nama Pegawai/NoSK :
				<input type="text" name="nama_search" size="25" value="<?=$nama_seacrh;?>">
				Jenis Mutasi :<?=form_dropdown('group',$option_group,$group);?>
				<input type="submit" value=" Cari  " name="mulai">
			</form>
			</td>
		</tr>
<?php
	if ($this->input->post('mulai'))
{?>
		<tr class="trOdd" align="center">	
		    <td colspan="6">&nbsp;</td>
		</tr>
		<tr class="trhead" align="center">	
		    <th class="colEvn" width="20" align="center">No</th> 
			<th width="100"  align="left">NIP</th>
			<th class="colEvn" width="200" align="left">Nama Pegawai</th>
			<th class="colEvn" width="100" align="left">No. SK</th>
			<th class="trhead" width="90" >Tgl mutasi</th>
			<th width="200" >Unit Kerja Asal</th>
			<th class="colEvn" width="200" >Unit Kerja Baru </th>
		</tr>
		
		<?
		$i = 0;
		if ($mutasi_list!=FALSE){
		foreach ($mutasi_list as $mutasi) {
			$i++;
			if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
		?>   
		<tr class="<?= $class; ?>">
		    <td align="right" class="colEvn"><?= $i; ?></td>
			<td align="left" class="trhead" nowrap="nowrap"><?= $mutasi['NIP']; ?></td>
			<td align="left" class="colEvn" nowrap="nowrap"><?= strtoupper($mutasi['nama_pegawai']); ?></td>
			<td align="left" class="colEvn" nowrap="nowrap"><?= $mutasi['no_SK']; ?></td>
			<td align="center" class="trhead" nowrap="nowrap" ><?= $mutasi['tmt_SK']; ?></td>
			<td align="left" ><?= $unit_kerja_assoc[$mutasi['kode_unit_kerja_asal']]; ?></td>
			<td align="left" ><?= $unit_kerja_assoc[$mutasi['kode_unit_kerja_baru']]; ?></td>
		</tr>
		<? } 
		}
		else {
			echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";

		}

 } ?>
</table>
		<div class="paging"><?=$page_links?></div>
	</div>
</div>

	