<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('laporan/mutasi')?>" title="Kembali">
		<img class="noborder" src="<?=base_url().'public/images/back.jpg'?>"></a>
	</div>	
	<div class="clear"></div>
</div>
<br/>
	<div class="content-data">
		<table class="general">
		<tr>
			<td align="left">NIP</td>
			<td> : </td>
			<td align="left">
			<?=$NIP?>
			</td>
		</tr>
		<tr>
			<td align="left">Nama Pegawai</td>
			<td> : </td>
			<td align="left">
			<?=$nama_pegawai?>
			</td>
		</tr>
		<tr>
			<td align="left">Jenis Mutasi</td>
			<td> : </td>
			<td align="left">
			<?=$nama_mutasi;?>
			</td>
		</tr>
		<tr>
			<td align="left">No SK Mutasi</td>
			<td> : </td>
			<td align="left">
			<?=$no_SK?>
			</td>
		</tr>		
		<tr>
			<td align="left">Tgl SK</td>
			<td> : </td>
			<td align="left">
			<?= date('d M Y',strtotime($tgl_SK)); ?>
			</td>
		</tr>
		<tr>
			<td align="left">Tmt SK</td>
			<td> : </td>
			<td align="left">
			<?= date('d M Y',strtotime($tmt_SK)); ?>
			</td>
		</tr>
		<tr>
			<td align="left">Tgl Mutasi</td>
			<td> : </td>
			<td align="left">
			<?= date('d M Y',strtotime($tgl_mutasi)); ?>
			</td>
		</tr>
		<tr>
			<td align="left">Asal Unit Kerja</td>
			<td> : </td>
			<td align="left">
				<?=$unit_asal?>
			</td>
		</tr>
		<tr>
			<td align="left">Jabatan Asal</td>
			<td> : </td>
			<td align="left">
				<?=$jabatan_asal?>
			</td>
		</tr>	
		<tr>
			<td align="left">Unit Kerja Sekarang</td>
			<td> : </td>
			<td align="left">
				<?=$unit_baru?>
			</td>
		</tr>
		<tr>
			<td align="left">Jabatan Sekarang</td>
			<td> : </td>
			<td align="left">
				<?=$jabatan_baru?>
			</td>
		</tr>		
		<tr>
			<td align="left">Ket Mutasi</td>
			<td> : </td>
			<td align="left">
				<?=$ket_mutasi?>
			</td>
		</tr>			
		</table>
		<br>
		<br>

		<div class="clear"></div>
	</div>
</div>