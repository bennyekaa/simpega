<?
$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');
?>

<div class="content">
	<div id="content-header">
		<div id="module-title"><h3>LAPORAN VALIDASI DATA</h3></div>
		<!--<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/pegawai_askes/printtopdf/'.$group)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/pegawai_askes/printtoxls/'.$group.'/buku_pegawai.xls')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>-->
		<div class="clear"></div>
	</div>
	
	<div id="content-data">
		<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/pegawai_askes/browse')?>" >
				<label>NIP/Nama : </label>
				<input type="text" name="nama_search" size="20" value="<?=$nama_seacrh;?>">
				<label>Jenis Pegawai : </label><?=form_dropdown('group',$adm_akd_assoc,$group);?>
				<!--<label>Golongan : </label><?=form_dropdown('id_golpangkat',$golongan_assoc, $id_golpangkat);?>
				<label>Jabatan Struktural : </label><?=form_dropdown('id_jabatan',$jabatan_struktural_assoc, $id_jabatan);?><br />-->
				<label>Unit Kerja : </label><?=form_group_dropdown('unit_kerja',$option_unit,$unit_kerja);?>
				<input type="submit" value="Filter">
			</form>
		</center>
		<br/>
		
		<?
			//echo "<h3>Jenis Pegawai : ".$adm_akd_assoc[$group]." Golongan : ".$golongan_assoc[$id_golpangkat]."Jabatan :".$jabatan."Unit Kerja :".$unit."</h3>";
			$id_golpangkat = $_POST['id_golpangkat'];
			if ($id_golpangkat == 0)
			{
				$golongan = "-";
			}
			else
			{
				
				$golongan = $golongan_assoc['$id_golpangkat'];
			}
			
			if ($id_jabatan == 0)
			{
				$jabatan = "-";
			}
			else
			{
				$jabatan = $jabatan_struktural_assoc['$id_jabatan'];
			}
			
			if ($unit_kerja == 0)
			{
				$unit = "-";
			}
			else
			{
				$unit = $unit_kerja_assoc['$unit_kerja'];
			}
		?>
<?php
	$centang = $_POST['centang'];
	if(empty($centang)){
?>	
<form id="formCheck" method="POST" action="<?=site_url('laporan/pegawai_askes/browse')?>" >
		<table class="table-list">
			<tr class="trhead">
			<th width="68" align="center">
			<input type="button" id="tombolCheck" value="Check All" onClick="Check();"/></th>
				<th class="trhead" width="20" align="center">No</th>
				<th class="trhead" width="191" align="center">Nama</th>
				<th width="129" align="center" class="trhead">NIP</th>
				<!--<th width="100" align="center" class="trhead">Gol/Ruang</th>-->
				<th class="trhead" width="350" align="center">Sub Bagian - Unit Kerja</th>

			</tr>
			
				
			<?
			$i = $start++;
			$jml_peg =0;
			$jml_sc =0;
			if ($kerja_list!=FALSE){
			foreach ($kerja_list as $pegawai) {
				$i++;
				$jml_peg++;
				if ($pegawai['validasi_data']=='1'){$jml_sc++;;}
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				list($y, $m, $d) = explode('-', $pegawai['tgl_resign']);
				$str_tgl_resign = $d . "/" . $m . "/" . $y;
				$pegawai['tgl_resign']=$str_tgl_resign;
				
				list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
				$str_tgl_lahir = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tgl_lahir']=$str_tgl_lahir;
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_cpns']);
				$str_tmt_cpns = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tmt_cpns']=$str_tmt_cpns;
				if ($pegawai['tmt_cpns']=='00/00/0000')
				{
					$pegawai['tmt_cpns']='-';
				}
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_kgb']);
				$str_tmt_kgb = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tmt_kgb']=$str_tmt_kgb;
				if ($pegawai['tmt_kgb']=='00/00/0000')
				{
					$pegawai['tmt_kgb']='-';
				}
				
				if ($pegawai['eselon']=='non-eselon')
				{
					$pegawai['id_jabatan_struktural']='';
				}
				
			?>   
			<tr class="<?= $class; ?>">
			<td align="center"><input type="checkbox" name="centang[<?= $i; ?>]" value="<?=$pegawai['kd_pegawai']?>" 
			<?php if ($pegawai['validasi_data']=='1') 
			{?> 
				checked="checked"
			<? } ?> /></td>
			<td align="right" class="colEvn"><?= $i; ?></td>
			<!--<td align="left" class="trhead" ><?=$pegawai['NIP']?></td>-->
			<td  align="left" class="trhead"><?=$pegawai['gelar_depan']." ".strtoupper($pegawai['nama_pegawai'])." ".$pegawai{'gelar_belakang'} ?></td>
			<td align="left" class="colEvn"><?=$pegawai['NIP']?></td>
			<!--<td align="center" class="colEvn"><?=$golongan_assoc[$pegawai['id_golpangkat_terakhir']]?></td>-->
			<td align="left" class="colEvn"><?=$jabatan_struktural_assoc[$pegawai['id_jabatan_struktural']]. " ".$unitkerja_assoc[$pegawai['kode_unit']]?> - 
				<?php 
			$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
								where  u.kode_unit='".$pegawai['kode_unit']."'");					
			if ($query) 
				{
					$data=mysql_fetch_array($query);  
					$tingkat_unit = $data['tingkat'];
					$namaunit = $data['nama_unit'];
					$kodeunit = $data['kode_unit'];
					$kodeunitg = $data['kode_unit_general'];
				}
				if ($tingkat_unit!=1) {
					$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
										where  u.kode_unit'".$kodeunitg."'");					
					if ($query) {
						$data=mysql_fetch_array($query);  
						$tingkat_unit = $data['tingkat'];
						$namaunit = $data['nama_unit'];
						$kodeunit = $data['kode_unit'];
						$kodeunitg = $data['kode_unit_general'];
					}
					if ($tingkat_unit!=1) {
						$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM 
						unit_kerja u where  u.kode_unit='".$kodeunitg."'");					
						if ($query) {
							$data=mysql_fetch_array($query);  
							$tingkat_unit = $data['tingkat'];
							$namaunit = $data['nama_unit'];
							$kodeunit = $data['kode_unit'];
							$kodeunitg = $data['kode_unit_general'];
						}
						if ($tingkat_unit!=1) {
							$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM 
							unit_kerja  u where  u.kode_unit='".$kodeunitg."'");					
							if ($query) {
								$data=mysql_fetch_array($query);  
								$tingkat_unit = $data['tingkat'];
								$namaunit = $data['nama_unit'];
								$kodeunit = $data['kode_unit'];
								$kodeunitg = $data['kode_unit_general'];
							}
							if ($tingkat_unit!=1) {
								$namaunit = $pegawai['unit_kerja'];
							}													
						}
					}
				}					
				
		  		
		  ?>
				&nbsp;<?= $namaunit;?>
				
			</tr>
			<? } }
			else {
				echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
			} ?>
			<tr class="trhead">
					<td align="center"><input type="submit" value="Simpan" /></td>
					<td colspan="22" align="center">&nbsp;Total Jumlah Pegawai = <?=$jml_peg?>
					<? $jmltot = $jml_peg-$jml_sc; ?>
					&nbsp;&nbsp;&nbsp;&nbsp; ==> Jumlah Pegawai yang belum skrining= <?=$jmltot?> orang</td>
			</tr>
		</table>
	</form>
		<div class="paging">
		  <?=$page_links?>
		</div>
	</div>
</div>




<?php
	}
		if(is_array($centang)){
		echo '<b>centang yang anda pilih : <br />';
		/*tulis disini action simpannya cek jika tercentang lakukan 
		- update untuk validasi_data pada tabel pegawai 
		- add pada tabel riwayat ksehatan, sesuai data skrinning askes 
		*/
		foreach($centang as $indek=>$kd_peg){
		 
			//update validasi_data
			$query = mysql_query("select * from pegawai where kd_pegawai='".$kd_peg."'");					
			if ($query) {
				$datax=mysql_fetch_array($query); 
				$nama_pegawai = $datax['nama_pegawai'];
				//echo 'Data Skrinning askes '.$nama_pegawai.' sudah terproses!<br />';

				$query = mysql_query("UPDATE pegawai 
				SET validasi_data = '1' 
				WHERE kd_pegawai='".$kd_peg."'");	
				if ($query) {
					$query = mysql_query("SELECT max(id_riwayat_kes)+1 as idr from riwayat_kesehatan");		
					if ($query) 
						{
							$data=mysql_fetch_array($query);
							$id_riwayat_kes = $data['idr'];			
						}
				
					$query = mysql_query("SELECT * From riwayat_kesehatan 
					WHERE kd_pegawai='".$kd_peg."' and tahun='".date("Y")."' and keterangan='Skrinning Askes' ");	
					
					$row_count = mysql_num_rows($query);
					if ($row_count==0) {											
						/*$insertquery = mysql_query("
						INSERT INTO `dbsimpeg_um`.`riwayat_kesehatan` (
						`id_riwayat_kes` ,`kd_pegawai` ,`tahun` ,`keterangan` )
						VALUES ('".$id_riwayat_kes."', '".$kd_peg."', '".date("Y")."', 'Skrinning Askes')");*/
					}
					//set_success('Data Skrinning ' .$nama_pegawai.' berhasil disimpan');
					}
				else
					{
					//set_success('Data Skrinning' . $nama_pegawai.' gagal disimpan');
					}
				}
			}
		echo '<a href="'.site_url('laporan/pegawai_askes/browse').'">Kembali &raquo;</a>';
		}
	?>


<script type="text/javascript">
	function Check(){
	//Ambil semua elemen dalam id formCheck
	allCheckList = document.getElementById("formCheck").elements;
	//Hitung banyaknya elemen
	jumlahCheckList = allCheckList.length;
	//Jika tombolCheck bernilai "Check All"
	if(document.getElementById("tombolCheck").value == "Check All"){
	for(i = 0; i < jumlahCheckList; i++){
	//semua elemen ke-i checkbox nya diset true (dicentang)
	allCheckList[i].checked = true;
	}
	//Set nilai tombolCheck menjadi "Uncheck All"
	document.getElementById("tombolCheck").value = "Uncheck";
	//Jika tombolCheck tidak bernilai "Check All" (sudah dirubah menjadi Uncheck All)
	}else{
	for(i = 0; i < jumlahCheckList; i++){
	//semua elemen ke-i checkbox nya diset false (tidak dicentang)
	allCheckList[i].checked = false;
	}
	//Set nilai tombolCheck menjadi "Check All"
	document.getElementById("tombolCheck").value = "Check All";
	}
}
</script>
