<?
$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');

?>
<div class="content">
<div id="content-header">
	<div id="module-title" align="center"><h5><?=$judul?></h5></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('laporan/dukcpns_adm/printpdf/'.$tahun)?>" title="Cetak">
		<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
        <a href="<?=site_url('laporan/dukcpns_adm/printtoxls/'.$tahun.'/duk-cpns-adm'.$tahun.'.xls')?>"  title="Eksport ke Excel">
	<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
	<div id="content-data">
		<center>
		<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/dukcpns_adm/browse')?>" >
		<label>KEADAAN: 31 DESEMBER </label>
		  <?=form_dropdown('tahun',$tahun_assoc,$tahun);?>
		<!--<label>Nama Pegawai
		<input id="search" name="search" value="<?=set_value('search',$search);?>" />
		<label>Pangkat, Gol/ruang : </label><?=form_dropdown('group',$golongan_assoc,$group);?>-->
		
		<input type="submit" value=" Cari  ">
		</form>
		</center>
		<br/>
		
		<table class="table-list">
			<tr class="trhead" align="center">
			  <th width="22" rowspan="3" align="trhead" class="">No</th>
			  <th width="100" rowspan="3" class="colEvn" ><span class="colEvn" >Nama</span></th>
			  <th width="27" rowspan="3" class="trhead">NIP</th>
			  <th colspan="2" rowspan="2" class="trhead" >Pangkat</th>
			  <th colspan="2" rowspan="2" class="trhead">Jabatan</th>
			  <th colspan="4" class="trhead" align="center">Masa Kerja </th>
			  <th colspan="3" rowspan="2" class="trhead">Latihan Jabatan </th>
			  <th colspan="3" class="trhead">Pendidikan</th>
			  <th width="130" rowspan="3" class="trhead">Tempat/Tanggal lahir </th>
			  <th width="51" rowspan="3" class="trhead">Mutasi</th>
			  <th width="70" rowspan="3" class="trhead">Ket.</th>
			</tr>
			<tr class="trhead" align="center">
			  <th colspan="2" class="colEvn">Gol</th>
			  <th colspan="2" class="colEvn">Keseluruhan</th>
			  <th width="59" rowspan="2" class="colEvn">Nama</th>
			  <th width="38" rowspan="2" class="colEvn">THN Lulus </th>
			  <th width="56" rowspan="2" class="colEvn">Tingkat Ijazah </th>
			</tr>
			<tr class="trhead" align="center">
			  <th width="72" class="colEvn" >Gol/Ruang</th>
			  <th width="44" class="colEvn" >TMT</th>
			  <th width="40" class="trhead">Nama</th>
			  <th width="36" class="trhead">TMT</th>
			  <th width="34" class="colEvn">THN</th>
			  <th width="33" class="colEvn">BLN</th>
			  <th width="46" class="colEvn">THN</th>
			  <th width="35" class="colEvn">BLN</th>
			  <th width="49" class="trhead">Nama</th>
			  <th width="35" class="trhead">THN</th>
			  <th width="28" class="trhead">Jam</th>
			  </tr>
			
			<?
			$i = $start++;
			if ($kerja_list!=FALSE){
			foreach ($kerja_list as $jabatan) {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				
				if ($jabatan['tmt_golpangkat_terakhir']=='0000-00-00') {
					$str_tmt_golpangkat_terakhir = '-';
				}
				else {
					list($y, $m, $d) = explode('-', $jabatan['tmt_golpangkat_terakhir']);
					$str_tmt_golpangkat_terakhir = $d . "-" . $m . "-" . $y;
					   
				}
				$jabatan['tmt_golpangkat_terakhir'] =$str_tmt_golpangkat_terakhir;
				
				if ($jabatan['tmt_cpns']=='0000-00-00') {
					$str_tmt_cpns = '-';
				}
				else {
					list($y, $m, $d) = explode('-', $jabatan['tmt_cpns']);
					$str_tmt_cpns = $d . "-" . $m . "-" . $y;
					   
				}
				$jabatan['tmt_cpns']=$str_tmt_cpns;
				
				if ($jabatan['tmt_jabatan_terakhir']=='0000-00-00') {
					$str_tmt_jabatan = '-';
				}
				else {
					list($y, $m, $d) = explode('-', $jabatan['tmt_jabatan_terakhir']);
					$str_tmt_jabatan = $d . "-" . $m . "-" . $y;
					   
				}
				$jabatan['tmt_jabatan_terakhir']=$str_tmt_jabatan;
				
				list($y, $m, $d) = explode('-', $jabatan['tgl_lahir']);
				$str_tgl_lahir = $d . "-" . $m . "-" . $y;
				$jabatan['tgl_lahir']=$str_tgl_lahir;
			   
				if ($jabatan['nama_jabatan']=='Fungsional Umum')
				{
					$jabatan['nama_jabatan']='-';
				}
			?>   
			<tr class="<?= $class; ?>">
			  <td rowspan="2" align="center" class="colEvn"><?= $i; ?></td>
			  <td rowspan="2" align="left" class="trhead" >
				<?= $jabatan['gelar_depan']." ".$jabatan['nama_pegawai']." ".$jabatan['gelar_belakang']; ?>		 </td>
			  <td rowspan="2" align="left" class="trhead" ><?=$jabatan['NIP']?></td>
			  <td rowspan="2" align="center" class="colEvn" ><?=$jabatan['nama_golongan']?></td>
			  <td rowspan="2" align="left" class="colEvn" ><span class="trhead">
				<?=$jabatan['tmt_golpangkat_terakhir']?>
			  </span></td>
			  <td align="center" class="trhead" ><?=$jabatan['nama_jabatan']?></td>
			  <td rowspan="2" align="center" class="trhead" ><?=$jabatan['tmt_jabatan_terakhir']?></td>
			  <?php
				$mkk_th = $jabatan['mk_tahun'] + $jabatan['mk_tambahan_th'];
				$mkk_bl = $jabatan['mk_bulan'] + $jabatan['mk_tambahan_bl'];
				$mkg_th = $mkk_th + $jabatan['mk_selisih_th'];
				$mkg_bl = $mkk_bl + $jabatan['mk_selisih_bl'];
			  ?>
			  <td rowspan="2" align="center" class="colEvn" ><?= $mkg_th?></td>
			  <td rowspan="2" align="center" class="colEvn" ><?= $mkg_bl?></td>
			  <td rowspan="2" align="center" class="colEvn" ><?= $mkk_th?></td>
			  <td rowspan="2" align="center" class="colEvn" ><?= $mkk_bl?></td>
			  <?php 
			  
			  $query = mysql_query("SELECT nama_pelatihan FROM riwayat_pelatihan
									WHERE kd_pegawai='".$jabatan['kd_pegawai']."'");
			  $data = mysql_fetch_array($query);
			  $nama_pelatihan = $data['nama_pelatihan'];
			 
			  list($txt_nama, $txt_tahun) = array_map('trim', explode(",", $nama_pelatihan, 2));
			  unset($tahun);
			  preg_match('/([\d]+)/', $txt_tahun, $tahun);
	
			
			  ?>
			  <td rowspan="2" align="left" class="trhead" ><?= $txt_nama?></td>
			  <td rowspan="2" align="center" class="trhead" ><?= $tahun[1]?></td>
			  <td rowspan="2" align="center" class="trhead" >&nbsp;</td>
			  <?php 
			  $query = mysql_query("SELECT a.keterangan, a.th_lulus, b.nama_pendidikan FROM `riwayat_pendidikan` a, pendidikan b 
									WHERE a.id_pendidikan = b.id_pendidikan AND a.aktif = '1' AND kd_pegawai='".$jabatan['kd_pegawai']."'
									");
			  
			  $data = mysql_fetch_array($query);
			  $keterangan = $data['keterangan'];
			  $nama_pendidikan = $data['nama_pendidikan'];
			  $th_lulus = $data['th_lulus'];
			  ?>
			  <td rowspan="2" align="left" class="colEvn" ><?= $keterangan;?></td>
			  <td rowspan="2" align="left" class="colEvn" ><?= $th_lulus;?></td>
			  <td rowspan="2" align="left" class="colEvn" ><?= $nama_pendidikan;?></td>
			   <?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$jabatan['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
					?>
			  <td rowspan="2" align="center" class="trhead" ><?=$tempat_lahir?>
			  , <?=$jabatan['tgl_lahir']?></td>
			  <td rowspan="2" align="left" class="colEvn" >&nbsp;</td>
			  <?php 
				$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
									where  u.kode_unit='".$jabatan['kode_unit']."'");					
				if ($query) 
					{
						$data=mysql_fetch_array($query);  
						$tingkat_unit = $data['tingkat'];
						$namaunit = $data['nama_unit'];
						$kodeunit = $data['kode_unit'];
						$kodeunitg = $data['kode_unit_general'];
					}
					if ($tingkat_unit!=1) {
						$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
											where  u.kode_unit'".$kodeunitg."'");					
						if ($query) {
							$data=mysql_fetch_array($query);  
							$tingkat_unit = $data['tingkat'];
							$namaunit = $data['nama_unit'];
							$kodeunit = $data['kode_unit'];
							$kodeunitg = $data['kode_unit_general'];
						}
						if ($tingkat_unit!=1) {
							$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
												where  u.kode_unit='".$kodeunitg."'");					
							if ($query) {
								$data=mysql_fetch_array($query);  
								$tingkat_unit = $data['tingkat'];
								$namaunit = $data['nama_unit'];
								$kodeunit = $data['kode_unit'];
								$kodeunitg = $data['kode_unit_general'];
							}
							if ($tingkat_unit!=1) {
								$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
													where  u.kode_unit='".$kodeunitg."'");					
								if ($query) {
									$data=mysql_fetch_array($query);  
									$tingkat_unit = $data['tingkat'];
									$namaunit = $data['nama_unit'];
									$kodeunit = $data['kode_unit'];
									$kodeunitg = $data['kode_unit_general'];
								}
								if ($tingkat_unit!=1) {
									$namaunit = $jabatan['unit_kerja'];
								}													
							}
						}
					}					
					
					
			  ?>
			  <td rowspan="2" align="left" class="trhead" ><?= $namaunit;?></td> <!--.'-'.$jabatan['unit_kerja']-->
			  </tr>
			<tr class="<?= $class; ?>">
			  <?php
				if ($jabatan['eselon']=='non-eselon'){
					$unit_kerja='';
				}
				else{
					$unit_kerja=$jabatan['unit_kerja'];
				}
				
			  ?>
			  <td align="left" class="trhead" ><?=$unit_kerja?></td>
			  </tr>
			
			<? } }
			else {
				echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
			} ?>
		</table>
		<br/>
		<div class="paging"><?=$page_links?></div>
	</div>
</div>
