<!--<div class ="content">
<div class="bodyminimal">
--><font size="-3">
<table border="0" align="center">
<tr>
	<td colspan="20" align="center"><h3>DAFTAR URUT KEPANGKATAN (DUK) CALON PEGAWAI NEGERI SIPIL</h3></td>
</tr>
<tr>
	<td colspan="20" align="center"><h3>TENAGA ADMINISTRASI UNIVERSITAS NEGERI MALANG</h3></td>
</tr>
<tr>
	<td colspan="20" align="center"><h3>KEADAAN : 31 DESEMBER <?= $tahun;?></h3></td>
</tr>
</table>
<table class="table-list" cellpadding="4" border="1">
<tr class="trhead" align="center">
	<th width="16" rowspan="3" align="center" class="colEvn">No</th>
	<th width="60" rowspan="3" class="trhead" ><span class="trhead" >Nama</span></th>
	<th width="83" rowspan="3" class="trhead" >NIP</th>
	<th width="80" colspan="2" rowspan="2" class="colEvn" >Pangkat</th>
	<th width="100" colspan="2" rowspan="2" class="trhead">Jabatan</th>
	
	<th width="100" colspan="4" class="colEvn" align="center">Masa Kerja </th>
	<th width="100" colspan="3" rowspan="2" class="trhead">Latihan Jabatan </th>
	<th width="105" colspan="3" class="colEvn">Pendidikan</th>
	<th width="60" rowspan="3" class="trhead">Tempat/ <br /> 
	  Tanggal lahir </th>
	<th width="45" rowspan="3" class="colEvn">Mutasi</th>
	<th width="45" rowspan="3" class="trhead">Ket.</th>
</tr>
<tr class="trhead" align="center">
  <th width="50" colspan="2" class="colEvn">Gol</th>
  <th width="50" colspan="2" class="colEvn">Keseluruhan</th>
  
  <th width="50" rowspan="2" class="colEvn">Nama</th>
  <th width="25" rowspan="2" class="colEvn">THN<br />Lulus </th>
  <th width="30" rowspan="2" class="colEvn">Tingkat<br />Ijazah </th>
</tr>
<tr class="trhead" align="center">
		  <th width="35" class="colEvn" >Gol<br />
	    /Ruang</th>
		  <th width="45" class="colEvn" >TMT</th>
		  
		  <th width="50" class="trhead">Nama</th>
		  <th width="50" class="trhead">TMT</th>
		  
		  <th width="25" class="colEvn">THN</th>
		  <th width="25" class="colEvn">BLN</th>
		  
		  <th width="25" class="colEvn">THN</th>
		  
		  <th width="25" class="colEvn">BLN</th>
		  
		  <th width="50" class="trhead">Nama</th>
		  <th width="25" class="trhead">THN</th>
		  <th width="25" class="trhead">Jam</th>
  </tr>
  
  <?
		$i = $start++;
		if ($kerja_list!=FALSE){
		foreach ($kerja_list as $jabatan) {
			$i++;
			if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
			
            if ($jabatan['tmt_golpangkat_terakhir']=='0000-00-00') {
                $str_tmt_golpangkat_terakhir = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jabatan['tmt_golpangkat_terakhir']);
                $str_tmt_golpangkat_terakhir = $d . "-" . $m . "-" . $y;
                   
            }
            $jabatan['tmt_golpangkat_terakhir'] =$str_tmt_golpangkat_terakhir;
            
			if ($jabatan['tmt_cpns']=='0000-00-00') {
                $str_tmt_cpns = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jabatan['tmt_cpns']);
                $str_tmt_cpns = $d . "-" . $m . "-" . $y;
                   
            }
            $jabatan['tmt_cpns']=$str_tmt_cpns;
			
			if ($jabatan['tmt_jabatan_terakhir']=='0000-00-00') {
                $str_tmt_jabatan = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jabatan['tmt_jabatan_terakhir']);
                $str_tmt_jabatan = $d . "-" . $m . "-" . $y;
                   
            }
            $jabatan['tmt_jabatan_terakhir']=$str_tmt_jabatan;
			
			list($y, $m, $d) = explode('-', $jabatan['tgl_lahir']);
            $str_tgl_lahir = $d . "-" . $m . "-" . $y;
            $jabatan['tgl_lahir']=$str_tgl_lahir;
           
            if ($jabatan['nama_jabatan']=='Fungsional Umum')
			{
				$jabatan['nama_jabatan']='';
			}
			if ($jabatan['nama_jabatan']=='')
			{
				$jabatan['unit_kerja']='';
			}
		?>   
		
<tr class="<?= $class; ?>">
		  <td width="16" align="center" class="colEvn"><?= $i; ?></td>
		  <td width="60" align="left" class="trhead" >
		    <?= $jabatan['gelar_depan']." ".$jabatan['nama_pegawai']." ".$jabatan['gelar_belakang']; ?>		 </td>
		  <td width="83" align="left" class="trhead" >&nbsp;<?=$jabatan['NIP']?></td>
		  <td width="35" align="center" class="colEvn" ><?=$jabatan['nama_golongan']?></td>
		  <td width="45" align="left" class="colEvn" ><span class="trhead">
		    <?=$jabatan['tmt_golpangkat_terakhir']?>
		  </span></td>
		  <td  width="50" align="center" class="trhead" >	-    </td>
          <td width="50" align="center" class="trhead" >-</td>
		  <?php
		  	$mkk_th = $jabatan['mk_tahun'] + $jabatan['mk_tambahan_th'];
			$mkk_bl = $jabatan['mk_bulan'] + $jabatan['mk_tambahan_bl'];
			$mkg_th = $mkk_th + $jabatan['mk_selisih_th'];
			$mkg_bl = $mkk_bl + $jabatan['mk_selisih_bl'];
		  ?>
          <td width="25" align="center" class="colEvn" ><?= $mkg_th?></td>
		  <td width="25" align="center" class="colEvn" ><?= $mkg_bl?></td>
		  <td width="25" align="center" class="colEvn" ><?= $mkk_th?></td>
		  <td width="25" align="center" class="colEvn" ><?= $mkk_bl?></td>
		  <?php 
		  
		  $query = mysql_query("SELECT nama_pelatihan FROM riwayat_pelatihan
		  						WHERE kd_pegawai='".$jabatan['kd_pegawai']."'");
		  $data = mysql_fetch_array($query);
		  $nama_pelatihan = $data['nama_pelatihan'];
		 
		  list($txt_nama, $txt_tahun) = array_map('trim', explode(",", $nama_pelatihan, 2));
		  unset($tahun);
		  preg_match('/([\d]+)/', $txt_tahun, $tahun);

		
		  ?>
		  <td width="50" align="center" class="trhead" >-</td>
		  <td width="25" align="center" class="trhead" >-</td>
		  <td width="25" align="center" class="trhead" >-</td>
		  <?php 
		  $query = mysql_query("SELECT a.keterangan, a.th_lulus, b.nama_pendidikan FROM `riwayat_pendidikan` a, pendidikan b 
		  						WHERE a.id_pendidikan = b.id_pendidikan AND a.aktif = '1' AND kd_pegawai='".$jabatan['kd_pegawai']."'");
		  
		  $data = mysql_fetch_array($query);
		  $keterangan = $data['keterangan'];
		  $nama_pendidikan = $data['nama_pendidikan'];
		  $th_lulus = $data['th_lulus'];
		  ?>
		  <td  width="50" align="left" class="colEvn" ><?= $keterangan;?></td>
		  <td width="25" align="left" class="colEvn" ><?= $th_lulus;?></td>
		  <td  width="30" align="left" class="colEvn" ><?= $nama_pendidikan;?></td>
		   <?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$jabatan['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
					?>
		  <td width="60" align="center" class="trhead" ><?=$tempat_lahir?>, <?=$jabatan['tgl_lahir']?></td>
		  <td width="45" align="left" class="colEvn" >&nbsp;</td>
		  <?php 
			$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
								where  u.kode_unit='".$jabatan['kode_unit']."'");					
			if ($query) 
				{
					$data=mysql_fetch_array($query);  
					$tingkat_unit = $data['tingkat'];
					$namaunit = $data['nama_unit'];
					$kodeunit = $data['kode_unit'];
					$kodeunitg = $data['kode_unit_general'];
				}
				if ($tingkat_unit!=1) {
					$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
										where  u.kode_unit'".$kodeunitg."'");					
					if ($query) {
						$data=mysql_fetch_array($query);  
						$tingkat_unit = $data['tingkat'];
						$namaunit = $data['nama_unit'];
						$kodeunit = $data['kode_unit'];
						$kodeunitg = $data['kode_unit_general'];
					}
					if ($tingkat_unit!=1) {
						$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
											where  u.kode_unit='".$kodeunitg."'");					
						if ($query) {
							$data=mysql_fetch_array($query);  
							$tingkat_unit = $data['tingkat'];
							$namaunit = $data['nama_unit'];
							$kodeunit = $data['kode_unit'];
							$kodeunitg = $data['kode_unit_general'];
						}
						if ($tingkat_unit!=1) {
							$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
												where  u.kode_unit='".$kodeunitg."'");					
							if ($query) {
								$data=mysql_fetch_array($query);  
								$tingkat_unit = $data['tingkat'];
								$namaunit = $data['nama_unit'];
								$kodeunit = $data['kode_unit'];
								$kodeunitg = $data['kode_unit_general'];
							}
							if ($tingkat_unit!=1) {
								$namaunit = $jabatan['unit_kerja'];
							}													
						}
					}
				}					
				
		  		
		  ?>
		  <td width="45" align="left" class="trhead" ><?= $namaunit;?></td> <!--.'-'.$jabatan['unit_kerja']-->
  </tr>
		
		<? } }
		else {
			echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
		} ?>
  
</table>
</font>
</div>
</div>
