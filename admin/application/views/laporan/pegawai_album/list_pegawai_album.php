<?php
$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');

function bulan($bulan)
{
Switch ($bulan){
    case 1 : $bulan="Januari";
        Break;
    case 2 : $bulan="Februari";
        Break;
    case 3 : $bulan="Maret";
        Break;
    case 4 : $bulan="April";
        Break;
    case 5 : $bulan="Mei";
        Break;
    case 6 : $bulan="Juni";
        Break;
    case 7 : $bulan="Juli";
        Break;
    case 8 : $bulan="Agustus";
        Break;
    case 9 : $bulan="September";
        Break;
    case 10 : $bulan="Oktober";
        Break;
    case 11 : $bulan="November";
        Break;
    case 12 : $bulan="Desember";
        Break;
    }
return $bulan;
}
?>

<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul;?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/pegawai_album/printtopdf/'.$group.'/'.$id_golpangkat.'/'.$id_jabatan.'/'.$id_jabatan_f)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/pegawai_album/printtoxls/'.$group.'/'.$id_golpangkat.'/'.$id_jabatan.'/'.$id_jabatan_f.'/buku_pegawai.xls')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	
	<div id="content-data" align="left">
		<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/pegawai_album/browse')?>" >
				<label>NIP/Nama Pegawai : </label>
				<input type="text" name="nama_search" size="25" value="<?=$nama_seacrh;?>">
				&nbsp;&nbsp;&nbsp;<label>Jenis Pegawai : </label><?=form_dropdown('group',$adm_akd_assoc,$group);?><br />
				<label>Jabatan Fungsional : </label><?=form_dropdown('id_jabatan_f',$jabatan_assoc, $id_jabatan_f);?>
				&nbsp;&nbsp;&nbsp;<label>Jabatan Struktural : </label><?=form_dropdown('id_jabatan',$jabatan_struktural_assoc, $id_jabatan);?><br />
				<label>Unit Kerja : </label><?=form_group_dropdown('unit_kerja',$option_unit,$unit_kerja);?>&nbsp;&nbsp;&nbsp;<label>Golongan : </label><?=form_dropdown('id_golpangkat',$golongan_assoc, $id_golpangkat);?>
				<input type="submit" value="Filter">
			</form>
		</center>
		<br/>
		
		<?
			//echo "<h3>Jenis Pegawai : ".$adm_akd_assoc[$group]." Golongan : ".$golongan_assoc[$id_golpangkat]."Jabatan :".$jabatan."Unit Kerja :".$unit."</h3>";
			$id_golpangkat = $_POST['id_golpangkat'];
			if ($id_golpangkat == 0)
			{
				$golongan = "-";
			}
			else
			{
				
				$golongan = $golongan_assoc['$id_golpangkat'];
			}
			
			if ($id_jabatan == 0)
			{
				$jabatan = "-";
			}
			else
			{
				$jabatan = $jabatan_struktural_assoc['$id_jabatan'];
			}
			
			
			if ($unit_kerja == 0)
			{
				$unit = "-";
			}
			else
			{
				$unit = $unit_kerja_assoc['$unit_kerja'];
			}
		?>
		<table class="table-list">
			<tr class="trhead">
				<th class="trhead" width="20" align="center" valign="top">No</th>
				<th class="trhead" width="20" align="center" valign="top">Photo</th>
				<th class="trhead" width="1500" align="center" >Keterangan Pegawai</th>
			</tr>
			<?
			$i = $start++;
			if ($kerja_list!=FALSE){
			foreach ($kerja_list as $pegawai) {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				list($y, $m, $d) = explode('-', $pegawai['tgl_resign']);
				$str_tgl_resign = $d . "/" . $m . "/" . $y;
				$pegawai['tgl_resign']=$str_tgl_resign;
				
				list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
				$str_tgl_lahir = $d . "/" . $m . "/" . $y;
				$pegawai['tgl_lahir']=$str_tgl_lahir;
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_cpns']);
				$str_m = bulan($m);
				$str_tmt_cpns = $d . " " . $str_m . " " . $y;
				
				$pegawai['tmt_cpns']=$str_tmt_cpns;
				if ($pegawai['tmt_cpns']=='00/00/0000')
				{
					$pegawai['tmt_cpns']='-';
				}
				list($y, $m, $d) = explode('-', $pegawai['tmt_golpangkat_terakhir']);
				$str_tmt_golpangkat_terakhir = $d . "/" . $m . "/" . $y;
				$pegawai['tmt_golpangkat_terakhir']=$str_tmt_golpangkat_terakhir;
				if ($pegawai['tmt_golpangkat_terakhir']=='00/00/0000')
				{
					$pegawai['tmt_golpangkat_terakhir']='-';
				}
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_kgb']);
				$str_tmt_kgb = $d . "/" . $m . "/" . $y;
				$pegawai['tmt_kgb']=$str_tmt_kgb;
				if ($pegawai['tmt_kgb']=='00/00/0000')
				{
					$pegawai['tmt_kgb']='-';
				}
				
				if ($pegawai['eselon']=='non-eselon')
				{
					$pegawai['id_jabatan_struktural']='';
				}
			if ($pegawai['kd_agama'] == 0)
			{
				$agama = "-";
			}
			else
			{
				$agama = $agama_assoc[$pegawai['kd_agama']];
			}				
                        if ($pegawai['jns_kelamin'] == 1)
			{
				$jns_kelamin = "L";
			}
			else
			{
				$jns_kelamin = "P";
			}
			?>   
			<tr class="<?= $class; ?>">
				<td align="center" class="colEvn" valign="baseline"><?= $i; ?>.</td>
				<td width="100" align="center" class="colEvn" valign="top">
				<?php
				$expected_file = PUBLICPATH . 'photo/photo_' . $pegawai['kd_pegawai'] . '.jpg';
						if (file_exists($expected_file)){
							$photo = base_url() . '/public/photo/photo_' . $pegawai['kd_pegawai'] . '.jpg';
						}
						else{
							$expected_file = PUBLICPATH . 'photo/photo_' . $pegawai['kd_pegawai'] . '.gif';
							if (file_exists($expected_file)){
								$photo = base_url() . '/public/photo/photo_' . $pegawai['kd_pegawai'] . '.gif';
								}
							else{
								$expected_file = PUBLICPATH . 'photo/photo_' . $pegawai['kd_pegawai'] . '.bmp';
								if (file_exists($expected_file)){
										$photo = base_url() . '/public/photo/photo_' . $pegawai['kd_pegawai'] . '.bmp';
									}
									else{	
										$photo = base_url() . '/public/images/image/nophotos.jpg';
								}
							}
						}
				?>
				<img height="240" width="160" src="<?php echo $photo; ?>" />
				</td>
				<td  align="left" >
					<table>
						<tr valign="top">
						<td align="left" class="colEvn">Nama</td><td width="4">:</td>
						<td class="colEvn"><a href="<?=site_url("pegawai/pegawai/view/".$pegawai['kd_pegawai']); ?>" title="Detil Data Pegawai" ><?=$pegawai['gelar_depan']." ".$pegawai['nama_pegawai']." ".$pegawai{'gelar_belakang'} ?> </a></td>
						</tr>
						<tr>
							<td align="left" class="colEvn">NIP</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$pegawai['NIP']?></td>
						</tr>
						<tr>
							<td align="left" class="colEvn">Pangkat/Gol</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$pangkat_assoc[$pegawai['id_golpangkat_terakhir']]?>, <?=$golongan_assoc[$pegawai['id_golpangkat_terakhir']]?></td>
						</tr>
						<tr>
							<td align="left" class="colEvn">Agama</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$agama?></td>
						</tr>
						<tr>
							<td align="left" class="colEvn">TMT CPNS</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$str_tmt_cpns?></td>
						</tr>
						<tr>
						<?php 
					$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general, u.GD FROM unit_kerja  u 
										where  u.kode_unit='".$pegawai['kode_unit']."'");					
					if ($query) 
						{
							$data=mysql_fetch_array($query);  
							$tingkat_unit = $data['tingkat'];
							$namaunit = $data['nama_unit'];
							$kodeunit = $data['kode_unit'];
							$kodeunitg = $data['kode_unit_general'];
						}
						if ($tingkat_unit==1 and $data['GD'] != 'D') {
							$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general, u.GD FROM unit_kerja  u 
												where  u.kode_unit='".$kodeunitg."'");					
							if ($query) {
								$data=mysql_fetch_array($query);  
								$tingkat_unit = $data['tingkat'];
								$namaunit = $data['nama_unit'];
								$kodeunit = $data['kode_unit'];
								$kodeunitg = $data['kode_unit_general'];
							}
							if ($tingkat_unit==1 and $data['GD'] != 'D' ) {
								$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general, u.GD FROM unit_kerja  u 
													where  u.kode_unit='".$kodeunitg."'");					
								if ($query) {
									$data=mysql_fetch_array($query);  
									$tingkat_unit = $data['tingkat'];
									$namaunit = $data['nama_unit'];
									$kodeunit = $data['kode_unit'];
									$kodeunitg = $data['kode_unit_general'];
								}
								if ($tingkat_unit==1 and $data['GD'] != 'D') {
									$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general, u.GD FROM unit_kerja  u 
														where  u.kode_unit='".$kodeunitg."'");					
									if ($query) {
										$data=mysql_fetch_array($query);  
										$tingkat_unit = $data['tingkat'];
										$namaunit = $data['nama_unit'];
										$kodeunit = $data['kode_unit'];
										$kodeunitg = $data['kode_unit_general'];
									}
									if ($tingkat_unit==1 and $data['GD'] != 'D') {
										$namaunit = $pegawai['unit_kerja'];
									}													
								}
							}
						}					
						
						
				  ?>
							<td align="left" class="colEvn">Jabatan</td><td width="4">:</td>
							<td align="left" class="colEvn"><?php 
							//$jabatan=$jabatan_struktural_assoc[$pegawai['id_jabatan_struktural']] ;
							
							
$query = mysql_query("SELECT
concat('`',pegawai.NIP) AS NIP,
CONCAT(pegawai.gelar_depan,pegawai.nama_pegawai,pegawai.gelar_belakang) AS nama_gelar,
jabatan_struktural.nama_jabatan_s,
jenis_pegawai.jenis_pegawai
FROM
pegawai
INNER JOIN jabatan_struktural ON pegawai.id_jabatan_struktural = jabatan_struktural.id_jabatan_s
INNER JOIN jenis_pegawai ON pegawai.id_jns_pegawai = jenis_pegawai.id_jns_pegawai
where pegawai.kd_pegawai='".$pegawai['kd_pegawai']."'");					
							if ($query) {
								$data=mysql_fetch_array($query);  
								$jabatan = $data['nama_jabatan_s'];
								$jenisp = $data['jenis_pegawai'];
							}




							if ($jabatan=="")
							{
								//echo "Staf ".$unitkerja_assoc[$pegawai['kode_unit']];
								echo $jenis_pegawai_assoc[$pegawai['id_jns_pegawai']]." ".$unitkerja_assoc[$pegawai['kode_unit']];




							}
							else {
							if ($namaunit=="")
								{
								echo $jabatan." ".$unitkerja_assoc[$pegawai['kode_unit']];
								}
							else 
								{
								echo $jabatan." ".$namaunit;
								}
							}
							?></td>
						</tr>
						<tr>
							<td align="left" class="colEvn">Alamat Rumah</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$pegawai['alamat']?></td>
						</tr>
						<tr>
							<td align="left" class="colEvn">email</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$pegawai['email']?></td>
						</tr>
						
						<!--<tr>
							<td align="left" class="colEvn">Tgl Lahir</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$pegawai['tgl_lahir']?></td>
						</tr>
						<tr>
							<td align="left" class="colEvn">L/P</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$jns_kelamin?></td>
						</tr>
						
						
						<tr>
							<td align="left" class="colEvn">TMT Pangkat</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$pegawai['tmt_golpangkat_terakhir']?></td>
						</tr>
						<tr>
							<td align="left" class="colEvn">Subag</td><td width="4">:</td>
							<td align="left" class="colEvn"><?=$unitkerja_assoc[$pegawai['kode_unit']]?></td>
						</tr>
						<tr>
							
				  			<td align="left" class="colEvn">Unit</td><td width="4">:</td>
							<td align="left" class="colEvn"><?= $namaunit;?></td>
						</tr>-->
					</table>
				</td>
				
				
				
				
				
			</tr>
				

			
		  
		
		
			<? } }
			else {
				echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
			} ?>
		</table>
		<div class="paging">
		  <?=$page_links?>
		</div>
	</div>
</div>

