<?
$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');
?>

<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul;?></h3></div>
		<div class="clear"></div>
	</div>
	
	<div id="content-data" align="left">
		<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/pegawai_album/browse')?>" >
				<label>NIP/Nama Pegawai : </label>
				<input type="text" name="nama_search" size="25" value="<?=$nama_seacrh;?>">
				&nbsp;&nbsp;&nbsp;<label>Jenis Pegawai : </label><?=form_dropdown('group',$adm_akd_assoc,$group);?><br />
				<label>Jabatan Fungsional : </label><?=form_dropdown('id_jabatan_f',$jabatan_assoc, $id_jabatan_f);?>
				&nbsp;&nbsp;&nbsp;<label>Jabatan Struktural : </label><?=form_dropdown('id_jabatan',$jabatan_struktural_assoc, $id_jabatan);?><br />
				<label>Unit Kerja : </label><?=form_group_dropdown('unit_kerja',$option_unit,$unit_kerja);?>&nbsp;&nbsp;&nbsp;<label>Golongan : </label><?=form_dropdown('id_golpangkat',$golongan_assoc, $id_golpangkat);?>
				<input type="submit" value="Filter">
			</form>
		</center>
		<br/>
		
		<?
			//echo "<h3>Jenis Pegawai : ".$adm_akd_assoc[$group]." Golongan : ".$golongan_assoc[$id_golpangkat]."Jabatan :".$jabatan."Unit Kerja :".$unit."</h3>";
			$id_golpangkat = $_POST['id_golpangkat'];
			if ($id_golpangkat == 0)
			{
				$golongan = "-";
			}
			else
			{
				
				$golongan = $golongan_assoc['$id_golpangkat'];
			}
			
			if ($id_jabatan == 0)
			{
				$jabatan = "-";
			}
			else
			{
				$jabatan = $jabatan_struktural_assoc['$id_jabatan'];
			}
			
			
			if ($unit_kerja == 0)
			{
				$unit = "-";
			}
			else
			{
				$unit = $unit_kerja_assoc['$unit_kerja'];
			}
		?>
		
		<table class="table-list">
			
			<?
			$i = $start++;
			if ($kerja_list!=FALSE){
			foreach ($kerja_list as $pegawai) {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				list($y, $m, $d) = explode('-', $pegawai['tgl_resign']);
				$str_tgl_resign = $d . "/" . $m . "/" . $y;
				$pegawai['tgl_resign']=$str_tgl_resign;
				
				list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
				$str_tgl_lahir = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tgl_lahir']=$str_tgl_lahir;
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_cpns']);
				$str_tmt_cpns = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tmt_cpns']=$str_tmt_cpns;
				if ($pegawai['tmt_cpns']=='00/00/0000')
				{
					$pegawai['tmt_cpns']='-';
				}
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_kgb']);
				$str_tmt_kgb = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tmt_kgb']=$str_tmt_kgb;
				if ($pegawai['tmt_kgb']=='00/00/0000')
				{
					$pegawai['tmt_kgb']='-';
				}
				
				if ($pegawai['eselon']=='non-eselon')
				{
					$pegawai['id_jabatan_struktural']='';
				}
				
			?>   
			<tr class="<?= $class; ?>">
				<td class="trhead" width="700" align="left">
				<?=$pegawai['nama_pegawai'].", ".$pegawai['gelar_depan']."".$pegawai{'gelar_belakang'}?>
				<?php 
					if ($pegawai['id_jns_pegawai']=='5'){
				?>	
				<?=", (".$jabatan_assoc[$pegawai['id_jabatan_terakhir']].", ".$golongan_assoc[$pegawai['id_golpangkat_terakhir']].")" ?>
				<? } else {?>
				<?=", Gol.".$golongan_assoc[$pegawai['id_golpangkat_terakhir']].")" ?>
				<? } ?>
				<?=", NIP.".$pegawai['NIP']?>
				<!--tampilkan pendidikan dosen-->
				<?php 
					if ($pegawai['id_jns_pegawai']=='5'){
						$ss="select * from riwayat_pendidikan where kd_pegawai='".$pegawai['kd_pegawai']."' AND id_pendidikan > 8 
						order by id_pendidikan, th_lulus";
	//					echo $ss;					
						$query2=mysql_query("select * from riwayat_pendidikan where kd_pegawai='".$pegawai['kd_pegawai']."' AND id_pendidikan > 8 
						order by id_pendidikan, th_lulus");		
						while ($list_pendidikan=mysql_fetch_array($query2)) {
				?>
				<?= $list_pendidikan['prodi'].", ".$list_pendidikan['universitas']." (".$list_pendidikan['th_lulus'].")" ?>
				<? }}?>
				<?=", ".$pegawai['alamat'];?>
				<?php 
					if ($pegawai['RT']!='')
						echo  " RT ".$pegawai['RT'];
					if ($pegawai['RW']!='')
						echo  " RW ".$pegawai['RW'];
					if ($pegawai['kd_kelurahan']!='0')
						echo  $kelurahan_assoc[$pegawai['kd_kelurahan']]." Malang";
					if ($pegawai['no_telp']!='')	
						echo  ", ".$pegawai['no_telp'];
					if ($pegawai['hp']!='')
						echo  ", ".$pegawai['hp'];
					if (ltrim($pegawai['email'])!='')
						echo  ", <i>e-mail</i> :".$pegawai['email'];

				?>
				</td>
			</tr>
			<? } } ?>
		
	</div>
</div>

