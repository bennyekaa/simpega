<?
$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');
?>
<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul?></h3></div>
			<div id="module-menu">
				<a class="icon" href="<?=site_url('laporan/naikpangkat/printpdf/'.$group.'/'.$tahun)?>" title="Cetak">
				<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
				<a href="<?=site_url('laporan/naikpangkat/printtoxls/'.$group.'/'.$tahun)?>"  title="Eksport ke Excel">
			<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
			</div>
		<div class="clear"></div>
	</div>
<br/>
	<div id="content-data">
		<center>
			<form name="usersdetail" id="usersdetail" method="POST" action="<?=site_url('laporan/naikpangkat/browse')?>" >
			<label>Bulan: </label><select name="group">
			<option value="" <?php if($group == ''){echo "selected"; }?>>-- Pilih Periode KP --</option>
			<option value="04" <?php if($group == '04'){echo "selected"; }?>>April</option>
			<option value="10" <?php if($group == '10'){echo "selected"; }?>>Oktober</option>
			</select>
			<label> Tahun: </label><?=form_dropdown('tahun',$tahun_assoc,$tahun);?>
			<!--<label>Jenis Pegawai: </label>
					<?php echo form_dropdown('id_jns_pegawai', $get_jenis_pegawai_all, $id_jns_pegawai); ?>-->
			
			<input type="submit" value=" Cari  ">
			</form>
		</center>
		<br/>
		<?php
			$centang = $_POST['centang'];
			if(empty($centang)){
		?>		
		<form id="formCheck" method="POST" action="<?=site_url('laporan/naikpangkat/browse')?>" >
			<table class="table-list">
				<tr class="trhead" align="center">
					<th width="68" rowspan="2" ALIGN='CENTER' >
					<input type="button" id="tombolCheck" value="Check All" onClick="Check();"/>
				  <!--<input type="checkbox" class="check_all"/>--></th>
					<th width="29" rowspan="2" align="center" class="colEvn">No</th>
					<th width="80" rowspan="2" class="trhead" >Nama</th>
					<th width="129" rowspan="2" class="colEvn" >NIP</th>
					<th width="129" rowspan="2" class="colEvn" >Karpeg</th>
					<th width="129" rowspan="2" class="colEvn" >Pendidikan Terakhir</th>
					<th width="129" rowspan="2" class="colEvn" >Tahun Lulus </th>
					<th width="129" rowspan="2" class="colEvn" >Tempat Lahir, Tanggal Lahir </th>
					
					<th colspan="4" align="center" class="trhead">Lama</th>
					<th colspan="4" class="trhead"><span class="colEvn">Baru</span></th>
					<th colspan="3" class="trhead"><span class="colEvn">Atasan Langsung</span></th>
					<!--<th width="113" class="trhead"><span class="colEvn">Masa Kerja</span></th>-->
					<th width="95" rowspan="2" class="trhead">Masa Kerja Lama </th>
					<th width="120" rowspan="2" class="trhead">Masa Kerja Baru </th>
					
					<th width="84" rowspan="2" class="trhead">Cetak Model D </th>
			  </tr>
				<tr class="<?= $class; ?>">
				  <th width="64" class="trhead">Pangkat/ Gol. Ruang/ TMT </th>
				  <th width="64" class="trhead">Masa Kerja Gol.</th>
					<th width="64" class="trhead">Gaji Pokok </th>
					<th width="75" class="trhead">Jabatan/ TMT/ Jumlah AK </th>
					<th width="64" class="trhead">Pangkat/ Gol. Ruang/ TMT </th>
				  <th width="64" class="trhead">Masa Kerja Gol.</th>
					<th width="64" class="trhead">Gaji Pokok </th>
					<th width="75" class="trhead">Jabatan/ TMT/ Jumlah AK </th>
					<th width="64" class="trhead">Nama/ NIP</th>
					<th width="64" class="trhead">Pangkat/ Gol. Ruang/ TMT </th>
					<th width="75" class="trhead">Jabatan</th>
				</tr>
				<?
				$i = $start++;
				if ($kerja_list!=FALSE){
				foreach ($kerja_list as $jabatan) {
					$i++;
					if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
					list($y, $m, $d) = explode('-', $jabatan['tmt_golpangkat_terakhir']);
					$str_tmt_golpangkat_terakhir = $d . "/" . $m . "/" . $y;
					$jabatan['tmt_golpangkat_terakhir']=$str_tmt_golpangkat_terakhir;
					
					list($y, $m, $d) = explode('-', $jabatan['tmt_golpangkat_baru']);
					$str_tmt_baru = $d . "/" . $m . "/" . $y;
					$jabatan['tmt_golpangkat_baru']=$str_tmt_baru;
					
					list($y, $m, $d) = explode('-', $jabatan['tmt_cpns']);
					$str_tmt_cpns = $d . "/" . $m . "/" . $y;
					$jabatan['tmt_cpns']=$str_tmt_cpns;
					if ($jabatan['nama_jabatan']=='Fungsional Umum')
					{
						$jabatan['nama_jabatan']='-';
					}
				?>   
		
				<tr class="<?= $class; ?>">
					<td align="center"><input type="checkbox" name="centang[<?= $i; ?>]" value="<?=$jabatan['kd_pegawai']?>"/></td>
					<td align="center" class="colEvn"><?= $i; ?></td>
					<td align="left" class="trhead" ><?= $jabatan['gelar_depan']." ".strtoupper($jabatan['nama_pegawai']).$jabatan['gelar_belakang']; ?></td>
					<td align="left" class="trhead" ><?=$jabatan['NIP']?></td>
					<td align="left" class="trhead" ><?=$jabatan['karpeg']?></td>
					<td align="left" class="trhead" ><?=$jabatan['nama_pendidikan']?></td>
					<td align="left" class="trhead" >&nbsp;</td>
						 				<?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$jabatan['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
					?>

					<td align="left" class="trhead" ><?=$tempat_lahir?>, <?=$jabatan['tgl_lahir']?></td>
					
					<td align="center" class="trhead" ><?=$jabatan['nama_pangkat']?>, <?=$jabatan['nama_golongan']?>, <?=$jabatan['tmt_golpangkat_terakhir']?> </td>
					<td align="center" class="trhead" >&nbsp;</td>
					<td align="center" class="trhead" >&nbsp;</td>
					<td align="center" class="trhead" ></td>
					<td align="center" class="trhead" ><?=$jabatan['nama_pangkat_baru']?>, <?=$jabatan['nama_golongan_baru']?>, <?=$jabatan['tmt_golpangkat_terakhir']?> </td>
					<td align="center" class="trhead" ></td>
					
					<td align="center" class="trhead" ></td>
					<td align="center" class="trhead" ></td>
					<td align="center" class="trhead" ></td>
					<td align="center" class="trhead" ></td>
					<td align="center" class="trhead" ></td>
					
					<td align="center" class="trhead" ></td>
					
					<td align="left" class="trhead" ><br />
                      <input type="hidden" name="nama_pegawai<?= $i; ?>"  value="<?= $jabatan['nama_pegawai']; ?>" />
                      <input type="hidden" class='datepicker' name="tmt_golpangkat_baru<?= $i; ?>"  value="<?= $jabatan['tmt_golpangkat_baru']; ?>" />
                      <input type="hidden" name="id_golpangkat_baru<?= $i; ?>"  value="<?= $jabatan['id_golpangkat_baru']; ?>" />
                      <input type="hidden" name="keterangan<?= $i; ?>"  value="<?= $this->user->user_id; ?>" />
                      <input type="hidden" name="status_pegawai<?= $i; ?>"  value="2" />
                      <input type="hidden" name="masa_kerja<?= $i; ?>"  value="0" />
                      <input type="hidden" name="gaji_pokok<?= $i; ?>"  value="0" />
                      <input type="hidden" name="no_sk_pangkat<?= $i; ?>"  value="" />
                      <input type="hidden" class='datepicker' name="tgl_sk_pangkat<?= $i; ?>"  value="" /></td>
					<td align="center" class="trhead" ><a class="icon" href="
					<?=site_url('laporan/naikpangkat/printmodel_d/'.$jabatan['kd_pegawai'].'/'.$group.'/'.$tahun)?>" title="Cetak">
				<img class="noborder" src="<?=base_url().'public/images/print_pdf.jpg'?>"></a></td>
				 </tr>
				
				<? } }
				else {
					$conspan = ($group == '') ? 7 : 6;
					echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
				} 
				if($this->user->user_group=="Administrator") {
				?>
				
				<tr>
					<td align="center"><input type="submit" value="Proses" /></td>
					<td colspan="22" align="center">&nbsp;</td>
				</tr>
				<? } ?>
			</table>
		</form>
		<br/>
		<div class="paging"><?=$page_links?></div>
	</div>
</div>

<?php
	}
		if(is_array($centang)){
		echo '<b>centang yang anda pilih : <br />';
		/*tulis disini action simpannya cek jika tercentang lakukan 
		- update untuk golongan-pangkat terakhir dan tanggal KP pada tabel pegawai 
		- add pada tabel riwayat golongan pangkat, sesuai data rencana kenaikan pangkat 
		centang
		tmt_golpangkat_baru
		id_golpangkat_baru
		keterangan
		status_pegawai
		masa_kerja
		gaji_pokok
		no_sk_pangkat
		tgl_sk_pangkat
		*/
		foreach($centang as $indek=>$kd_peg){
		//echo 'centang indek '.$indek.' : '.$kd_peg.'<br />';
		$tmt_gol_baru = $this->input->post('tmt_golpangkat_baru'.$indek, TRUE);
		//echo 'tmt_golpangkat_baru : '.$tmt_gol_baru.'<br />'; 
		$id_gol_baru = $this->input->post('id_golpangkat_baru'.$indek, TRUE);
		//echo 'id_golpangkat_baru : '.$id_gol_baru.'<br />'; 
		$nama_pegawai = $this->input->post('nama_pegawai'.$indek, TRUE);
		echo 'Data kenaikan  pangkat: '.$nama_pegawai.' sudah terproses!<br />'; 
			//update id_golpangkat_terakhir dan tmt_gol
			$query = mysql_query("select * from pegawai where kd_pegawai='".$kd_peg."'");					
			if ($query) {
				//$datapeg=mysql_fetch_array($query); 
				$query = mysql_query("UPDATE pegawai 
				SET id_golpangkat_terakhir = '".$id_gol_baru."' , tmt_golpangkat_terakhir = '".$tmt_gol_baru."'
				WHERE kd_pegawai='".$kd_peg."'");	
				if ($query) {
					$query = mysql_query("SELECT max(id_riwayat_gol)+1 as idr from riwayat_gol_kepangkatan");		
					if ($query) 
						{
							$data=mysql_fetch_array($query);
							$id_riwayat_gol = $data['idr'];			
						}
					
					
					$query = mysql_query("UPDATE riwayat_gol_kepangkatan 
					SET aktif = '0' 
					WHERE kd_pegawai='".$kd_peg."'");		
						
					$query = mysql_query("INSERT INTO riwayat_gol_kepangkatan 
					(id_riwayat_gol, kd_pegawai, id_golpangkat, no_SK_pangkat, 
					tgl_SK_pangkat, tmt_pangkat, keterangan, status_pegawai, masa_kerja, gaji_pokok, aktif) 
					VALUES	('".$id_riwayat_gol."', '".$kd_peg."', '".$id_gol_baru."', 
					'', '0000-00-00', '".$tmt_gol_baru."', '".$this->user->user_id."', '2', '0', 0, '1')");	
					
					/*$queryku = "INSERT INTO riwayat_gol_kepangkatan 
					(id_riwayat_gol, kd_pegawai, id_golpangkat, no_SK_pangkat, 
					tgl_SK_pangkat, tmt_pangkat, keterangan, status_pegawai, masa_kerja, gaji_pokok, aktif) 
					VALUES	('".$id_riwayat_gol."','".$kd_peg."', '".$id_gol_baru."', 
					'', '0000-00-00', '".$tmt_gol_baru."', '".$this->user->user_id."', '2', '0', 0, '1')";*/
					
					set_success('Data kenaikan pangkat ' . $kd_peg.' berhasil disimpan');
					//echo $queryku;
					}
				else
					{
						set_success('Data kenaikan pangkat ' . $kd_peg.' gagal disimpan');
					}
				}
			// end update
		}
		echo '<a href="'.site_url('laporan/naikpangkat/browse').'">Kembali &raquo;</a>';
		}
	?>

<script type="text/javascript">
	function Check(){
	//Ambil semua elemen dalam id formCheck
	allCheckList = document.getElementById("formCheck").elements;
	//Hitung banyaknya elemen
	jumlahCheckList = allCheckList.length;
	//Jika tombolCheck bernilai "Check All"
	if(document.getElementById("tombolCheck").value == "Check All"){
	for(i = 0; i < jumlahCheckList; i++){
	//semua elemen ke-i checkbox nya diset true (dicentang)
	allCheckList[i].checked = true;
	}
	//Set nilai tombolCheck menjadi "Uncheck All"
	document.getElementById("tombolCheck").value = "Uncheck All";
	//Jika tombolCheck tidak bernilai "Check All" (sudah dirubah menjadi Uncheck All)
	}else{
	for(i = 0; i < jumlahCheckList; i++){
	//semua elemen ke-i checkbox nya diset false (tidak dicentang)
	allCheckList[i].checked = false;
	}
	//Set nilai tombolCheck menjadi "Check All"
	document.getElementById("tombolCheck").value = "Check All";
	}
}
</script>
