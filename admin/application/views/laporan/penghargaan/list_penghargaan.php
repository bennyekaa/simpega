<?php

?>
<div class="content">
	<div id="content-header">
		<div align="left" class="ch-title"><h3><?=$judul?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/penghargaan/printtopdf')?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
		</div>	
		<div class="clear"></div>
	</div>
	<br/>
	<div id="content-data">
		<div class="paging"><?=$page_links?></div>
			<table  class="table-list">
				<tr class="trhead">
					<th class="colEvn" width="20" align="center">No</th> 
					<th width='20'  align="center">NIP</th>
					<th class="colEvn" width='250' class="colEvn" align="center">Nama</th>
					<th class="trhead" width='150' align="center">Tgl Penghargaan</th>
					<th class="colEvn" width='250' class="colEvn" align="center">Nama Penghargaan</th>
					<th class="trhead" width='40'>Edit</th>
				</tr>
				<?	$i = 0;
					if ($penghargaan_list!=FALSE){
						foreach ($penghargaan_list as $penghargaan) {
					$i++;
				   if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				?>
				<tr class="<?=$class;?>">
					<td align="right" class="colEvn"><?= $i; ?></td>
					<td class="trhead" align="left" nowrap="nowrap"><?= $penghargaan['nippeg']; ?></td>
					<td class="colEvn" align="left" nowrap="nowrap" ><?= $penghargaan['nama_peg']; ?></td>
					<td class="trhead" align="left" class="colEvn" nowrap="nowrap" ><?= date('d M Y',strtotime($penghargaan['tgl_penghargaan'])); ?></td>
					<td class="colEvn" align="left" class="colEvn" nowrap="nowrap"><?= $penghargaan['nama_penghargaan']; ?></td>
					<td class="trhead" align="center" class="colEvn" >
						<a href = "<?=site_url("laporan/penghargaan/view/".$penghargaan['id_riwayat_penghargaan']); ?>" class="view action" >Lihat Detil</a>
					</td>
				</tr>
				<? 	} 
				}
				else {
					echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
				}?>
			</table>
		<div class="paging"><?=$page_links?></div>
	</div>
</div>	
