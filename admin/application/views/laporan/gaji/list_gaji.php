
<?
$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');
?>
<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/gaji/printtopdf/'.$group.'/'.$tahun.'/'.$jns_pegawai)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/gaji/printtoxls/'.$group.'/'.$tahun.'/'.$jns_pegawai.'/list_gaji_berkala') ?>"  
			title="Eksport ke Excel"><img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
<br/>
<div class="content-data">
	<div class='detail' style="position:absolute z-index"></div>
		<center>
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/gaji/browse')?>" >
				<label>Jenis Pegawai : </label><?=form_dropdown('jns_pegawai',$jenis_pegawai_assoc,$jns_pegawai);?>
				<label>Bulan: </label><?=form_dropdown('group',$group_assoc,$group);?><label> Tahun: </label><?=form_dropdown('tahun',$tahun_assoc,$tahun);?>
				<input name = "mulai" type="submit" value="Filter">
			</form>
		</center>
	<br/>
	<br/>
		<?php
			if ($this->input->post('mulai')){
		?>
		<table class="table-list">
		<tr class="trhead" align="center">	
			<th class="colEvn" width="20" align="center">No</th> 
			<th class="trhead" width="117" >NIP</th>
			<th class="colEvn" width="130" >Nama Pegawai</th>
			<th width="106" >TMT Kenaikan Gaji Berkala</th>
			<th width="96" class="colEvn">TMT CPNS </th>
			<th class="trhead" width="89" >Golongan</th>
			<th width="50" class="colEvn">MK</th>
			<th width="50" >MKG</th>
			<th class="colEvn"  width="125" >Gaji Pokok</th>
			<th class="trhead" width="125" >Gaji Baru</th>
			<th class="colEvn"  width="125" >Pendidikan</th>
			<th width="103">Jabatan</th>
			<th width="118" class="colEvn" >Unit Kerja</th>
		</tr>
		<?
		$i = $start++;
		
		if ($kerja_list!=FALSE){
		foreach ($kerja_list as $kerja) {
			$i++;
			if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
            if ($kerja['tmt_kgb']=='0000-00-00') {
                $str_tmt_kgb = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $kerja['tmt_kgb']);
                $str_tmt_kgb = $d . "/" . $m . "/" . $y;
                   
            }
            $kerja['tmt_kgb']=$str_tmt_kgb;
            
            if ($kerja['tmt_cpns']=='0000-00-00') {
                $str_tmt_cpns = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $kerja['tmt_cpns']);
                $str_tmt_cpns = $d . "/" . $m . "/" . $y;
                   
            }
            $kerja['tmt_cpns']=$str_tmt_cpns;
            if ($kerja['nama_jabatan']=='Fungsional Umum')
            {
				$kerja['nama_jabatan'] = '-';
			}
		?>   
		<tr class="<?= $class; ?>">
			<td align="right" class="colEvn"><?= $i; ?></td>
			<td align="left" class="trhead" ><?=$kerja['NIP']?></td>
			<td  align="left" class="trhead"><?=$kerja['gelar_depan']." " .strtoupper($kerja['nama_pegawai'])." ".$kerja{'gelar_belakang'} ?></td>
			<td align="center" class="colEvn" nowrap="nowrap"><?= $kerja['tmt_kgb']; ?></td>
			<td align="center" class="colEvn" ><?=$kerja['tmt_cpns']?></td>
			<td align="center" class="colEvn" nowrap="nowrap"><?= $kerja['nama_golongan']; ?></td>
			<td align="center" class="colEvn" nowrap="nowrap"><?= $kerja['masa_kerja']; ?></td>
			<td align="center" class="colEvn" nowrap="nowrap"><?= $kerja['masa_kerja_golongan']; ?></td>
			<td align="right" class="trhead" nowrap="nowrap"><?= number_format($kerja['gajilama']); ?></td>
			<td align="right" class="trhead" nowrap="nowrap"><?= number_format($kerja['gajibaru']); ?></td>	
			<td align="center" class="trhead" nowrap="nowrap"><?= $kerja['nama_pendidikan']; ?></td>	
			<td align="left" class="colEvn" ><?= $kerja['nama_jabatan']?></td>
			<td align="left" class="trhead" ><?= $kerja['unit_kerja']; ?></td>	
		</tr>
		<? } }
		else {
			echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
		}?>

		</table>
		<div class="paging"><?=$page_links?></div>
	</div>
</div>
<? } ?>