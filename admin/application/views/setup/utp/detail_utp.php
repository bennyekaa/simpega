<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/utp/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/utp');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="utpdetail" id="utpdetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="150px">Kode</td>
				<td> : </td>
				<td>
					<input type='text' name='id_utp' id='id_utp' class="js_required required" value='<?= $id_utp?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('id_utp'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">Nama UTP</td>
				<td> : </td>
				<td>
					<input type='text' name='nama_utp'  value='<?= $nama_utp?>' style='width:25em'/>
				</td>
			</tr>
		
					
			<tr align="left">
				<td width ="150px">Keterangan</td>
				<td> : </td>
				<td>
					<input type='text' name='keterangan'  value='<?= $keterangan?>' style='width:25em'/>
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center">
		<input id="submit"  size="100px" name="submit" type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
