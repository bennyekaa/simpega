<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/pendidikan/$action/";
if (!async_request())
{
?>

<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/pendidikan');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>

<? } ?>
<br/>
<div class="content-data">
	<form name="pendidikandetail" id="pendidikandetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
		<tr align="left">
			<td width ="150px">Kode</td>
			<td> : </td>
			<td><?= $id_pendidikan?></td>
		</tr>
		<tr align="left">
			<td width ="150px">Tingkat Pendidikan</td>
			<td> : </td>
			<td>
			<input type='text' name='nama_pendidikan' id='nama_pendidikan' class="js_required required" value='<?= $nama_pendidikan?>' style='width:15em'/>
			<span style="color:#F00;">* <?php echo form_error('nama_pendidikan'); ?></span>
			</td>
			<td></td>
			
		</tr>
		<tr align="left">
			<td width ="150px">Jenis Pendidikan</td>
			<td> : </td>
			<td  align="left">
				<select name="jenis_pendidikan">
					<option value="1" > Formal</option>
					<option value="2" > Non Formal</option>
				</select> 
				
				<?php echo form_error('jenis_pendidikan'); ?>
			</td>
		</tr>
		<tr align="left">
			<td width ="200px">Golongan/Pangkat Awal</td>
			<td> : </td>
			<td>
				<?php
						echo form_dropdown('id_golpangkat_awal', $golongan_pangkat_assoc, $id_golpangkat_awal);
						echo form_error('id_golpangkat_awal');
					?>
			</td>
		</tr>
		<tr align="left">
			<td width ="200px">Golongan/Pangkat Akhir</td>
			<td> : </td>
			<td>
				<?php
						echo form_dropdown('id_golpangkat_akhir', $golongan_pangkat_assoc, $id_golpangkat_akhir);
						echo form_error('id_golpangkat_akhir');
					?>
			</td>
		</tr>
		<tr align="left">
			<td width ="150px">Keterangan</td>
			<td> : </td>
			<td>
			<input type='text' name='keterangan' id='keterangan' class="js_required required" value='<?= $keterangan?>' style='width:30em'/>
			</td>
		</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>