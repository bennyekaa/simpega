<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/golongan/add')?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>

<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='80' class="colEvn">Gol/Ruang</th>
		<th width='200' >Pangkat</th>			
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_golongan!=FALSE){
	foreach ($list_golongan as $jns_golongan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$jns_golongan['id_golpangkat'];?></td>
		<td align="center" ><?= $jns_golongan['golongan']; ?></td>
		<td align="left" ><?=$jns_golongan['pangkat'];?></td>
		<td align="center" class="colEvn">		
			<?php if($this->user->user_group!="Staf") {	?>	
			<a href="<?=site_url("setup/golongan/edit/".$jns_golongan['id_golpangkat']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/golongan/delete/".$jns_golongan['id_golpangkat']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>