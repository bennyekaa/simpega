<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/golongan/$action/";
if (!async_request())
{
?>
<div class="content">
	<div id="content-header">
		<div class="ch-title"><h3><?=$judul;?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('setup/golongan');?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div class="content-data">
		<form name="golongandetail" id="golongandetail" method="POST" action="<?= $action_url; ?>">
			<table class="general">
				<tr align="left">
					<td width ="100px">Kode</td>
					<td> : </td>
					<td>
						<?= $id_golpangkat?>
					</td>
				</tr>
				<tr align="left">
					<td width ="100px">Golongan</td>
					<td> : </td>
					<td>
						<input type='text' name='golongan' id='golongan' class="js_required required" value='<?= $golongan?>' style='width:4em'/>
						<span style="color:#F00;">* <?php echo form_error('golongan'); ?></span>
					</td>
				</tr>
				<tr align="left">
					<td width ="100px">Pangkat</td>
					<td> : </td>
					<td>
						<input type='text' name='pangkat' id='pangkat' class="js_required required" value='<?= $pangkat?>' style='width:13em'/>
						<span style="color:#F00;">* <?php echo form_error('pangkat'); ?></span>
					</td>
				</tr>
			</table>
			<br/>
			<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>