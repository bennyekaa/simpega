<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/jenis_mutasi/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/jenis_mutasi');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="jenis_mutasidetail" id="jenis_mutasidetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="100px">Kode</td>
				<td> : </td>
				<td>
					<input type='text' name='jns_mutasi' id='jns_mutasi' class="js_required required" value='<?= $jns_mutasi?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('jns_mutasi'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="100px">Nama Jenis Mutasi</td>
				<td> : </td>
				<td>
					<input type='text' name='nama_mutasi' id='nama_mutasi' class="js_required required" value='<?= $nama_mutasi?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('nama_mutasi'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="100px">Keterangan</td>
				<td> : </td>
				<td>
					<textarea name='keterangan' id='keterangan' value='<?= $keterangan?>' rows="2"><?= $keterangan?></textarea>
					<!--<input type='text' name='keterangan' id='keterangan' value='<?= $keterangan?>' style='width:20em'/>-->
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>