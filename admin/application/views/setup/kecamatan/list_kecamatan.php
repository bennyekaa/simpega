<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?>&nbsp;DI KABUPATEN&nbsp;<?=$nama_kabupaten[$kd_kabupaten]?></div>
	<div id="module-menu">
	<a class="icon" href="<?=site_url('setup/kabupaten');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a> &nbsp;
<a class="icon" href="<?=site_url("setup/kecamatan/add/$kd_kabupaten")?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='300' class="colEvn">Nama kecamatan</th>
		
		<th>Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_kecamatan!=FALSE){
	foreach ($list_kecamatan as $jns_kecamatan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$jns_kecamatan['kd_kecamatan'];?></td>
		<td align="left" >
		<a href="<?=site_url("setup/kelurahan/browse/".$jns_kecamatan['kd_kecamatan']); ?>" title="View Kelurahan"><?= $jns_kecamatan['nama_kecamatan']; ?></a> 
		</td>
		
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/kecamatan/edit/".$jns_kecamatan['kd_kecamatan']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/kecamatan/delete/".$jns_kecamatan['kd_kecamatan']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			<a href="<?=site_url("setup/kelurahan/browse/".$jns_kecamatan['kd_kecamatan']); ?>" title="Kelurahan" class="view action">Kelurahan</a>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>