<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/jabatan_struktural/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/jenis_pegawai');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="jenis_pegawaidetail" id="jenis_pegawaidetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="150px">Kode</td>
				<td> : </td>
				<td>
					<?= $id_jabatan_s?>
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">Nama Jabatan Struktural </td>
				<td> : </td>
				<td>
					<input type='text' name='nama_jabatan_s' id='nama_jabatan_s' class="js_required required" value='<?= $nama_jabatan_s?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('nama_jabatan_s'); ?></span>
				</td>
			</tr>
		
			<tr align="left">
				<td width ="150px">Jenis Pegawai </td>
				<td> : </td>
				<td>
					<?php
					echo form_dropdown('id_jns_pegawai',$jenis_pegawai_assoc,$id_jns_pegawai );
					echo form_error('id_jns_pegawai'); ?> 
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">Eselon </td>
				<td> : </td>
				<td>
					<input type='text' name='eselon' id='eselon' value='<?= $eselon?>' style='width:20em'/>
					
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">Tunjangan </td>
				<td> : </td>
				<td>
					<input type='text' name='tunjangan' id='tunjangan' value='<?= $tunjangan?>' style='width:20em'/>
					
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">Batas Usia Pensiun  </td>
				<td> : </td>
				<td>
					<input type='text' name='batas_maks_pensiun' id='batas_maks_pensiun' value='<?= $batas_maks_pensiun?>' style='width:20em'/>
					
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <? if ($action=="add"){?> disabled="disabled"  <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>