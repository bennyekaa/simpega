<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
			<a class="icon" href="<?=site_url('setup/jabatan_struktural/add')?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table width="577"  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='300' class="colEvn">Nama Jabatan Struktural</th>
		
		<th width='200' >Jenis Pegawai</th>	
		<th width='64' >Eselon</th>		
		<th width="258" class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_jabatan_struktural!=FALSE){
	foreach ($list_jabatan_struktural as $jabatan_struktural) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="left" ><?= $jabatan_struktural['nama_jabatan_s']; ?></td>
		
		<td align="left" ><?=$jenis_pegawai_assoc[$jabatan_struktural['id_jns_pegawai']]?></td>
		<td align="left" ><?= $jabatan_struktural['eselon']; ?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/jabatan_struktural/edit/".$jabatan_struktural['id_jabatan_s']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/jabatan_struktural/delete/".$jabatan_struktural['id_jabatan_s']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>