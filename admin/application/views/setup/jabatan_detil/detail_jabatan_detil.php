<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/jabatan_detil/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?> &nbsp;"<?=$nama_jabatan[$id_jabatan]?>"</div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url()."/setup/jabatan_detil/$action_list/";?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="jabatandetail" id="jabatandetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="100px">Kode</td>
				<td> : </td>
				<td>
					<input type='text' name='id_jabatan_detil' id='id_jabatan_detil' class="js_required required" value='<?= $id_jabatan_detil?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('id_jabatan_detil'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Nama Detil Jabatan</td>
				<td> : </td>
				<td>
					<input type='text' name='nama_jabatan_detil' id='nama_jabatan_detil' class="js_required required" value='<?= $nama_jabatan_detil?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('nama_jabatan_detil'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Pangkat,Golongan</td>
				<td> : </td>
				<td>
					<?php
							echo form_dropdown('id_golpangkat', $golongan_pangkat_assoc, $id_golpangkat);
							echo form_error('id_golpangkat');
						?>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Angka Kredit Minimal</td>
				<td> : </td>
				<td>
					<input type='text' name='ak_minimal' id='ak_minimal' value='<?= $ak_minimal?>' style='width:5em'/>
				</td>
			</tr>
					<input type='hidden' name='id_jabatan' id='id_jabatan' class="js_required required" value='<?= $id_jabatan?>' style='width:20em'/>
		
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>