<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/jenjang/$action/";
if (!async_request())
{
?>

<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/jenjang');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>

<? } ?>
<br/>
<div class="content-data">
	<form name="jenjangdetail" id="jenjangdetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="150px">Kode</td>
				<td> : </td>
				<td><?= $id_jenjang_kenaikan?></td>
			</tr>
				<tr align="left">
				<td width ="200px">Golongan/Pangkat Awal</td>
				<td> : </td>
				<td>
					<?php
							echo form_dropdown('id_golpangkat_awal', $golongan_pangkat_assoc, $id_golpangkat_awal);
							echo form_error('id_golpangkat_awal');
						?>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Golongan/Pangkat Akhir</td>
				<td> : </td>
				<td>
					<?php
							echo form_dropdown('id_golpangkat_akhir', $golongan_pangkat_assoc, $id_golpangkat_akhir);
							echo form_error('id_golpangkat_akhir');
						?>
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center">
		<input id="submit"  type="submit" size="100px" name="submit"  value="  Simpan  " /></div>
		<!--<?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>-->
	</form>
</div>
</div>
