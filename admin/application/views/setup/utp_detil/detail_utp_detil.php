<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/utp_detil/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?> &nbsp;"<?=$nama_utp[$id_utp]?>"</div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url()."/setup/utp_detil/$action_list/";?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="utpdetail" id="utpdetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="100px" align="center">Kode</td>
				<td> : </td>
				<td>
					<input type='text' name='id_detil' id='id_detil' class="js_required required" value='<?= $id_detil?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('id_detil'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Nama Rincian UTP</td>
				<td> : </td>
				<td>
				<textarea name='nama_detil' id='nama_detil' class="js_required required" rows="3" cols="30"> <?= trim($nama_detil)?> </textarea>
					<span style="color:#F00;">* <?php echo form_error('nama_detil'); ?></span>
				</td>
			</tr>
			
					<input type='hidden' name='id_utp' id='id_utp' class="js_required required" value='<?= $id_utp?>' style='width:20em'/>
		
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>