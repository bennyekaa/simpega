<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?> &nbsp;"<?=$nama_utp[$id_utp]?>"</div>
	<div id="module-menu">
	<a class="icon" href="<?=site_url('setup/utp');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a> &nbsp;
<a class="icon" href="<?=site_url("setup/utp_detil/add/$id_utp")?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='20'>Kode </th>
		<th width='570' >Nama Jabatan</th>
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = $start;
	if ($list_utp_detil!=FALSE){
	foreach ($list_utp_detil as $daftar_utp_detil) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="left" ><?= $daftar_utp_detil['nama_detil']; ?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/utp_detil/edit/".$daftar_utp_detil['id_detil']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/utp_detil/delete/".$daftar_utp_detil['id_detil']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>