<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/staf/menu/$action/";
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('staf/menu/index')?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div>
	<?=$js_var;?>
	<form name="menudetails" id="menudetails" method="POST" action="<?= $action_url; ?>">
		<input type='hidden' name='id_menu' id='id_menu' value='<?= $id_menu; ?>' >
		<table class="general">
		
		<tr>
			<td width ="150px" align="left">Nama Menu</td>
			<td> : </td>
			<td>
				<input type='text' name='name' id='name' class="required js_required" value='<?= set_value('name', $name); ?>' style='width:15em'/>
				<span style="color:#F00;">* <?php echo form_error('name'); ?></span>
			</td>
		</tr>	
		<tr>
			<td width ="150px" align="left">Rule</td>
			<td> : </td>
			<td>
			  <? $option_rule=$this->rule_model->get_rule_awal($id_rule); ?>
			  <?=form_group_dropdown('id_rule',$option_rule,$sel_rule);?><?php echo form_error('id_rule'); ?>
			</td>
		</tr>
		<tr>
			<td width ="150px" align="left">Parent </td>
			<td> : </td>
			<td>
				<? $option_menu=$this->menus_model->get_menu_awal($parent); ?>
				<?=form_group_dropdown('parent',$option_menu,$sel_menu);?><?php echo form_error('parent'); ?>
			</td>
		</tr>
		<tr>
			<td width ="150px" align="left">Ordering</td>
			<td> : </td>
			<td>
				<input name="ordering" class="required js_required" id="ordering" width="200px" type="text"  value='<?= set_value('ordering', $ordering); ?>' style='width:15em'/> 
				<span style="color:#F00;">* <?php echo form_error('ordering'); ?></span>
			</td>
		</tr>
		
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>

<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>