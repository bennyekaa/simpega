<?
$option_group=array('0',"Semua Grup") + $this->usergroup->get_group();
$option_rule=$this->rule_model->get_rule();
?>

<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
<a class="icon" href="<?=site_url('staf/rule');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>

<br/>
<div class="content-data">
	<?php echo validation_errors(); ?>
	<center>
	<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('staf/rule/assign')?>" >
		<input type="hidden" name="group_id" value="<?=$group_id?>">
		<label>Daftar Rule : </label><?=form_group_dropdown('id_rule',$option_rule,$sel_rule);?><input type="submit" value='Tambah'>
	</form>
	</center>
	<div class="divline"></div>
	<br/>
	<div class="paging"><?=$page_links?></div>	

	<table  class="table-list">
	<tr class="trhead">	
		<th class="colEvn" width="50px" align="center">
			No
		</th>
		<th width="300px" align="center">
			Nama Rule
		</th>
		<th class="colEvn" align="center">
			Aksi
		</th>
	</tr>
	<?
	$i = 0;
	if ($group_list!=FALSE){
	foreach ($group_list as $user) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>   
	<tr class="<?= $bgColor; ?>">
		<td class= "colEvn" align="left" nowrap="nowrap"><?=$i;?></td>
		<td align="left" nowrap="nowrap"><?=$user['rule_name']; ?></td>
		<td class="colEvn" align="center" nowrap="nowrap">	
			<a href = "<?=site_url("staf/rule/delete_assign_rule/".$user['id']); ?>" class="delete action">Hapus</a>
		</td>
	</tr>
	<? } }
	else {
		echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
	}?>
	</table>
	<div class="paging"><?=$page_links?></div>
</div>
</div>
	