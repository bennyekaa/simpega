<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/staf/menu/$action/";
if (!async_request()){
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
<a class="icon" href="<?=site_url('setup/user');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel-icon.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="menudetail" id="menudetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
		<tr align="left">
			<td width ="100px">Kode</td>
			<td> : </td>
			<td><?= $id_menu?>
	
			<input type='hidden' name='id_menu' id='id_menu' class="js_required required" value='<?= $id_menu?>'/>
	
			</td>
		</tr>
		<tr align="left">
			<td width ="100px">Nama Menu</td>
			<td> : </td>
			<td>
				<input type='text' name='name' id='name' class="js_required required" value='<?= $name?>'/>
				<span style="color:#F00;">* <?php echo form_error('name'); ?></span>
			</td>
		</tr>
		<tr>
			<td width ="150px" align="left">Rule</td>
			<td> : </td>
			<td>
			  
			  <?=form_group_dropdown('id_rule',$option_rule,$sel_rule);?><?php echo form_error('id_rule'); ?>
			</td>
		</tr>
		<tr>
			<td width ="150px" align="left">Parent</td>
			<td> : </td>
			<td>
				<?=form_dropdown('parent',$option_menu,$sel_menu);?><?php echo form_error('parent'); ?>
			</td>
		</tr>	
		<tr align="left">
			<td width ="100px">Urutan</td>
			<td> : </td>
			<td>
			<input type='text' name='ordering' id='ordering' value='<?= $ordering?>' onkeyup="gsub_num(this)" onchange="gsub_num(this)" 
				onblur="gsub_num(this)" style='width:2em'/>
			<?php echo form_error('ordering'); ?>  </td>
		</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>