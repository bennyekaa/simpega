<?
$this->load->helper('url');
$this->load->library('form_validation');
$option_group=$this->usergroup_model->get_group();
if (!async_request()){
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('staf/manage');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<?=$js_var;?>
	<form name="usersdetails" id="usersdetails" method="POST" action="<?= $action_url; ?>">
		<input type='hidden' name='user_id' id='user_id' value='<?= $user_id; ?>' >
		<table class="general">
		<tr>
			<td width ="150px" align="left">Grup</td>
			<td> : </td>
			<td>
			  <?=form_dropdown('FK_group_id',$option_group,$FK_group_id,'id="FK_group_id"');?><?php echo form_error('FK_group_id'); ?>
			</td>
		</tr>
		<tr>
			<td width ="150px" align="left">NIP Pegawai</td>
			<td> : </td>
			<td>
				<?=form_dropdown('kd_pegawai',$pegawai_options,$kd_pegawai);?>
				<?php echo form_error('kd_pegawai'); ?>
			</td>
		</tr>	
		<tr>
			<td width ="150px" align="left">Username</td>
			<td> : </td>
			<td>
				<input type='text' name='username' id='username' class="required js_required" value='<?= set_value('username', $username); ?>' style='width:15em'/>
				<span style="color:#F00;">* <?php echo form_error('username'); ?></span>
			</td>
		</tr>
		<tr>
			<td width ="150px" align="left">Nama Lengkap</td>
			<td> : </td>
			<td>
				<input name="name" class="required js_required" id="name" width="200px" type="text"  value='<?= set_value('name', $name); ?>' style='width:15em'/> 
				<span style="color:#F00;">* <?php echo form_error('name'); ?></span>
			</td>
		</tr>
		<tr>
			<td width ="150px" align="left">Email</td>
			<td> : </td>
			<td>
				<input name="email" class="required js_required" id="email" width="200px"   value='<?= set_value('email', $email); ?>' /> 
				<span style="color:#F00;">* <?php echo form_error('email'); ?></span>
			</td>
		</tr>
		<?if($is_new!=FALSE) {?>
		<tr>
			<td width ="150px" align="left">Password</td>
			<td> : </td>
			<td>
				<input name="password" class="required js_required" id="password" width="200px"   value='<?= set_value('password', $password); ?>' /> 
				<span style="color:#F00;">* <?php echo form_error('password'); ?></span>
			</td>
		</tr>
		<? } ?>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>

<script type="text/javascript">
	$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]==""){
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
	var selectmenu=document.getElementById("FK_group_id")	
	selectmenu.onchange=function(){ 
		var idx=this.options[this.selectedIndex] //this refers to "selectmenu"
		if (idx.value>2){
			document.getElementById("kd_pegawai").style.display='';
		} else {
			document.getElementById("kd_pegawai").style.display='none';
		}
	}
</script>