<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?>&nbsp;DI KECAMATAN&nbsp;<?=$nama_kecamatan[$kd_kecamatan]?></div>
	<div id="module-menu">
	<a class="icon" href="<?=site_url('setup/kabupaten/');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a> &nbsp;
<a class="icon" href="<?=site_url("setup/kelurahan/add/$kd_kecamatan")?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='300' class="colEvn">Nama Kelurahan</th>
		<th >Edit</th>
	</tr>

	<?
	$i = $start ;
	if ($list_kelurahan!=FALSE){
	foreach ($list_kelurahan as $jns_kelurahan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="left" ><?= $jns_kelurahan['nama_kelurahan']; ?></td>
		
		<td align="center" class="colEvn">		
			<?php if($this->user->user_group!="Staf") {	?>	
			<a href="<?=site_url("setup/kelurahan/edit/".$jns_kelurahan['kd_kelurahan']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/kelurahan/delete/".$jns_kelurahan['kd_kelurahan']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>