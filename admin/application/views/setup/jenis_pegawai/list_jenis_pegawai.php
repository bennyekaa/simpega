<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
			<a class="icon" href="<?=site_url('setup/jenis_pegawai/add')?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='300' class="colEvn">Nama Jenis Pegawai</th>
		
		<th width='200' >Parameter</th>		
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_jenis_pegawai!=FALSE){
	foreach ($list_jenis_pegawai as $jns_jenis_pegawai) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$jns_jenis_pegawai['id_jns_pegawai'];?></td>
		<td align="left" ><?= $jns_jenis_pegawai['jenis_pegawai']; ?></td>
		<!--<td align="left" ><?=$jns_jenis_pegawai['usia_pensiun'];?></td>-->
		<td align="left" ><?=$parameter_assoc[$jns_jenis_pegawai['id_parameter']]?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/jenis_pegawai/edit/".$jns_jenis_pegawai['id_jns_pegawai']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/jenis_pegawai/delete/".$jns_jenis_pegawai['id_jns_pegawai']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			<a href="<?=site_url("setup/jenis_jabatan/browse/".$jns_jenis_pegawai['id_jns_pegawai']); ?>" title="Jenis Jabatan" class="view action">Kelola Jenis Jabatan</a>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>