<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
<a class="icon" href="<?=site_url('setup/agama/add')?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table class="table-list">
	<tr class="trhead">
		<th width='50' class="colEvn">Kode</th>
		<th width='200' >Agama</th>			
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_agama!=FALSE){
	foreach ($list_agama as $jns_agama) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$jns_agama['kd_agama'];?></td>
		<td align="left" ><?= $jns_agama['agama']; ?></td>
		<td align="center" class="colEvn">		
			<?php if($this->user->user_group!="Staf") {	?>				
				<a href = "<?=site_url("setup/agama/edit/".$jns_agama['kd_agama']); ?>" class="edit action" >Ubah</a>
				<a href = "<?=site_url("setup/agama/delete/".$jns_agama['kd_agama']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
					
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>

