<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">

		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a> &nbsp;
<a class="icon" href="<?=site_url("setup/nilai_huruf/add")?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode </th>
		<th width='50' >Nilai Awal</th>
		<th width='50' >Nilai Akhir</th>
		<th width='200' >Keterangan</th>
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_nilai_huruf!=FALSE){
	foreach ($list_nilai_huruf as $nil_huruf) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="center" ><?= $nil_huruf['nilai_awal']; ?></td>
		<td align="center" ><?= $nil_huruf['nilai_akhir']; ?></td>
		<td align="center" ><?= $nil_huruf['keterangan']; ?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/nilai_huruf/edit/".$nil_huruf['id_nilai_huruf']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/nilai_huruf/delete/".$nil_huruf['id_nilai_huruf']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>