<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/nilai_huruf/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?> &nbsp;"<?=$nama_jenis_pegawai[$id_jns_pegawai]?>"</div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url()."/setup/nilai_huruf/$action_list/";?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="nilai_hurufdetail" id="nilai_hurufdetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="100px">Kode</td>
				<td> : </td>
				<td>
					<input type='text' name='id_nilai_huruf' id='id_nilai_huruf' class="js_required required" value='<?= $id_nilai_huruf?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('id_nilai_huruf'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Nilai Awal</td>
				<td> : </td>
				<td>
					<input type='text' name='nilai_awal' id='nilai_awal' class="js_required required" value='<?= $nilai_awal?>' style='width:5em' maxlength="3" />
					<span style="color:#F00;">* <?php echo form_error('nilai_awal'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Nilai Akhir</td>
				<td> : </td>
				<td>
					<input type='text' name='nilai_akhir' id='nilai_akhir' class="js_required required" value='<?= $nilai_akhir?>' style='width:5em' maxlength="3" />
					<span style="color:#F00;">* <?php echo form_error('nilai_akhir'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Keterangan</td>
				<td> : </td>
				<td>
					<input type='text' name='keterangan' id='keterangan' value='<?= $keterangan?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('keterangan'); ?></span>
				</td>
			</tr>
			
		
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>