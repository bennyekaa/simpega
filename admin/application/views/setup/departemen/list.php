<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/departemen/add')?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"/></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div class="paging"><?=$page_links?></div>
<div>
	<table class="table-list">
	<tr class="trhead">
		<th width='50' class="colEvn">Kode</th>
		<th width='200'>Departemen</th>			
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_departemen!=FALSE){
	foreach ($list_departemen as $jns_departemen) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$jns_departemen['kd_departemen'];?></td>
		<td align="left" ><?= $jns_departemen['departemen']; ?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>					
				<a href = "<?=site_url("setup/departemen/edit/".$jns_departemen['kd_departemen']); ?>" class="edit action">Ubah</a>
				<a href = "<?=site_url("setup/departemen/delete/".$jns_departemen['kd_departemen']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
</div>
</div>

