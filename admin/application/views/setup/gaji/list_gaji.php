<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
<a class="icon" href = "<?=site_url('setup/gaji/add')?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
		<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('setup/gaji/browse')?>" >
		<label>Pangkat, Gol/ruang : </label><?=form_dropdown('group',$golongan_assoc,$group);?>&nbsp;&nbsp;&nbsp;
		<label> Tahun Gaji: </label><?=form_dropdown('tahun',$tahun_assoc,$tahun);?>
		<input type="submit" value=" Cari  ">
		</form>
	</center>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50' >Kode</th>
		<th width='100' class="colEvn">Golongan</th>
		<th width='150'>Masa Kerja</th>			
		<th width='150'class="colEvn">Gaji Pokok</th>			
		<th class="trhead">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_gaji!=FALSE){
	foreach ($list_gaji as $jns_gaji) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$jns_gaji['id_gaji'];?></td>
		<td align="center" class="colEvn"><?= $jns_gaji['golongan']; ?></td>
		<td align="center" ><?= $jns_gaji['masa_kerja']; ?> tahun</td>
		<td align="center" ><?= number_format($jns_gaji['gaji_pokok']); ?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href = "<?=site_url("setup/gaji/edit/".$jns_gaji['id_gaji']); ?>" class="edit action" >Ubah</a>
			<a href = "<?=site_url("setup/gaji/delete/".$jns_gaji['id_gaji']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div></div>
</div>