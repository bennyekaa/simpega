<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/pangkat/add')?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table class="table-list">
	<tr class="trhead">
		<th width='50' class="colEvn">Kode</th>
		<th width='200' >Parameter</th>			
		<th width='100' >Nilai</th>			
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_pangkat!=FALSE){
	foreach ($list_pangkat as $jns_pangkat) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$jns_pangkat['id_konfig'];?></td>
		<td align="left" ><?= $jns_pangkat['jenis_konfig']; ?></td>
		<td align="center" ><?= $jns_pangkat['value']; ?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
				<a href = "<?=site_url("setup/pangkat/edit/".$jns_pangkat['id_konfig']); ?>" class="edit action" >Ubah</a>
				<? if($jns_pangkat['altered'] != 1) { ?>
					<a href = "<?=site_url("setup/pangkat/delete/".$jns_pangkat['id_konfig']); ?>" class="nyroModal delete action" >Hapus</a>
				<? } ?>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>

