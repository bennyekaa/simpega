<?php
  // session_start();

  // include("config/conf.php");

 //    $loadpage = $_REQUEST['lp'];
  // $hsldes = decrypt_url($loadpage);

?>
<?php //$v =& $this->validation ?>
<!DOCTYPE html>
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->

<head>
  
  <!-- charset -->
  <meta charset="utf-8">

  <title>SIMPEGA - Sistem Informasi Kepegawaian</title>

  <!-- viewport --> 
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!--  including stylesheets-->

  <link rel="stylesheet" type="text/css" href="<?=base_url()?>/application/views/staff/css/reset.min.css">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>/application/views/staff/css/d_blue.css" class="main-stylesheet">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>/application/views/staff/css/bootstrap-glyphicons.css" class="main-stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  <link rel="shortcut icon" href="<?=base_url()?>/application/views/staff/images/icon.ico">

  <!--[if IE]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- google fonts-->
<link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

</head>
<body>  

  <div class="container-left">

    <div class="container-left-picture">

      <!-- Your profile picture -->

      <img class="avatar" src="<?=base_url()?>/application/views/staff/images/avatar.png" alt="">

      <!-- Social media links -->

     <!-- lokasi pencarian -->


   </div>

   <!-- Personal facts -->
   <form name="loginform" action="" method="post" class="form-horizontal">
     <fieldset class="textbox" style="text-align:center;padding:25px 0px 10px 0px">
      <input type="hidden" name="token" value="<?php echo $token?>" id="token" />
      <input type="hidden" class="ids" name="id" value="" />

      <input style="margin-top: 7px" type="text" name="username" placeholder="Nama Pengguna" id="username" value="<?php echo $v->username?>" required  />
      <input style="margin-top: 7px" type="password" name="password" placeholder="Kata Kunci" id="password" required />
                                    <!-- <input class="btn  btn-default" style="margin-top: 8px" type="submit" value="&nbsp;&nbsp;&nbsp;Masuk&nbsp;&nbsp;&nbsp;" />
                                      <input class="btn btn-default" style="margin-top: 8px" type="reset" value="&nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;" /> -->
                                      <button class="btn btn-success" type="submit" style="margin-top: 8px; width: 68%">Login</button>
                                      <!-- <button class="btn btn-default" type="cancel" style="margin-top: 8px; width: 45%">Batal</button> -->
                                    </fieldset>
                                  </form>
<!--    <div class="container-left-info">
      <ul class="container-left-info-tabs">
      <li>  
        <strong>SIMPEGA</strong> adalah aplikasi Sistem Informasi Kepegawaian yang digunakan untuk menunjang proses administrasi kepegawaian di Universitas Negeri Malang. Kritik, saran, pertanyaan silahkan email ke kepegawaian@um.ac.id
    
      </li>

      </ul> 
    </div> -->

   <div class="social-media2">
     <center>
       |<a href="http://www.um.ac.id" target="_blank" title="Universitas Negeri Malang" class="link-intern"><b> UM </b></a>|
       <a href="http://www.bkn.go.id/" target="_blank" title="Badan Kepegawaian Negara" class="link-intern"><b> BKN </b></a>|
       <!-- <a href="http://www.ptaskes.com/" target="_blank" title="PT.ASKES (Persero)" class="link-intern"><b> PT.ASKES </b></a>|-->
       <a href="http://www.taspen.com/" target="_blank" title="TASPEN" class="link-intern"><b> TASPEN </b></a>|
       <a href="http://www.dikti.go.id/" target="_blank" title="Direktorat Jenderal Pendidikan Tinggi" class="link-intern"><b> DIKTI </b></a>|
       <a href="http://www.kemenkumham.go.id/" target="_blank" title="Biro Hukum Dan HAM" class="link-intern"><b> KEMENKUMHAM </b></a>|
     </center>

   </div>

 </div>


 <!-- Content for right container -->
 <div class="container-right" align="center">

   <div class="show-info-mobile"><span class="icon-menu"></span></div>


   <div >
    <!-- <h3><strong>Selamat Datang di <font size="+2">SIMPEGA&nbsp;</font></strong></h3> -->
    <strong>SISTEM INFORMASI KEPEGAWAIAN</strong>
    <h4>
<a href="http://www.um.ac.id" target="_blank" title="Universitas Negeri Malang">UNIVERSITAS NEGERI MALANG (UM)</a><!-- <br>
Jl. Semarang No. 5 Malang 65145&nbsp;<br>Telp. (0341) 551312&nbsp;  --></h4>
<p style="font-size:12px;color:#3a3a3a;">
  <strong>SIMPEGA</strong> adalah aplikasi Sistem Informasi Kepegawaian yang digunakan untuk menunjang proses administrasi kepegawaian di Universitas Negeri Malang. Kritik, saran, pertanyaan silahkan email ke kepegawaian@um.ac.id</p>
  <div style="font-size:12px; color: #2e2e2e;"> 


    <ul class="nav nav-tabs" role="tablist" style="margin-top:20px">
      <li role="presentation" class="active"><a href="#pns" aria-controls="home" role="tab" data-toggle="tab">PNS</a></li>
      <li role="presentation"><a href="#ptt" aria-controls="profile" role="tab" data-toggle="tab">Non-PNS</a></li>
      <li role="presentation"><a href="#cpns" aria-controls="profile" role="tab" data-toggle="tab">CPNS</a></li>
    </ul>
  </div>  
</div>
<div class="tab-content">

  <!-- Tabel Pegawai PNS -->
  <div role="tabpanel" class="tab-pane active" id="pns">
   <ul class="tab-list">
    <li class="tab-komunitas">
      <div id="tmpdetailpegawai">
       <table class="table table-hover table-bordered" id='pegawaiTable'>
        <thead> 
          <tr>
            <th width="30" style="text-align:center">No.</th>
            <th width="50" style="text-align:center">NIP</th>
            <th width='300' style="text-align:center">Nama Pegawai</th>
            <th width="30" style="text-align:center">Gol</th>
            <th style="text-align:center">Unit Kerja</th>
            <th width="30" style="text-align:center"></th>
          </tr>
        </thead>
        <tbody class="table-condensed">
          <?php 
          $x=1;
          foreach ($pegawai as $data_pegawai) { ?>
            <tr>
              <td style="text-align:left;vertical-align:top"><?php echo $x++; ?>.</td>
              <td style="text-align:left"><?php echo $data_pegawai['NIP']; ?></td>
              <td style="text-align:left">
                <?php echo $data_pegawai['gelar_depan'];?>
                <?php echo ' '.$data_pegawai['nama_pegawai'];?> 
                <?php echo $data_pegawai['gelar_belakang']; ?>
              </td>
              <td style="text-align:center"><?php echo $data_pegawai['golongan']; ?></td>
              <td style="text-align:left"><?php echo $data_pegawai['nama_unit']; ?></td>
              <td style="text-align:center">
                <a href="#" onClick="cekDetailPegawai(<?php echo $data_pegawai['kd_pegawai']; ?>)" class="link-intern" title="Detail <?php echo $data_pegawai['nama_pegawai']; ?>">
                  <span class="glyphicon glyphicon-book"></span>
                </a>
              </td>
            </tr>   
            <?php
          } ?>
        </tbody>
      </table>
    </div>
  </li>
</ul>
<div id="tmpdetailcekpengawai" style="display: none"></div>
</div>

<!-- Tabel Non-PNS -->
<div role="tabpanel" class="tab-pane" id="ptt">
 <ul class="tab-list">
  <li class="tab-komunitas">
   <div id="tmpdetailpegawai">
     <table class="table table-hover table-bordered" id='pttTable' style="width:100%">
      <thead> 
        <tr>
          <th width="20" style="text-align:center">No.</th>
          <th width="30" style="text-align:center">NIP</th>
          <th width='80' style="text-align:center">Nama Pegawai</th>
          <th style="text-align:center">Unit Kerja</th>
          <th width="10" style="text-align:center">Detail</th>
        </tr>
      </thead>
      <tbody class="table-condensed">
       <?php 
       $x=1;
       foreach ($ptt as $data_pegawai) { ?>
         <tr>
          <td style="text-align:left;vertical-align:top"><?php echo $x++; ?>.</td>
          <td style="text-align:left"><?php echo $data_pegawai['NIP']; ?></td>
          <td style="text-align:left">
            <?php echo $data_pegawai['gelar_depan'];?>
            <?php echo ' '.$data_pegawai['nama_pegawai'];?>
            <?php echo $data_pegawai['gelar_belakang']; ?>
          </td>
          <td style="text-align:left"><?php echo $data_pegawai['nama_unit']; ?></td>
          <td style="text-align:center">
            <a href="#" onClick="cekDetailPegawaiPTT(<?php echo $data_pegawai['kd_pegawai']; ?>)" class="link-intern" title="Detail <?php echo $data_pegawai['nama_pegawai']; ?>"><span class="glyphicon glyphicon-book"></span>
            </a>
          </td>
        </tr>   
        <?php
      } ?>
    </tbody>
  </table>
</div>
</li>
</ul>
<div id="detailPTT" style="display: none"></div>
</div>

<!-- Tabel CPNS -->
<div role="tabpanel" class="tab-pane" id="cpns">
 <ul class="tab-list">
  <li class="tab-komunitas">
   <div id="tmpdetailpegawai">
     <table class="table table-hover table-bordered" id='cpnsTable' style="width:100%">
      <thead> 
        <tr>
          <th width="10" style="text-align:center">No.</th>
          <th width="20" style="text-align:center">NIP</th>
          <th width='60' style="text-align:center">Nama Pegawai</th>
          <th style="text-align:center">Gol</th>
          <th style="text-align:center">Unit Kerja</th>
          <th width="10" style="text-align:center">Detail</th>
        </tr>
      </thead>
      <tbody class="table-condensed">
       <?php 
       $x=1;
       foreach ($cpns as $data_pegawai) { ?>
         <tr>
          <td style="text-align:left;vertical-align:top"><?php echo $x++; ?>.</td>
          <td style="text-align:left"><?php echo $data_pegawai['NIP']; ?></td>
          <td style="text-align:left">
            <?php echo $data_pegawai['gelar_depan'];?>
            <?php echo ' '.$data_pegawai['nama_pegawai'];?>
            <?php echo $data_pegawai['gelar_belakang']; ?>
          </td>
          <td style="text-align:center"><?php echo $golongan_assoc[$data_pegawai['id_golpangkat_terakhir']]; ?></td>
          <td style="text-align:left"><?php echo $data_pegawai['nama_unit']; ?></td>
          <td style="text-align:center">
            <a href="#" onClick="cekDetailPegawaiCPNS(<?php echo $data_pegawai['kd_pegawai']; ?>)" class="link-intern" title="Detail <?php echo $data_pegawai['nama_pegawai']; ?>"><span class="glyphicon glyphicon-book"></span>
            </a>
          </td>
        </tr>   
        <?php
      } ?>
    </tbody>
  </table>
</div>
</li>
</ul>
<div id="detailCPNS" style="display: none"></div>
</div>
</div>

</div>
</div>


<!-- including js -->
<script type="text/javascript" src="<?=base_url()?>/application/views/staff/js/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!--<script type="text/javascript" src="<?=base_url()?>/application/views/staff/js/additional-scripts.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>/application/views/staff/js/custom.js"></script>
<script type="text/javascript" src="<?=base_url()?>/application/views/staff/js/action.js"></script>-->

</center>


<script type="text/javascript">
  $(document).ready(function(){
    // datatable
    $("#pegawaiTable").DataTable();
    $("#pttTable").DataTable();
    $("#cpnsTable").DataTable();

  });

  //detail pegawai


  function cekDetailPegawai(kd_pegawai){
    $("#detail").remove();
    var base_url="<?php echo base_url() ?>";
    $.get("detail_pegawai/"+kd_pegawai, function(data){
      $("#tmpdetailcekpengawai").append(data);
      $("#tmpdetailcekpengawai").show("slow");
      $("html, body").animate({
        scrollTop:$("#tmpdetailcekpengawai").offset().top
      }, 500);
    });
  }

  function cekDetailPegawaiPTT(kd_pegawai){
    $("#detail").remove();
    var base_url="<?php echo base_url() ?>";
    $.get("detail_ptt/"+kd_pegawai, function(data){
      $("#detailPTT").append(data);
      $("#detailPTT").show("slow");
      $("html, body").animate({
        scrollTop:$("#detailPTT").offset().top
      }, 500);
    });
  }

  function cekDetailPegawaiCPNS(kd_pegawai){
    $("#detail").remove();
    var base_url="<?php echo base_url() ?>";
    $.get("detail_cpns/"+kd_pegawai, function(data){
      $("#detailCPNS").append(data);
      $("#detailCPNS").show("slow");
      $("html, body").animate({
        scrollTop:$("#detailCPNS").offset().top
      }, 500);
    });
  }
</script>
</body>

</html>
