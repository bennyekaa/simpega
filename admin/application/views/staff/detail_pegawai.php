
<div class="row" style="padding-bottom: 0px;">
  <div class="col-md-4">
    <div class="center-block text-center">
      <!-- <img src="<?php echo URL_IMG?>_default_avatar_male.jpg" class="img-responsive" alt="Foto Pegawai" style="margin-top: 10px;"> -->
      <img src="<?php echo base_url()?>public/photo/photo_<?php echo $detail['photo']?>.jpg" class="img-responsive img-thumbnail" alt="Foto <?php echo $detail['nama_pegawai'];?>" style="margin-top: 10px;">
    </div>
  </div>
  <div class="col-md-8" style="padding-top: 10px;">
    <b><?php echo $detail['gelar_depan']; ?><?php echo $detail['nama_pegawai']; ?><?php echo $detail['gelar_belakang']; ?></b>
    <hr style="margin-top: 10px;">
    <div class="form-horizontal form-custom">
    	<div class="form-group">
		    <label class="col-sm-3 text-left">NIP</label>
		    <div class="col-sm-9">
		    	<span><?php echo ($detail['NIP']) ? $detail['NIP'] : "-" ?></span>
		    </div>
		  </div>

      <?php if($detail['status_pegawai'] < 3) { ?>
      <div class="form-group">
        <label class="col-sm-3">Golongan</label>
        <div class="col-sm-9">
          <span><?php echo $detail['pangkat'].', '.$detail['golongan'] ?></span>
        </div>                                  
      </div>
      <div class="form-group">
        <label class="col-sm-3">Jabatan</label>
        <div class="col-sm-9">
          <span><?php 
          if($detail['nama_jabatan_s'] == null){
            echo "Jabatan Sudah Tidak Aktif";
          }else{
            echo $detail['nama_jabatan_s'] ;
          }
          ?></span>
        </div>                                  
      </div>
   	 	<?php } ?>

      <div class="form-group">
        <label class="col-sm-3">NPWP</label>
        <div class="col-sm-9">
          <span><?php echo ($detail['npwp']) ? $detail['npwp'] : '-';?></span>
        </div>                                  
      </div>
      <div class="form-group">
        <label class="col-sm-3">Unit Kerja</label>
        <div class="col-sm-9">
          <span><?php echo ($detail['nama_unit']) ? $detail['nama_unit'] : '-'; ?></span>
        </div>                                  
      </div>
      <div class="form-group">
        <label class="col-sm-3">Email</label>
        <div class="col-sm-9">
          <span><?php echo ($detail['email']) ? $detail['email'] : '-'; ?></span>
        </div>                                  
      </div>
    </div>
  </div>  
  <div class="col-md-12 text-center">
    <hr>
    <p class="text-muted" style="font-size:90%;"><i>Jika ada kesalahan informasi, silahkan melaporkan melalui email kepegawaian@um.ac.id atau menghubungi bagian kepegawaian di pesawat 1142 (Dosen) / 1146 (T.Kependidikan)</i></p>
  </div>
</div>
