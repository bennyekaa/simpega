// JavaScript Document
//--------------ajax function-------------------
var xmlHttp;

function GetXmlHttpObject() {
	var xmlHttp = null;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	} catch (e) {
		//Internet Explorer
		try {
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function detailberita(idpilih) {
	xmlHttp = GetXmlHttpObject();
	if (xmlHttp == null) {
		alert("Browser does not support HTTP Request");
		return;
	}
	var url = "https://simpega.um.ac.id/admin/application/views/staff/app/det_berita.php";
	url = url + "?act=read";
	url = url + "&dataYangDiCari=" + idpilih;
	xmlHttp.onreadystatechange = StateShowBeritaDetail;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function StateShowBeritaDetail() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		document.getElementById("tmpdetailberita").innerHTML = xmlHttp.responseText;
	}
}

function ShowPegawai(page, dataYangDiCari) {
	xmlHttp = GetXmlHttpObject();
	if (xmlHttp == null) {
		alert("Browser does not support HTTP Request");
		return;
	}
	var url = "https://simpega.um.ac.id/admin/application/views/staff/app/det_pegawai.php";
	url = url + "?act=read";
	url = url + "&page=" + page;
	url = url + "&dataYangDiCari=" + dataYangDiCari;
	xmlHttp.onreadystatechange = StateShowPegawai;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);

}

function StateShowPegawai() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		document.getElementById("tmpdetailpegawai").innerHTML = xmlHttp.responseText;
	}
}

function ShowPegawai(page, dataYangDiCari) {
	xmlHttp = GetXmlHttpObject();
	if (xmlHttp == null) {
		alert("Browser does not support HTTP Request");
		return;
	}
	var url = "https://simpega.um.ac.id/admin/application/views/staff/app/det_pegawai.php";
	url = url + "?act=read";
	url = url + "&page=" + page;
	url = url + "&dataYangDiCari=" + dataYangDiCari;
	xmlHttp.onreadystatechange = StateShowPegawai;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
	console.log('ok');
}

function StateShowPegawai() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		document.getElementById("tmpdetailpegawai").innerHTML = xmlHttp.responseText;
	}
}

function detailcekpegawai(dataYangDiCari) {
	xmlHttp = GetXmlHttpObject();
	if (xmlHttp == null) {
		alert("Browser does not support HTTP Request");
		return;
	}
	var url = "https://simpega.um.ac.id/admin/application/views/staff/app/detc_pegawai.php";
	url = url + "?act=read";
	url = url + "&dataYangDiCari=" + dataYangDiCari;
	xmlHttp.onreadystatechange = Statedetailcekpegawai;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function Statedetailcekpegawai() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		document.getElementById("tmpdetailcekpengawai").innerHTML = xmlHttp.responseText;
	}
}