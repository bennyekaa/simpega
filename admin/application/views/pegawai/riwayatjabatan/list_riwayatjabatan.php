<?
//$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url("pegawai/riwayatjabatan/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="jabatandetails" id="jabatandetails" method="POST" action="<?=site_url('pegawai/riwayatjabatan/browse')?>" >
	</form>
	</center>
	<br/>
	<table class="table-list">
	<tr class="trhead">
		<th width="20" >No</th>
		<th width="53" class="colEvn" >Jabatan</th>
		<th width="92" >No. SK </th>
		<th width="92" class="colEvn">Tanggal SK </th>
		<th width="90" >TMT Jabatan </th>
		<th width="92" class="colEvn">Tanggal Selesai </th>
		<th  width="74">Unit Kerja </th>
		<th  width="140" class="colEvn">Bidang Ilmu </th>
		<th  width="72" class="colEvn">Angka Kredit </th>
		<th width="101" >Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_jabatan!=FALSE){
	foreach ($list_jabatan as $jns_jabatan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
       if ($jns_jabatan['tgl_sk_jabatan']=='0000-00-00') {
                $str_sk_jabatan = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tgl_sk_jabatan']);
                $str_sk_jabatan = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
            }
        $jns_jabatan['tgl_sk_jabatan']=$str_sk_jabatan;
        if ($jns_jabatan['tgl_selesai']=='0000-00-00') {
                $str_tgl_selesai = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tgl_selesai']);
                $str_tgl_selesai = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
            }
        $jns_jabatan['tgl_selesai']=$str_tgl_selesai;
		
		if ($jns_jabatan['tmt_jabatan']=='0000-00-00') {
                $str_tmt_jabatan = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tmt_jabatan']);
                $str_tmt_jabatan = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
            }
        $jns_jabatan['tmt_jabatan']=$str_tmt_jabatan;
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="center" ><?= $jabatan_assoc[$jns_jabatan['id_jabatan']]; ?></td>
		<td align="center" ><?= $jns_jabatan['no_sk_jabatan']; ?></td>
		<td align="center" ><?= $jns_jabatan['tgl_sk_jabatan']; ?></td>
		<td align="center" ><?= $jns_jabatan['tmt_jabatan']; ?></td>
		<td align="center" ><?= $jns_jabatan['tgl_selesai']; ?></td>
		<td align="center" ><?= $unit_kerja_assoc[$jns_jabatan['kode_unit']]; ?></td>
		<td align="center" ><?= $jns_jabatan['bidang_ilmu']; ?></td>
		<td align="left" ><?= $jns_jabatan['angka_kredit']; ?></td>
		<td align="center" class="colEvn">		
		<?php	
					if($this->user->user_group=="Administrator") {
				?>		
			<a href="<?=site_url("pegawai/riwayatjabatan/edit/".$jns_jabatan['id_riwayat_jabatan']); ?>" class="edit action" >Ubah</a>
			<?php if($jns_jabatan['aktif']=='0') {?>
			<a href="<?=site_url("pegawai/riwayatjabatan/delete/".$jns_jabatan['id_riwayat_jabatan']); ?>" class="delete action" >Hapus</a>		</td>
			<? }  } ?>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

