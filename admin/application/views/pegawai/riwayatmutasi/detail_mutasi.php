<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatmutasi/$action/";
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/riwayatmutasi/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div>
	<form name="jabatandetail" id="jabatandetail" method="POST" action="<?= $action_url; ?>">
		 <?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
		<table class="general">
			<tr align="left">
				<td width ="200">Kode</td>
				<td> : </td>
				<td >
				<?= $id_mutasi?></td>
			</tr>
			<tr align="left">
			  <td>Jabatan Lama</td>
			  <td> : </td>
			  <td ><?php
					echo form_dropdown('id_jabatan_asal',$jabatan_assoc, $id_jabatan_asal);
					?> <? echo form_error('id_jabatan_asal'); ?> </td>
			  </tr>
			<tr align="left">
			  <td>Jabatan Baru</td>
			  <td> : </td>
			  <td ><?php
					echo form_dropdown('id_jabatan_baru',$jabatan_assoc, $id_jabatan_baru);
					?> <? echo form_error('id_jabatan_baru'); ?> </td>
			  </tr>
			<tr align="left">
				<td width ="200">Nomor SK</td>
				<td> : </td>
				<td >
					<input type='text' name='no_SK' id='no_SK' class="js_required" value='<?= $no_SK?>'  class="required"/>
					<span style="color:#F00;">*<?php echo form_error('no_SK'); ?></span>		</td>
			</tr>
			<tr align="left">
				<td width ="200">Tanggal SK</td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tgl_SK' id='tgl_SK' class="js_required" value='<?= $tgl_SK?>'  class="required"/>
					<?php echo form_error('tgl_SK'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
			</tr>
			
			<tr align="left">
				<td width ="200">Tanggal Berlaku SK </td>
				<td> : </td>
				<td ><input type='text' class='datepicker' name='tmt_SK' id='tmt_SK' class="js_required" value='<?= $tmt_SK?>'  class="required"/>
				  <?php echo form_error('tmt_SK'); ?> <span style="color:#F00;"><i> yyyy-mm-dd</i></span></td>
			</tr>
			<tr align="left">
					<td width ="200px">Gol/Ruang </td>
					<td> : </td>
					<td >
					<?php
						echo form_dropdown('id_golpangkat',$golongan_assoc, $id_golpangkat);
						?>
						<? echo form_error('id_golpangkat'); ?> </td>
				</tr>
			<!--<tr align="left">
				<td width ="200px">Tanggal mutasi</td>
				<td> : </td>
				<td >-->
					<input type='hidden' class='datepicker' name='tgl_mutasi' id='tgl_mutasi' value='<?=set_value('tgl_mutasi',$tgl_mutasi);?>'/>
					<!--<span style="color:#F00;"><?php echo form_error('tgl_mutasi'); ?></span>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	</td>-->
			</tr>
			<tr>
					<td  align="left">Unit Kerja Lama</td>
					<td> : </td>
					<td colspan="3"  align="left">
						<?php
							echo form_group_dropdown('kode_unit_kerja_asal',$option_unit,$kode_unit_kerja_asal);
							echo form_error('kode_unit_kerja_asal');
						?>			
					</td>
			</tr>
			<tr>
					<td  align="left">Unit Kerja Baru</td>
					<td> : </td>
					<td colspan="3"  align="left">
						<?php
							echo form_group_dropdown('kode_unit_kerja_baru',$option_unit,$kode_unit_kerja_baru);
							echo form_error('kode_unit_kerja_baru');
						?>			
					</td>
			</tr>
			<tr>
					<td  align="left">Jenis Mutasi</td>
					<td> : </td>
					<td  align="left">
					<?
						echo form_dropdown('jns_mutasi', $jenis_mutasi_assoc, $jns_mutasi);
					?>
					<?php echo form_error('jns_mutasi'); ?>
					</td>
				</tr>
			<tr align="left">
			  <td>Keterangan</td>
			  <td> : </td>
			  <td ><textarea name="ket_mutasi" id="ket_mutasi"  rows="2"><?= $ket_mutasi?>
			  </textarea>
				  <?php echo form_error('ket_mutasi'); ?> </td>
			  </tr>
		   
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>