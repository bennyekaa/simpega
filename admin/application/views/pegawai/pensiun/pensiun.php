<?
$this->load->helper('form');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/pensiun/$action/";
if (!async_request()) {
?>
    <div class="content">
		<div id="content-header">
			<div id="module-title"><?=$judul?> <?=$pegawai['nama_pegawai']?></div>
			<div id="module-menu">
				<?php if ("add" !== $action) { ?>
				<div id="tab-title">
					<a class="icon" href="<?=site_url('pegawai/pasanganpegawai/index/' . $pegawai['kd_pegawai']) ?>" title="Pasangan">Pasangan</a> |
					<a class="icon" href="<?=site_url('pegawai/anakpegawai/index/' . $pegawai['kd_pegawai']) ?>" title="Anak">Anak</a> |
					<a class="icon" href="<?=site_url('pegawai/riwayatgolpangkat/index/' . $pegawai['kd_pegawai']) ?>" title="Pangkat">Kepangkatan</a> |
					<a class="icon" href="<?=site_url('pegawai/riwayatpenggajian/index/' . $pegawai['kd_pegawai']) ?>" title="Gaji">Penggajian</a> |
					<a class="icon" href="<?=site_url('pegawai/riwayatangkakredit/index/' . $pegawai['kd_pegawai']) ?>" title="AngkaKredit">Angka Kredit</a> |
					<a class="icon" href="<?=site_url('pegawai/riwayatjabatan/index/' . $pegawai['kd_pegawai']) ?>" title="Jabatan">Jabatan</a> |
					<a class="icon" href="<?=site_url('pegawai/riwayatpelatihan/index/' . $pegawai['kd_pegawai']) ?>" title="Pelatihan">Pelatihan</a> |
					<a class="icon" href="<?=site_url('pegawai/riwayatpendidikan/index/' . $pegawai['kd_pegawai']) ?>" title="Pendidikan">Pendidikan</a> |
					<a class="icon" href="<?=site_url('pegawai/pegawai/photo/' . $pegawai['kd_pegawai']) ?>" title="Foto">Foto</a> |
	
	<?php } ?>
					<a class="icon" href="<?=site_url('pegawai/pensiun/') ?>" title="Batal">
						<img class="noborder" src="<?=base_url() . 'public/images/cancel2.png' ?>"></a>
				</div>
			 </div>
			 <div class="clear"></div>
		</div>
		<? } ?>
    <br/>
    <div class="content-data">
        <form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?= $action_url; ?>">
            <table class="general">
                <tr align="left">
                    <td colspan="4"><div align="left" class="ch-title"><h3>Data Pribadi</h3></div></td>
                    <td width="119">&nbsp;</td>
                </tr>
                <tr>
					<td  align="left">NIP</td>
					<td>&nbsp;</td>
					<td width="300"  align="left">
					<input type='text' name='NIP' id='NIP' onchange="gsub_test(this)" onblur="gsub_test(this)" value='<?= set_value('NIP', $pegawai['NIP']); ?>'  class="js_required required"/>
					  <?php echo form_error('NIP'); ?> 
					</td>
					<td colspan="2" rowspan="6"  align="left">
						<a class="icon" href="<?=site_url('pegawai/pegawai/photo/' . $pegawai['kd_pegawai']) ?>" title="Foto"><img height="150" width="120" src="<?php echo $photo; ?>" /></a>
					</td>
                </tr>
                <tr>
					<td  align="left">NIP Lama</td>
					<td>&nbsp;</td>
					<td width="300"  align="left">
						<input type="text" name="nip_lama" id="nip_lama" onchange="gsub_test(this)" onblur="gsub_test(this)" value="<?= set_value('nip_lama', $pegawai['nip_lama']); ?>"  class="js_required required"/>
						<?php echo form_error('nip_lama'); ?> 
					</td>
                </tr>
                <tr>
					<td  align="left">Nama Lengkap</td>
					<td>&nbsp;</td>
					<td width="300"  align="left">
						<input  size='30' type="text" name="nama_pegawai" id="nama_pegawai" value="<?= set_value('nama', $pegawai['nama_pegawai']); ?>" class="required"/>
						<?php echo form_error('nama_pegawai'); ?> 
					</td>
                </tr>
                <tr>
					<td  align="left">Gelar Depan</td>
					<td>&nbsp;</td>
					<td width="300"  align="left">
						<input type="text" name="gelar_depan" id="gelar_depan" value="<?= set_value('gelar_depan', $pegawai['gelar_depan']); ?>" />
						<?php echo form_error('gelar_depan'); ?> 
					</td>
                </tr>
                <tr>
					<td  align="left">Gelar Belakang</td>
					<td>&nbsp;</td>
					<td width="300"  align="left">
						<input type="text" name="gelar_belakang" id="gelar_belakang" value="<?= set_value('gelar_belakang', $pegawai['gelar_belakang']); ?>" />
						<?php echo form_error('gelar_belakang'); ?> 
					</td>
                </tr>
                <tr>
					<td  align="left">Pendidikan Terakhir </td>
					<td>&nbsp;</td>
					<td width="300"  align="left">
						<?=$pendidikan_assoc[$pegawai['id_pendidikan_terakhir']] ?>
						, <?=$pegawai['ket_pendidikan'] ?>
					</td>
                </tr>
                <tr>
                    <td  align="left">Tempat dan Tanggal Lahir</td>
                    <td> : </td>
                    <td colspan="3" align="left">
					<?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$pegawai['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
					?>
				
                        <input size="50" type="text" class="required" class="js_required" name="tempat_lahir" id="tempat_lahir" value="<?= set_value('tempat_lahir', $tempat_lahir); ?>" />
						&nbsp;&nbsp;
                        <input size="10" type="text" class="datepicker" class="js_required" name="tgl_lahir" id="tgl_lahir" value="<?= set_value('tgl_lahir', $pegawai['tgl_lahir']); ?>" class="required"/>
						<?php echo form_error('tgl_lahir'); ?>&nbsp;<?php echo form_error('tgl_lahir'); ?> <span style="color:#F00;"><i> yyyy-mm-dd</i></span>
					</td>
                </tr>
                <tr>
                    <td  align="left">Jenis Kelamin</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <? echo form_dropdown('jns_kelamin', $gender_assoc, $pegawai['jns_kelamin'], 'class="js_required"'); ?>
						<?php echo form_error('jns_kelamin'); ?>			
					</td>
                </tr>
                <tr>
                    <td  align="left">Agama</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                     	<?php echo form_dropdown('kd_agama', $agama_assoc, $pegawai['kd_agama'], 'class="js_required"');
                        echo form_error('kd_agama'); ?>
					</td>
                </tr>
                <tr>
                    <td  align="left">Status Perkawinan</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <?php
                        echo form_dropdown('status_kawin', $status_perkawinan_assoc, $pegawai['status_kawin'], 'class="js_required"');
                        echo form_error('status_kawin');
                        ?>			
					</td>
                </tr>
                <tr>
                    <td width ="111" align="left">Alamat</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
						<textarea rows="3" id="alamat" name="alamat" cols="25"><?=set_value('alamat', $pegawai['alamat'])?> </textarea>
						<?php echo form_error('alamat'); ?>			
					</td>
                </tr>
                <tr>
                    <td  align="left">RT/RW</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input size='5' type='text' name='rt' id='rt' value='<?= set_value('rt', $pegawai['RT']); ?>'/><?php echo form_error('rt'); ?>
                        <input size='5' type='text' name='rw' id='rw' value='<?= set_value('rw', $pegawai['RW']); ?>'/><?php echo form_error('rw'); ?>
					</td>
                </tr>
                
				<tr>
					<td  align="left">Kelurahan</td>
					<td> : </td>
					<td colspan="3"  align="left">
						<?=form_group_dropdown('kd_kelurahan',$option_kecamatan,$pegawai['kd_kelurahan']);?><?php echo form_error('kd_kelurahan'); ?>	
					</td>
                </tr>
                <tr>
                    <td  align="left">Kode Pos</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input type="text" name="kode_pos" id="kode_pos" value="<?= set_value('kode_pos', $pegawai['kode_pos']);?>" /><?php echo form_error('kode_pos'); ?>                	</td>
                </tr>
                <tr>
                    <td  align="left">Telepon</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input name="no_telp" id="no_telp" width="200px" type="text" value="<?= set_value('no_telp', $pegawai['no_telp']); ?>" />
                        <?php echo form_error('no_telp'); ?>			
					</td>
                </tr>
                <tr>
                    <td  align="left">Golongan Darah</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
						<?php
                        echo form_dropdown('gol_darah', $gol_darah_assoc, $pegawai['gol_darah'], 'class="js_required"');
                        echo form_error('gol_darah');
						?>			
					</td>
                </tr>

                </tr>
                <tr>
                    <td colspan="5"  align="left">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5"  align="left"><div align="left" class="ch-title"><h3>Data Kepegawaian</h3></div></td>
                </tr>
                <tr>
                    <td  align="left">Status Asal</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
						<?php
                        echo form_dropdown('status_asal', $status_asal_assoc, $pegawai['status_asal'], 'class="js_required"');
                        echo form_error('status_asal');
					?>			
					</td>
                </tr>
                </tr>
                <tr>
                    <td  align="left">Jenis Pegawai</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
						<?php
                        echo form_dropdown('id_jns_pegawai', $jenis_pegawai_assoc, $pegawai['id_jns_pegawai'], 'class="js_required"');
                        echo form_error('id_jns_pegawai');
						?>			
					</td>
                </tr>
                </tr>
                <tr>
                    <td  align="left">Status Pegawai</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
							<?php
                        echo form_dropdown('status_pegawai', $status_pegawai_assoc, $pegawai['status_pegawai'], 'class="js_required"');
                        echo form_error('status_pegawai');
						?>			
					</td>
                </tr>
                <tr>
                    <td  align="left">Golongan/Pangkat</td>
                    <td>:</td>
                    <td colspan="3"  align="left"><?=$golongan_assoc[$pegawai['id_golpangkat_terakhir']]?></td>
                </tr>
                <tr>
                    <td  align="left">Jabatan</td>
                    <td>:</td>
                    <td colspan="3"  align="left"><?=$jabatan_assoc[$pegawai['id_jabatan_terakhir']] ?></td>
                </tr>
                <tr>
                    <td  align="left">Unit Kerja</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
						<?php
                        echo form_group_dropdown('kode_unit',$option_unit,$pegawai['unit_kerja']);
                        echo form_error('kode_unit');
					?>			
					</td>
                </tr>
                <tr>
                    <td  align="left">TMT Masuk Unit Kerja</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
						<input size="10" type="text" class="datepicker" class="js_required" name="tmt_unit_terakhir" id="tmt_unit_kerja" value="<?= set_value('tmt_unit_terakhir', $pegawai['tmt_unit_terakhir']);?>" class="required" /> 
						<?php echo form_error('tmt_unit_terakhir'); ?>			<span style="color:#F00;"><i> yyyy-mm-dd</i></span>
					</td>
                </tr>
                <tr>
					<td  align="left">TMT Kenaikan Pangkat</td>
					<td> : </td>
					<td colspan="3" align="left">
						<input size="10" type="text" class="datepicker" class="js_required" name="tmt_golpangkat_terakhir" id="tmt_golpangkat_terakhir" value="<?= set_value('tmt_golpangkat_terakhir', $pegawai['tmt_golpangkat_terakhir']); ?>" class="required" />
						<?php echo form_error('tmt_golpangkat_terakhir'); ?> 	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>		
					</td>
				</tr>

				<tr>
					<td  align="left">TMT Kenaikan Gaji Berkala</td>
					<td> : </td>
					<td colspan="3" align="left">
						<input size="10" type="text" class="datepicker" class="js_required" name="tmt_kgb" id="tmt_kgb" value="<?= set_value('tmt_kgb', $pegawai['tmt_kgb']); ?>" class="required" />
						<?php echo form_error('tmt_kgb'); ?> 		<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	
					</td>
				</tr>

                <tr>
                    <td  align="left">Nomor SK CPNS</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input size='30' name="no_sk_cpns" id="no_sk_cpns" width="200px" type="text" value="<?= set_value('no_sk_cpns', $pegawai['no_sk_cpns']); ?>" />
						<?php echo form_error('no_sk_cpns'); ?>			
					</td>
                </tr>

                <tr>
                    <td  align="left">TMT CPNS</td>
                    <td> : </td>
                    <td colspan="3" align="left">
                        <input size="10" type="text" class="datepicker" class="js_required" name="tmt_cpns" id="tmt_cpns" value="<?= set_value('tmt_cpns', $pegawai['tmt_cpns']); ?>" class="required" />
                        <?php echo form_error('tmt_cpns'); ?> 		<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	
					</td>
                </tr>

                <tr>
                    <td  align="left">Nomor SK PNS</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input size='30' name="no_sk_pns" id="no_sk_pns" width="200px" type="text" value="<?= set_value('no_sk_pns', $pegawai['no_sk_pns']); ?>" />
						<?php echo form_error('no_sk_pns'); ?>			
					</td>
                </tr>

                <tr>
                    <td  align="left">TMT PNS</td>
                    <td> : </td>
                    <td colspan="3" align="left">
                        <input size="10" type="text" class="datepicker" class="js_required" name="tmt_pns" id="tmt_pns" value="<?= set_value('tmt_pns', $pegawai['tmt_pns']); ?>" class='required'/>
                        <?php echo form_error('tmt_pns'); ?> <span style="color:#F00;"><i> yyyy-mm-dd</i></span>
					</td>
                </tr>

               
                <tr>
                    <td  align="left">Loker</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input size='5' name="loker" id="loker" width="200px" type="text" value="<?= set_value('loker', $pegawai['loker']); ?>" />
<?php echo form_error('loker'); ?>			</td>
                </tr>
                <tr>
                    <td  align="left">NPWP</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input size='30' name="npwp" id="npwp" width="200px" type="text" value="<?= set_value('npwp', $pegawai['npwp']); ?>" />
                        <?php echo form_error('npwp'); ?>			</td>
                </tr>
                <tr>
                    <td  align="left">Tanggal NPWP</td>
                    <td> : </td>
                    <td colspan="3" align="left">
                        <input size="10" type="text" class="datepicker" class="js_required" name="tgl_npwp" id="tgl_npwp" value="<?= set_value('tgl_npwp', $pegawai['tgl_npwp']); ?>" class="required" />
                        <?php echo form_error('tgl_npwp'); ?> <span style="color:#F00;"><i> yyyy-mm-dd</i></span>	
					</td>
                </tr>
                <tr>
                    <td  align="left">Eselon</td>
                    <td>:</td>
                    <td colspan="3"  align="left">
						<?php
                        echo form_dropdown('eselon', $eselon_assoc, $pegawai['eselon'], 'class="js_required"');
                        echo form_error('eselon');
                        ?>
					</td>
                </tr>
                <tr>
                    <td  align="left">Angka Kredit </td>
                    <td>:</td>
                    <td colspan="3"  align="left">
						<input name="angka_kredit" id="angka_kredit" width="200px" type="text" value="<?= set_value('angka_kredit', $pegawai['angka_kredit']); ?>" />
						<?php echo form_error('angka_kredit'); ?>	
					</td>
                </tr>
                <tr>
                    <td  align="left">Karpeg</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input name="karpeg" id="karpeg" width="200px" type="text" value="<?= set_value('karpeg', $pegawai['karpeg']); ?>" />
						<?php echo form_error('karpeg'); ?>			
					</td>
                </tr>
                <tr>
                    <td  align="left">No. ASKES</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input size='30' name="no_ASKES" id="no_ASKES" width="200px" type="text" value="<?= set_value('no_ASKES', $pegawai['no_ASKES']); ?>"/>
						<?php echo form_error('no_ASKES'); ?>			
					</td>
                </tr>
                <tr>
                    <td  align="left">Taspen</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input size='30' name="taspen" id="taspen" width="200px" type="text" value="<?= set_value('taspen', $pegawai['taspen']); ?>" />
						<?php echo form_error('taspen'); ?>			
					</td>
                </tr>
			</table>
               <br />
			<table class="general">
                <tr align="left">
                    <td colspan="4"><div align="left" class="ch-title"><h3>Data Pensiun</h3></div></td>
                    <td width="119">&nbsp;</td>
                </tr>
				 <tr>
                    <td  align="left">Tanggal Pensiun</td>
                    <td> : </td>
                    <td colspan="3" align="left">
                        <input size="10" type="text" class="datepicker" class="js_required" name="tgl_resign" id="tgl_resign" value="<?= set_value('tgl_resign', $pegawai['tgl_resign']); ?>" class="required"/>
<?php echo form_error('tgl_resign'); ?> 	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>		
					</td>
                </tr>
				<tr>
                    <td  align="left">Nomor SK Pensiun</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input size='30' name="no_sk_pensiun" id="no_sk_pensiun" width="200px" type="text" value="<?= set_value('no_sk_pensiun', $pegawai['no_sk_pensiun']); ?>" />
						<?php echo form_error('no_sk_pensiun'); ?>			
					</td>
                </tr>
                <tr>
                    <td  align="left">Tanggal SK Pensiun</td>
                    <td> : </td>
                    <td colspan="3" align="left">
                        <input size="10" type="text" class="datepicker" class="js_required" name="tgl_sk_pensiun" id="tgl_sk_pensiun" value="<?= set_value('tgl_sk_pensiun', $pegawai['tgl_sk_pensiun']); ?>" class="required"/>
						<?php echo form_error('tgl_sk_pensiun'); ?> 	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>		
					</td>
                </tr>
				<tr>
                    <td  align="left">Keterangan Pensiun</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
                        <input size="80" name="ket_pensiun" id="ket_pensiun" width="200px" type="text" value="<?= set_value('ket_pensiun', $pegawai['ket_pensiun']); ?>" />
						<?php echo form_error('ket_pensiun'); ?>			
					</td>
                </tr>
			</table>
            <br>
            <div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
        </form>
    </div>
    <div class="clear"></div>
</div>
