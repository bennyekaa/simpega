<?php
	$total_header_page2 = 30;
	$add_page=1;
	$diklat_kosong='0';
	$baris_awal = 52; // jika cp 1.5 //35 jika cp=0
	$hitungbaris=0;
	$pjkarakter=0;
	$hal =1 ;
?> 
		<table width="519" class="general">
				<tr align="left">
					<td width="319" align="right">	
					<h5><br>
					</h5>					</td>
				    <td width="200" align="left">KEPUTUSAN KEPALA BADAN <br>
KEPEGAWAIAN NEGARA<br>
NOMOR	&nbsp;&nbsp;&nbsp;:&nbsp;	11 TAHUN 2002<br>
TANGGAL	:	17 Juni 2002</td>
				</tr>
</table>
		<table width="519" class="general">
				<tr align="left">
					<td width="511" align="center">	
					<h1>DAFTAR RIWAYAT HIDUP</h1>	
					</td>
				</tr>
</table>
		<h3>I.	KETERANGAN PERORANGAN </h3>
		<table border="1" cellspacing="0" cellpadding="1.5"  width="511">
				<tr>
					<td width="20"><p align="center">1.</p></td>
					<td width="250" colspan="2"><p>&nbsp;Nama Lengkap</p></td>
					<td width="286"><p>&nbsp;<?=$gelar_depan.' '.$nama_peg.$gelar_belakang?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">2.</p></td>
					<td width="250" colspan="2"><p>&nbsp;NIP</p></td>
					<td width="286"><p>&nbsp;<?=$NIP;?></p></td>
				</tr>
				
				<tr>
					<td width="20"><p align="center">3.</p></td>
					<td width="250" colspan="2"><p>&nbsp;Pangkat dan Golongan Ruang</p></td>
					<td width="286"><p>&nbsp;<?=trim($pangkat)?>, <?=trim($golongan)?></p></td>
				</tr>
				
				<tr>
					<td width="20"><p align="center">4.</p></td>
					<td width="250" colspan="2"><p>&nbsp;Tempat Lahir/Tgl. Lahir</p></td>
					<?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$pegawai['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
					?>
					<td width="286"><p>&nbsp;<?=$tempat_lahir?>, <?= date('d M Y',strtotime($tanggal)); ?></p></td>
				</tr>
				<?php
				if ($tmt_cpns=='0000-00-00') {
					$str_tmt_cpns = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $tmt_cpns);
					$str_tmt_cpns = $d . "-" . $m . "-" . $y;
					}
					$tmt_cpns=$str_tmt_cpns;
				?>
				
				<tr>
					<td width="20"><p align="center">5.</p></td>
					<td width="250" colspan="2"><p>&nbsp;Jenis Kelamin</p></td>
					<td width="286"><p>&nbsp;<?=$jenis;?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">6.</p></td>
					<td width="250" colspan="2"><p>&nbsp;Agama</p></td>
					<td width="286"><p>&nbsp;<?=$agama?></p></td>
				</tr>
				
				<tr>
					<td width="20"><p align="center">7.</p></td>
					<td width="250" colspan="2"><p>&nbsp;Status Perkawinan</p></td>
					<td width="286"><p>&nbsp;<?=$status_perkawinan_assoc[$status_kawin];?></p></td>
				</tr>
				<tr>
					<td width="20" rowspan="5"><p align="center">8.</p></td>
					<td width="100" rowspan="5"><p>&nbsp;Alamat &nbsp;Rumah</p></td>
					<td width="150">&nbsp;a.&nbsp; Jalan</td>
					<td width="286"><p>&nbsp;<?=$alamat;?>&nbsp;<? if ($rt!=''){?> RT.<?=$rt;?>&nbsp;<? } if ($rw!=''){?> RW. <? } ?><?=$rw;?></p></td>
				</tr>
				<tr>
					<td width="150">&nbsp;b.&nbsp; Kelurahan/Desa</td>
					<td width="286"><p>&nbsp;<?= $kelurahan_assoc[$kelurahan_id]; ?></p></td>
				</tr>
				<?php
					  $query = mysql_query("SELECT `kd_kelurahan`,nama_kelurahan,nama_kecamatan,nama_kabupaten,nama_propinsi FROM `kelurahan`,kecamatan,kabupaten WHERE kelurahan.kd_kecamatan=kecamatan.kd_kecamatan and kecamatan.kd_kabupaten=kabupaten.kd_kabupaten 
and kd_kelurahan='".$pegawai['kd_kelurahan']."'");					
						if ($query) {
							$dataalamat=mysql_fetch_array($query); 
							$kec = $dataalamat['nama_kecamatan'];
							$kab = $dataalamat['nama_kabupaten'];
							$prop = $dataalamat['nama_propinsi'];
						}
				?>
				<tr>
					<td width="150"><p>&nbsp;c.&nbsp; Kecamatan</p></td>
					<td width="286"><p>&nbsp;<?= $kec;?></p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;d.&nbsp; Kabupaten/Kota</p></td>
					<td width="286"><p>&nbsp;<?= $kab;?></p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;e.&nbsp; Propinsi</p></td>
					<td width="286"><p>&nbsp;<?= $prop;?></p></td>
				</tr>
				
				<tr>
					<td width="20" rowspan="7"><p align="center">9.</p></td>
					<td width="100" rowspan="7"><p>&nbsp;Keterangan &nbsp;Badan</p></td>
					<td width="150">&nbsp;a.&nbsp; Tinggi    (cm)</td>
					<td width="286"><p>&nbsp;<?=$drh_tinggi;?></p></td>
				</tr>
				<tr>
					<td width="150">&nbsp;b.&nbsp; Berat Badan (kg)</td>
					<td width="286"><p>&nbsp;<?=$drh_bb;?></p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;c.&nbsp; Rambut</p></td>
					<td width="286"><p>&nbsp;<?=$drh_rambut;?></p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;d.&nbsp; Bentuk Muka</p></td>
					<td width="286"><p>&nbsp;<?=$drh_bentuk_muka;?></p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;e.&nbsp; Warna Kulit</p></td>
					<td width="286"><p>&nbsp;<?=$drh_warna_kulit;?></p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;f.&nbsp; Ciri-ciri Khas</p></td>
					<td width="286"><p>&nbsp;<?=$drh_ciri_ciri;?></p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;g.&nbsp; Cacat Tubuh</p></td>
					<td width="286"><p>&nbsp;<?=$drh_cacat;?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">10.</p></td>
					<td width="250" colspan="2"><p>&nbsp;Kegemaran (Hobby)</p></td>
					<td width="286"><p>&nbsp;<?=$hobi_kesenian;?><?=$hobi_olahraga;?><?=$hobi_lain;?></p></td>
				</tr>
				
</table>
			
		

		<h3>II.	PENDIDIKAN<!-- <?=$baris_awal;?>--> </h3>
		<h4>1. Pendidikan di Dalam dan di Luar Negeri  </h4>
			<table border="1" cellspacing="0" cellpadding="0" width="559">
				<tr>
					<td width="20"><p align="center"><strong>NO.</strong></p></td>
					<td width="45"><p align="center"><strong>TINGKAT</strong></p></td>
					<td width="104"><p align="center"><strong>NAMA PENDIDIKAN</strong></p></td>
					<td width="80"><p align="center"><strong>JURUSAN</strong></p></td>
					<td width="95"><p align="center"><strong>STTB/TANDA LULUS/IJAZAH TAHUN</strong></p></td>
					<td width="95"><p align="center"><strong>TEMPAT</strong></p></td>
					<td width="120"><p align="center"><strong>NAMA KEPALA SEKOLAH/DIREKTUR/<br />DEKAN/PROMOTOR</strong></p></td>
				</tr>
<tr>
					<td width="20"><p align="center">1</p></td>
					<td width="45"><p align="center">2</p></td>
					<td width="104"><p align="center">3</p></td>
					<td width="80"><p align="center">4</p></td>
					<td width="95"><p align="center">5</p></td>
					<td width="95"><p align="center">6</p></td>
					<td width="120"><p align="center">7</p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pendidikan!=FALSE){
				foreach ($list_pendidikan as $pendidikan) {
				$i++;
				if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="45">&nbsp;<?= $pendidikan_assoc[$pendidikan['id_pendidikan']]; ?></td>
					<td width="104">&nbsp;<?= $pendidikan['universitas']; ?></td>
					<td width="80">&nbsp;<?= $pendidikan['prodi']; ?></td>
					<td width="95" align="center"><?= $pendidikan['th_lulus']; ?></td>
					<td width="95" align="center">&nbsp;<?= $pendidikan['tempat_studi']; ?></td>
					<td width="120">&nbsp;</td>
				</tr>
				<? }}?>
			</table>
            <h4>2. Kursus/Latihan di Dalam dan di Luar Negeri <!-- <?=$baris_awal;?>--></h4>
				
			<table border="1" cellspacing="0" cellpadding="0" width="559">
				<tr>
					<td width="20"><p align="center"><strong>NO.</strong></p></td>
					<td width="205"><p align="center"><strong><br />
					NAMA KURSUS/LATIHAN </strong></p></td>
					<td width="75"><p align="center"><strong>LAMANYA/TGL/BLN/THN s.d. </strong><strong>TGL/BLN/THN</strong></p></td>
					<td width="75"><p align="center"><strong>IJAZAH/TANDA LULUS/SURAT KETERANGAN TAHUN</strong></p></td>
					<td width="100"><p align="center"><strong><br />TEMPAT</strong></p></td>
					<td width="84"><p align="center"><strong><br />KETERANGAN</strong></p></td>
				</tr>
<tr>
					<td width="20"><p align="center">1</p></td>
					<td width="205"><p align="center">2</p></td>
					<td width="75"><p align="center">3</p></td>
					<td width="75"><p align="center">4</p></td>
					<td width="100"><p align="center">5</p></td>
					<td width="84"><p align="center">6</p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pelatihan!=FALSE){
					foreach ($list_pelatihan as $pelatihans) {
					$i++;
					$pjkarakter =strlen($pelatihans['nama_pelatihan']);
					if ($pjkarakter<45) {
						$hitungbaris=$hitungbaris; 
						$pjkarakter =strlen($pelatihans['tempat_pelatihan']);
						if ($pjkarakter<23) $hitungbaris=$hitungbaris; else $hitungbaris = $hitungbaris +(ceil($pjkarakter/23)-1);
						}
					else {
						$hitungbaris = $hitungbaris+(ceil($pjkarakter/45)-1);
						}					
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="205"><?= $pelatihans['nama_pelatihan'];?></td>
					<td width="75">&nbsp;<?= $pelatihans['lama_hari'];?></td>
					<td width="75" align="center"><?= $pelatihans['tahun']; ?></td>
					<td width="100">&nbsp;<?= $pelatihans['tempat_pelatihan']; ?></td>
					<td width="84">&nbsp;<!--<?=$pjkarakter?> baris <?=$hitungbaris?>--></td>
				</tr>
<? }}?>
		</table>
			
<h3>III.	RIWAYAT PEKERJAAN   <!--<?=$baris_awal;?> inya <?=$i?> hb <?=$hitungbaris?> sisa baris <?=$sisa_baris?>--></h3>
		<h4>1. Riwayat Kepangkatan/Golongan Ruang Penggajian</h4> 
			
			<table border="1" cellspacing="0" cellpadding="0" width="559">
				<tr>
				  <td width="20" rowspan="2"><p align="center"><strong>NO</strong></p></td>
					<td width="110" rowspan="2"><p align="center"><strong>PANGKAT</strong></p></td>
					<td width="40" rowspan="2"><p align="center"><strong>GOL. RUANG PENGGAJIAN </strong></p></td>
					<td width="60" rowspan="2"><p align="center"><strong>BERLAKU TERHITUNG MULAI TANGGAL</strong></p></td>
					<td width="45" rowspan="2"><p align="center"><strong>GAJI POKOK</strong></p></td>
					<td width="220" colspan="3" valign="top"><p align="center"><strong>SURAT KEPUTUSAN</strong></p></td>
					<td width="65" rowspan="2"><p align="center"><strong>PERATURAN YANG DIJADIKAN DASAR </strong></p></td>
				</tr>
				<tr>
				  <td width="60" align="center"><p><strong>PEJABAT</strong></p></td>
					<td width="100" align="center"><p><strong>NOMOR</strong></p></td>
					<td width="60" align="center"><p><strong>TANGGAL</strong></p></td>
				</tr>
<tr>
				  <td width="20" rowspan="2"><p align="center">1</p></td>
					<td width="110" rowspan="2"><p align="center">2</p></td>
					<td width="40" rowspan="2"><p align="center">3</p></td>
					<td width="60" rowspan="2"><p align="center">4</p></td>
					<td width="45" rowspan="2"><p align="center">5</p></td>
					<td width="60" align="center"><p>6</p></td>
					<td width="100" align="center"><p>7</p></td>
					<td width="60" align="center"><p>8</p></td>
					<td width="65" rowspan="2"><p align="center">9</p></td>
				</tr>
				<tr>
				  
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pangkat!=FALSE){
				foreach ($list_pangkat as $jns_pangkat) {
				$i++;
				
				if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
				if ($jns_pangkat['tgl_SK_pangkat']=='0000-00-00') {
				$str_sk_pangkat = '-';
				}
				else {
				list($y, $m, $d) = explode('-', $jns_pangkat['tgl_SK_pangkat']);
				$str_sk_pangkat = $d . "-" . $m . "-" . $y;
				}
				$jns_pangkat['tgl_SK_pangkat']=$str_sk_pangkat;
				if ($jns_pangkat['tmt_pangkat']=='0000-00-00') {
				$str_tmt_pangkat = '-';
				}
				else {
				list($y, $m, $d) = explode('-', $jns_pangkat['tmt_pangkat']);
				$str_tmt_pangkat = $d . "-" . $m . "-" . $y;
				}
				$jns_pangkat['tmt_pangkat']=$str_tmt_pangkat;
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="110">&nbsp;<?= $pangkat_assoc[$jns_pangkat['id_golpangkat']]; ?></td>
					<td width="40" align="center">&nbsp;<?= $golongan_assoc[$jns_pangkat['id_golpangkat']]; ?></td>
					<td width="60">&nbsp;<?= $jns_pangkat['tmt_pangkat']; ?></td>
					<td width="45" align="right"><?=number_format($jns_pangkat['gaji_pokok']);?></td>
					<td width="60">&nbsp;<?= $jns_pangkat['pejabat']; ?></td>
					<td width="100">&nbsp;<?= $jns_pangkat['no_SK_pangkat']; ?></td>
					<td width="60">&nbsp;<?= $jns_pangkat['tgl_SK_pangkat']; ?></td>
					<td width="65">&nbsp;</td>
				</tr>
				<? }}
				else {
				$baris_awal=$baris_awal+2;
				//jika kosong tambahkan kolom kosong 2 baris
				?>
				<!--<tr>
					<td width="20" align="center">&nbsp;1.</td>
					<td width="110">&nbsp;</td>
					<td width="40" align="center">&nbsp;</td>
					<td width="60">&nbsp;</td>
					<td width="45" align="right">&nbsp;</td>
					<td width="60">&nbsp;</td>
					<td width="100">&nbsp;</td>
					<td width="60">&nbsp;</td>
					<td width="65">&nbsp;</td>
				</tr>
				<tr>
					<td width="20" align="center">&nbsp;2.</td>
					<td width="110">&nbsp;</td>
					<td width="40" align="center">&nbsp;</td>
					<td width="60">&nbsp;</td>
					<td width="45" align="right">&nbsp;</td>
					<td width="60">&nbsp;</td>
					<td width="100">&nbsp;</td>
					<td width="60">&nbsp;</td>
				</tr>-->
				<? }?>
			</table>
		
		    <h4>2. Pengalaman Jabatan/Pekerjaan <!--<?=$baris_awal?>--></h4> 
				<table border="1" cellspacing="0" cellpadding="0" width="595">
				<tr>
					<td width="20" rowspan="2"><p align="center"><strong>NO</strong></p></td>
					<td width="105" rowspan="2"><p align="center"><strong>JABATAN/PEKERJAAN </strong></p></td>
					<td width="75" rowspan="2"><p align="center"><strong>MULAI DAN SAMPAI</strong></p></td>
<td width="70" rowspan="2"><p align="center"><strong>GOL. RUANG PENGGAJIAN</strong></p></td>
<td width="70" rowspan="2"><p align="center"><strong>GAJI POKOK</strong></p></td>					
<td width="220" colspan="3" valign="top"><p align="center"><strong>SURAT</strong><strong> KEPUTUSAN</strong></p></td>
					
				</tr>
				<tr>
					<td width="60" align="center"><p><strong>PEJABAT</strong></p></td>
					<td width="100" align="center"><p><strong>NOMOR</strong></p></td>
					<td width="60" align="center"><p><strong>TANGGAL</strong></p></td>
				</tr>
<tr>
					<td width="20" ><p align="center">1</p></td>
					<td width="105" ><p align="center">2</p></td>
					<td width="75" ><p align="center">3</p></td>
<td width="70" ><p align="center">4</p></td>
<td width="70" ><p align="center">5</p></td>					

					<td width="60" align="center"><p>6</p></td>
					<td width="100" align="center"><p>7</p></td>
					<td width="60" align="center"><p>8</p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_jabatan_struktural!=FALSE){
				foreach ($list_jabatan_struktural as $jns_jabatan_struktural) {
				$pjkarakter =strlen($unit_kerja_assoc[$jns_jabatan_struktural['kode_unit']]);
				if ($pjkarakter<18) $hitungbaris=$hitungbaris; else $hitungbaris = $hitungbaris + (ceil($pjkarakter/18)-1);
				
				if ($jabatan_struktural_assoc[$jns_jabatan_struktural['id_jabatan_s']]!='') {
					$i++;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_jabatan_struktural['tgl_sk_jabatan']=='0000-00-00') {
					$str_sk_jabatan_s = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_jabatan_struktural['tgl_sk_jabatan']);
					$str_sk_jabatan_s = $d . "-" . $m . "-" . $y;
					}
					$jns_jabatan_struktural['tgl_sk_jabatan']=$str_tmt_jabatan_s;
					
					if ($jns_jabatan_struktural['tmt_jabatan']=='0000-00-00') {
					$str_tmt_jabatan_s = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_jabatan_struktural['tmt_jabatan']);
					$str_tmt_jabatan_s = $d . "-" . $m . "-" . $y;
					}
					$jns_jabatan_struktural['tmt_jabatan']=$str_tmt_jabatan_s;
					?>
					<tr>
						<td width="20" align="center"><?=$i;?></td>
						<td width="105">&nbsp;<?= rtrim($jabatan_struktural_assoc[$jns_jabatan_struktural['id_jabatan_s']]); ?></td>
						<td width="75">&nbsp;<?= rtrim($jns_jabatan_struktural['tahun_mulai']); ?> s.d <?= rtrim($jns_jabatan_struktural['tahun_selesai']); ?></td>
<td width="70"></td>
<td width="70"></td>
						<td width="60">&nbsp;Rektor</td>
						<td width="100" align="center"><?= trim($jns_jabatan_struktural['no_sk_jabatan']); ?></td>
						<td width="60">&nbsp;<?= rtrim($jns_jabatan_struktural['tgl_sk_jabatan']); ?></td>
						 <!--<?=$hitungbaris;?>-->
					</tr>
				
				<? }}}?>
				<?php 
				if ($i==0){
					$baris_awal=$baris_awal+2;
					//jika kosong tambahkan kolom kosong 2 baris
					?>
					<!--<tr>
						<td width="20">&nbsp;1.</td>
						<td width="105">&nbsp;</td>
						<td width="75">&nbsp;</td>
						<td width="80">&nbsp;</td>
						<td width="130">&nbsp;</td>
						<td width="60">&nbsp;</td>
						<td width="100">&nbsp;</td>
					</tr>
					<tr>
						<td width="20">&nbsp;2.</td>
						<td width="105">&nbsp;</td>
						<td width="75">&nbsp;</td>
						<td width="80">&nbsp;</td>
						<td width="130">&nbsp;</td>
						<td width="60">&nbsp;</td>
						<td width="100">&nbsp;</td>
					</tr>-->
				<? }?>	
			</table>
			
                <h4>IV.	TANDA  JASA/PENGHARGAAN 
                  <!-- <?= $baris_awal;?>-->
                </h4>
				
				<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20" align="center"><p align="center"><strong>NO</strong></p></td>
					<td width="290"><p align="center"><strong>NAMA    BINTANG/SATYA LENCANA/PENGHARGAAN</strong></p></td>
					<td width="75"><p align="center"><strong>TAHUN    PEROLEHAN</strong></p></td>
					<td width="175"><p align="center"><strong>NAMA    NEGARA/INSTANSI YANG MEMBERI</strong></p></td>
				</tr>
<tr>
					<td width="20" align="center"><p align="center">1</p></td>
					<td width="290"><p align="center">2</p></td>
					<td width="75"><p align="center">3</p></td>
					<td width="175"><p align="center">4</p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_penghargaan!=FALSE){
				foreach ($list_penghargaan as $penghargaan) {
				$i++;
				if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="290">&nbsp;<?= $jenis_penghargaan_assoc[$penghargaan['jenis_penghargaan']]; ?></td>
					<td width="75">&nbsp;<?= $penghargaan['tahun']; ?></td>
					<td width="175">&nbsp;<?= $penghargaan['keterangan']; ?></td>
				</tr>
				<? }}
				else {
				$baris_awal=$baris_awal+2;
				//jika kosong tambahkan kolom kosong 2 baris
				?>
				<!--<tr>
					<td width="20">&nbsp;1.</td>
					<td width="300">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="175">&nbsp;</td>
				</tr>
				<tr>
					<td width="20">&nbsp;2.</td>
					<td width="300">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="175">&nbsp;</td>
				</tr>-->
				<? }?>
			</table>
			
                <h4>V.	PENGALAMAN KUNJUNGAN KE LUAR NEGERI
                  <!-- <?= $baris_awal;?>-->
                </h4>
                <table border="1" cellspacing="0" cellpadding="0" width="570">
                  <tr>
                    <td width="20" align="center"><p align="center"><strong>NO</strong></p></td>
                    <td width="100"><p align="center"><strong>NEGARA</strong></p></td>
                    <td width="240"><p align="center"><strong>TUJUAN KUNJUNGAN </strong></p></td>
                    <td width="100"><p align="center"><strong>LAMANYA</strong></p></td>
		    <td width="100"><p align="center"><strong>YANG MEMBIAYAI</strong></p></td>
                  </tr>
<tr>
                    <td width="20" align="center"><p align="center">1</p></td>
                    <td width="100"><p align="center">2</p></td>
                    <td width="240"><p align="center">3</p></td>
                    <td width="100"><p align="center">4</p></td>
		    <td width="100"><p align="center">5</p></td>
                  </tr>
                  <?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_penghargaan!=FALSE){
				foreach ($list_penghargaan as $penghargaan) {
				$i++;
				if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
				?>
                  <tr>
                    <td width="20" align="center">&nbsp;</td>
                    <td width="100">&nbsp;</td>
                    <td width="240">&nbsp;</td>
                    <td width="100">&nbsp;</td>
						<td width="100">&nbsp;
                        </td>
                  </tr>
<tr>
                    <td width="20" align="center">&nbsp;</td>
                    <td width="100">&nbsp;</td>
                    <td width="240">&nbsp;</td>
                    <td width="100">&nbsp;</td>
						<td width="100">&nbsp;
                        </td>
                  </tr>
                  <? }}
				else {
				$baris_awal=$baris_awal+2;
				//jika kosong tambahkan kolom kosong 2 baris
				?>
                  <!--<tr>
					<td width="20">&nbsp;1.</td>
					<td width="300">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="175">&nbsp;</td>
				</tr>
				<tr>
					<td width="20">&nbsp;2.</td>
					<td width="300">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="175">&nbsp;</td>
				</tr>-->
                  <? }?>
                </table>
            <h3>VI. KETERANGAN  KELUARGA 
              <!--<?=$baris_awal?>-->
            </h3>
           <h4>1. ISTERI/SUAMI </h4> 
			<table border="1" cellspacing="0" cellpadding="0" width="595">
				<tr>
					<td width="20" align="center"><strong>NO</strong></td>
					<td width="100" align="center"><strong>NAMA</strong></td>
					<td width="80" align="center"><strong>TEMPAT  LAHIR</strong></td>
					<td width="80" align="center"><strong>TANGGAL  LAHIR</strong></td>
					<td width="80" align="center"><strong>TANGGAL MENIKAH</strong></td>
					<td width="120" align="center"><strong>PEKERJAAN</strong></td>
					<td width="80" align="center"><strong>KETERANGAN</strong></td>
				</tr>
<tr>
					<td width="20" align="center">1</td>
					<td width="100" align="center">2</td>
					<td width="80" align="center">3</td>
					<td width="80" align="center">4</td>
					<td width="80" align="center">5</td>
					<td width="120" align="center">6</td>
					<td width="80" align="center">7</td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pasangan!=FALSE){
				foreach ($list_pasangan as $jns_keluarga) {
					$i++;
					$jml_baris_pasangan = $jml_baris_penghargaan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_keluarga['tanggal_lahir']=='0000-00-00') {
					$str_tanggal_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_lahir']);
					$str_tanggal_lahir = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_lahir']=$str_tanggal_lahir;
					
					if ($jns_keluarga['tanggal_nikah']=='0000-00-00') {
					$str_tanggal_nikah = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_nikah']);
					$str_tanggal_nikah = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_nikah']=$str_tanggal_nikah;
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="100">&nbsp;<?= $jns_keluarga['nama_pasangan']; ?></td>
					<td width="80">&nbsp;<?= $jns_keluarga['tempat_lahir']; ?></td>
					<td width="80">&nbsp;<?= $jns_keluarga['tanggal_lahir']; ?></td>
					<td width="80">&nbsp;<?= $jns_keluarga['tanggal_nikah']; ?></td>
					<td width="120"><?= $jns_keluarga['pekerjaan']; ?></td>
					<td width="80">&nbsp;</td>
				</tr>
				<? }}?>
			</table>


             <h4>2. ANAK <!--<?=$baris_awal?> sisa baris = <?=$sisa_baris?> hal = <?=$hal?> --></h4>
			<table border="1" cellspacing="0" cellpadding="0" width="595">
				<tr>
					<td width="20" align="center"><p 

align="center"><strong>NO</strong></p></td>
					<td width="135"><p align="center"><strong>NAMA</strong></p></td>
					<td width="75"><p align="center"><strong>JENIS KELAMIN</strong></p></td>
					<td width="80"><p align="center"><strong>TEMPAT LAHIR</strong></p></td>
					<td width="80"><p align="center"><strong>TANGGAL LAHIR</strong></p></td>
					
					<td width="90"><p align="center"><strong>PEKERJAAN</strong></p></td>
					<td width="80"><p align="center"><strong>KETERANGAN</strong></p></td>
				</tr>
<tr>
					<td width="20" align="center"><p 

align="center">1</p></td>
					<td width="135"><p align="center">2</p></td>
					<td width="75"><p align="center">3</p></td>
					<td width="80"><p align="center">4</p></td>
					<td width="80"><p align="center">5</p></td>
					
					<td width="90"><p align="center">6</p></td>
					<td width="80"><p align="center">7</p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_anak!=FALSE){
				foreach ($list_anak as $jns_anak) {
					$i++;
					$jml_baris_anak = $jml_baris_pasangan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_anak['tgl_lahir']=='0000-00-00') {
					$str_tgl_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_anak['tgl_lahir']);
					$str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
					}
					$jns_anak['tgl_lahir']=$str_tgl_lahir;
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="135">&nbsp;<?= $jns_anak['nama_anak']; ?></td>
					<td width="75">&nbsp;<?= $gender_assoc[$jns_anak['jns_kelamin']]; ?></td>
					<td width="80">&nbsp;<?= $jns_anak['tempat_lahir']; ?></td>
					<td width="80">&nbsp;<?= $jns_anak['tgl_lahir']; ?></td>
					<td width="90">&nbsp;<?= $pendidikan_assoc[$jns_anak['id_pendidikan_anak']]; ?><?= $jns_anak['pekerjaan']; ?></td>
					<td width="80">&nbsp; <!--baris awal <?=$baris_awal+$hitungbaris+$i;?> hal= <?=$hal?> --></td>
				</tr>
		<? }}?>
		</table>
			<br />

        <h4>3. Bapak dan Ibu Kandung 
        <!--<?=$baris_awal?>--></h4>
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20" align="center"><p align="center"><strong>NO</strong></p></td>
					<td width="200"><p align="center"><strong>NAMA</strong></p></td>
					
					<td width="120"><p align="center"><strong>TEMPAT DAN TANGGAL LAHIR</strong></p></td>
					
					<td width="120"><p align="center"><strong>PEKERJAAN</strong></p></td>
					<td width="100"><p align="center"><strong>KETERANGAN</strong></p></td>
				</tr>
<tr>
					<td width="20" align="center"><p align="center">1</p></td>
					<td width="200"><p align="center">2</p></td>
					
					<td width="120"><p align="center">3</p></td>
					
					<td width="120"><p align="center">4</p></td>
					<td width="100"><p align="center">5</p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_ortukandung!=FALSE){
				foreach ($list_ortukandung as $jns_keluarga) {
					$i++;
					$jml_baris_keluarga = $jml_baris_pasangan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_keluarga['tgl_lahir']=='0000-00-00') {
					$str_tgl_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tgl_lahir']);
					$str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
					}
					$jns_keluarga['tgl_lahir']=$str_tgl_lahir;
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="200">&nbsp;<?= $jns_keluarga['nama_keluarga']; ?></td>
					
					<td width="120">&nbsp;<?= $jns_keluarga['tempat_lahir']; ?>, <?= $jns_keluarga['tgl_lahir']; ?></td>
					
					<td width="120">&nbsp;<?= $jns_keluarga['pekerjaan']; ?></td>
					<td width="100">&nbsp;</td>
				</tr>
		<? }}?>
			</table>
		
            <h4>4. Bapak dan Ibu Mertua <!--<?=$baris_awal?>--></h4>
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20" align="center"><p align="center"><strong>NO</strong></p></td>
					<td width="200"><p align="center"><strong>NAMA</strong></p></td>
					
					<td width="120"><p align="center"><strong>TEMPAT DAN TANGGAL LAHIR</strong></p></td>
					
					<td width="120"><p align="center"><strong>PEKERJAAN</strong></p></td>
					<td width="100"><p align="center"><strong>KETERANGAN</strong></p></td>
				</tr>
<tr>
					<td width="20" align="center"><p align="center">1</p></td>
					<td width="200"><p align="center">2</p></td>
					
					<td width="120"><p align="center">3</p></td>
					
					<td width="120"><p align="center">4</p></td>
					<td width="100"><p align="center">5</p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_mertua!=FALSE){
				foreach ($list_mertua as $list_mertua) {
					$i++;
					$jml_baris_keluarga = $jml_baris_pasangan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($list_mertua['tgl_lahir']=='0000-00-00') {
					$str_tgl_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $list_mertua['tgl_lahir']);
					$str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
					}
					$list_mertua['tgl_lahir']=$str_tgl_lahir;
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="200">&nbsp;<?= $list_mertua['nama_keluarga']; ?></td>
					
					<td width="120">&nbsp;<?= $list_mertua['tempat_lahir']; ?>, <?= $list_mertua['tgl_lahir']; ?></td>
					
					<td width="120">&nbsp;<?= $list_mertua['pekerjaan']; ?></td>
					<td width="100">&nbsp;</td>
				</tr>
		<? }}?>
			</table>

            <h4>5. Saudara Kandung <!--<?=$baris_awal?>--></h4>
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20" align="center"><p align="center"><strong>NO</strong></p></td>
					<td width="155"><p align="center"><strong>NAMA</strong></p></td>
					<td width="75"><p align="center"><strong>JENIS KELAMIN</strong></p></td>
					<td width="100"><p align="center"><strong>TEMPAT DAN TANGGAL LAHIR</strong></p></td>
					
					<td width="110"><p align="center"><strong>PEKERJAAN</strong></p></td>
					<td width="100"><p align="center"><strong>KETERANGAN</strong></p></td>
				</tr>
<tr>
					<td width="20" align="center"><p align="center">1</p></td>
					<td width="155"><p align="center">2</p></td>
					<td width="75"><p align="center">3</p></td>
					<td width="100"><p align="center">4</p></td>
					
					<td width="110"><p align="center">5</p></td>
					<td width="100"><p align="center">6</p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_saudara!=FALSE){
				foreach ($list_saudara as $list_saudara) {
					$i++;
					$jml_baris_keluarga = $jml_baris_pasangan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($list_saudara['tgl_lahir']=='0000-00-00') {
					$str_tgl_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $list_saudara['tgl_lahir']);
					$str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
					}
					$list_saudara['tgl_lahir']=$str_tgl_lahir;
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="155">&nbsp;<?= $list_saudara['nama_keluarga']; ?></td>
					<td width="75">&nbsp;<?= $gender_assoc[$list_saudara['jns_kelamin']]; ?></td>
					<td width="100">&nbsp;<?= $list_saudara['tempat_lahir']; ?>, <?= $list_saudara['tgl_lahir']; ?></td>
					
					<td width="110">&nbsp;<?= $list_saudara['pekerjaan']; ?></td>
					<td width="100">&nbsp;</td>
				</tr>
		<? }}?>
			</table>

            <h3>VII. KETERANGAN  ORGANISASI
              <!--<?=$baris_awal?>-->
            </h3>
            <h4>1. Semasa mengikuti pendidikan pada SLTA ke bawah </h4>
            <table border="1" cellspacing="0" cellpadding="0" width="560">
              <tr>
                <td width="20" align="center"><strong>NO</strong></td>
                <td width="195" align="center"><strong>NAMA ORGANISASI </strong></td>
                <td width="80" align="center"><strong>KEDUDUKAN DALAM ORGANISASI </strong></td>
                <td width="80" align="center"><strong>DALAM TH s.d. TH </strong></td>
                <td width="75" align="center"><strong>TEMPAT</strong></td>
                <td width="110" align="center"><strong>NAMA PIMPINAN ORGANISASI </strong></td>
                
              </tr>
<tr>
                <td width="20" align="center">1</td>
                <td width="195" align="center">2</td>
                <td width="80" align="center">3</td>
                <td width="80" align="center">4</td>
                <td width="75" align="center">5</td>
                <td width="110" align="center">6</td>
                
              </tr>
              <?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pasangan!=FALSE){
				foreach ($list_pasangan as $jns_keluarga) {
					$i++;
					$jml_baris_pasangan = $jml_baris_penghargaan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_keluarga['tanggal_lahir']=='0000-00-00') {
					$str_tanggal_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_lahir']);
					$str_tanggal_lahir = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_lahir']=$str_tanggal_lahir;
					
					if ($jns_keluarga['tanggal_nikah']=='0000-00-00') {
					$str_tanggal_nikah = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_nikah']);
					$str_tanggal_nikah = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_nikah']=$str_tanggal_nikah;
				?>
              <tr>
                <td width="20" align="center">&nbsp;</td>
                <td width="195">&nbsp;</td>
                <td width="80">&nbsp;</td>
                <td width="80">&nbsp;</td>
                <td width="75">&nbsp;</td>
                <td width="110">&nbsp;</td>
                
              </tr>
              <? }}?>
            </table>
            <h4>2. Semasa mengikuti pendidikan pada Perguruan Tinggi </h4>
            <table border="1" cellspacing="0" cellpadding="0" width="560">
              <tr>
                <td width="20" align="center"><strong>NO</strong></td>
                <td width="195" align="center"><strong>NAMA ORGANISASI </strong></td>
                <td width="80" align="center"><strong>KEDUDUKAN DALAM ORGANISASI </strong></td>
                <td width="80" align="center"><strong>DALAM TH s.d. TH </strong></td>
                <td width="75" align="center"><strong>TEMPAT</strong></td>
                <td width="110" align="center"><strong>NAMA PIMPINAN ORGANISASI </strong></td>
              </tr>
<tr>
                <td width="20" align="center">1</td>
                <td width="195" align="center">2</td>
                <td width="80" align="center">3</td>
                <td width="80" align="center">4</td>
                <td width="75" align="center">5</td>
                <td width="110" align="center">6</td>
              </tr>
              <?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pasangan!=FALSE){
				foreach ($list_pasangan as $jns_keluarga) {
					$i++;
					$jml_baris_pasangan = $jml_baris_penghargaan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_keluarga['tanggal_lahir']=='0000-00-00') {
					$str_tanggal_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_lahir']);
					$str_tanggal_lahir = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_lahir']=$str_tanggal_lahir;
					
					if ($jns_keluarga['tanggal_nikah']=='0000-00-00') {
					$str_tanggal_nikah = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_nikah']);
					$str_tanggal_nikah = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_nikah']=$str_tanggal_nikah;
				?>
              <tr>
                <td width="20" align="center">&nbsp;</td>
                <td width="195">&nbsp;</td>
                <td width="80">&nbsp;</td>
                <td width="80">&nbsp;</td>
                <td width="75">&nbsp;</td>
                <td width="110">&nbsp;</td>
              </tr>
              <? }}?>
            </table>
            <h4>2. Sesudah selesai pendidikan dan atau selama menjadi pegawai </h4>
            <table border="1" cellspacing="0" cellpadding="0" width="560">
              <tr>
                <td width="20" align="center"><strong>NO</strong></td>
                <td width="195" align="center"><strong>NAMA ORGANISASI </strong></td>
                <td width="80" align="center"><strong>KEDUDUKAN DALAM ORGANISASI </strong></td>
                <td width="80" align="center"><strong>DALAM TH s.d. TH </strong></td>
                <td width="75" align="center"><strong>TEMPAT</strong></td>
                <td width="110" align="center"><strong>NAMA PIMPINAN ORGANISASI </strong></td>
              </tr>
<tr>
                <td width="20" align="center">1</td>
                <td width="195" align="center">2</td>
                <td width="80" align="center">3</td>
                <td width="80" align="center">4</td>
                <td width="75" align="center">5</td>
                <td width="110" align="center">6</td>
              </tr>
              <?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pasangan!=FALSE){
				foreach ($list_pasangan as $jns_keluarga) {
					$i++;
					$jml_baris_pasangan = $jml_baris_penghargaan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_keluarga['tanggal_lahir']=='0000-00-00') {
					$str_tanggal_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_lahir']);
					$str_tanggal_lahir = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_lahir']=$str_tanggal_lahir;
					
					if ($jns_keluarga['tanggal_nikah']=='0000-00-00') {
					$str_tanggal_nikah = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_nikah']);
					$str_tanggal_nikah = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_nikah']=$str_tanggal_nikah;
				?>
              <tr>
                <td width="20" align="center">&nbsp;</td>
                <td width="195">&nbsp;</td>
                <td width="80">&nbsp;</td>
                <td width="80">&nbsp;</td>
                <td width="75">&nbsp;</td>
                <td width="110">&nbsp;</td>
              </tr>
              <? }}?>
            </table>
            <h3>VIII. KETERANGAN  LAIN-LAIN
              <!--<?=$baris_awal?>-->
</h3>
            <table border="1" cellspacing="0" cellpadding="0" width="560">
              <tr>
                <td width="20" rowspan="2"><p align="center"><strong>NO</strong></p></td>
                <td width="270" rowspan="2"><p align="center"><strong>NAMA KETERANGAN  </strong></p></td>
                
                <td width="270" colspan="3" valign="top"><p align="center"><strong>SURAT</strong><strong> KETERANGAN</strong></p></td>
              </tr>
              <tr>
                <td width="80" align="center"><p><strong>PEJABAT</strong></p></td>
                <td width="130" align="center"><p><strong>NOMOR</strong></p></td>
                <td width="60" align="center"><p><strong>TANGGAL</strong></p></td>
              </tr>
<tr>
                <td width="20"><p align="center">1</p></td>
                <td width="270"><p align="center">2</p></td>
                
               
                <td width="80" align="center"><p>3</p></td>
                <td width="130" align="center"><p>4</p></td>
                <td width="60" align="center"><p>5</p></td>
              </tr>
<tr>
                <td width="20" align="center"> 1.</td>
                <td width="270">KETERANGAN BERKELAKUAN BAIK </td>
                
                <td width="80">&nbsp;</td>
                <td width="130" align="center">&nbsp;</td>
                <td width="60">&nbsp;</td>
                <!--<?=$hitungbaris;?>-->
              </tr>
              <tr>
                <td align="center">2.</td>
                <td>KETERANGAN BERBADAN SEHAT </td>
                <td>&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center">3.</td>
                <td COLSPAN="4"><p>KETERANGAN LAIN YANG DIANGGAP PERLU </p>
		<br><br>
                </td>
                
              </tr>
              <?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_jabatan_struktural!=FALSE){
				foreach ($list_jabatan_struktural as $jns_jabatan_struktural) {
				$pjkarakter =strlen($unit_kerja_assoc[$jns_jabatan_struktural['kode_unit']]);
				if ($pjkarakter<18) $hitungbaris=$hitungbaris; else $hitungbaris = $hitungbaris + (ceil($pjkarakter/18)-1);
				
				if ($jabatan_struktural_assoc[$jns_jabatan_struktural['id_jabatan_s']]!='') {
					$i++;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_jabatan_struktural['tgl_sk_jabatan']=='0000-00-00') {
					$str_sk_jabatan_s = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_jabatan_struktural['tgl_sk_jabatan']);
					$str_sk_jabatan_s = $d . "-" . $m . "-" . $y;
					}
					$jns_jabatan_struktural['tgl_sk_jabatan']=$str_tmt_jabatan_s;
					
					if ($jns_jabatan_struktural['tmt_jabatan']=='0000-00-00') {
					$str_tmt_jabatan_s = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_jabatan_struktural['tmt_jabatan']);
					$str_tmt_jabatan_s = $d . "-" . $m . "-" . $y;
					}
					$jns_jabatan_struktural['tmt_jabatan']=$str_tmt_jabatan_s;
					?>
              
              <? }}}?>
              <?php 
				if ($i==0){
					$baris_awal=$baris_awal+2;
					//jika kosong tambahkan kolom kosong 2 baris
					?>
              <!--<tr>
						<td width="20">&nbsp;1.</td>
						<td width="105">&nbsp;</td>
						<td width="75">&nbsp;</td>
						<td width="80">&nbsp;</td>
						<td width="130">&nbsp;</td>
						<td width="60">&nbsp;</td>
						<td width="100">&nbsp;</td>
					</tr>
					<tr>
						<td width="20">&nbsp;2.</td>
						<td width="105">&nbsp;</td>
						<td width="75">&nbsp;</td>
						<td width="80">&nbsp;</td>
						<td width="130">&nbsp;</td>
						<td width="60">&nbsp;</td>
						<td width="100">&nbsp;</td>
					</tr>-->
              <? }?>
            </table>
            
            <table width="560">
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;<!--baris awal <?=$baris_awal;?>-->
				    Demikian daftar riwayat hidup ini saya buat  dengan sesungguhnya dan apabila dikemudian hari terdapat keterangan yang tidak  benar saya bersedia dituntut dimuka pengadilan, serta bersedia menerima segala  tindakan yang diambil oleh Pemerintah.</td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>
					<td width="36">&nbsp;</td>
					<td width="25">&nbsp;</td>
					<td width="279" colspan="3" align="left">&nbsp;</td>
					<td colspan="5" width="255" align="left">Malang, <?= $tglsurat; ?></td>
				</tr>
				<tr>
					<td width="36">&nbsp;</td>
					<td width="25">&nbsp;</td>
					<td width="279" colspan="3" align="left">&nbsp;</td>
					<td colspan="5" width="255" align="left">Yang membuat, </td>
				</tr>
				<tr>
					<td colspan="5" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td width="36">&nbsp;</td>
					<td width="25">&nbsp;</td>
					<td width="279" colspan="3" align="left">&nbsp;</td>
					<td colspan="5" width="255" align="left"><?=$gelar_depan.' '.$nama_peg.$gelar_belakang?></td>
				</tr>
				<tr>
					<td width="36">&nbsp;</td>
					<td width="25">&nbsp;</td>
					<td width="279" colspan="3" align="left">&nbsp;</td>
					<td colspan="5" width="255" align="left">NIP. <?=$NIP;?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="3" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="3" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="3" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5">&nbsp;<strong>PERHATIAN : </strong></td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					 	
				  <td colspan="11" align="left"><ol>
				    <li>Harus ditulis dengan <strong>tangan sendiri</strong>,  menggunakan <strong>huruf kapital/balok</strong> dan <strong>tinta hitam</strong>.</li>
				    <li>Jika ada yang salah harus dicoret, yang dicoret tersebut tetap terbaca,  kemudian yang benar dituliskan diatas atau dibawahnya dan diparaf.</li>
				    <li>Kolom yang kosong diberi tanda <strong>&ndash;</strong>.</li>
				  </ol>
			      </td>
				</tr>
			</table>
