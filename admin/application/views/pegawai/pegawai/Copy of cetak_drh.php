<?php
	$total_header_page2 = 30;
	$add_page=1;
	$diklat_kosong='0';
	$baris_awal = 52; // jika cp 1.5 //35 jika cp=0
	$hitungbaris=0;
	$pjkarakter=0;
	$hal =1 ;
?> 
		<table width="519" class="general">
				<tr align="left">
					<td width="511" align="center">	
					<h1>DAFTAR RIWAYAT HIDUP</h1>	
					</td>
				</tr>
</table>
		<h3>I.	KETERANGAN PERORANGAN</h3>
		<table border="1" cellspacing="0" cellpadding="1.5">
				<tr>
					<td width="20"><p align="center">1.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Nama Lengkap</p></td>
					<td width="300"><p>&nbsp;<?=$gelar_depan.' '.$nama_peg.$gelar_belakang?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">2.</p></td>
					<td width="205" colspan="2"><p>&nbsp;NIP/Karpeg</p></td>
					<td width="300"><p>&nbsp;<?=$NIP;?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">3.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Nomor KTP</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">4.</p></td>
					<td width="205" colspan="2"><p>&nbsp;NPWP</p></td>
					<td width="300"><p>&nbsp;<?=$NPWP;?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">5.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Nama kecil(Panggilan)</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">6.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Pangkat dan Golongan Ruang</p></td>
					<td width="300"><p>&nbsp;<?=trim($pangkat)?>, <?=trim($golongan)?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">7.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Jabatan</p></td>
					<td width="300"><p>&nbsp;<?=$non_jabatan?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">8.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Tempat dan Tanggal Lahir</p></td>
					<td width="300"><p>&nbsp;<?=$tempat?>, <?= date('d M Y',strtotime($tanggal)); ?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">9.</p></td>
					<td width="205" colspan="2"><p>&nbsp;TMT CPNS</p></td>
					<td width="300"><p>&nbsp;<?=$tmt_cpns?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">10.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Masa Kerja Seluruhnya s.d saat ini</p></td>
					<td width="300"><p>&nbsp;<?=$masa_kerja_seluruhnya?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">11.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Agama</p></td>
					<td width="300"><p>&nbsp;<?=$agama?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">12.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Jenis Kelamin</p></td>
					<td width="300"><p>&nbsp;<?=$jenis;?></p></td>
				</tr>
				<tr>
					<td width="20"><p align="center">13.</p></td>
					<td width="205" colspan="2"><p>&nbsp;Status Perkawinan</p></td>
					<td width="300"><p>&nbsp;Belum Kawin / Kawin / Janda / Duda</p></td>
				</tr>
				<tr>
					<td width="20" rowspan="6"><p align="center">14.</p></td>
					<td width="55" rowspan="6"><p>&nbsp;Alamat Rumah</p></td>
					<td width="150">&nbsp;a.&nbsp; Jalan/Nomor Rumah</td>
					<td width="300"><p>&nbsp;<?=$alamat;?>&nbsp;<?=$rt;?>&nbsp;<?=$rw;?></p></td>
				</tr>
				<tr>
					<td width="150">&nbsp;b.&nbsp; Kelurahan/Desa/Kode Pos</td>
					<td width="300"><p>&nbsp;<?= $kelurahan_assoc[$kelurahan_id]; ?></p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;c.&nbsp; Kecamatan</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;d.&nbsp; Kabupaten/Kotamadya</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;e.&nbsp; Propinsi</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;f.&nbsp; Telp/HP</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="20" rowspan="8"><p align="center">15.</p></td>
					<td width="55" rowspan="8"><p>&nbsp;Keterangan Badan</p></td>
					<td width="150">&nbsp;a.&nbsp; Tinggi    (cm)</td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150">&nbsp;b.&nbsp; Berat Badan (kg)</td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;c.&nbsp; Rambut</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;d.&nbsp; Bentuk Muka</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;e.&nbsp; Warna Kulit</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;f.&nbsp; Ciri-ciri Khas</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;g.&nbsp; Cacat Tubuh</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;h.&nbsp; Golongan Darah</p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="20" rowspan="3"><p align="center">16.</p></td>
					<td width="55" rowspan="3"><p>&nbsp;Hobby</p></td>
					<td width="150">&nbsp;a.&nbsp; Kesenian </td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150">&nbsp;b.&nbsp; Olahraga </td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
				<tr>
					<td width="150"><p>&nbsp;c.&nbsp; Lainnya </p></td>
					<td width="300"><p>&nbsp;</p></td>
				</tr>
</table>
			
<?php $baris_awal=$baris_awal+2;?>			

		<h3>II.	PENDIDIKAN<!-- <?=$baris_awal;?>--> </h3>
		<h4>1. PENDIDIKAN  DI DALAM DAN DI LUAR NEGERI  </h4>
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20"><p align="center"><strong>NO.</strong></p></td>
					<td width="45"><p align="center"><strong>TINGKAT</strong></p></td>
					<td width="160"><p align="center"><strong>NAMA PENDIDIKAN</strong></p></td>
					<td width="100"><p align="center"><strong>JURUSAN</strong></p></td>
					<td width="75"><p align="center"><strong>STTB/TANDA LULUS/IJAZAH TAHUN</strong></p></td>
					<td width="50"><p align="center"><strong>TEMPAT</strong></p></td>
					<td width="120"><p align="center"><strong>NAMA KEPALA SEKOLAH/DIREKTUR/<br />DEKAN/PROMOTOR</strong></p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pendidikan!=FALSE){
				foreach ($list_pendidikan as $pendidikan) {
				$i++;
				if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
				?>
				<tr>
					<td width="20">&nbsp;<?=$i;?></td>
					<td width="45">&nbsp;<?= $pendidikan_assoc[$pendidikan['id_pendidikan']]; ?></td>
					<td width="160">&nbsp;<?= $pendidikan['universitas']; ?></td>
					<td width="100">&nbsp;<?= $pendidikan['prodi']; ?></td>
					<td width="75" align="center"><?= $pendidikan['th_lulus']; ?></td>
					<td width="50">&nbsp;</td>
					<td width="120">&nbsp;</td>
				</tr>
				<? }}?>
			</table>
<?php 
//lakukan penambahan baris jika data kosong
if (($list_pelatihan==FALSE) and ($i>=5)){
$tambahbaris=8;
}
else {
$tambahbaris=0;
}
if (($baris_awal+$hitungbaris+$i+$tambahbaris)>=60) 
	$baris_awal=$baris_awal+$hitungbaris+$i+10;
	$sisa_baris = 0;
	if (($baris_awal>=62) and ($hal==1)) {
		$sisa_baris = ($baris_awal)-(62);
		if ($sisa_baris < 3) $sisa_baris=3;
		for($j = 1; $j <= ($sisa_baris); $j++)
		{
			echo '<br/>&nbsp;'; 
		}
		$baris_awal=1;
		$hitungbaris=0;
		$pjkarakter=0;
		$hal =2 ;
	}
else

	$baris_awal=$baris_awal+$hitungbaris+$i+6;
?>

			
		<h4>2. KURSUS/DIKLAT  DI DALAM DAN DI LUAR NEGERI <!-- <?=$baris_awal;?>--></h4>
				
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20"><p align="center"><strong>NO.</strong></p></td>
					<td width="205"><p align="center"><strong><br />NAMA KURSUS/DIKLAT</strong></p></td>
					<td width="75"><p align="center"><strong>LAMANYA TGL/BLN/THN s.d TGL/BLN/THN</strong></p></td>
					<td width="75"><p align="center"><strong>IJAZAH/TANDA LULUS SURAT KETERANGAN TAHUN</strong></p></td>
					<td width="100"><p align="center"><strong><br />TEMPAT</strong></p></td>
					<td width="95"><p align="center"><strong><br />KETERANGAN</strong></p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pelatihan!=FALSE){
					foreach ($list_pelatihan as $pelatihans) {
					$i++;
					$pjkarakter =strlen($pelatihans['nama_pelatihan']);
					if ($pjkarakter<45) {
						$hitungbaris=$hitungbaris; 
						$pjkarakter =strlen($pelatihans['tempat_pelatihan']);
						if ($pjkarakter<23) $hitungbaris=$hitungbaris; else $hitungbaris = $hitungbaris +(ceil($pjkarakter/23)-1);
						}
					else {
						$hitungbaris = $hitungbaris+(ceil($pjkarakter/45)-1);
						}					
				?>
				<tr>
					<td width="20">&nbsp;<?=$i;?></td>
					<td width="205">&nbsp;<?= $pelatihans['nama_pelatihan'];?></td>
					<td width="75">&nbsp;</td>
					<td width="75" align="center"><?= $pelatihans['tahun']; ?></td>
					<td width="100">&nbsp;<?= $pelatihans['tempat_pelatihan']; ?></td>
					<td width="95">&nbsp;<!--<?=$pjkarakter?> baris <?=$hitungbaris?>--></td>
				</tr>
<?php 
	if (($baris_awal+$hitungbaris+$i+8>=62) and ($hal==1)) {
		$sisa_baris=(($baris_awal+$hitungbaris+$i+8)-62);
		$hal =2 ;
		?>
		</table>
			<!--1<br />2<br />3<br />4<br />-->
			<br /><br /><br /><br />
		<table border="1" cellspacing="0" cellpadding="0" width="570">
		<?	
	}
	if (($baris_awal+$hitungbaris+$i+8>62) and ($hal==2)){
	$sisa_baris++;
	$baris_awal=$sisa_baris-8;
	}
?>				
				<? }}
				else {
				$baris_awal=$baris_awal+2;
				//jika kosong tambahkan kolom kosong 2 baris
				?>
				<tr>
					<td width="20">&nbsp;1.</td>
					<td width="205">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="75" align="center">&nbsp;</td>
					<td width="100">&nbsp;</td>
					<td width="95">&nbsp;</td>
				</tr>
				<tr>
					<td width="20">&nbsp;2.</td>
					<td width="205">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="75" align="center">&nbsp;</td>
					<td width="100">&nbsp;</td>
					<td width="95">&nbsp;</td>
				</tr>
				<? }?>
</table>
<?php 
//jika riwayat pekerjaan kosong maka dimasukkan dihalaman ke-2
if (($baris_awal<62) and ($hal==1) and ($baris_awal>=45)) {
	if (($i==0) and ($baris_awal>=45)) {
		$hitungbaris = (62-$baris_awal);
		$i = ($baris_awal-45) ;
	}
	$baris_awal=$baris_awal+$hitungbaris-$i+11;
}
else {
	$baris_awal=$baris_awal+$i+2;
}
?>
<?php 
	$sisa_baris = 0;
	if (($baris_awal>=62) and ($hal==1)) {
		$sisa_baris = ($baris_awal)-(62);
		if ($sisa_baris < 3) $sisa_baris=3;
		if ($sisa_baris > 5) $sisa_baris=2;
		for($j = 1; $j <= ($sisa_baris); $j++)
		{
			//echo 'baris awal '.$baris_awal. '- 62 = '.$sisa_baris.'<br/> 	'; 
			echo '<br/>&nbsp;'; 
		}
		$baris_awal=1;
		$hitungbaris=0;
		$pjkarakter=0;
		$hal =2 ;
	}
?>

		<h3>III.	RIWAYAT PEKERJAAN   <!--<?=$baris_awal;?> inya <?=$i?> hb <?=$hitungbaris?> sisa baris <?=$sisa_baris?>--></h3>
		<h4>1. RIWAYAT KEPANGKATAN/RUANG </h4> 
			
			<table border="1" cellspacing="0" cellpadding="0" width="840">
				<tr>
					<td width="20" rowspan="2"><p align="center"><strong>NO</strong></p></td>
					<td width="120" rowspan="2"><p align="center"><strong>PANGKAT</strong></p></td>
					<td width="50" rowspan="2"><p align="center"><strong>GOL. RUANG</strong></p></td>
					<td width="60" rowspan="2"><p align="center"><strong>TMT</strong></p></td>
					<td width="50" rowspan="2"><p align="center"><strong>GAJI POKOK</strong></p></td>
					<td width="270" colspan="3" valign="top"><p align="center"><strong>SURAT KETERANGAN</strong></p></td>
				</tr>
				<tr>
					<td width="80" align="center"><p><strong>PEJABAT</strong></p></td>
					<td width="130" align="center"><p><strong>NOMOR</strong></p></td>
					<td width="60" align="center"><p><strong>TANGGAL</strong></p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pangkat!=FALSE){
				foreach ($list_pangkat as $jns_pangkat) {
				$i++;
				
				if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
				if ($jns_pangkat['tgl_SK_pangkat']=='0000-00-00') {
				$str_sk_pangkat = '-';
				}
				else {
				list($y, $m, $d) = explode('-', $jns_pangkat['tgl_SK_pangkat']);
				$str_sk_pangkat = $d . "-" . $m . "-" . $y;
				}
				$jns_pangkat['tgl_SK_pangkat']=$str_sk_pangkat;
				if ($jns_pangkat['tmt_pangkat']=='0000-00-00') {
				$str_tmt_pangkat = '-';
				}
				else {
				list($y, $m, $d) = explode('-', $jns_pangkat['tmt_pangkat']);
				$str_tmt_pangkat = $d . "-" . $m . "-" . $y;
				}
				$jns_pangkat['tmt_pangkat']=$str_tmt_pangkat;
				?>
				<tr>
					<td width="20" align="center"><?=$i;?></td>
					<td width="120">&nbsp;
				    <?= $pangkat_assoc[$jns_pangkat['id_golpangkat']]; ?></td>
					<td width="50" align="center">&nbsp;<?= $golongan_assoc[$jns_pangkat['id_golpangkat']]; ?></td>
					<td width="60">&nbsp;<?= $jns_pangkat['tmt_pangkat']; ?></td>
					<td width="50" align="right"><?=number_format($jns_pangkat['gaji_pokok']);?></td>
					<td width="80">&nbsp;<?= $jns_pangkat['pejabat']; ?></td>
					<td width="130">&nbsp;<?= $jns_pangkat['no_SK_pangkat']; ?></td>
					<td width="60">&nbsp;<?= $jns_pangkat['tgl_SK_pangkat']; ?></td>
				</tr>
				<? }}
				else {
				$baris_awal=$baris_awal+2;
				//jika kosong tambahkan kolom kosong 2 baris
				?>
				<tr>
					<td width="20" align="center">&nbsp;1.</td>
					<td width="120">&nbsp;</td>
					<td width="50" align="center">&nbsp;</td>
					<td width="60">&nbsp;</td>
					<td width="50" align="right">&nbsp;</td>
					<td width="80">&nbsp;</td>
					<td width="130">&nbsp;</td>
					<td width="60">&nbsp;</td>
				</tr>
				<tr>
					<td width="20" align="center">&nbsp;2.</td>
					<td width="120">&nbsp;</td>
					<td width="50" align="center">&nbsp;</td>
					<td width="60">&nbsp;</td>
					<td width="50" align="right">&nbsp;</td>
					<td width="80">&nbsp;</td>
					<td width="130">&nbsp;</td>
					<td width="60">&nbsp;</td>
				</tr>
				<? }?>
			</table>
		
		<?
		if ($id_jns_pegawai == 1){
		?>

<?php $baris_awal=$baris_awal+$hitungbaris+$i+6;?>

		<h4>2. PENGALAMAN  JABATAN/PEKERJAAN <!--<?=$baris_awal?>--></h4> 
				<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20" rowspan="2"><p align="center"><strong>NO</strong></p></td>
					<td width="105" rowspan="2"><p align="center"><strong>JABATAN / PEKERJAAN</strong></p></td>
					<td width="75" rowspan="2"><p align="center"><strong>MULAI DAN SAMPAI</strong></p></td>
					<td width="270" colspan="3" valign="top"><p align="center"><strong>SURAT</strong><strong> KEPUTUSAN</strong></p></td>
					<td width="100" rowspan="2" valign="center"><p align="center"><strong>UNIT KERJA</strong></p></td>
				</tr>
				<tr>
					<td width="80" align="center"><p><strong>PEJABAT</strong></p></td>
					<td width="130" align="center"><p><strong>NOMOR</strong></p></td>
					<td width="60" align="center"><p><strong>TANGGAL</strong></p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_jabatan_struktural!=FALSE){
				foreach ($list_jabatan_struktural as $jns_jabatan_struktural) {
				$pjkarakter =strlen($unit_kerja_assoc[$jns_jabatan_struktural['kode_unit']]);
				if ($pjkarakter<18) $hitungbaris=$hitungbaris; else $hitungbaris = $hitungbaris + (ceil($pjkarakter/18)-1);
				
				if ($jabatan_struktural_assoc[$jns_jabatan_struktural['id_jabatan_s']]!='') {
					$i++;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_jabatan_struktural['tgl_sk_jabatan']=='0000-00-00') {
					$str_sk_jabatan_s = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_jabatan_struktural['tgl_sk_jabatan']);
					$str_sk_jabatan_s = $d . "-" . $m . "-" . $y;
					}
					$jns_jabatan_struktural['tgl_sk_jabatan']=$str_tmt_jabatan_s;
					
					if ($jns_jabatan_struktural['tmt_jabatan']=='0000-00-00') {
					$str_tmt_jabatan_s = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_jabatan_struktural['tmt_jabatan']);
					$str_tmt_jabatan_s = $d . "-" . $m . "-" . $y;
					}
					$jns_jabatan_struktural['tmt_jabatan']=$str_tmt_jabatan_s;
					?>
					<tr>
						<td width="20"><?=$i;?></td>
						<td width="105">&nbsp;<?= rtrim($jabatan_struktural_assoc[$jns_jabatan_struktural['id_jabatan_s']]); ?></td>
						<td width="75">&nbsp;<?= rtrim($jns_jabatan_struktural['tahun_mulai']); ?> s.d <?= rtrim($jns_jabatan_struktural['tahun_selesai']); ?></td>
						<td width="80">&nbsp;</td>
						<td width="130"><?= trim($jns_jabatan_struktural['no_sk_jabatan']); ?></td>
						<td width="60">&nbsp;<?= rtrim($jns_jabatan_struktural['tgl_sk_jabatan']); ?></td>
						<td width="100">&nbsp;<?= rtrim($unit_kerja_assoc[$jns_jabatan_struktural['kode_unit']]); ?> <!--<?=$hitungbaris;?>--></td>
					</tr>
				
				<? }}}?>
				<?php 
				if ($i==0){
					$baris_awal=$baris_awal+2;
					//jika kosong tambahkan kolom kosong 2 baris
					?>
					<tr>
						<td width="20">&nbsp;1.</td>
						<td width="105">&nbsp;</td>
						<td width="75">&nbsp;</td>
						<td width="80">&nbsp;</td>
						<td width="130">&nbsp;</td>
						<td width="60">&nbsp;</td>
						<td width="100">&nbsp;</td>
					</tr>
					<tr>
						<td width="20">&nbsp;2.</td>
						<td width="105">&nbsp;</td>
						<td width="75">&nbsp;</td>
						<td width="80">&nbsp;</td>
						<td width="130">&nbsp;</td>
						<td width="60">&nbsp;</td>
						<td width="100">&nbsp;</td>
					</tr>
				<? }?>	
			</table>
			
<?php $baris_awal=$baris_awal+$hitungbaris+$i+6;?>			

		<h4>3. PENGALAMAN JABATAN FUNGSIONAL <!--<?=$baris_awal?>--> </h4>
				
				<table border="1" cellspacing="0" cellpadding="0" width="570">
					<tr>
						<td width="20" rowspan="2"><p align="center"><strong>NO</strong></p></td>
						<td width="105" rowspan="2"><p align="center"><strong>JABATAN FUNGSIONAL</strong></p></td>
						<td width="75" rowspan="2"><p align="center"><strong>MULAI DAN SAMPAI</strong></p></td>
						<!--<td width="104" rowspan="2"><p align="center"><strong>GOL/ RUANG</strong></p></td>
						<td width="95" rowspan="2"><p align="center"><strong>GAJI POKOK</strong></p></td>-->
						<td width="270" colspan="3" valign="top"><p align="center"><strong>SURAT KEPUTUSAN</strong></p></td>
						<td width="100" rowspan="2" valign="center"><p align="center"><strong>UNIT KERJA</strong></p></td>
					</tr>
					<tr>
						<td width="80"><p align="center"><strong>PEJABAT</strong></p></td>
						<td width="130"><p align="center"><strong>NOMOR</strong></p></td>
						<td width="60"><p align="center"><strong>TANGGAL</strong></p></td>
					</tr>
					<?
					$i = 0;
					$hitungbaris=0;
					$pjkarakter=0;
					if ($list_jabatan!=FALSE){
					foreach ($list_jabatan as $jns_jabatan) {
					$pjkarakter =strlen($unit_kerja_assoc[$jns_jabatan['kode_unit']]);
					if ($pjkarakter<18) $hitungbaris=$hitungbaris; else $hitungbaris = $hitungbaris + (ceil($pjkarakter/18)-1);
					
					if ($jabatan_assoc[$jns_jabatan['id_jabatan']]!='') {
						$i++;
						$jml_baris_jabatan = $jml_baris_pangkat + $i;
						if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
						if ($jns_jabatan['tgl_sk_jabatan']=='0000-00-00') {
						$str_sk_jabatan = '-';
						}
						else {
						list($y, $m, $d) = explode('-', $jns_jabatan['tgl_sk_jabatan']);
						$str_sk_jabatan = $d . "-" . $m . "-" . $y;
						}
						$jns_jabatan['tgl_sk_jabatan']=$str_tmt_jabatan;
						
						if ($jns_jabatan['tmt_jabatan']=='0000-00-00') {
						$str_tmt_jabatan = '-';
						}
						else {
						list($y, $m, $d) = explode('-', $jns_jabatan['tmt_jabatan']);
						$str_tmt_jabatan = $d . "-" . $m . "-" . $y;
						}
						$jns_jabatan['tmt_jabatan']=$str_tmt_jabatan;
						
						if ($jns_jabatan['tgl_selesai']=='0000-00-00') {
						$str_tgl_selesai = '-';
						}
						else {
						list($y, $m, $d) = explode('-', $jns_jabatan['tgl_selesai']);
						$str_tgl_selesai = $d . "-" . $m . "-" . $y;
						}
						$jns_jabatan['tgl_selesai']=$str_tgl_selesai;
						?>
						<tr>
							<td width="20">&nbsp;<?=$i;?></td>
							<td width="105">&nbsp;<?= $jabatan_assoc[$jns_jabatan['id_jabatan']]; ?></td>
							<td width="75">&nbsp;<?= $jns_jabatan['tmt_jabatan']; ?> s.d <?= $jns_jabatan['tgl_selesai']; ?></td>
							<td width="80">&nbsp;</td>
							<td width="130">&nbsp;<?= $jns_jabatan['no_sk_jabatan']; ?></td>
							<td width="60">&nbsp;<?= $jns_jabatan['tgl_sk_jabatan']; ?></td>
							<td width="100">&nbsp;<?= $unit_kerja_assoc[$jns_jabatan['kode_unit']]; ?><!-- <?=$hitungbaris;?>--></td>
						</tr>
						<? }}}?>
					    <?php 
						if ($i==0){
							$baris_awal=$baris_awal+2;
							//jika kosong tambahkan kolom kosong 2 baris
							?>
							<tr>
								<td width="20">&nbsp;1.</td>
								<td width="105">&nbsp;</td>
								<td width="75">&nbsp;</td>
								<td width="80">&nbsp;</td>
								<td width="130">&nbsp;</td>
								<td width="60">&nbsp;</td>
								<td width="100">&nbsp;</td>
							</tr>
							<tr>
								<td width="20">&nbsp;2.</td>
								<td width="105">&nbsp;</td>
								<td width="75">&nbsp;</td>
								<td width="80">&nbsp;</td>
								<td width="130">&nbsp;</td>
								<td width="60">&nbsp;</td>
								<td width="100">&nbsp;</td>
							</tr>
						<? }?>
				</table>
		        <p>
  <? }?>
		          
  <?php $baris_awal=$baris_awal+$hitungbaris+$i+5;?>
  <?php 
	$sisa_baris =0;
	if (($baris_awal>=62) and ($hal==1)) {
	$sisa_baris = ($baris_awal)-62;
	if ($sisa_baris < 3) $sisa_baris=3;
	for($j = 1; $j <= ($sisa_baris); $j++)
	{
		//echo 'baris awal '.$baris_awal. '- 62 = '.$sisa_baris.'<br/> 	';
		echo '<br/>&nbsp;'; 
	}
	$baris_awal=1;
	$hitungbaris=0;
	$pjkarakter=0;
	$hal =2 ;
	}
?>
</p>
		        <h4>4. RIWAYAT MUTASI <!--<?=$baris_awal?>-->
                </h4>
		        <table border="1" cellspacing="0" cellpadding="0" width="573">
                  <tr>
                    <td width="36"><p align="center"><strong>NO</strong></p></td>
                    <td width="107"><p align="center"><strong>TMT</strong></p></td>
                    <td width="156"><p align="center"><strong>UNIT KERJA LAMA </strong></p></td>
                    <!--<td width="104" rowspan="2"><p align="center"><strong>GOL/ RUANG</strong></p></td>
						<td width="95" rowspan="2"><p align="center"><strong>GAJI POKOK</strong></p></td>-->
                    <td width="156" valign="top"><p align="center"><strong>UNIT KERJA BARU </strong></p>                      </td>
                    <td width="106" valign="center"><p align="center"><strong>GOL/RUANG</strong></p></td>
                  </tr>

                  <?
					$i = 0;
					$hitungbaris=0;
					$pjkarakter=0;
					if ($list_jabatan!=FALSE){
					foreach ($list_jabatan as $jns_jabatan) {
					$pjkarakter =strlen($unit_kerja_assoc[$jns_jabatan['kode_unit']]);
					if ($pjkarakter<18) $hitungbaris=$hitungbaris; else $hitungbaris = $hitungbaris + (ceil($pjkarakter/18)-1);
					
					if ($jabatan_assoc[$jns_jabatan['id_jabatan']]!='') {
						$i++;
						$jml_baris_jabatan = $jml_baris_pangkat + $i;
						if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
						if ($jns_jabatan['tgl_sk_jabatan']=='0000-00-00') {
						$str_sk_jabatan = '-';
						}
						else {
						list($y, $m, $d) = explode('-', $jns_jabatan['tgl_sk_jabatan']);
						$str_sk_jabatan = $d . "-" . $m . "-" . $y;
						}
						$jns_jabatan['tgl_sk_jabatan']=$str_tmt_jabatan;
						
						if ($jns_jabatan['tmt_jabatan']=='0000-00-00') {
						$str_tmt_jabatan = '-';
						}
						else {
						list($y, $m, $d) = explode('-', $jns_jabatan['tmt_jabatan']);
						$str_tmt_jabatan = $d . "-" . $m . "-" . $y;
						}
						$jns_jabatan['tmt_jabatan']=$str_tmt_jabatan;
						
						if ($jns_jabatan['tgl_selesai']=='0000-00-00') {
						$str_tgl_selesai = '-';
						}
						else {
						list($y, $m, $d) = explode('-', $jns_jabatan['tgl_selesai']);
						$str_tgl_selesai = $d . "-" . $m . "-" . $y;
						}
						$jns_jabatan['tgl_selesai']=$str_tgl_selesai;
						?>
                  <tr>
                    <td width="36">&nbsp;
                        <?=$i;?></td>
                    <td width="107">&nbsp;
                        <?= $jabatan_assoc[$jns_jabatan['id_jabatan']]; ?></td>
                    <td width="156">&nbsp;
                        <?= $jns_jabatan['tmt_jabatan']; ?></td>
                    <td width="156"> <?= $jns_jabatan['tgl_selesai']; ?></td>
                    <td width="106">&nbsp;
                        <?= $unit_kerja_assoc[$jns_jabatan['kode_unit']]; ?>
                      <!-- <?=$hitungbaris;?>--></td>
                  </tr>
                  <? }}}?>
                  <?php 
						if ($i==0){
							$baris_awal=$baris_awal+2;
							//jika kosong tambahkan kolom kosong 2 baris
							?>
                  <tr>
                    <td width="36">&nbsp;1.</td>
                    <td width="107">&nbsp;</td>
                    <td width="156">&nbsp;</td>
                    <td width="156">&nbsp;</td>
                    <td width="106">&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="36">&nbsp;2.</td>
                    <td width="107">&nbsp;</td>
                    <td width="156">&nbsp;</td>
                    <td width="156">&nbsp;</td>
                    <td width="106">&nbsp;</td>
                  </tr>
                  <? }?>
                </table>
		        <h4>5. TUPOKSI <!--<?=$baris_awal?>-->
                </h4>
		        <table border="1" cellspacing="0" cellpadding="0" width="525">
                  <tr>
                    <td width="25"><p align="center"><strong>NO</strong></p></td>
                    <td width="175"><p align="center"><strong>RUMUSAN TUGAS </strong></p></td>
                    <td width="250"><p align="center"><strong>RINCIAN</strong></p>                    </td>
                    <!--<td width="104" rowspan="2"><p align="center"><strong>GOL/ RUANG</strong></p></td>
						<td width="95" rowspan="2"><p align="center"><strong>GAJI POKOK</strong></p></td>-->
                    <td width="100" valign="center"><p align="center"><strong>UNIT KERJA</strong></p></td>
                  </tr>
                  
                  <?
					$i = 0;
					$hitungbaris=0;
					$pjkarakter=0;
					if ($list_jabatan!=FALSE){
					foreach ($list_jabatan as $jns_jabatan) {
					$pjkarakter =strlen($unit_kerja_assoc[$jns_jabatan['kode_unit']]);
					if ($pjkarakter<18) $hitungbaris=$hitungbaris; else $hitungbaris = $hitungbaris + (ceil($pjkarakter/18)-1);
					
					if ($jabatan_assoc[$jns_jabatan['id_jabatan']]!='') {
						$i++;
						$jml_baris_jabatan = $jml_baris_pangkat + $i;
						if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
						if ($jns_jabatan['tgl_sk_jabatan']=='0000-00-00') {
						$str_sk_jabatan = '-';
						}
						else {
						list($y, $m, $d) = explode('-', $jns_jabatan['tgl_sk_jabatan']);
						$str_sk_jabatan = $d . "-" . $m . "-" . $y;
						}
						$jns_jabatan['tgl_sk_jabatan']=$str_tmt_jabatan;
						
						if ($jns_jabatan['tmt_jabatan']=='0000-00-00') {
						$str_tmt_jabatan = '-';
						}
						else {
						list($y, $m, $d) = explode('-', $jns_jabatan['tmt_jabatan']);
						$str_tmt_jabatan = $d . "-" . $m . "-" . $y;
						}
						$jns_jabatan['tmt_jabatan']=$str_tmt_jabatan;
						
						if ($jns_jabatan['tgl_selesai']=='0000-00-00') {
						$str_tgl_selesai = '-';
						}
						else {
						list($y, $m, $d) = explode('-', $jns_jabatan['tgl_selesai']);
						$str_tgl_selesai = $d . "-" . $m . "-" . $y;
						}
						$jns_jabatan['tgl_selesai']=$str_tgl_selesai;
						?>
                  <tr>
                    <td width="25">&nbsp;
                        <?=$i;?></td>
                    <td width="175">&nbsp;
                        <?= $jabatan_assoc[$jns_jabatan['id_jabatan']]; ?></td>
                    <td width="250">&nbsp;<?= $jns_jabatan['tgl_sk_jabatan']; ?></td>
                    <td width="100">&nbsp;
                        <?= $unit_kerja_assoc[$jns_jabatan['kode_unit']]; ?>
                        <!-- <?=$hitungbaris;?>--></td>
                  </tr>
                  <? }}}?>
                  <?php 
						if ($i==0){
							$baris_awal=$baris_awal+2;
							//jika kosong tambahkan kolom kosong 2 baris
							?>
                  <tr>
                    <td width="25">&nbsp;1.</td>
                    <td width="175">&nbsp;</td>
                    <td width="250">&nbsp;</td>
                    <td width="100">&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="25">&nbsp;2.</td>
                    <td width="175">&nbsp;</td>
                    <td width="250">&nbsp;</td>
                    <td width="100">&nbsp;</td>
                  </tr>
                  <? }?>
                </table>
		        <p>&nbsp;        </p>
		        <h3>IV.	TANDA  JASA/PENGHARGAAN/PRESTASI YANG PERNAH DICAPAI <!-- <?= $baris_awal;?>--><br /></h3>
			
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20"><p align="center"><strong>NO</strong></p></td>
					<td width="300"><p align="center"><strong>NAMA    BINTANG/SATYA LENCANA/PENGHARGAAN/PRESTASI</strong></p></td>
					<td width="75"><p align="center"><strong>TAHUN    PEROLEHAN</strong></p></td>
					<td width="175"><p align="center"><strong>NAMA    NEGARA/INSTANSI YANG MEMBERI</strong></p></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_penghargaan!=FALSE){
				foreach ($list_penghargaan as $penghargaan) {
				$i++;
				if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
				?>
				<tr>
					<td width="20">&nbsp;<?=$i;?></td>
					<td width="300">&nbsp;<?= $jenis_penghargaan_assoc[$penghargaan['jenis_penghargaan']]; ?></td>
					<td width="75">&nbsp;<?= $penghargaan['tahun']; ?></td>
					<td width="175">&nbsp;<?= $penghargaan['keterangan']; ?></td>
				</tr>
				<? }}
				else {
				$baris_awal=$baris_awal+2;
				//jika kosong tambahkan kolom kosong 2 baris
				?>
				<tr>
					<td width="20">&nbsp;1.</td>
					<td width="300">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="175">&nbsp;</td>
				</tr>
				<tr>
					<td width="20">&nbsp;2.</td>
					<td width="300">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="175">&nbsp;</td>
				</tr>
				<? }?>
			</table>
			
<?php 
if ($baris_awal<=53)
$baris_awal=$baris_awal+$hitungbaris+$i+9;
else
$baris_awal=$baris_awal+$hitungbaris+$i+6;
?>
<?php 
	$sisa_baris = 0;
	if ((($baris_awal)>=62) and ($hal==2)) {
	$sisa_baris = ($baris_awal)-62;
	if ($sisa_baris < 3) $sisa_baris=3;
	for($j = 1; $j <= ($sisa_baris); $j++)
	{
		//echo 'baris awal '.$baris_awal. '- 62 = '.$sisa_baris.'<br/> 	'; 
		echo '<br/>&nbsp;'; 
	}
	$baris_awal=1;
	$hitungbaris=0;
	$pjkarakter=0;
	$hal =3 ;
	}
?>


		<h3>V. KETERANGAN  KELUARGA <!--<?=$baris_awal?>--></h3>	
		<h4>1. ISTERI/SUAMI </h4> 
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20" align="center"><strong>NO</strong></td>
					<td width="195" align="center"><strong>NAMA</strong></td>
					<td width="80" align="center"><strong>TEMPAT DAN<br />TGL LAHIR</strong></td>
					<td width="75" align="center"><strong>TANGGAL MENIKAH</strong></td>
					<td width="100" align="center"><strong>PEKERJAAN</strong></td>
					<td width="100" align="center"><strong>KETERANGAN</strong></td>
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_pasangan!=FALSE){
				foreach ($list_pasangan as $jns_keluarga) {
					$i++;
					$jml_baris_pasangan = $jml_baris_penghargaan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_keluarga['tanggal_lahir']=='0000-00-00') {
					$str_tanggal_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_lahir']);
					$str_tanggal_lahir = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_lahir']=$str_tanggal_lahir;
					
					if ($jns_keluarga['tanggal_nikah']=='0000-00-00') {
					$str_tanggal_nikah = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_nikah']);
					$str_tanggal_nikah = $d . "-" . $m . "-" . $y;
					}
					$jns_keluarga['tanggal_nikah']=$str_tanggal_nikah;
				?>
				<tr>
					<td width="20"><?=$i;?></td>
					<td width="195">&nbsp;<?= $jns_keluarga['nama_pasangan']; ?></td>
					<td width="80">&nbsp;<?= $jns_keluarga['tanggal_lahir']; ?></td>
					<td width="75">&nbsp;<?= $jns_keluarga['tanggal_nikah']; ?></td>
					<td width="100">&nbsp;<?= $jns_keluarga['pekerjaan']; ?></td>
					<td width="100">&nbsp;</td>
				</tr>
				<? }}?>
			</table>


<?php $baris_awal=$baris_awal+$hitungbaris+$i+3;?>
<?php 
	$sisa_baris = 0;
	if ((($baris_awal)>=62) and ($hal==2)) {
	$sisa_baris = ($baris_awal)-62;
	if ($sisa_baris < 3) $sisa_baris=3;
	for($j = 1; $j <= ($sisa_baris); $j++)
	{
		//echo 'baris awal '.$baris_awal. '- 62 = '.$sisa_baris.'<br/> 	'; 
		echo '<br/>&nbsp;'; 
	}
	$baris_awal=1;
	$hitungbaris=0;
	$pjkarakter=0;
	$hal =3 ;
	}
?>
				
		<h4>2. ANAK <!--<?=$baris_awal?> sisa baris = <?=$sisa_baris?> hal = <?=$hal?> --></h4>
			<table border="1" cellspacing="0" cellpadding="0" width="595">
				<tr>
					<td width="20"><p align="center"><strong>NO</strong></p></td>
					<td width="100"><p align="center"><strong>NAMA</strong></p></td>
					<td width="75"><p align="center"><strong>JENIS KELAMIN</strong></p></td>
					<td width="125"><p align="center"><strong>TEMPAT DAN TANGGAL LAHIR</strong></p></td>
					<td width="75"><p align="center"><strong>STATUS ANAK</strong></p></td>
					<td width="75"><p align="center"><strong>PENDIDIKAN</strong></p></td>
					<td width="100"><p align="center"><strong>PEKERJAAN</strong></p></td>
					<!--<td width="104"><p align="center"><strong>ALAMAT</strong></p></td>-->
				</tr>
				<?
				$i = 0;
				$hitungbaris=0;
				$pjkarakter=0;
				if ($list_anak!=FALSE){
				foreach ($list_anak as $jns_anak) {
					$i++;
					$jml_baris_anak = $jml_baris_pasangan + $i;
					if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
					if ($jns_anak['tgl_lahir']=='0000-00-00') {
					$str_tgl_lahir = '-';
					}
					else {
					list($y, $m, $d) = explode('-', $jns_anak['tgl_lahir']);
					$str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
					}
					$jns_anak['tgl_lahir']=$str_tgl_lahir;
				?>
				<tr>
					<td width="20">&nbsp;<?=$i;?></td>
					<td width="100">&nbsp;<?= $jns_anak['nama_anak']; ?></td>
					<td width="75">&nbsp;<?= $gender_assoc[$jns_anak['jns_kelamin']]; ?></td>
					<td width="125">&nbsp;<?= $jns_anak['tempat_lahir']; ?>, <?= $jns_anak['tgl_lahir']; ?></td>
					<td width="75">&nbsp;<?= $status_keluarga_assoc[$jns_anak['kd_status_keluarga']]; ?></td>
					<td width="75">&nbsp;<?= $pendidikan_assoc[$jns_anak['id_pendidikan_anak']]; ?></td>
					<td width="100">&nbsp;<?= $jns_anak['pekerjaan']; ?> <!--baris awal <?=$baris_awal+$hitungbaris+$i;?> hal= <?=$hal?> --></td>
				</tr>
<?php 
	if (($baris_awal+$hitungbaris+$i>=62) and ($hal==2)) {
		$sisa_baris=(($baris_awal+$hitungbaris+$i)-62);
		$hal=3 ;
		?>
		</table>
			<!--anak 1<br />anak 2<br />anak 3<br />anak 4<br />-->
			<br /><br /><br /><br />
		<table border="1" cellspacing="0" cellpadding="0" width="595">
		<?	
	}
	if (($baris_awal+$i>62) and ($hal==3)){
	$sisa_baris++;
	$baris_awal=$sisa_baris;
	}
?>					
				<? }}?>
</table> 

<?php $baris_awal=$baris_awal+$hitungbaris+$i+7;?>
<?php 
	$sisa_baris = 0;
	if (($baris_awal>=62) and ($hal==2)) {
	$sisa_baris = ($baris_awal)-62;
	if ($sisa_baris < 3) $sisa_baris=3;
	for($j = 1; $j <= ($sisa_baris); $j++)
	{
		//echo 'baris awal '.$baris_awal. '- 62 = '.$sisa_baris.'<br/> 	'; 
		echo '<br/>&nbsp;'; 
	}
	$baris_awal=1;
	$hitungbaris=0;
	$pjkarakter=0;
	$hal =3 ;
	}
?>
		
		<h4>3. BAPAK  DAN IBU KANDUNG/TIRI/ANGKAT <!--<?=$baris_awal?>--></h4>
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20"><p align="center"><strong>NO</strong></p></td>
					<td width="100"><p align="center"><strong>NAMA</strong></p></td>
					<td width="75"><p align="center"><strong>JENIS KELAMIN</strong></p></td>
					<td width="125"><p align="center"><strong>TEMPAT DAN TANGGAL LAHIR</strong></p></td>
					<td width="75"><p align="center"><strong>PENDIDIKAN</strong></p></td>
					<td width="75"><p align="center"><strong>PEKERJAAN</strong></p></td>
					<td width="100"><p align="center"><strong>ALAMAT</strong></p></td>
				</tr>
				<tr>
					<td width="20">&nbsp;<br />&nbsp;</td>
					<td width="100">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="125">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="100">&nbsp;</td>
				</tr>
			</table>
		
<?php $baris_awal=$baris_awal+$hitungbaris+$i+4;?>
<?php 
	$sisa_baris = 0;
	if (($baris_awal>=62) and ($hal==2)) {
	$sisa_baris = ($baris_awal)-62;
	if ($sisa_baris < 3) $sisa_baris=3;
	for($j = 1; $j <= ($sisa_baris); $j++)
	{
		//echo 'baris awal '.$baris_awal. '- 62 = '.$sisa_baris.'<br/> 	'; 
		echo '<br/>&nbsp;'; 
	}
	$baris_awal=1;
	$hitungbaris=0;
	$pjkarakter=0;
	$hal =3 ;
	}
?>	
		<h4>4. BAPAK  DAN IBU MERTUA <!--<?=$baris_awal?>--></h4>
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20"><p align="center"><strong>NO</strong></p></td>
					<td width="100"><p align="center"><strong>NAMA</strong></p></td>
					<td width="75"><p align="center"><strong>JENIS KELAMIN</strong></p></td>
					<td width="125"><p align="center"><strong>TEMPAT DAN TANGGAL LAHIR</strong></p></td>
					<td width="75"><p align="center"><strong>PENDIDIKAN</strong></p></td>
					<td width="75"><p align="center"><strong>PEKERJAAN</strong></p></td>
					<td width="100"><p align="center"><strong>ALAMAT</strong></p></td>
				</tr>
				<tr>
					<td width="20">&nbsp;<br />&nbsp;</td>
					<td width="100">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="125">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="100">&nbsp;</td>
				</tr>
			</table>

<?php $baris_awal=$baris_awal+$hitungbaris+$i+4;?>		
			
		<h4>5. SAUDARA  KANDUNG <!--<?=$baris_awal?>--></h4>
			<table border="1" cellspacing="0" cellpadding="0" width="570">
				<tr>
					<td width="20"><p align="center"><strong>NO</strong></p></td>
					<td width="100"><p align="center"><strong>NAMA</strong></p></td>
					<td width="75"><p align="center"><strong>JENIS KELAMIN</strong></p></td>
					<td width="125"><p align="center"><strong>TEMPAT DAN TANGGAL LAHIR</strong></p></td>
					<td width="75"><p align="center"><strong>PENDIDIKAN</strong></p></td>
					<td width="75"><p align="center"><strong>PEKERJAAN</strong></p></td>
					<td width="100"><p align="center"><strong>ALAMAT</strong></p></td>
				</tr>
				<tr>
					<td width="20">&nbsp;<br />&nbsp;</td>
					<td width="100">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="125">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="75">&nbsp;</td>
					<td width="100">&nbsp;</td>
				</tr>
			</table>
<?php $baris_awal=$baris_awal+15;?>		
<?php 
	$sisa_baris =0;
	if (($baris_awal>=62)) {
	$sisa_baris = ($baris_awal)-62;
	if ($sisa_baris < 3) $sisa_baris=3;
	for($j = 1; $j <= ($sisa_baris); $j++)
	{
		//echo 'baris awal '.$baris_awal. '- 62 = '.$sisa_baris.'<br/> 	';
		echo '<br/>&nbsp;'; 
	}
	$hitungbaris=0;
	$pjkarakter=0;
	}
	//jika tidak kembalikan nilai baris awal hnya + 5
	else $baris_awal=$baris_awal-7;
?>	
			<table width="605">
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;<!--baris awal <?=$baris_awal;?>--></td>
				</tr>
				<tr>
					<td width="36">&nbsp;</td>
					<td width="25">&nbsp;</td>
					<td width="279" colspan="3" align="left">&nbsp;</td>
					<td colspan="5" width="255" align="left">Malang, <?= $tglsurat; ?></td>
				</tr>
				<tr>
					<td width="36">&nbsp;</td>
					<td width="25">&nbsp;</td>
					<td width="279" colspan="3" align="left">&nbsp;</td>
					<td colspan="5" width="255" align="left">Yang Menerangkan</td>
				</tr>
				<tr>
					<td colspan="5" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td width="36">&nbsp;</td>
					<td width="25">&nbsp;</td>
					<td width="279" colspan="3" align="left">&nbsp;</td>
					<td colspan="5" width="255" align="left"><?=$gelar_depan.' '.$nama_peg.$gelar_belakang?></td>
				</tr>
				<tr>
					<td width="36">&nbsp;</td>
					<td width="25">&nbsp;</td>
					<td width="279" colspan="3" align="left">&nbsp;</td>
					<td colspan="5" width="255" align="left">NIP. <?=$NIP;?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="3" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="3" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="3" align="left">&nbsp;</td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5">&nbsp;<strong>PERHATIAN</strong></td>
					<td colspan="5" align="left">&nbsp;</td>
				</tr>
				<tr>
					<td width="20">&nbsp;</td> 	
					<td colspan="11" align="left">
					&nbsp;&bull;&nbsp;Harus ditulis  dengan tangan sendiri , menggunakan huruf kapital/balok dan tinta hitam.<br />
					&nbsp;&bull;&nbsp;Apabila format  tidak cukup bisa ditambah pada halaman tersendiri dengan format yang sama.
					</td>
				</tr>
			</table>
