<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatpelatihan/$action/";
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/riwayatpelatihan/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div>
	<form name="pelatihandetail" id="pelatihandetail" method="POST" action="<?= $action_url; ?>">
		<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
		<table class="general">
			<tr align="left">
				<td width ="200">Kode</td>
				<td> : </td>
				<td >
				<?= $id_riwayat_pelatihan?></td>
			</tr>
			<tr align="left">
			  <td>Nama Pelatihan </td>
			  <td> : </td>
			  <td >
			  <textarea name="nama_pelatihan" id="nama_pelatihan" rows="2" cols="50"><?=set_value('nama_pelatihan',$nama_pelatihan);?></textarea> 
				  <span style="color:#F00;">* <?php echo form_error('nama_pelatihan'); ?></span> </td>
			  </tr>
			<tr align="left">
			  <td>Jenis Pelatihan </td>
			  <td>:</td>
			  <td ><?
						echo form_dropdown('jns_pelatihan', $jenis_pelatihan_assoc, $jns_pelatihan);
					?>
					<?php echo form_error('jns_pelatihan'); ?></td>
		  </tr>
		  <tr align="left">
			  <td>Nomor Surat Tugas</td>
			  <td>:</td>
			  <td ><input type='text' name='nomor_surat_tugas' id='nomor_surat_tugas' value='<?=set_value('nomor_surat_tugas',$nomor_surat_tugas);?>'/>
				  <span style="color:#F00;">* <?php echo form_error('nomor_surat_tugas'); ?></span></td>
		  </tr>
		  </tr>
		  <tr align="left">
				<td width ="200">Bukti Penugasan</td>
				<td> : </td>
				<td >
				<textarea name='bukti_penugasan' id='bukti_penugasan' rows="1" cols="40"> <?= $bukti_penugasan?> </textarea>
				 </td>
			</tr>
		  <tr align="left">
			  <td>Semester</td>
			  <td>:</td>
			  <td ><?
						echo form_dropdown('semester', $semester_assoc, $semester);
					?>
					<?php echo form_error('semester'); ?></td>
		  </tr>
			<tr align="left">
			  <td>Tahun</td>
			  <td>:</td>
			  <td ><input type='text' name='tahun' id='tahun' value='<?=set_value('tahun',$tahun);?>'/>
				  <span style="color:#F00;">* <?php echo form_error('tahun'); ?></span></td>
		  </tr>
			<tr align="left">
				<td width ="200">Tanggal Mulai </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tgl_mulai' id='tgl_mulai'  value='<?= $tgl_mulai?>'  class="required"/>
					<?php echo form_error('tgl_mulai'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
			</tr>
			<tr align="left">
				<td width ="200">Tanggal Selesai </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tgl_selesai' id='tgl_selesai'  value='<?= $tgl_selesai?>'  class="required"/>
					<?php echo form_error('tgl_selesai'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>	</td>
			</tr>
			
			<tr align="left">
				<td width ="200">Lama</td>
				<td> : </td>
				<td ><input type='text' name='lama_hari' id='lama_hari' value='<?= $lama_hari?>'  class="required"/>
				  <span style="color:#F00;">*<?php echo form_error('lama_hari'); ?> hari </span></td>
			</tr>
			<tr align="left">
				<td width ="200">Jumlah Jam</td>
				<td> : </td>
				<td ><input type='text' name='jml_jam' id='jml_jam' value='<?= $jml_jam?>'  class="required"/> jam </td>
			</tr>
			<tr align="left">
				<td width ="200">SKS</td>
				<td> : </td>
				<td ><input type='text' name='sks' id='sks' value='<?= $sks?>'  class="required"/> sks</td>
			</tr>
			 <tr align="left">
			  <td>Capaian</td>
			  <td>:</td>
			  <td ><?
			  		echo form_dropdown('capaian', $capaian_assoc, $capaian);
					?>
					<?php echo form_error('capaian'); ?></td>
		  
			<tr align="left">
				<td width ="200">Tempat Pelatihan </td>
				<td> : </td>
				<td >
				<textarea name='tempat_pelatihan' id='tempat_pelatihan' rows="1" cols="40"> <?= $tempat_pelatihan?> </textarea>
				<?php echo form_error('tempat_pelatihan'); ?> </td>
			</tr>
			<tr align="left">
				<td width ="200"></td>
				<td>  </td>
				<td> 
					<select name="dn_ln">
					
							
						 <?php if ($dn_ln=='') {?>
						 <option value="Dalam Negeri" selected="selected">Dalam Negeri
						 <? } else { ?>
						 <option value='<?=set_value('dn_ln',$dn_ln);?>' selected="selected"><?=set_value('dn_ln',$dn_ln);?>
						 <option value="Dalam Negeri">Dalam Negeri
						 <? } ?>
						 <option value="Luar Negeri">Luar Negeri
					</select>				</td>
			</tr>
			<tr align="left">
			  <td>Keterangan</td>
			  <td> : </td>
			  <td> 
			<textarea name='keterangan' id='keterangan' rows="1" cols="40"> <?= $keterangan?> </textarea>
				<?php echo form_error('keterangan'); ?> </td>	  
				   </td>
			  </tr>
		</table>
		<br/>
		<!--<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>-->
		<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>