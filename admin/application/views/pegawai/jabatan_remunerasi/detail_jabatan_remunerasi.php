<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/jabatan_remunerasi/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/jabatan_remunerasi/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
			
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="jabatan_remunerasi" id="jabatan_remunerasi" method="POST" action="<?= $action_url; ?>">
			 <?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
			 <?php echo form_hidden('NIP', $pegawai['NIP']); ?>
			<table class="general">
				<tr align="left">
					
					<?= $id_riwayat_jabatan_s?><!-- jika baru $id_riwayat_jabatan_s null, selain itu tampilkan id_riwayat_jabatan uneditable-->
				</tr>
				
				<tr align="left">
				  <td>Jabatan</td>
				  <td> : </td>
				  <td ><?php
						echo form_dropdown('id_jabatan_uj',$id_jabatan_uj, $id_jabatan, 'id="jabatan"');
						?> <? echo form_error('id_jabatan_uj'); ?> </td>
				  </tr>
				  <tr align="left">
				  <td>Golongan</td>
				  <td> : </td>
				  <td>
						<?php echo form_dropdown("id_golpangkat", $id_golongan, $id_golpangkat, 'id="golongan"'); ?>
					  	<?php echo form_error('id_golongan'); ?> </td>
				  </tr>
				<tr align="left">
					<td width ="200">TMT Golongan</td>
					<td> : </td>
					<td >
						<input size="25" type="text" name="tmt_golongan" id="tmt_golongan" class="datepicker js_required" value="<?= $tmt_pangkat?>" class="required" />
						<span style="color:#F00;">*<?php echo form_error('tmt_golongan'); ?></span>		</td>
				</tr>
				<tr align="left">
					<td width ="200">Job Class</td>
					<td> : </td>
					<td >
						<input type="text" name="job_class" id="job_class" class="js_required" value="<?= $job_class?>" class="required" />
						<?php echo form_error('job_class'); ?><span style="color:#F00;"></span>
					</td>
				</tr>
				
				<tr align="left">
					<td width ="200px">Job Value</td>
					<td> : </td>
					<td >
						<input type="text" name="job_value" id="job_value" value="<?=set_value('job_value',$job_value);?>" />
						<span style="color:#F00;"><?php echo form_error('job_value'); ?></span></td>
				</tr>
				<tr align="left">
					<td width ="200">Nomor SK Jabatan </td>
					<td> : </td>
					<td >
						<input size="20" type="text" name="sk_jabatan" id="sk_jabatan" value="<?= $no_sk_jabatan?>" />
					  	<?php echo form_error('no_sk_jabatan'); ?></td>
				</tr>
				<tr align="left">
					<td width ="200">Tanggal SK Jabatan </td>
					<td> : </td>
					<td >
						<input size="20" class="datepicker" type="text" name="tgl_sk_jabatan" id="tgl_sk_jabatan" value="<?= $tgl_sk_jabatan?>" />
					  	<?php echo form_error('tgl_sk_jabatan'); ?></td>
				</tr>
				<tr>
					<td  align="left">TMT Jabatan</td>
					<td> : </td>
					<td colspan="3"  align="left">
						<input type="text" name="tmt_jabatan" id="tmt_jabatan" value="<?= $tmt_jabatan ?>"  class="datepicker" >
					</td>
				</tr>
							
				
				<tr align="left">
					<td width ="200px">Unit Kerja</td>
					<td> : </td>
					<td width ="200px">
					<?php
						echo form_dropdown('unit_kerja',$unit_kerja, $kode_unit ,'class="js_required"');
						?>
						<?
						echo form_error('unit_kerja'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Keterangan</td>
					<td> : </td>
					<td width ="200px">
						<textarea name="keterangan" id="" cols="30" rows="10"><?= $keterangan ?></textarea>
					</td>
				</tr>
			</table>
			<br/>
			<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});

$("#golongan").change(function(){
	var id_jabatan_uj = $("#jabatan").val();
	var id_golpangkat = $("#golongan").val();
	var url 		  = "<?php echo base_url()."/pegawai/jabatan_remunerasi/get_jv_rendah/"; ?>"+id_jabatan_uj;
	
	if (id_golpangkat>=9 && id_golpangkat!=18) {
		// load url for jv_tinggi
		url="<?php echo base_url()."/pegawai/jabatan_remunerasi/get_jv_tinggi/" ?>"+id_jabatan_uj;
	}

	$.get(url, function(data){
		$("#job_value").val(data);
	});
});

$("#jabatan").change(function(){
	var id_jabatan_uj = $("#jabatan").val();
	var id_golpangkat = $("#golongan").val();
	var url 		  = "<?php echo base_url()."/pegawai/jabatan_remunerasi/get_jv_rendah/"; ?>"+id_jabatan_uj;
	
	if (id_golpangkat>=9 && id_golpangkat!=18) {
		// load url for jv_tinggi
		url="<?php echo base_url()."/pegawai/jabatan_remunerasi/get_jv_tinggi/" ?>"+id_jabatan_uj;
	}

	$.get(url, function(data){
		$("#job_value").val(data);
	});
});
</script>