<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table class="table-list">
	<tr class="trhead">
		<th width='50' >No</th>
		<th width='100' class="colEvn">NIP</th>
		<th width='200' >Nama Pegawai</th>
		<th width='150' class="colEvn">Status</th>
		<th width='150' >Unit Kerja</th>
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_pegawai!=FALSE){
	foreach ($list_pegawai as $jns_pegawai) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	   if ($jns_pegawai['aktif'] == 1) { $stat = "Aktif"; } else { $stat = "Non-Aktif"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$jns_pegawai['kd_pegawai'];?></td>
		<td align="left" class="colEvn"><?= $jns_pegawai['NIP']; ?></td>
		<td align="left" ><?= $jns_pegawai['nama']; ?></td>
		<td align="center" class="colEvn"><?= $jns_pegawai['aktif']?"<span style='color:#000000'>Aktif</span>":"<span style='color:#FF0000'>Non-Aktif</span>"; ?></td>
		<td align="left" ><?= $jns_pegawai['unit_kerja']; ?></td>
		<td align="center" class="colEvn">			
			<a href = "<?=site_url("pegawai/terminasi/view/".$jns_pegawai['kd_pegawai']); ?>" class="manage-mutasi action" >Rincian</a>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

