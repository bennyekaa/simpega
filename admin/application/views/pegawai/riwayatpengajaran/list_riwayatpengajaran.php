<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url("pegawai/riwayatpengajaran/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        <?php	
					if($this->user->user_group=="Staf") {
				?>
                <a class="icon" href="<?=site_url('pegawai/caripegawai/view/' .$kd_pegawai)?>" title="Batal">
                <img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
            
            <?php } else { ?>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
            <? } ?>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="pengajarandetails" id="pengajarandetails" method="POST" action="<?=site_url('pegawai/riwayatpengajaran/browse')?>" >
	</form>
	</center>
	<br/>
	<table class="table-list">
	<tr class="trhead">
		<th >No</th>
		<th class="colEvn" >Kategori Pengajaran </th>
		<th class="colEvn" >Nama</th>
		<th >Tahun Ajaran</th>
		<th class="colEvn">Semester </th>
		<th >S K S</th>
		<th width="250" class="colEvn">Tempat</th>
		<th >Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_pengajaran!=FALSE){
	foreach ($list_pengajaran as $pengajarans) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="left" ><?= $jenis_pengajaran_assoc[$pengajarans['jenis_pengajaran']];?></td>
		<td align="left" ><?= $pengajarans['nama']; ?></td>
		<td align="center" ><?= $pengajarans['tahun_ajaran']; ?></td>
		<td align="left" ><?= $semester_assoc[$pengajarans['semester']];?></td>
		<td align="center" ><?= $pengajarans['sks']; ?></td>
		<td align="left" ><?= $pengajarans['keterangan']; ?></td>
		<td align="center" class="colEvn">			
			<a href="<?=site_url("pegawai/riwayatpengajaran/edit/".$pengajarans['id_riwayat_pengajaran']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("pegawai/riwayatpengajaran/delete/".$pengajarans['id_riwayat_pengajaran']); ?>" class="delete action" >Hapus</a>		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

