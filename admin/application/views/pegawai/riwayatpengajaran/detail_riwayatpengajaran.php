<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatpengajaran/$action/";
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/riwayatpengajaran/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div>
	<form name="pengajarandetail" id="pengajarandetail" method="POST" action="<?= $action_url; ?>">
		<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
		<table class="general">
			<tr align="left">
				<td width ="200px">Kategori Pengajaran</td>
				<td> : </td>
				<td width ="200px">
				<?php
					echo form_dropdown('jenis_pengajaran',$jenis_pengajaran_assoc, $jenis_pengajaran);
 				?>
				</td>
			</tr>
		
			<tr align="left">
			  <td>Nama Pengajaran</td>
			  <td> : </td>
			  <td >
			  <textarea name="nama" id="nama" rows="2" cols="50"><?=set_value('nama',$nama);?></textarea> 
				 </td>
			  </tr>
		<tr align="left">
			  <td>Jenjang</td>
			  <td> : </td>
			  <td ><?php
					echo form_dropdown('id_pendidikan',$pendidikan_assoc, $id_pendidikan, 'class="js_required"'); 
					?> </td>
		  </tr>
		  
		  <tr align="left">
			  <td>Semester</td>
			  <td>:</td>
			  <td ><?
						echo form_dropdown('semester', $semester_assoc, $semester);
					?>
					<?php echo form_error('semester'); ?></td>
		  </tr>
		<tr align="left">
		  <td>Tahun Ajaran</td>
		  <td>:</td>
		  <td ><input type='text' name='tahun_ajaran' id='tahun_ajaran' value='<?=set_value('tahun_ajaran',$tahun);?>'/>
			  <span style="color:#F00;">* <?php echo form_error('tahun_ajaran'); ?></span></td>
		</tr>
		
		
			
			
		  <tr align="left">
				<td width ="200">Bukti Penugasan</td>
				<td> : </td>
				<td >
				<textarea name='bukti_penugasan' id='bukti_penugasan' rows="1" cols="40"><?= $bukti_penugasan?></textarea>
				 </td>
			</tr>
			
			<tr align="left">
				<td>Capaian</td>
				<td>:</td>
				<td ><?
					echo form_dropdown('capaian', $capaian_assoc, $capaian);
					?>
					<?php echo form_error('capaian'); ?></td>
			</tr>
		
				<input type="hidden" name='tempat_pengajaran' id='tempat_pengajaran' value="-" />
			<tr align="left">
			  <td>Tempat</td>
			  <td> : </td>
			  <td> 
			<textarea name='keterangan' id='keterangan' rows="1" cols="40"><?= $keterangan?></textarea>
				<?php echo form_error('keterangan'); ?> </td>	  
				   </td>
			  </tr>
			  <tr align="left">
				<td width ="200">Jumlah Mahasiswa</td>
				<td> : </td>
				<td ><input type='text' name='jml_mhs' id='jml_mhs' value='<?= $jml_mhs?>' /></td>
			</tr>
			<tr align="left">
			<td width ="200">SKS</td>
			<td> : </td>
			<td ><input type='text' name='sks' id='sks' value='<?= $sks?>'  class="required"/> sks</td>
		</tr>
		</table>
		<br/>
		<!--<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>-->
		<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>