<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url("pegawai/riwayatdp3/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="dp3details" id="dp3details" method="POST" action="<?=site_url('pegawai/riwayatdp3/browse')?>" >
	</form>
	</center>
	
	<table class="table-list">
	<tr class="trhead">
		<th width='20' rowspan="2" >No</th>
		<th width='20' rowspan="2" >Tahun  </th>
		<th width='55' class="colEvn" rowspan="2" >Nilai DP3</th>
		<th width='610' class="colEvn"colspan="5" >Pejabat Penilai</th>	
		<th width='20' class="colEvn" rowspan="2">Cetak</th>
		<th width='20' class="colEvn" rowspan="2">Aksi</th>	
	</tr>
	<tr class="trGre">
		<th width="90" class="colEvn" align="left" >NIP</th>
		<th width="200" align="left" >Nama</th>
		<th width="50" class="colEvn" align="left" >Gol.<!--/Ruang--></th>
		<th width="180" align="left" >Jabatan</th>
		<th width="180" align="left" >Unit Kerja</th>
	</tr>

	<?
	$i = 0;
	if ($list_dp3!=FALSE){
	foreach ($list_dp3 as $dp3) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn" rowspan="2"><?=$i;?></td>
		<td align="center" rowspan="2"><?= $dp3['tahun']; ?></td>
		<td align="left" rowspan="2"><?= $dp3['nilai_dp3']; ?></td>
		<td align="left" ><?= $dp3['pp_nip']; ?></td>
		<td align="left" ><?= $dp3['pp_nama']; ?></td>
		<td align="left" ><?= $dp3['pp_golpangkat']; ?></td>
		<td align="left" ><?= $dp3['pp_jabatan']; ?></td>
		<td align="left" ><?= $dp3['pp_unit_kerja']; ?></td>
		<td align="center" class="colEvn" rowspan="2">
					<a class="icon" href="<?=site_url("laporan/riwayatdp3/printtopdf/".$dp3['id_riwayat_dp3']); ?>" title="Cetak">
					<img class="noborder" src="<?=base_url().'public/images/print_pdf.jpg'?>"></a>
					</td>
		<td align="center" class="colEvn" rowspan="2">
		<a href="<?=site_url("pegawai/riwayatdp3/edit/".$dp3['id_riwayat_dp3']); ?>" class="edit action" >Ubah</a>
		<!--<a href="<?=site_url("pegawai/riwayatdp3_detil/browse/".$dp3['id_riwayat_dp3']); ?>" class="view action">Rincian DP3</a>-->
		<?php	
		   //if($this->user->user_group=="Administrator") {
		?>
		<a href="<?=site_url("pegawai/riwayatdp3/delete/".$dp3['id_riwayat_dp3']); ?>" class="delete action" >Hapus</a>			
		<? //} ?>
		</td>
	</tr>
	
	<tr class="trGre">
		<td align="left" ><?= $dp3['app_nip']; ?></td>
		<td align="left" ><?= $dp3['app_nama']; ?></td>
		<td align="left" ><?= $dp3['app_golpangkat']; ?></td>
		<td align="left" ><?= $dp3['app_jabatan']; ?></td>
		<td align="left" ><?= $dp3['app_unit_kerja']; ?></td>
	</tr>
	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

