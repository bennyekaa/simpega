


<?
function right($value, $count){
    return substr($value, ($count*-1));
}

function left($string, $count){
    return substr($string, 0, $count);
}

$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatdp3/$action/".$pegawai['kd_pegawai'];
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/riwayatdp3/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		
	</div>
	<div class="clear"></div>
</div>
<? } ?>

<script language="javascript">		

	function MakeNullPP(){
	document.getElementById('tahun').value = '';
	document.getElementById('pp_nama').value = '';
	document.getElementById('pp_golpangkat').value = '';
	document.getElementById('pp_jabatan').value = '';
	document.getElementById('pp_unit_kerja').value = '';
	}
	function MakeNullAPP(){
	document.getElementById('tahun').value = '';
	document.getElementById('app_nama').value = '';
	document.getElementById('app_golpangkat').value = '';
	document.getElementById('app_jabatan').value = '';
	document.getElementById('app_unit_kerja').value = '';
	}
	
</script>

<script language="javascript">
	function MakeSum(unsur1,unsur2,unsur3,unsur4,unsur5,unsur6,unsur7,unsur8,unsur9,unsur10,jmlrow,bsedang,bcukup,bbaik,babaik) {
		var total = 0;
		var ratatotal = 0;	
		var pengurang = 0;	
		
		var total = unsur1+unsur2+unsur3+unsur4+unsur5+unsur6+unsur7+unsur8+unsur9+unsur10;
		if (unsur8==0)
			pengurang++;

			
		var ratatotal = (total)/(jmlrow-pengurang);
		
		ShowName(0,ratatotal,bsedang,bcukup,bbaik,babaik);
		
		document.getElementById('nilai_total').value = total;
		document.getElementById('nilai_dp3').value = ratatotal;
		
		ShowName(1,unsur1,bsedang,bcukup,bbaik,babaik);
		ShowName(2,unsur2,bsedang,bcukup,bbaik,babaik);
		ShowName(3,unsur3,bsedang,bcukup,bbaik,babaik);
		ShowName(4,unsur4,bsedang,bcukup,bbaik,babaik);
		ShowName(5,unsur5,bsedang,bcukup,bbaik,babaik);
		ShowName(6,unsur6,bsedang,bcukup,bbaik,babaik);
		ShowName(7,unsur7,bsedang,bcukup,bbaik,babaik);
		ShowName(8,unsur8,bsedang,bcukup,bbaik,babaik);
		ShowName(9,unsur9,bsedang,bcukup,bbaik,babaik);
		ShowName(10,unsur10,bsedang,bcukup,bbaik,babaik);
			
	}
</script>

<script language="javascript">
	function ShowName(idunsur,nilai,bsedang,bcukup,bbaik,babaik){
	var nilaihuruf='baik';
	
		if (nilai==0 ) 
			nilaihuruf = '-';
		else if (nilai<bsedang ) 
			nilaihuruf = 'kurang';
		else if (nilai<bcukup) 
			nilaihuruf = 'sedang';
		else if (nilai<bbaik ) 
			nilaihuruf = 'cukup'; 
		else if (nilai<babaik) 
			nilaihuruf = 'baik'; 
		else if (nilai>=babaik) 
			nilaihuruf = 'amat baik';
		
		if (idunsur==1) 
			document.getElementById('satuan1').value = nilaihuruf;	
		else if (idunsur==2) 
			document.getElementById('satuan2').value = nilaihuruf;
		else if (idunsur==3) 
			document.getElementById('satuan3').value = nilaihuruf;
		else if (idunsur==4) 
			document.getElementById('satuan4').value = nilaihuruf;	
		else if (idunsur==5) 
			document.getElementById('satuan5').value = nilaihuruf;
		else if (idunsur==6) 
			document.getElementById('satuan6').value = nilaihuruf;
		else if (idunsur==7) 
			document.getElementById('satuan7').value = nilaihuruf;
		else if (idunsur==8) 
			document.getElementById('satuan8').value = nilaihuruf;
		else if (idunsur==9) 
			document.getElementById('satuan9').value = nilaihuruf;
		else if (idunsur==10) 
			document.getElementById('satuan10').value = nilaihuruf;
		else if (idunsur==0) 
			document.getElementById('satuan_nilai_dp3').value = nilaihuruf;
	}
</script>


<br/>
<div>
<form name="pelatihandetail" id="pelatihandetail" method="POST" action="<?= $action_url; ?>">
	<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
	<table class="general" width="700" border="0" cellspacing="0" cellpadding="1">
	<tr><td>
	<table class="general" width="100%" border="0" cellspacing="0" cellpadding="0">
		<input type='hidden' name='id_riwayat_dp3' id='id_riwayat_dp3' value='<?= $id_riwayat_dp3?>' />
		<input type="hidden" name="user_id" id="user_id" value="<?=$this->user->user_id?>" />
		<!--<tr align="left">
		  <td colspan="3" align="center">
		  <img height="135" width="110" src="<?php echo $photo; ?>" />
		  </td>
	    </tr>-->
		<tr align="left">
		  <td colspan="3" align="center"><font size="+1" face="Arial, Helvetica, sans-serif"> <strong>DAFTAR PENILAIAN PELAKSANAAN PEKERJAAN</strong></font></td>
	    </tr>
		<tr align="left">
		  <td colspan="3" align="center"><font size="+1" face="Arial, Helvetica, sans-serif"><strong>PEGAWAI NEGERI SIPIL</strong></font></td>
	    </tr>

		<tr align="left">
		  <td colspan="3" align="center">&nbsp;</a>
		  </td>
	    </tr>
		
		<tr align="left">
		  <td colspan="3"><div align="right">JANGKA WAKTU PENILAIAN</div></td>
	    </tr>
		<tr align="left">
		  <td colspan="3"><div align="right">BULAN : Januari s.d. Desember 
		 
		      <input type='text' name='tahun' id='tahun' value='<?=$tahun;?>' size="4px" maxlength="4"  />
		      <span style="color:#F00;"><?php echo form_error('tahun'); ?></span></div>
			 
			  </td>
	    </tr>
	</table>
	</td></tr>
	<tr><td>
		<table class="general" width="100%" border="1" cellspacing="0" cellpadding="1">
			<tr align="left">
			  <td width="30" align="center" rowspan="6" valign="top">1.</td>
				<td colspan="4"><strong>YANG DINILAI</strong>			</td>
			  </tr>
			<?php
					
			$sql = "SELECT *,
			(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit)) as unit_kerja ,			
			(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit_induk)) as unit_kerja_induk ,	
			(CASE id_jns_pegawai WHEN 5 THEN CONCAT((select nama_jabatan from jabatan where id_jabatan=(pegawai.id_jabatan_terakhir)),(CASE id_jabatan_struktural WHEN '' THEN CONCAT('/ Dosen Jurusan ',(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit))) 
ELSE ' ' 
 END)) ELSE (select nama_jabatan_s from jabatan_struktural where id_jabatan_s=(pegawai.id_jabatan_struktural)) 
END ) as jabatan,		
			(select golongan from golongan_pangkat where id_golpangkat=(pegawai.id_golpangkat_terakhir)) as golongan,			
			(select concat(golongan,', ',pangkat) from golongan_pangkat where id_golpangkat=(pegawai.id_golpangkat_terakhir)) as golpangkat
			 FROM pegawai WHERE kd_pegawai = '".$pegawai['kd_pegawai']."' ";
			$rs = mysql_query($sql);
			if($r = mysql_fetch_array($rs)){
				$nip = $r['NIP'];
				$nama = $r['gelar_depan'].$r['nama_pegawai'].$r['gelar_belakang'];
				$golpangkat =  $r['golongan'];
				if ($dinilai_unit!='-'){
					$unit_kerja = $dinilai_unit;
				}
				else
				{
					if ($r['unit_kerja_induk']!=''){
					$unit_kerja = $r['unit_kerja'].'-'.$r['unit_kerja_induk'];
					}
					else{
					$unit_kerja = $r['unit_kerja'];
					}
				}
				
				if (($dinilai_jabatan!='-')or($dinilai_jabatan=='0')){
				$jabatan =  $dinilai_jabatan;
				}
				else{
				$jabatan =  $r['jabatan'];
				}
				}
			?>
			<tr align="left">
			  <td width="370" align="left">NIP</td>
			  <td colspan="3" ><?=$nip;?></td>
			  </tr>
			<tr align="left">
			  <td width="370">Nama</td>
			  <td colspan="3"><?=$nama?></td>
			  </tr>
			<tr align="left">
			  <td width="370" align="left">Gol/Ruang</td>
			  <td colspan="3"><?=$golpangkat?></td>
			  </tr>
			 <?php
			 if((isset($_POST['dinilai_unit'])))	
			{
				$dinilai_jabatan = $_POST['dinilai_jabatan'];
				$dinilai_unit = $_POST['dinilai_unit'];
			}
			else{
				$dinilai_jabatan = $jabatan;
				$dinilai_unit = $unit_kerja;
			}
			 ?>
			 
			<tr align="left">
			  <td width="370" align="left">Jabatan</td>
			  <td colspan="3" >
			   <input type='text' name='dinilai_jabatan' id='dinilai_jabatan' value='<?=set_value('dinilai_jabatan',$dinilai_jabatan);?>' size="40px" />
			  </td>
			  </tr>
			
			<tr align="left">
			  <td>Unit Kerja</td>
			  <td colspan="3" >
			  <!--<input type='text' name='unit_kerja' id='unit_kerja' value='<?=set_value('unit_kerja',$unit_kerja);?>' size="40px" />-->
			  <input type='text' name='dinilai_unit' id='dinilai_unit' value='<?=set_value('dinilai_unit',$dinilai_unit);?>' size="40px" />
			  </td>
			  
			  </tr>
			
			<tr align="left">
			  <td width="30" align="center" rowspan="6" valign="top">2.</td>
				<td colspan="4"><strong>PEJABAT PENILAI</strong>			</td>
			  </tr>
			<?php
				
				if((isset($_POST['pp_nipsubmit'])))	
				{
					//if ($_POST['otomatis']==1){ jabatan fungsional tidak tampil
						$sql = "SELECT *,
						(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit)) as unit_kerja ,			
						(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit_induk)) as unit_kerja_induk ,	
						(CASE id_jns_pegawai WHEN 5 THEN CONCAT('',
(CASE id_jabatan_struktural WHEN '' THEN CONCAT('Dosen Jurusan ',(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit))) 
ELSE CONCAT((select nama_jabatan_s from jabatan_struktural where id_jabatan_s=(pegawai.id_jabatan_struktural)),' ',(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit))) 
 END)) ELSE (select nama_jabatan_s from jabatan_struktural where id_jabatan_s=(pegawai.id_jabatan_struktural)) 
END ) as jabatan,	
						(select golongan from golongan_pangkat where id_golpangkat=(pegawai.id_golpangkat_terakhir)) as golongan,			
						(select concat(pangkat,', ',golongan) from golongan_pangkat where id_golpangkat=(pegawai.id_golpangkat_terakhir)) as golpangkat
						 FROM pegawai WHERE status_pegawai < 3 and NIP = '$_POST[pp_nip]'";
						$rs = mysql_query($sql);
						if($r = mysql_fetch_array($rs)){
							$pp_nip = $r['NIP'];
							$pp_nama = $r['gelar_depan'].$r['nama_pegawai'].$r['gelar_belakang'];
							$pp_golpangkat =  $r['golpangkat'];
							if ($r['unit_kerja_induk']!=''){
							$pp_unit_kerja = $r['unit_kerja'].'-'.$r['unit_kerja_induk'];
							}
							else{
							$pp_unit_kerja = $r['unit_kerja'];
							}
							
							$pp_jabatan =  $r['jabatan'];
							if (left($pp_jabatan,5)=='Dekan'){
							$pp_jabatan = 'Dekan';
							}
							$pp_cek='1';
							}
					//}
					}
					else if((isset($_POST['pp_nip'])))	
					{
						$pp_nip = $_POST['pp_nip'];
						$pp_nama = $_POST['pp_nama'];
						$pp_golpangkat = $_POST['pp_golpangkat'];
						$pp_unit_kerja = $_POST['pp_unit_kerja'];
						$pp_jabatan =  $_POST['pp_jabatan'];
						$pp_cek='0';
					}
	
			?>
			<tr align="left">
			  <td  width="370" align="left">NIP</td>
			  <td colspan="3"  width="430">
				<input type='text' name='pp_nip' id='pp_nip' value='<?=set_value('pp_nip',$pp_nip);?>' /> <!--onKeyUp="MakeNullPP()"-->
				
				<input name="pp_nipsubmit" id="pp_nipsubmit" size="100px" name="search" type="submit" value="&raquo;cari data"/>		
				
						</td>
			  </tr>
			<tr align="left">
			  <td width="370" align="left">Nama</td>
			  <td colspan="3" ><input type='text' name='pp_nama' id='pp_nama' value='<?=set_value('pp_nama',$pp_nama);?>' size="40px" /></td>
			  </tr>
			<tr align="left">
			  <td width="370" align="left">Gol/Ruang</td>
			  <td colspan="3"><input type='text' name='pp_golpangkat' id='pp_golpangkat' value='<?=set_value('pp_golpangkat',$pp_golpangkat);?>' size="40px"/></td>
			  </tr>
			<tr align="left">
			  <td width="370" align="left">Jabatan</td>
			  <td colspan="3" ><input type='text' name='pp_jabatan' id='pp_jabatan' value='<?=set_value('pp_jabatan',$pp_jabatan);?>' size="60px"/></td>
			  </tr>
			<tr align="left">
			  <td width="370" align="left">Unit Kerja</td>
			  <td colspan="3" ><input type='text' name='pp_unit_kerja' id='pp_unit_kerja' value='<?=set_value('pp_unit_kerja',$pp_unit_kerja);?>' size="40px" /></td>
			  </tr>
			
			<tr align="left">
			  <td width="30" align="center" rowspan="6" valign="top">3.</td>
				<td colspan="4"><strong>ATASAN PEJABAT PENILAI</strong></td>
			  </tr>		
			<?php
				if((isset($_POST['app_nipsubmit'])))	
				{
				//if ($_POST['otomatis2']==1){
					$sql = "SELECT *,
						(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit)) as unit_kerja ,			
						(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit_induk)) as unit_kerja_induk ,	
						(CASE id_jns_pegawai WHEN 5 THEN CONCAT('',
(CASE id_jabatan_struktural WHEN '' THEN CONCAT('Dosen Jurusan ',(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit))) 
ELSE CONCAT((select nama_jabatan_s from jabatan_struktural where id_jabatan_s=(pegawai.id_jabatan_struktural)),' ',(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit))) 
 END)) ELSE (select nama_jabatan_s from jabatan_struktural where id_jabatan_s=(pegawai.id_jabatan_struktural)) 
END ) as jabatan,	
						(select golongan from golongan_pangkat where id_golpangkat=(pegawai.id_golpangkat_terakhir)) as golongan,			
						(select concat(pangkat,', ',golongan) from golongan_pangkat where id_golpangkat=(pegawai.id_golpangkat_terakhir)) as golpangkat
						 FROM pegawai WHERE status_pegawai < 3 and NIP = '$_POST[app_nip]'";
					$rs = mysql_query($sql);
					if($r = mysql_fetch_array($rs)){
						$app_nip = $r['NIP'];
						$app_nama = $r['gelar_depan'].$r['nama_pegawai'].$r['gelar_belakang'];
						$app_golpangkat =  $r['golpangkat'];
						if ($r['unit_kerja_induk']!=''){
						$app_unit_kerja = $r['unit_kerja'].'-'.$r['unit_kerja_induk'];
						}
						else{
						$app_unit_kerja = $r['unit_kerja'];
						}
						$app_jabatan =  $r['jabatan'];
						if (left($app_jabatan,5)=='Dekan'){
							$app_jabatan = 'Dekan';
							}
						$app_cekatasan='1';
						}
				}
				else if((isset($_POST['app_nip'])))	{
						$app_nip = $_POST['app_nip'];
						$app_nama = $_POST['app_nama'];
						$app_golpangkat = $_POST['app_golpangkat'];
						$app_unit_kerja = $_POST['app_unit_kerja'];
						$app_jabatan =  $_POST['app_jabatan'];
						$app_cekatasan='0';
					}


			?>
			<tr align="left">
			  <td width="370" align="left">NIP</td>
			  <td colspan="3" >
				<input type='text' name='app_nip' id='app_nip' value='<?=set_value('app_nip',$app_nip);?>' />  <!--onKeyUp="MakeNullAPP()"-->
					 
				<input name="app_nipsubmit" id="app_nipsubmit" size="100px" name="search" type="submit" value="&raquo;cari data"/>
					</td>
				
			  </tr>
			<tr align="left">
			  <td width="370" align="left">Nama</td>
			  <td colspan="3" ><input type='text' name='app_nama' id='app_nama' value='<?=set_value('app_nama',$app_nama);?>' size="40px"/></td>
			  </tr>
			<tr align="left">
			  <td width="370" align="left">Gol/Ruang</td>
			  <td colspan="3"><input type='text' name='app_golpangkat' id='app_golpangkat' value='<?=set_value('app_golpangkat',$app_golpangkat);?>' size="40px" /></td>
			  </tr>
			<tr align="left">
			  <td width="370" align="left">Jabatan</td>
			  <td colspan="3" ><input type='text' name='app_jabatan' id='app_jabatan' value='<?=set_value('app_jabatan',$app_jabatan);?>' size="60px"/>
			  
			    <?php 
			  if ($app_jabatan!=""){
					$bolehsimpan = $app_jabatan;}
			  else {$bolehsimpan = "";}
			  ?>
			    <input type='hidden' name='bolehsimpan' id='bolehsimpan' value='<?=set_value('bolehsimpan',$bolehsimpan);?>' size="40px"/></td>
			  </tr>
			<tr align="left">
			  <td width="370" align="left">Unit Kerja</td>
			  <td colspan="3" ><input type='text' name='app_unit_kerja' id='app_unit_kerja' value='<?=set_value('app_unit_kerja',$app_unit_kerja);?>' size="40px"/></td>
			  </tr>
			
					<?php
						//sintax pembacaan nilai huruf
						$sql = "SELECT nilai_awal,keterangan from nilai_huruf where keterangan='sedang'";
						$rs = mysql_query($sql);
						if($r = mysql_fetch_array($rs)){
							$bsedang = $r['nilai_awal'];
							}
						$sql = "SELECT nilai_awal,keterangan from nilai_huruf where keterangan='cukup'";
						$rs = mysql_query($sql);
						if($r = mysql_fetch_array($rs)){
							$bcukup = $r['nilai_awal'];
							}
						$sql = "SELECT nilai_awal,keterangan from nilai_huruf where keterangan='baik'";
						$rs = mysql_query($sql);
						if($r = mysql_fetch_array($rs)){
							$bbaik = $r['nilai_awal'];
							}
						
						$sql = "SELECT nilai_awal,keterangan from nilai_huruf where keterangan='amat baik'";
						$rs = mysql_query($sql);
						if($r = mysql_fetch_array($rs)){
							$babaik = $r['nilai_awal'];
							}
						
						$sql = "select id_unsur_dp3,urutan, nama_unsur_dp3 from unsur_dp3 order by urutan";
						$rs = mysql_query($sql);
						$i=0;
						$row_count = mysql_num_rows($rs);
					?>
			
					<tr>
					  <td width="30" align="center" rowspan="<?=5+$row_count?>" valign="top">4.</td>
					  <td colspan="4" align="center"><strong>PENILAIAN</strong></td>
				    </tr>
					
					<tr>
					  <td width="370" rowspan="2" align="center">UNSUR YANG DINILAI</td>
					  <td width="140" align="center" colspan="2">NILAI</td>
					  <td width="70" rowspan="2" align="center" >KETERANGAN</td>
					</tr>
					<tr>
					  <td width="70" align="center">ANGKA</td>
					  <td width="70" align="center">SATUAN</td>
					</tr>
					
					<input  type="hidden" name="row_count" id="row_count" value='<?=$row_count;?>' />
					<?php 
					if ($row_count==8){?>
						<input  type="hidden" name="unsur9" id="unsur[9]" value='<?=$unsur9;?>' />
						<input  type="hidden" name="unsur10" id="unsur[10]" value='<?=$unsur10;?>' />
					<? } ?>
					<?php 
					if ($row_count==9){?>
						<input  type="hidden" name="unsur10" id="unsur[10]" value='<?=$unsur10;?>' />
					<? } ?>
					<?php
						$i=0;
						while($r = mysql_fetch_array($rs)){
						$i++;
						?>
							<tr>
							  <td align="left">&nbsp;<?=$r['urutan'].'. '.$r['nama_unsur_dp3']?></td>
							  <td align="center">
								  <?php
									if ($i==1) 
										{$unsur=$unsur1;}
									else if ($i==2) 
										{$unsur=$unsur2;}
									else if ($i==3) 
										{$unsur=$unsur3;}
									else if ($i==4) 
										{$unsur=$unsur4;}
									else if ($i==5) 
										{$unsur=$unsur5;}
									else if ($i==6) 
										{$unsur=$unsur6;}
									else if ($i==7) 
										{$unsur=$unsur7;}
									else if ($i==8) 
										{$unsur=$unsur8;}
									else if ($i==9) 
										{$unsur=$unsur9;}
									else if ($i==10)
										{$unsur=$unsur10;}
									if ($unsur==0 )
										{$nilaihuruf = '-';}
									else if ($unsur<$bsedang )
										{$nilaihuruf = 'kurang';}
									else if ($unsur<$bcukup)
										{$nilaihuruf = 'sedang';}
									else if ($unsur<$bbaik)
										{$nilaihuruf = 'cukup';}
									else if ($unsur<$babaik )
										{$nilaihuruf = 'baik';}
									else if ($unsur>=$babaik)
										{$nilaihuruf = 'amat baik';}
								  ?>
						<input  type="text" name="unsur<?=$i?>" id="unsur[<?=$i?>]" value='<?=$unsur;?>' size="5px" maxlength="5" onKeyUp="MakeSum(parseFloat(document.getElementById('unsur[1]').value),parseFloat(document.getElementById('unsur[2]').value),parseFloat(document.getElementById('unsur[3]').value),parseFloat(document.getElementById('unsur[4]').value),parseFloat(document.getElementById('unsur[5]').value),parseFloat(document.getElementById('unsur[6]').value),parseFloat(document.getElementById('unsur[7]').value),parseFloat(document.getElementById('unsur[8]').value),parseFloat(document.getElementById('unsur[9]').value),parseFloat(document.getElementById('unsur[10]').value),parseInt(document.getElementById('row_count').value),<?=$bsedang?>,<?=$bcukup?>,<?=$bbaik?>,<?=$babaik?>)" />							   </td>
							  <td align="center">

								  <input  type="text" name="satuan<?=$i?>" id="satuan<?=$i?>" value="<?=$nilaihuruf?>" size="7px" readonly="yes" disabled="disabled"/>			
							  </td>
							  <td align="center">&nbsp;</td>
							</tr>
						<?	
						}
					?>
					
					<tr align="left">
					  <td width="370" align="left"><strong>Total Nilai DP3</strong> </td>
					  <td align="center"><strong><input type='text' style="background-color:#CCCCFF" name='nilai_total' id='nilai_total' size="5px" value="<?=set_value('nilai_total',$nilai_total);?>" maxlength="5" readonly="yes"/></strong>					  </td>
					  <td align="center">&nbsp;</td>
					  <td align="center">&nbsp;</td>
					</tr>
					
					<tr align="left">
					  <td  width="370" align="left"><strong>Rata-Rata Nilai DP3</strong> </td>
	<?php
	if ($nilai_dp3<$bsedang ){$satuan_nilai_dp3 = 'kurang';}else if ($nilai_dp3<$bcukup){$satuan_nilai_dp3 = 'sedang';}else if ($nilai_dp3<$bbaik){$satuan_nilai_dp3 = 'cukup';}else if ($nilai_dp3<$babaik ){$satuan_nilai_dp3 = 'baik';}else if ($nilai_dp3>=$babaik){$satuan_nilai_dp3 = 'amat baik';}
	?>
					  <td align="center"><strong><input type='text' style="background-color:#CCCCFF" name='nilai_dp3' id='nilai_dp3' value='<?=set_value('nilai_dp3',$nilai_dp3);?>' size="5px" maxlength="5"/></strong>
						  <span style="color:#F00;"><?php echo form_error('nilai_dp3'); ?></span>				  </td>
					  <td align="center"><input  type="text" name="satuan_nilai_dp3" id="satuan_nilai_dp3" value="<?=$satuan_nilai_dp3?>" size="7px" readonly="yes" disabled="disabled"/> </td>
					  <td align="center">&nbsp;</td>
					</tr>
			  
			<tr align="left">
			  <td width="30" rowspan="2" align="center">5.</td>
			  <td colspan="4"><strong>KEBERATAN DARI PEGAWAI NEGERI SIPIL <br />YANG DINILAI (APABILA ADA)</strong> <br />
			  <textarea name='keberatan' id='keberatan' rows="2" cols="40"> <?= $keberatan?> </textarea>		  </td>
			</tr>
			<tr align="left">
			  <td colspan="4">Tanggal &nbsp;&nbsp; : <input type="text" name="tgl_keberatan" id="tgl_keberatan"  
value="<?= $tgl_keberatan?>" class="datepicker" maxlength="10" size="10" /> 
<?php echo form_error('tgl_keberatan'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	
			  </td>
			  </tr>
			<tr align="left">
			  <td width="30" rowspan="2" align="center">6.</td>
			  <td colspan="4"><strong>TANGGAPAN DARI PENILAI<br />ATAS KEBERATAN</strong><br />
			  <textarea name='tanggapan' id='tanggapan' rows="2" cols="40"> <?= $tanggapan?> </textarea>		   </td>
			</tr>
			<tr align="left">
			  <td colspan="4">Tanggal &nbsp;&nbsp; : <input type="text" name="tgl_tanggapan" id="tgl_tanggapan"  
value="<?= $tgl_tanggapan?>" class="datepicker" maxlength="10" size="10" /> 
<?php echo form_error('tgl_tanggapan'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	
			  </td>
			  </tr>
			<tr align="left">
			  <td width="60" rowspan="2" align="center" >7.</td>
			  <td colspan="4"><strong>KEPUTUSAN ATASAN PEJABAT PENILAI <BR /> ATAS KEBERATAN</strong> <br />
			  <textarea name='keputusan' id='keputusan' rows="2" cols="40"> <?= $keputusan?> </textarea>		  </td>
			</tr>
			<tr align="left">
			  <td colspan="4">Tanggal &nbsp;&nbsp; : <input type="text" name="tgl_keputusan" id="tgl_keputusan"  
value="<?= $tgl_keputusan?>" class="datepicker" maxlength="10" size="10" /> 
<?php echo form_error('tgl_keputusan'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	
			  </td>
			</tr>
			 
			<tr align="left">
			  <td align="center" >8.</td>
			  <td colspan="4"><strong>Keterangan lain</strong> <br />
			  <textarea name='keterangan' id='keterangan' rows="2" cols="40"> <?= $keterangan?> </textarea>			</td>
			</tr>
			<tr align="left">
			  <td colspan="2">Tanggal Buat &nbsp;&nbsp; </td>
			  <td colspan="3"><input type="text" name="tgl_buat" id="tgl_buat"  
value="<?= $tgl_buat?>" class="datepicker" maxlength="10" size="10" /> 
<?php echo form_error('tgl_buat'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	
			  </td>
			</tr>
			<tr align="left">
			  <td colspan="2">Tanggal diterima oleh PNS&nbsp;&nbsp; </td>
			  <td colspan="3"><input type="text" name="tgl_terima_dinilai" id="tgl_terima_dinilai"  
value="<?= $tgl_terima_dinilai?>" class="datepicker" maxlength="10" size="10" /> 
<?php echo form_error('tgl_terima_dinilai'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	
			  </td>
			</tr>
			<tr align="left">
			  <td colspan="2">Tanggal diterima oleh Pejabat Penilai &nbsp;&nbsp; </<br />
			  <td colspan="3"><input type="text" name="tgl_terima_penilai" id="tgl_terima_penilai"  
value="<?= $tgl_terima_penilai?>" class="datepicker" maxlength="10" size="10" /> 
<?php echo form_error('tgl_terima_penilai'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	
			  </td>
			</tr>
		</table>
	</td>
	</tr>
	
	</table>
	<br/>
	<?php	
	   //if($this->user->user_group=="Administrator") {
	?>
	<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
	<? //} ?>
</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>
