<?
//$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
	<?php	
		if($this->user->user_group=="Administrator") {
	?>
			<a class="icon" href="<?=site_url("pegawai/riwayatjabatanstruktural/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
		<? } ?>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="jabatandetails2" id="jabatandetails2" method="POST" action="<?=site_url('pegawai/riwayatjabatanstruktural/browse')?>" >
	</form>
	</center>
	<br/>
	<table class="table-list">
	<tr class="trhead">
		<th width="20" >No</th>
		<th width="53" class="colEvn" >Jabatan</th>
		<th width="52" >No. SK </th>
		<th width="83" class="colEvn">Tanggal SK </th>
		<th width="77" >TMT Jabatan </th>
		<th  width="114">Unit Kerja </th>
		<th  width="172" class="colEvn">Keterangan </th>
		<th width="101" >Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_jabatan_struktural!=FALSE){
	foreach ($list_jabatan_struktural as $jns_jabatan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
       if ($jns_jabatan['tgl_sk_jabatan']=='0000-00-00') {
                $str_sk_jabatan = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tgl_sk_jabatan']);
                $str_sk_jabatan = $d . "-" . $m . "-" . $y;
            }
        $jns_jabatan['tgl_sk_jabatan']=$str_sk_jabatan;
        if ($jns_jabatan['tmt_jabatan']=='0000-00-00') {
                $str_tmt_jabatan = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tmt_jabatan']);
                $str_tmt_jabatan = $d . "-" . $m . "-" . $y;
            }
        $jns_jabatan['tmt_jabatan']=$str_tmt_jabatan;
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="center" ><?= $jabatan_struktural_assoc[$jns_jabatan['id_jabatan_s']]; ?></td>
		<td align="center" ><?= $jns_jabatan['no_sk_jabatan']; ?></td>
		<td align="center" ><?= $jns_jabatan['tgl_sk_jabatan']; ?></td>
		<td align="center" ><?= $jns_jabatan['tmt_jabatan']; ?></td>
		<td align="left" ><?= $unit_kerja_assoc[$jns_jabatan['kode_unit']]; ?></td>
		
		<td align="left" ><?= $jns_jabatan['keterangan']; ?></td>
		<td align="center" class="colEvn">		
	<?php	
		if($this->user->user_group=="Administrator") {
	?>	
			<a href="<?=site_url("pegawai/riwayatjabatanstruktural/edit/".$jns_jabatan['id_riwayat_jabatan_s']); ?>" class="edit action" >Ubah</a>
			<?php if($jns_jabatan['aktif']=='0') {?>
			<a href="<?=site_url("pegawai/riwayatjabatanstruktural/delete/".$jns_jabatan['id_riwayat_jabatan_s']); ?>" class="delete action" >Hapus</a>	
		<?  } ?>	</td>
			<? } ?>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

