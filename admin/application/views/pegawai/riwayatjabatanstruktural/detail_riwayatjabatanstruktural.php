<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatjabatanstruktural/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/riwayatjabatanstruktural/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
			
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="jabatandetail2" id="jabatandetail2" method="POST" action="<?= $action_url; ?>">
			 <?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
			 <?php echo form_hidden('NIP', $pegawai['NIP']); ?>
			<table class="general">
				<tr align="left">
					<td width ="200">Kode</td>
					<td> : </td>
					<td >
					<?= $id_riwayat_jabatan_s?></td>
				</tr>
				
				<tr align="left">
				  <td>Jabatan</td>
				  <td> : </td>
				  <td ><?php
						echo form_dropdown('id_jabatan_s',$jabatan_struktural_assoc, $id_jabatan_s,'class="js_required"');
						?> <? echo form_error('id_jabatan_s'); ?> </td>
				  </tr>
				  <tr align="left">
				  <td>Keterangan</td>
				  <td> : </td>
				  <td >
						<input size="45" type="text" name="keterangan" id="keterangan" value="<?= $keterangan?>" />
					  	<?php echo form_error('keterangan'); ?> </td>
				  </tr>
				<tr align="left">
					<td width ="200">Nomor SK</td>
					<td> : </td>
					<td >
						<input size="25" type="text" name="no_sk_jabatan" id="no_sk_jabatan" class="js_required" value="<?= $no_sk_jabatan?>" class="required" />
						<span style="color:#F00;">*<?php echo form_error('no_sk_jabatan'); ?></span>		</td>
				</tr>
				<tr align="left">
					<td width ="200">Tanggal SK</td>
					<td> : </td>
					<td >
						<input type="text" class="datepicker" name="tgl_sk_jabatan" id="tgl_sk_jabatan" class="js_required" value="<?= $tgl_sk_jabatan?>" class="required" />
						<?php echo form_error('tgl_sk_jabatan'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
				</tr>
				
				<tr align="left">
					<td width ="200px">TMT Jabatan</td>
					<td> : </td>
					<td >
						<input type="text" class="datepicker" name="tmt_jabatan" id="tmt_jabatan" value="<?=set_value('tmt_jabatan',$tmt_jabatan);?>" />
						<span style="color:#F00;"><?php echo form_error('tmt_jabatan'); ?></span>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span>	</td>
				</tr>
				<tr align="left">
					<td width ="200">Tahun Mulai </td>
					<td> : </td>
					<td >
						<input size="20" type="text" name="tahun_mulai" id="tahun_mulai" value="<?= $tahun_mulai?>" />
					  	<?php echo form_error('tahun_mulai'); ?></td>
				</tr>
				<tr align="left">
					<td width ="200">Tahun Selesai </td>
					<td> : </td>
					<td >
						<input size="20" type="text" name="tahun_selesai" id="tahun_selesai" value="<?= $tahun_selesai?>" />
					  	<?php echo form_error('tahun_selesai'); ?></td>
				</tr>
				<tr>
					<td  align="left">Unit Kerja</td>
					<td> : </td>
					<td colspan="3"  align="left">
						<?php
							echo form_group_dropdown('kode_unit',$option_unit,$kode_unit);
							echo form_error('kode_unit');
							//echo form_dropdown('kode_unit',$unit_kerja_assoc,$kode_unit,'class="js_required"');
							//echo form_error('kode_unit');
						?>	
							
						
					</td>
							</tr>
							
				
				<tr align="left">
					<td width ="200px">Aktif</td>
					<td> : </td>
					<td width ="200px">
					<?php
						echo form_dropdown('aktif',$status_assoc, $aktif,'class="js_required"');
						?>
						<?
						echo form_error('aktif'); ?>  </td>
				</tr>
			</table>
			<br/>
			<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>