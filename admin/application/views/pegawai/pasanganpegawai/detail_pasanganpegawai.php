<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/pasanganpegawai/$action/" .$pegawai['kd_pegawai'];
$search_url = site_url() . "/pegawai/pasanganpegawai/cari/" .$pegawai['kd_pegawai'];
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/pasanganpegawai/index/' .$pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
	<form name="searchdetails" id="searchdetails" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr>
			  <td colspan="4">Ketikkan NIP jika pasangan juga merupakan Pegawai UM </td>
			</tr>
			<tr>
				<td width="28">NIP</td>
				<td width="3">:</td>
				<td width="153">
				<input id="nip" name="nip" />			</td>
				<td width="178">
					<div class="submit"><input id="submit" size="100px" name="search" type="submit" value="&nbsp;&nbsp;&nbsp;  Cari  &nbsp;&nbsp;&nbsp;"/>
					<br /><br /></div>			</td>
			</tr>
		 </table>
	</form>
		<?php
		
		if(isset($_POST['search']))	
		{
		$sql = "SELECT *,(select jenis_pegawai from jenis_pegawai where id_jns_pegawai = (pegawai.id_jns_pegawai)) as jenis_pegawai, (select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit)) as unit_kerja  FROM pegawai WHERE status_pegawai < 3 and NIP = '$_POST[nip]'";
		$rs = mysql_query($sql);
		$kode=$kategori=$bentuk=$skor=$keterangan = '';
		if($r = mysql_fetch_array($rs)){
			$kd_pegawai_pasangan =$r['kd_pegawai'];
			$nama_pasangan =  $r['gelar_depan']." ".$r['nama_pegawai']." ".$pegawai['gelar_belakang'];
			$tempat_lahir = $r['tempat_lahir'];
			$kerja = $r['jenis_pegawai'];
			$unit = $r['unit_kerja'];
			$tanggal_lahir = $r['tgl_lahir'];
			$pekerjaan = $kerja . ', ' . $unit;
			}
		}
		
		?>
		
	<form name="pasangandetail" id="pasangandetail" method="POST" action="<?= $action_url; ?>">
		<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']);
		echo form_hidden('NIP_pasangan', $_POST[nip]);
		?>
		<input type="hidden" name="kd_pegawai_pasangan" id="kd_pegawai_pasangan" class="js_required" value="<?= $kd_pegawai_pasangan?>" />
		<table class="general">
			<tr align="left">
				<td width ="200">Kode <?= $jk;?></td>
				<td> : </td>
				<td width ="400">
				<?= $id_pasangan_pegawai;?></td>
			</tr>
<!--			<tr align="left">
				<td width ="200">Status Keluarga </td>
				<td> : </td>
				<td width ="400">
				<?php
					echo form_dropdown('kd_status_keluarga',$status_keluarga_assoc, $kd_status_keluarga);
					echo form_error('kd_status_keluarga'); ?>		</td>
			</tr>-->
			<input type="hidden" name="kd_status_keluarga" id="kd_status_keluarga" class="js_required" value="<?= $kd_status_keluarga?>" />
			<tr align="left">
				<td width ="200">Nama</td>
				<td> : </td>
				<td width ="400">
				<input type="text" name="nama_pasangan" id="nama_pasangan" class="js_required" value="<?= $nama_pasangan?>" size="50" />
				<?php echo form_error('nama_pasangan'); ?>  </td>
			</tr>
			
			<tr>
					<td  align="left">Tempat dan Tanggal Lahir</td>
					<td> : </td>
					<td width ="400" align="left">
						<input type="text" class="required" class="js_required" name="tempat_lahir" id="tempat_lahir" value="<?= set_value('tempat_lahir', $tempat_lahir); ?>" />
						&nbsp;&nbsp; 
						<input  type="text" class="datepicker" class="js_required" name="tanggal_lahir" id="tanggal_lahir" value="<?= set_value('tanggal_lahir', $tanggal_lahir); ?>" class="required"/>
							<?php echo form_error('tanggal_lahir'); ?>&nbsp;<?php echo form_error('tanggal_lahir'); ?> <span style="color:#F00;"><i> yyyy-mm-dd</i></span> </td>
				</tr>
			<tr align="left">
				<td width ="200">Tanggal Menikah</td>
				<td> : </td>
				<td width ="400">
				<input type="text" class="datepicker" class="js_required" name="tanggal_nikah" id="tanggal_nikah" value="<?= set_value('tanggal_nikah', $tanggal_nikah); ?>" class="required" />
				<?php echo form_error('tanggal_nikah'); ?> <span style="color:#F00;"><i> yyyy-mm-dd</i></span> </td>
			</tr><tr align="left">
				<td width ="200">No. Karis/Karsu</td>
				<td> : </td>
				<td width ="400">
				<input type="text" name="no_karis_karsu" id="no_karis_karsu" value="<?= $no_karis_karsu?>" />
				<?php echo form_error('no_karis_karsu'); ?>  </td>
			</tr>
			<tr align="left">
				<td width ="200">No. Urut</td>
				<td> : </td>
				<td width ="400">
				<input type="text" name="no_urut" id="no_urut" value="<?= $no_urut?>" /> 
				<?php echo form_error('no_urut'); ?>  </td>
			</tr>
			<tr align="left">
				<td width ="200">Pekerjaan</td>
				<td> : </td>
				<td width ="400">
				<input size="60" type="text" name="pekerjaan" id="pekerjaan" value="<?= $pekerjaan?>"/>
				<?php echo form_error('pekerjaan'); ?>  </td>
			</tr>
			<tr align="left">
				<td width ="200">Tanggal Karis/Karsu</td>
				<td> : </td>
				<td width ="400">
				<input type="text" class="datepicker" class="js_required" name="tgl_karis_karsu" id="tgl_karis_karsu" value='<?= set_value('tgl_karis_karsu', $tgl_karis_karsu); ?>' class="required" />
				<?php echo form_error('tgl_karis_karsu'); ?>  <span style="color:#F00;"><i> yyyy-mm-dd</i></span></td>
			</tr><tr align="left">
				<td width ="200">Keterangan</td>
				<td> : </td>
				<td width ="400">
				<input size="60" type="text" name="keterangan" id="keterangan" value="<?= $keterangan?>"/>
				<?php echo form_error('keterangan'); ?>  </td>
			</tr>
			<tr align="left">
				<td width ="200">Aktif</td>
				<td> : </td>
				<td width ="400">
				<?php
					echo form_dropdown('aktif',$status_assoc, $aktif);
					echo form_error('aktif'); ?>  </td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
/*$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});*/
</script>