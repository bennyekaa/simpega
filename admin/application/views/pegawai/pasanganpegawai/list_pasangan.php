<?
//$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');
?>

<div class="content">
<div id="content-header">

	<div id="module-title"><h3><?=$judul;?></h3></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url("pegawai/pegawai/add")?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
	
	<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
<center>
	
		<form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?=site_url('pegawai/pegawai/browse')?>" >
			<label>Nama Pegawai
			<input id="search" name="search" value="<?=set_value('search',$search);?>" />
			Jenis Pegawai : </label>
			<?=form_dropdown('group',$jenis_pegawai_assoc,$group);?>
			<label>Unit Kerja : </label>
			<?=form_group_dropdown('unit_kerja',$option_unit,$unit_kerja);?>
			
			<input type="submit" value="Filter">
		</form>
		</center>
		<br/>
<? //if (){
	//echo "Nama Pegawai= ".$search;}
	//if (($group==0)or($unit_kerja==0)) {
	//	echo "<h3>Jenis Pegawai : -Semua Jenis Pegawai-&nbsp;&nbsp;&nbsp;&nbsp;Unit Kerja : -Semua Unit Kerja-</h3>";
	//}
	//else
	//{
		echo "<h3>Jenis Pegawai : ".$nama_jenis[$group]."&nbsp;&nbsp;&nbsp;&nbsp;Unit Kerja :".$unitkerja_assoc[$unit_kerja]."</h3>";
	//}
	
	?>
		<table class="table-list">
			<tr class="trhead">
				<th  width="20" rowspan="2" align="center" class="colEvn">No</th>
				<th width="155" align="center" class="trhead" ><span class="trhead" >Nama</span></th>
				<th width="144" align="center" class="trhead">NIP</th>
				<th class="colEvn" width="76" align="center">Gol/Ruang</th>
				<th width="213" rowspan="2" align="center" class="trhead" >Pendidikan</th>
				<th width="205" rowspan="2" align="center" class="trhead" >Alamat</th>
				<th class="trhead" width="163" align="center" >Jabatan</th>
				<th width="71" rowspan="2" align="center" class="colEvn">Edit</th>
			</tr>
			<tr class="trheadEvn">
			  <th width="155" align="center" class="trhead">Tempat, Tanggal Lahir</th>
				  <th width="144" align="center" class="trhead">Karpeg</th>
				  <td class="trheadEvn" align="center">TMT</td>
				  <td class="trheadEvn" align="center">Unit Kerja </td>
			  </tr>
				
				<?
				$i = $start++;
				if ($kerja_list!=FALSE){
				foreach ($kerja_list as $pegawai) {
					$i++;
					if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
					list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
					$str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
					
					list($y, $m, $d) = explode('-', $pegawai['tmt_golpangkat_terakhir']);
					$str_tmt_golpangkat_terakhir = $d . "/" . $m . "/" . $y;
					$pegawai['tmt_golpangkat_terakhir']=$str_tmt_golpangkat_terakhir;
					if ($jabatan_assoc[$pegawai['id_jabatan_terakhir']]=='Fungsional Umum')
					{
						$jabatan_assoc[$pegawai['id_jabatan_terakhir']]='-';
					}
				?>   
				
			  <tr class="<?= $class; ?>">
				<td rowspan="2"  align="right" class="colEvn"><?= $i; ?></td>
				<td align="left" class="trhead">
				  <?=$pegawai['gelar_depan']." ".strtoupper($pegawai['nama_pegawai'])." ".$pegawai['gelar_belakang']?>
				</td>
				<td align="left" class="trhead"><?=$pegawai['NIP']?></td>
				<td  align="left" class="colEvn"><span class="trhead">
				  <?=$golongan_assoc[$pegawai['id_golpangkat_terakhir']]?>
				</span></td>
				<td rowspan="2" align="left" class="trhead"><?=$pegawai['ket_pendidikan']?></td>
				<td rowspan="2" align="left" class="trhead"><?=$pegawai['alamat']?></td>
				<td align="left" class="trhead"><?=$jabatan_assoc[$pegawai['id_jabatan_terakhir']]?></td>
				<td rowspan="2"  align="center" class="trhead">			
					<a href="<?=site_url("pegawai/pegawai/view/".$pegawai['kd_pegawai']); ?>" title="Detil Data Pegawai" class="view action">Lihat Detil</a>
					<a href = "<?=site_url("pegawai/pegawai/delete/".$pegawai['kd_pegawai']); ?>" class="nyroModal delete action" >Hapus</a>		</td>
			</tr>
			  <tr class="<?= $class; ?>">
				<td align="left" class="trhead">
				  <?=$pegawai['tempat_lahir']?>
				, 
				<?=$str_tgl_lahir?>
				</td>
				<td align="left" class="trhead"><?=$pegawai['karpeg']?></td>
				<td  align="left" class="colEvn"><?=$pegawai['tmt_golpangkat_terakhir']?></td>
				<td align="left" class="trhead"><?=$unitkerja_assoc[$pegawai['kode_unit']]?></td>
			  </tr>
			<? } }
			else {
				echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
			} ?>
		</table>
		<br />
		<div class="paging"><?=$page_links?></div>
	</div>
</div>