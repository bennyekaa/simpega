<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/jabatan/add')?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/add-icon.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div class="paging"><?=$page_links?></div>
	<div>
		<table class="table-list">
			<tr class="trhead">
				<th width='50' class="colEvn">Kode</th>
				<th width='150'  class="trhead">Jabatan</th>
				<th width='80' class="colEvn">Periode</th>
				<th width='170'  class="trhead">Instansi</th>
				<th width='100' class="colEvn">Keterangan</th>		
				<th  class="trhead">Edit</th>
			</tr>
		
			<?
			$i = 0;
			if ($list_jabatan!=FALSE){
			foreach ($list_jabatan as $jns_jabatan) {
			   $i++;
			   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			?>
			<tr class="<?=$bgColor?>">
				<td align="center" class="colEvn"><?=$jns_jabatan['id_riwayat_jabatan'];?></td>
				<td align="left" ><?= $jns_jabatan['jabatan']; ?></td>
				<td align="center" ><?= $jns_jabatan['periode']; ?></td>
				<td align="left" ><?= $jns_jabatan['instansi_jabat']; ?></td>
				<td align="left" ><?= $jns_jabatan['ket_riwayat_jabatan']; ?></td>
				<td align="center" class="colEvn">			
					<a href = "<?=site_url("pegawai/jabatan/edit/".$jns_jabatan['id_riwayat_jabatan']); ?>" class="edit action" >Ubah</a>
					<a href = "<?=site_url("pegawai/jabatan/delete/".$jns_jabatan['id_riwayat_jabatan']); ?>" class="nyroModal delete action" >Hapus</a>
				</td>
			</tr>
			<? } ?>
			<?}
			else {
				echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
			}?>
		</table>
	</div>
</div>

