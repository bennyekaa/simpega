<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/jabatan/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/jabatan/index')?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel-icon.png'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="jabatandetail" id="jabatandetail" method="POST" action="<?= $action_url; ?>">
			<table class="general">
				<tr align="left">
					<td width ="200px">Kode</td>
					<td> : </td>
					<td width ="200px">
					<?= $id_riwayat_jabatan?></td>
				</tr>
				<tr align="left">
					<td width ="200px">Jabatan</td>
					<td> : </td>
					<td width ="200px">
					<?php
						echo form_dropdown('id_jabatan',$jabatan_assoc, $id_jabatan);
						echo form_error('id_jabatan'); ?>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Periode</td>
					<td> : </td>
					<td width ="200px">
					<input type='text' name='periode' id='periode' class="js_required required" onkeyup="gsub_tahun(this)" onchange="gsub_num(this)" 
						onblur="gsub_num(this)" value='<?= $periode?>'/>
					<?php echo form_error('periode'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Instansi</td>
					<td> : </td>
					<td width ="200px">
					<input type='text' name='instansi_jabat' id='instansi_jabat' class="js_required required" value='<?= $instansi_jabat?>'/>
					<?php echo form_error('instansi_jabat'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">No SK</td>
					<td> : </td>
					<td width ="200px">
					<input type='text' name='no_SK' id='no_SK' class="js_required required" value='<?= $no_SK?>'/>
					<?php echo form_error('no_SK'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Tanggal SK</td>
					<td> : </td>
					<td width ="200px">
					<input type='text' name='tgl_SK' id='tgl_SK' class="datepicker js_required required" value='<?= $tgl_SK?>'>
					<?php echo form_error('tgl_SK'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Keterangan</td>
					<td> : </td>
					<td width ="200px">
					<input type='text' name='ket_riwayat_jabatan' id='ket_riwayat_jabatan' value='<?= $ket_riwayat_jabatan?>'/>
					<?php echo form_error('ket_riwayat_jabatan'); ?>  </td>
				</tr>
			</table>
			<br/>
			<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>