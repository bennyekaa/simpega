<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatkeluarga/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/riwayatkeluarga/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
			
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="keluargadetail" id="keluargadetail" method="POST" action="<?= $action_url; ?>">
			<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
			<table class="general">
				<tr align="left">
					<td width ="200px">Kode</td>
					<td> : </td>
					<td width ="200px">
					<?= $kd_keluarga?></td>
				</tr>
				<tr align="left">
					<td width ="200px">Status Keluarga</td>
					<td> : </td>
					<td width ="200px">
					<?php
						echo form_dropdown('kd_status_keluarga',$status_keluarga_assoc, $kd_status_keluarga);
						echo form_error('kd_status_keluarga'); ?>					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Nama</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" size="35" name="nama_keluarga" id="nama_keluarga" class="js_required" value="<?= $nama_keluarga?>"  class="js_required"/>
					<?php echo form_error('nama_keluarga'); ?>  </td>
				</tr>
				<tr>
					<td  align="left">Tempat dan Tanggal Lahir</td>
					<td> : </td>
					<td width ="400px" align="left">
						<input type="text" class="required" class="js_required" name="tempat_lahir" id="tempat_lahir" value="<?= set_value('tempat_lahir', $tempat_lahir); ?>" />
						&nbsp;&nbsp;
						<input type="text" class="datepicker" class="js_required" name="tgl_lahir" id="tgl_lahir" value="<?= set_value('tgl_lahir', $tgl_lahir); ?>" class="required" />
						<?php echo form_error('tgl_lahir'); ?>&nbsp;<?php echo form_error('tgl_lahir'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span> </td>
				</tr>
				<tr>
					<td  align="left">Jenis Kelamin</td>
					<td> : </td>
					<td  align="left">
					<?
						echo form_dropdown('jns_kelamin', $gender_assoc, $jns_kelamin, 'class="js_required"');
					?>
					<?php echo form_error('jns_kelamin'); ?>					</td>
				</tr>
				
				<tr align="left">
			  <td>Nama Pendidikan </td>
			  <td> : </td>
			  <td ><?php
					echo form_dropdown('id_pendidikan_keluarga',$pendidikan_assoc, $id_pendidikan_keluarga, 'class="js_required"');
					?>*<? echo form_error('id_pendidikan_keluarga'); ?></td>
			  </tr>
			  <tr align="left">
					<td width ="200px">Pekerjaan</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" size="35" name="pekerjaan" id="pekerjaan" value="<?= $pekerjaan?>"  class="js_required"/>
					<?php echo form_error('pekerjaan'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Keterangan</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" name="keterangan" id="keterangan" value="<?= $keterangan?>" />
					<?php echo form_error('keterangan'); ?>  </td>
				</tr>
				
				
				<tr align="left">
					<td width ="200px">Aktif</td>
					<td> : </td>
					<td width ="200px">
					<?php
						echo form_dropdown('aktif',$status_assoc, $aktif);
						echo form_error('aktif'); ?>  </td>
				</tr>
				
			
			</table>
			<br/>
			<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>