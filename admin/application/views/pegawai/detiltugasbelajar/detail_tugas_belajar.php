<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/detiltugasbelajar/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?> </div>
	<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/detiltugasbelajar/browse/' . $id_riwayat_tb)?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
			
		</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="detailtb" id="detailtb" method="POST" action="<?= $action_url; ?>">
	<?php echo form_hidden('id_riwayat_tb', $id_riwayat_tb); ?>
		<table class="general">
			<tr align="left">
				<td width ="200">Kode</td>
				<td> : </td>
				<td >
				<?= $id_detil_riwayat_tb?></td>
			</tr>
			<tr align="left">
					<td width ="291">Nomor SK Penunjang </td>
					<td> : </td>
					<td >
						<input type="text" size="60" name="no_SK_penunjang" id="no_SK_penunjang" value="<?= $no_SK_penunjang?>"/>
						
					<?php echo form_error('no_SK_penunjang'); ?>	</td>
		    </tr>
			<tr align="left">
				<td width ="200">Tanggal SK Penunjang </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tgl_SK_penunjang' id='tgl_SK_penunjang'  value='<?= $tgl_SK_penunjang?>' class="required"/>
					<?php echo form_error('tgl_SK_penunjang'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
			</tr>
			<tr align="left">
				<td width ="200">Keterangan </td>
				<td> : </td>
				<td >
				<textarea name='keterangan' id='keterangan' rows="2" cols="40"> <?= $keterangan?> </textarea>
				<?php echo form_error('keterangan'); ?> </td>
			</tr>		
		</table>
		<br/>
		<div class="submit" style="text-align:center">
		<input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?> type="submit"  value="  Simpan  " />
		</div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>