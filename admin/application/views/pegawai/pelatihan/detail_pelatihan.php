<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/pelatihan/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/pelatihan/index')?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel-icon.png'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="pelatihandetail" id="pelatihandetail" method="POST" action="<?= $action_url; ?>">
			<table class="general">
				<tr align="left">
					<td width ="200px">Kode</td>
					<td> : </td>
					<td width ="200px">
					<?= $id_riwayat_pelatihan?></td>
				</tr>
				<tr align="left">
					<td width ="200px">Tanggal Mulai Pelatihan</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" class="datepicker" name="tgl_mulai" id="tgl_mulai" value="<?= $tgl_mulai?>"/>
					<?php echo form_error('tgl_mulai'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Tanggal Akhir Pelatihan</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" class="datepicker" name="tgl_akhir" id="tgl_akhir" value="<?= $tgl_akhir?>"/>
					<?php echo form_error('tgl_akhir'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Nomor SK</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" name="no_sk" id="no_sk" class="js_required" value="<?= $no_sk?>" class="required" />
					<?php echo form_error('no_sk'); ?>  </td>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Tanggal SK</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" class="datepicker" name="tgl_sk" id="tgl_sk" class="js_required" value="<?= $tgl_sk?>" class="required" />
					<?php echo form_error('tgl_sk'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Jenis Pelatihan</td>
					<td> : </td>
					<td width ="200px">
					<?php
						echo form_dropdown('FK_jns_pelatihan',$jenis_pelatihan_assoc, $FK_jns_pelatihan);
						echo form_error('FK_jns_pelatihan'); ?>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Nama Pelatihan</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" name="nama_pelatihan" id="nama_pelatihan" class="js_required" value="<?= $nama_pelatihan?>"  class="required"/>
					<?php echo form_error('nama_pelatihan'); ?>  </td>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Nama Lembaga</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" name="nama_lembaga" id="nama_lembaga" value="<?= $nama_lembaga?>" />
					<?php echo form_error('nama_lembaga'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Keterangan</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" name="ket_riwayat_pelatihan" id="ket_riwayat_pelatihan" value="<?= $ket_riwayat_pelatihan?>" />
					<?php echo form_error('ket_riwayat_pelatihan'); ?>  </td>
				</tr>
			</table>
			<br/>
			<!--<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add")
			{?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>-->
			<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>