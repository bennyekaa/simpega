<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/pelatihan/add')?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/add-icon.png'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<br/>
	<div class="paging"><?=$page_links?></div>
	<div>
		<table class="table-list">
		<tr class="trhead">
			<th width='50' >Kode</th>
			<th width='150' class="colEvn">Nama Pelatihan</th>
			<th width='100' >Jenis Pelatihan</th>
			<th width='100' class="colEvn">Tgl Mulai</th>
			<th width='100' >Tgl Berakhir</th>
			<th width='100' class="colEvn">Nama Lembaga</th>
			<th class="colEvn">Edit</th>
		</tr>
		
		<?
		$i = 0;
		if ($list_pelatihan!=FALSE){
		foreach ($list_pelatihan as $jns_pelatihan) {
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$jns_pelatihan['id_riwayat_pelatihan'];?></td>
			<td align="left" ><?= $jns_pelatihan['jenis_pelatihan']; ?></td>
			<td align="left" ><?= $jns_pelatihan['nama_pelatihan'] ?></td>
			<td align="left" ><?= $jns_pelatihan['tgl_mulai']; ?></td>
			<td align="left" ><?= $jns_pelatihan['tgl_akhir']; ?></td>
			<td align="left" ><?= $jns_pelatihan['nama_lembaga']; ?></td>
			<td align="center" class="colEvn">			
				<a href = "<?=site_url("pegawai/pelatihan/edit/".$jns_pelatihan['id_riwayat_pelatihan']); ?>" class="edit action" >Ubah</a>
				<a href = "<?=site_url("pegawai/pelatihan/delete/".$jns_pelatihan['id_riwayat_pelatihan']); ?>" class="nyroModal delete action" >Hapus</a>
			</td>
		</tr>
	
		<? } ?>
		<?}
		else {
			echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
		}?>
		</table>
	</div>
</div>

