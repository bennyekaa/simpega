<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
	<?php	
					if($this->user->user_group=="Administrator") {
				?>
		<a class="icon" href="<?=site_url("pegawai/riwayatgolpangkat/add/" . $pegawai['kd_pegawai']."/".$pegawai['id_golpangkat_terakhir'])?>" title="Tambah">
		
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
		<? } ?>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="pangkatdetails" id="pangkatdetails" method="POST" action="<?=site_url('pegawai/riwayatgolpangkat/browse')?>" >
	</form>
	</center>
	<br/>
	<table width="960" class="table-list">
	<tr class="trhead">
		<th width="24" >No <?= $status_pegawai?></th>
		<th width="126" >Gol/Ruang</th>
		<th width="65" >Pejabat </th>
		<th width="61" >No. SK </th>
		<th width="93" >Tanggal SK</th>
		<th width="53" >TMT</th>
		<th width="99" >Masa Kerja</th>
		<th width="111" >Gaji Pokok</th>
		<th width="64" >Status Pegawai</th>
		<th width="83" >Keterangan</th>
		<th width="40" >Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_pangkat!=FALSE){
	foreach ($list_pangkat as $jns_pangkat) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
        if ($jns_pangkat['tgl_SK_pangkat']=='0000-00-00') {
                $str_sk_pangkat = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_pangkat['tgl_SK_pangkat']);
                $str_sk_pangkat = $d . "/" . $m . "/" . $y;
            }
        $jns_pangkat['tgl_SK_pangkat']=$str_sk_pangkat;
        if ($jns_pangkat['tmt_pangkat']=='0000-00-00') {
                $str_tmt_pangkat = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_pangkat['tmt_pangkat']);
                $str_tmt_pangkat = $d . "/" . $m . "/" . $y;
            }
        $jns_pangkat['tmt_pangkat']=$str_tmt_pangkat;
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<?
			if ($status_pegawai_assoc[$jns_pangkat['status_pegawai']]=='CPNS')
			{
			?>
				<td align="left" ><?= $gol_assoc[$jns_pangkat['id_golpangkat']]; ?></td>
			<?
			}else
			{
			?>
			 	<td align="left" ><?= $golongan_assoc[$jns_pangkat['id_golpangkat']]; ?></td>
			<? } ?>
		
		<td align="left" ><?= $jns_pangkat['pejabat']; ?></td>
		<td align="center" ><?= $jns_pangkat['no_SK_pangkat']; ?></td>
		<td align="left" ><?= $jns_pangkat['tgl_SK_pangkat']; ?></td>
		<td align="center" ><?= $jns_pangkat['tmt_pangkat']; ?></td>
		<td align="center" ><?= $jns_pangkat['mk_tahun']; ?> th <?= $jns_pangkat['mk_bulan']; ?> bl</td>
		<td align="center" ><?= $jns_pangkat['gaji_pokok']; ?></td>		
		<td align="center" ><?= $status_pegawai_assoc[$jns_pangkat['status_pegawai']]; ?></td>
		<td align="center" ><?= $jns_pangkat['keterangan']; ?></td>			
		<td width="89" align="center" class="colEvn">		
		<?php	
					if($this->user->user_group=="Administrator") {
				?>	
			<a href="<?=site_url("pegawai/riwayatgolpangkat/edit/".$jns_pangkat['id_riwayat_gol']); ?>" class="edit action" >Ubah</a>
			<?php if($jns_pangkat['aktif']=='0') {?>
				<a href="<?=site_url("pegawai/riwayatgolpangkat/delete/".$jns_pangkat['id_riwayat_gol']); ?>" class="delete action" >Hapus</a>
			<? } }?>		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

