<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatpendidikan/$action/";
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/riwayatpendidikan/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div>
	<form name="pendidikandetail" id="pendidikandetail" method="POST" action="<?= $action_url; ?>">
		<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
		<?php echo form_hidden('NIP', $pegawai['NIP']); ?>
		<table class="general">
			<tr align="left">
				<td width ="200">Kode</td>
				<td width="7"> : </td>
				<td >
				<?= $id_riwayat_pendidikan?></td>
			</tr>
			
			<tr align="left">
			  <td>Nama Pendidikan </td>
			  <td> : </td>
			  <td ><?php
					echo form_dropdown('id_pendidikan',$pendidikan_assoc, $id_pendidikan, 'class="js_required"');
					?><span style="color:#F00;">&nbsp;*</span><? echo form_error('id_pendidikan'); ?></td>
		  </tr>
			
			
			
			<tr align="left">
			  <td>Nama Tempat Studi </td>
			  <td> : </td>
			  <td ><input size="62" type='text' name='universitas' id='universitas' value='<?=set_value('universitas',$universitas);?>' />
		      <?php echo form_error('universitas'); ?></td>
			  </tr>
			  <tr align="left">
			    <td>Alamat</td>
			    <td>:</td>
			    <td ><textarea rows="2" id="alamat" name="alamat" cols="50" class="js_required"><?=set_value('alamat',$alamat);?> </textarea></td>
	      </tr>
			  <tr align="left">
			  <td>Kota </td>
			  <td> : </td>
			  <td ><input size="62" type='text' name='kota' id='kota' value='<?=set_value('kota',$kota);?>' />
		      <?php echo form_error('kota'); ?></td>
			  </tr>
			  <tr align="left">
			  <td>Negara</td>
			  <td> : </td>
			  <td ><input size="62" type='text' name='negara' id='negara' value='<?=set_value('negara',$negara);?>' />
		      <?php echo form_error('negara'); ?></td>
			  </tr>
			  <tr align="left">
			    <td>Nomor Ijazah </td>
			    <td>:</td>
			    <td ><input size="62" type='text' name='nomor_ijazah' id='nomor_ijazah' value='<?=set_value('nomor_ijazah',$nomor_ijazah);?>' />
		      <?php echo form_error('nomor_ijazah'); ?></td>
	      </tr>
			  <tr align="left">
				<td width ="200">Tanggal Ijazah </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tanggal_ijazah' id='tanggal_ijazah'  value='<?= $tanggal_ijazah?>'  class="required"/>
					<?php echo form_error('tanggal_ijazah'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
			</tr>
<tr align="left">
				<td width ="200">Tanggal Mulai </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tanggal_mulai' id='tanggal_mulai'  value='<?= $tanggal_ijazah?>'  class="required"/>
					<?php echo form_error('tanggal_mulai'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
			</tr>

			<tr align="left">
			  <td>Tahun Mulai </td>
			  <td> : </td>
			  <td ><input type='text' size="4" maxlength="4" name='th_mulai' id='th_mulai' value='<?=set_value('th_mulai',$th_mulai);?>' class="js_required"/>
				  <span style="color:#F00;">*<?php echo form_error('th_mulai'); ?></span> </td>
		  </tr>
<tr align="left">
				<td width ="200">Tanggal Selesai </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tanggal_selesai' id='tanggal_selesai'  value='<?= $tanggal_ijazah?>'  class="required"/>
					<?php echo form_error('tanggal_selesai'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
			</tr>

			<tr align="left">
			  <td>Tahun Lulus </td>
			  <td> : </td>
			  <td ><input type='text' size="4" maxlength="4" name='th_lulus' id='th_lulus' value='<?=set_value('th_lulus',$th_lulus);?>' class="js_required"/>
				  <span style="color:#F00;">*<?php echo form_error('th_lulus'); ?></span> </td>
		  </tr>
			
			<tr align="left">
			  <td>Program Studi</td>
			  <td> : </td>
			  <td ><input size="62" type='text' name='prodi' id='prodi' value='<?=set_value('prodi',$prodi);?>' />
				  <?php echo form_error('prodi'); ?> </td>
			  </tr>
			  <tr align="left">
			  <td>Bidang Ilmu </td>
			  <td> : </td>
			  <td ><input size="62" type='text' name='bidang_ilmu' id='bidang_ilmu' value='<?=set_value('bidang_ilmu',$bidang_ilmu);?>' />
				  <?php echo form_error('bidang_ilmu'); ?> </td>
			  </tr>
			  
			  <tr align="left">
			  <td>Gelar </td>
			  <td> : </td>
			  <td ><input size="62" type='text' name='gelar' id='gelar' value='<?=set_value('gelar',$gelar);?>' />
				  <?php echo form_error('gelar'); ?> </td>
			  </tr>
			  
			  <tr align="left">
			  <td>Sumber Dana/Sponsor</td>
			  <td> : </td>
			  <td ><textarea rows="1" id="sumber_dana" name="sumber_dana" cols="25" class="js_required"><?=set_value('sumber_dana',$sumber_dana);?> </textarea>
				  <span style="color:#F00;">*<?php echo form_error('sumber_dana'); ?></span>		 </td>
			  </tr>
			  
			  <tr align="left">
				<td width ="200">Keterangan </td>
				<td> : </td>
				<td >
					<textarea rows="2" id="keterangan" name="keterangan" cols="25" class="js_required"><?=set_value('keterangan',$keterangan);?> </textarea>   
					
				  <span style="color:#F00;">*<?php echo form_error('keterangan'); ?></span>			</td>
			</tr>
			  
			
			  
			  <tr align="left">
				<td width ="200">Tempat Studi</td>
				<td> : </td>
				<td> 
					<select name="tempat_studi">
					
							
						 <?php if ($tempat_studi=='') {?>
						 <option value="Dalam Negeri" selected="selected">Dalam Negeri
						 <? } else { ?>
						 <option value='<?=set_value('tempat_studi',$tempat_studi);?>' selected="selected"><?=set_value('tempat_studi',$tempat_studi);?>
						 <option value="Dalam Negeri">Dalam Negeri
						 <? } ?>
						 <option value="Luar Negeri">Luar Negeri
					</select>				</td>
			</tr>
			
			  
			
			<tr align="left">
				<td width ="200">Aktif</td>
				<td> : </td>
				<td width ="550">
				<?php
					echo form_dropdown('aktif',$status_assoc, $aktif);
					echo form_error('aktif'); ?>  </td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
  </div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>