<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
	<?php	
					if($this->user->user_group=="Administrator") {
				?>
		<a class="icon" href="<?=site_url("pegawai/riwayatpendidikan/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
		<? } ?>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="pendidikandetails" id="pendidikandetails" method="POST" action="<?=site_url('pegawai/riwayatpendidikan/browse')?>" >
	</form>
	</center>
	<br/>
	<table width="904" class="table-list">
	<tr class="trhead">
		<th width="28" >No</th>
		
		<th width="166" class="colEvn" >Nama Pendidikan   </th>
		
		<th width="185" >Sekolah/ Universitas </th>
		<th width="185" >Jurusan</th>
		<th width="106" >Tahun Lulus </th>
		<th width="103" class="colEvn" >Tempat Studi </th>
		<th width="85" >Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_pendidikan!=FALSE){
	foreach ($list_pendidikan as $pendidikan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="left" ><?= $pendidikan_assoc[$pendidikan['id_pendidikan']]; ?></td>
		
		<td align="left" ><?= $pendidikan['universitas']; ?></td>
		<td align="center" ><?= $pendidikan['prodi']; ?></td>
		<td align="center" ><?= $pendidikan['th_lulus']; ?></td>
		<td align="center" ><?= $pendidikan['tempat_studi']; ?></td>
		<td align="center" class="colEvn">			
		<?php	
					if($this->user->user_group=="Administrator") {
				?>
			<a href="<?=site_url("pegawai/riwayatpendidikan/edit/".$pendidikan['id_riwayat_pendidikan']); ?>" class="edit action" >Ubah</a>
			
			<?php if($pendidikan['aktif']=='0') {?>
			<a href="<?=site_url("pegawai/riwayatpendidikan/delete/".$pendidikan['id_riwayat_pendidikan']); ?>" class="delete action" >Hapus</a>		
			<? } }?>		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

