<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url("pegawai/riwayatpenghargaan/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="penghargaandetails" id="penghargaandetails" method="POST" action="<?=site_url('pegawai/riwayatpenghargaan/browse')?>" >
	</form>
	</center>
	<br/>
	<table class="table-list">
	<tr class="trhead">
		<th width="20" >No</th>
		<th width="150" class="colEvn" >Nama Penghargaan </th>
		<th width="60" >Tahun  </th>
		<th width="200" >Keterangan  </th>
		<th >Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_penghargaan!=FALSE){
	foreach ($list_penghargaan as $penghargaan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="left" ><?= $jenis_penghargaan_assoc[$penghargaan['jenis_penghargaan']]; ?></td>
		<td align="center" ><?= $penghargaan['tahun']; ?></td>
		<td align="center" ><?= $penghargaan['keterangan']; ?></td>
		<td align="center" class="colEvn">			
			<a href="<?=site_url("pegawai/riwayatpenghargaan/edit/".$penghargaan['id_riwayat_penghargaan']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("pegawai/riwayatpenghargaan/delete/".$penghargaan['id_riwayat_penghargaan']); ?>" class="delete action" >Hapus</a>		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

