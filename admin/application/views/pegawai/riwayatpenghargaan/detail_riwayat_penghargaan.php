<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatpenghargaan/$action/";
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/riwayatpelatihan/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div>
	<form name="pelatihandetail" id="pelatihandetail" method="POST" action="<?= $action_url; ?>">
		<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
		<table class="general">
			<tr align="left">
				<td width ="200">Kode</td>
				<td> : </td>
				<td >
				<?= $id_riwayat_penghargaan?></td>
			</tr>
			
			<tr align="left">
			  <td>Jenis Penghargaan </td>
			  <td>:</td>
			  <td ><?
						echo form_dropdown('jenis_penghargaan', $jenis_penghargaan_assoc, $jenis_penghargaan);
					?>
					<?php echo form_error('jenis_penghargaan'); ?></td>
		  </tr>
			<tr align="left">
			  <td>Tahun</td>
			  <td>:</td>
			  <td ><input type='text' name='tahun' id='tahun' value='<?=set_value('tahun',$tahun);?>'/>
				  <span style="color:#F00;">* <?php echo form_error('tahun'); ?></span></td>
		  </tr>
			
			
			
			<tr align="left">
			  <td>Keterangan</td>
			  <td> : </td>
			  <td> 
			<textarea name='keterangan' id='keterangan' rows="2" cols="40"> <?= $keterangan?> </textarea>
				<?php echo form_error('keterangan'); ?> </td>	  
				   </td>
			  </tr>
		</table>
		<br/>
		<!--<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>-->
		<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>