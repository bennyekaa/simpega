<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/pangkat/')?>" title="Kembali">
			<img class="noborder" src="<?=base_url().'public/images/back.jpg'?>" /></a>
		</div>
		<div class="clear"></div>
	</div>
	<br/>
	<div>
		<table class="table-list">
			<tr class="trhead">
				<th width='50' class="colEvn">No</th>
				<th width='100' >TMT Jabatan</th>
				<th width='120' class="colEvn">No. SK</th>
				<th width='100' >Tanggal SK</th>
				<th width='100' class="colEvn">Gol/Ruang</th>
				<th width='75' >Pangkat</th>
			</tr>
		
			<?
			$i = 0;
			if ($list_pangkat!=FALSE){
			foreach ($list_pangkat as $jns_pangkat) {
			   $i++;
			   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			?>
			<tr class="<?=$bgColor?>">
				<td align="center" class="colEvn"><?=$$jns_pangkat['no_urut'];?></td>
				<td align="left" ><?= $jns_pangkat['tmt_pangkat']; ?></td>
				<td align="left" ><?= $jns_pangkat['no_sk']; ?></td>
				<td align="left" ><?= $jns_pangkat['tgl_sk']; ?></td>
				<td align="center" ><?= $jns_pangkat['golongan']; ?></td>
				<td align="center" ><?= $jns_pangkat['pangkat']; ?></td>
			</tr>
		
			<? } ?>
			<?}
			else {
				echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
			}?>
		</table>
		<br/><br/>
	<div class="paging"><?=$page_links?></div>
	</div>
</div>

