<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
		<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('pegawai/pangkat/browse')?>" >
			<label>Periode Kenaikan : </label><?=form_dropdown('group',$group_option,$group);?>
			<input type="submit" value="Lihat">
		</form>
	</center>
	<br/>
	<table class="table-list">
		<tr class="trhead">
			<th >No</th>
			<th >NIP</th>
			<th >Nama Pegawai</th>
			<th >Gol/Ruang</th>
			<th >Masa Kerja</th>
			<th >Unit Kerja</th>
			<th >Edit</th>
		</tr>
	
		<?
		$i = 0;
		if ($list_pegawai!=FALSE){
		foreach ($list_pegawai as $jns_pegawai) {
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$jns_pegawai['kd_pegawai'];?></td>
			<td align="left" ><?= $jns_pegawai['NIP']; ?></td>
			<td align="left" ><?= $jns_pegawai['gelar_depan']." ".$jns_pegawai['nama']." ".$jns_pegawai['gelar_belakang']; ?></td>
			<td align="center" ><?= $jns_pegawai['golongan']; ?></td>
			<td align="center" ><?= $jns_pegawai['masa_kerja']; ?></td>
			<td align="left" ><?= $jns_pegawai['unit_kerja']; ?></td>
			<td align="center" class="colEvn">			
				<a href = "<?=site_url("pegawai/pangkat/kenaikan/".$jns_pegawai['kd_pegawai']); ?>" class="edit action" >Kenaikan Pangkat</a>
				<a href = "<?=site_url("pegawai/pangkat/rincian/".$jns_pegawai['kd_pegawai']); ?>" class="edit action" >Riwayat Pangkat</a>
			</td>
		</tr>
	
		<? } ?>
		<?}
		else {
			echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
		}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

