
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<table width="516" class="general">
				<tr align="left">
                  <td colspan="11" align="center">
				  <div align="center" class="ch-title">
				  <h3><span style="font:Tahoma; font-size:40px">SURAT KETERANGAN</span><br />					
					<span style="font:Tahoma; font-size:36px">
					UNTUK MENDAPATKAN PEMBAYARAN TUNJANGAN KELUARGA</span></h3></div>				  </td>
                </tr>
                <tr align="left">
                  <td colspan="11" align="left"><div align="left">Saya yang bertanda tangan di bawah ini: </div></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="530">&nbsp;</td>
                  <td width="47">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="440" align="left">&nbsp;</td>
                  <td width="57" align="left">&nbsp;</td>
                  <td width="57" align="left">&nbsp;</td>
                  <td width="59" align="left">&nbsp;</td>
                </tr>
                <tr>
					<td width="20">1.</td>
					<td colspan="3">Nama Lengkap </td>
                    <td width="10"> : </td>
                    <td colspan="4" width="440"  align="left"><strong>
                    <?=$gelar_depan.' '.$nama_peg.' '.$gelar_belakang?>
&nbsp;&nbsp; NIP.
<?=$NIP;?>
                    </strong></td>
                </tr>
                <tr>
					<td width="20">2.</td>
					<td colspan="3">Tempat / tgl. Lahir </td>
               	  <td width="10">:</td>
                  	<td colspan="4" width="440"  align="left"><strong>
					<?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$pegawai['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
					?>
					<?=$tempat_lahir?>, <?= date('d M Y',strtotime($tanggal)); ?>
                  	</strong></td>
                </tr>
                <tr>
					<td width="20">3.</td>
					<td colspan="3">Jenis Kelamin </td>
                  <td width="10">:</td>
                  <td colspan="4" width="440"  align="left"><strong><?=$jenis;?></strong></td>
                </tr>
                <tr>
					<td width="20">4.</td>
					<td colspan="3">Agama</td>
                  <td width="10">:</td>
                  <td colspan="4" width="440"  align="left"><strong><?=$agama?></strong></td>
                </tr>
                <tr>
					<td width="20">5.</td>
					<td colspan="3">Status Kepegawaian </td>
                  <td width="10">:</td>
                  <td colspan="4" width="440"  align="left"><strong><?=$status_pegawai_p?> (<?=$status_pegawai_s?>)</strong></td>
                </tr>
                <tr>
					<td width="20">6.</td>
					<td colspan="3">Jabatan Struktural / Fungsional </td>
                  <td width="10">:</td>
                  <td colspan="4" width="440"  align="left"><strong><?=$jabatan?></strong></td>
                </tr>
                <tr>
					<td width="20">7.</td>
					<td colspan="3">Pangkat / Golongan </td>
                  <td width="10">:</td>
                  <td colspan="4" width="440"  align="left"><strong><?=trim($pangkat)?>, <?=trim($golongan)?></strong></td>
                </tr>
                <tr>
				  <td width="20">8.</td>
				  <td colspan="3">Pada Instansi, Dep. / Lembaga </td>
                  <td width="10">:</td>
                  <td colspan="4" width="440"  align="left"><strong>Universitas Negeri Malang </strong></td>
                </tr>
				 <tr>
					<td width="20">9.</td>
					<td colspan="3">Masa Kerja Golongan </td>
					<td width="10">:</td>
					<td colspan="4" width="440"  align="left"><strong><?=$masa_kerja_golongan?> &nbsp;</strong>Masa Kerja Tambahan:<strong><?=$masa_kerja_tambahan?></strong> &nbsp;Masa Kerja Seluruhnya</td>
				</tr>
				 <tr>
					<td width="20">&nbsp;</td>
					<td colspan="3">&nbsp;</td>
					<td width="10">&nbsp;</td>
					<td colspan="4" width="440"  align="left"><strong><?=$masa_kerja_seluruhnya?></strong></td>
				</tr>
                <tr>
				<td width="20">10.</td>
				
				<td colspan="11"><strong>
				Di Gaji menurut PP 11 Tahun 2011 dengan gaji pokok Rp. <?php
					$query = mysql_query("select * from riwayat_gol_kepangkatan where kd_pegawai='".$kd_pegawai."' and aktif='1'");										
					$datasen=mysql_fetch_array($query); 
					if ($datasen!=FALSE){
							$gaji_pokok_asli = $datasen['gaji_pokok'];
						}
					if ($status_pegawai_s=='PNS') {
					echo number_format($gaji_pokok_asli,0,',','.'); 
					}
					else {
					$gaji_pokok_asli_cpns = 1.25 * $gaji_pokok_asli;
					echo "80% x ".number_format($gaji_pokok_asli_cpns,0,',','.').",- = Rp. ".number_format($gaji_pokok_asli,0,',','.');
					}
					?>,-
				</strong></td>
                </tr>
                <tr>
                  <td width="20">11.</td>
				  <td colspan="3">Alamat / tempat tinggal </td>
                  <td  align="left" width="10">:</td>
                  <td colspan="4" width="440"  align="left"><strong><?=$alamat?></strong></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="9">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="10" width="540">Menerangkan dengan sesungguhnya bahwa saya: </td>
			    </tr>
                <tr>
                  <td width="20">&nbsp;</td>
				  <td width="17">a.</td>
				  <td colspan="8"  width="530">disamping jabatan utama tersebut, bekerja pula sebagai: <?=$kp4_11a?></td>
                </tr>
                <tr>
                  <td width="20">&nbsp;</td>
                  <td width="17">&nbsp;</td>
                  <td colspan="8"  width="530">dengan mendapat penghasilan sebesar Rp. <?=$kp4_11b?> sebulan</td>
                </tr>
                <tr>
                  <td width="20">&nbsp;</td>
                  <td width="17">b.</td>
                  <td colspan="8"  width="530">mempunyai pensiun / pensiun janda sebesar Rp. <?=$kp4_11c?> sebulan</td>
                </tr>
                
                <tr>
                  <td width="20">&nbsp;</td>
                  <td width="17">c.</td>
                  <td colspan="7">mempunyai susunan keluarga sbb: </td>
                </tr>
                
                 <tr>
                  <td width="20">&nbsp;</td>
                  <td width="17">&nbsp;</td>
                  <td colspan="8"  width="530">
				  <table width="438" align="left" border="1">
						<tr class="trhead">
							<th width="18" rowspan="2" align="center" class="trhead"><strong>No</strong></th>
							<th width="140" rowspan="2" class="trhead" align="center">
							<strong>Nama istri / Suami / Anak<br />Tanggungan</strong></th>
							<th colspan="2" width="140" align="center" class="trhead"><strong>Tanggal</strong></th>
							<th width="70" rowspan="2" align="center" class="trhead"><strong>Pekerjaan <br />/ sekolah**</strong> </th>
							<th width="86" rowspan="2" align="center" class="trhead"><strong>Keterangan
						  </strong></th>
						</tr>
						  <tr class="trhead">
							<th width="70" align="center" class="trhead"><strong>Kelahiran</strong></th>
							<th width="70"align="center" class="trhead"><strong>Perkawinan</strong></th>
						  </tr>
				  <?
						$i = 0;
						if ($list_pasangan!=FALSE){
						foreach ($list_pasangan as $jns_keluarga) {
						$i++;
						if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
						if ($jns_keluarga['tanggal_lahir']=='0000-00-00') {
						$str_tanggal_lahir = '-';
						}
						else {
							list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_lahir']);
							$str_tanggal_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
						}
						$jns_keluarga['tanggal_lahir']=$str_tanggal_lahir;
						
						if ($jns_keluarga['tanggal_nikah']=='0000-00-00') {
							$str_tanggal_nikah = '-';
						}
						else {
							list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_nikah']);
							$str_tanggal_nikah = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
						}
						$jns_keluarga['tanggal_nikah']=$str_tanggal_nikah;
				  ?>
						  <tr class="<?=$bgColor?>">
							<td class="trhead" align="center" width="18"><strong><?=$i;?></strong></td>
							<td width="140" class="trhead">&nbsp;<strong><?= $jns_keluarga['nama_pasangan']; ?></strong></td>
							<td width="70" align="center" class="trhead"><strong><?= $jns_keluarga['tanggal_lahir']; ?></strong></td>
							<td width="70" align="center" class="trhead"><strong><?= $jns_keluarga['tanggal_nikah']; ?></strong></td>
							<td width="70" class="trhead" align="center"><strong><?= $jns_keluarga['pekerjaan']; ?></strong></td>
							<td width="86" class="trhead" align="center"><?= $status_keluarga_assoc[$jns_keluarga['kd_status_keluarga']]; ?></td>
						</tr>
					 
				 		 <? 	
				  		}
						}
						else {
								?>
						<tr class="<?=$bgColor?>">
							<td class="trhead" align="center" width="18"><strong>1</strong></td>
							<td width="140" class="trhead">&nbsp;<strong></strong></td>
							<td width="70" align="center" class="trhead"><strong></strong></td>
							<td width="70" align="center" class="trhead"><strong></strong></td>
							<td width="70" class="trhead" align="center"><strong></strong></td>
							<td width="86" class="trhead" align="center"></td>
						</tr>		
								
						<?
						}?>
						<?
							$j = 1;
							if ($list_anak!=FALSE){
						foreach ($list_anak as $jns_anak) {
						   $j++;
						   if (($j%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
							if ($jns_anak['tgl_lahir']=='0000-00-00') {
								$str_tgl_lahir = '-';
							}
							else {
								list($y, $m, $d) = explode('-', $jns_anak['tgl_lahir']);
								$str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
							}
							$jns_anak['tgl_lahir']=$str_tgl_lahir;
						
						 ?>
						  <tr class="<?=$bgColor?>">
							<td align="center" class="colEvn" width="18"><?=$j;?></td>
							<td width="140"><?= $jns_anak['nama_anak']; ?></td>
							<td  width="70"align="center"><?= $jns_anak['tgl_lahir']; ?></td>
							<td width="70" align="center">-</td>
							<td width="70" align="center"><?= $jns_anak['pekerjaan']; ?> <?= $pendidikan_assoc[$jns_anak['id_pendidikan_anak']]; ?></td>
							<td width="86" align="left">&nbsp;&nbsp;<?= $status_keluarga_assoc[$jns_anak['kd_status_keluarga']];?><? if ($jns_anak['aktif']==1){?>*<? } ?>
							</td>
						  </tr>
							  <? } ?>
							  <?}
						else {
							//echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
						}?>
				    </table>				  </td>
                </tr>
				 <tr>
				   <td>&nbsp;</td>
				   <td>&nbsp;</td>
				   <td colspan="8"  width="530">&nbsp;</td>
		      </tr>
			    <tr>
                  <td width="20">&nbsp;</td>
                  <td width="17">d.</td>
                  <td colspan="8"  width="530">Jumlah Anak : <?=$jumlah_anak_masuk_daftar?> orang &nbsp;<strong>(yang menjadi tanggungan dan yang masuk dalam daftar gaji diberi tanda <br />bintang) *</strong> </td>
                </tr>
				<tr>
                  <td width="20">&nbsp;</td>
                  <td width="17">e.</td>
                  <td colspan="8"  width="530">Jumlah Anak : <?=$jumlah_anak?> orang &nbsp; <strong>(yang menjadi tanggungan termasuk yang tidak masuk dalam daftar gaji).</strong> </td>
			      <!---<td width="72" align="left">Jumlah Anak: </td>
                  <td width="81" align="center"><?=$jumlah_anak?></td>
                  <td colspan="6"  align="left"><strong>(yang menjadi tanggungan termasuk yang tidak masuk dalam daftar gaji).</strong></td> --->
                </tr>
                <tr>
                  <td colspan="10" width="540">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="10" width="540">Keterangan ini saya buat dengan sesungguhnya &nbsp;dan apabila &nbsp;keterangan ini ternyata tidak benar (palsu) saya<br />bersedia dituntut dimuka pengadilan berdasarkan Undang-undang yang berlaku, dan bersedia mengembalikan<br />semua penghasilan yang telah saya terima yang seharusnya bukan menjadi hak saya.</td>
                </tr>
				<tr><td colspan="10" width="540">&nbsp;</td></tr>
				<tr><td colspan="10" width="540">&nbsp;</td></tr>
				<tr><td colspan="10" width="540">&nbsp;</td></tr>
				<tr><td colspan="10" width="540">&nbsp;</td></tr>
				<tr>
				  <td width="20">&nbsp;</td>
                  <td width="17">&nbsp;</td>
                  <td colspan="3" width="303" align="left">Mengetahui</td>
				  <td colspan="5" width="200" align="left">Malang, <?= $tglsurat; ?></td>
                </tr>
				<tr>
                 <td width="20">&nbsp;</td>
                  <td width="17">&nbsp;</td>
                  <td colspan="3" width="303" align="left"><?=$mengetahui?>,</td>
				  <td colspan="5" width="200" align="left">Yang Menerangkan</td>
                </tr>
				<tr>
				  <td colspan="5" align="left">&nbsp;</td>
				  <td colspan="5" align="left">&nbsp;</td>
			  </tr>
				<tr>
				  <td colspan="5" align="left">&nbsp;</td>
				  <td colspan="5" align="left">&nbsp;</td>
			  </tr>
				<tr>
				  <td colspan="5" align="left">&nbsp;</td>
				  <td colspan="5" align="left">&nbsp;</td>
			  </tr>
				<tr>
                  <td width="20">&nbsp;</td>
                  <td width="17">&nbsp;</td>
                  <td colspan="3" width="303" align="left"> <?=$mengetahui1?></td>
				  <td colspan="5" width="200" align="left"><?=$gelar_depan.' '.$nama_peg.$gelar_belakang?></td>
                </tr>
				<tr>
                  <td width="20">&nbsp;</td>
                  <td width="17">&nbsp;</td>
                  <td colspan="3" width="303" align="left"> <?=$mengetahui2?></td>
				  <td colspan="5" width="200" align="left">NIP. <?=$NIP;?></td>
                </tr>
				<tr>
                  <td colspan="5" width="340" align="left">&nbsp;</td>
				  <td colspan="5" width="200" align="left">&nbsp;</td>
                </tr>
				<tr>
				  <td colspan="5" align="left">&nbsp;</td>
				  <td colspan="5" align="left">&nbsp;</td>
			  </tr>
				<tr>
				  <td colspan="5" align="left">&nbsp;</td>
				  <td colspan="5" align="left">&nbsp;</td>
			  </tr>
				<tr>
				  <td colspan="5" align="left">CATATAN:<BR />
				  ** = sekolah diisi jenjang pendidikan anak<br /> (SD, SLTP, SLTA, D3, S1) </td>
				  <td colspan="5" align="left">&nbsp;</td>
			  </tr>
				<tr>
                  <td colspan="5" width="340" align="left">&nbsp;</td>
				  <td colspan="5" width="200" align="left">&nbsp;</td>
                </tr>
			</table>
