<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/pangkat/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/pangkat/browse')?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel-icon.png'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="pangkatdetail" id="pangkatdetail" method="POST" action="<?= $action_url; ?>">
			<input type='hidden' name='id_riwayat_pangkat' id='id_riwayat_pangkat' value='<?=$id_riwayat_pangkat?>'/>
			<input type='hidden' name='FK_pegawai' id='FK_pegawai' value='<?=$FK_pegawai?>'/>
			<table class="general">
				<tr align="left">
					<td width ="200px">Kode</td>
					<td> : </td>
					<td >
					<?= $no_urut?></td>
				</tr>
				<tr align="left">
					<td width ="200px">TMT Kenaikan Pangkat</td>
					<td> : </td>
					<td >
						<input type="text" class="datepicker" name="tmt_pangkat" id="tmt_pangkat" value="<?=set_value('tmt_pangkat',$tmt_pangkat);?>"/>
						<span style="color:#F00;">* <?php echo form_error('tmt_pangkat'); ?></span>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Nomor SK</td>
					<td> : </td>
					<td >
						<input type="text" name="no_sk" id="no_sk" class="js_required" value="<?= $no_sk?>"  class="required"/>
						<span style="color:#F00;">*<?php echo form_error('no_sk'); ?></span>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Tanggal SK</td>
					<td> : </td>
					<td >
						<input type="text" class="datepicker" name="tgl_sk" id="tgl_sk" class="js_required" value="<?= $tgl_sk?>"  class="required"/>
						
						<span style="color:#F00;">*<?php echo form_error('tgl_sk'); ?></span>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Gol/Ruang Lama</td>
					<td> : </td>
					<td >
					<?=$id_golpangkat;?>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Gol/Ruang Baru</td>
					<td> : </td>
					<td >
					<?php
						echo form_dropdown('FK_gol_baru',$golongan_assoc, $FK_gol_baru);
						?>*<?echo form_error('FK_gol_baru'); ?>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Jenis Kenaikan Pangkat</td>
					<td> : </td>
					<td >
					<?php
						echo form_dropdown('FK_jenis_kenaikan',$jenis_kenaikan_assoc, $FK_jenis_kenaikan);
						?>*<?echo form_error('FK_jenis_kenaikan'); ?>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Keterangan</td>
					<td> : </td>
					<td >
					<input type="text" name="ket_riwayat_pangkat" id="ket_riwayat_pangkat" value="<?= $ket_riwayat_pangkat?>" />
					<?php echo form_error('ket_riwayat_pangkat'); ?>  </td>
				</tr>
			</table>
			<br/>
			<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>