<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/penghargaan/add')?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/add-icon.png'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<br/>
	<div class="paging"><?=$page_links?></div>
	<div>
		<table class="table-list">
		<tr class="trhead">
			<th width='50' class="colEvn">Kode</th>
			<th width='125' >Nama Penghargaan</th>
			<th width='100' >Tingkat</th>
			<th width='125' >Tanggal Penerimaan</th>
			<th width='150' >Keterangan</th>
			<th class="colEvn">Edit</th>
		</tr>
		<?
		$i = 0;
		if ($list_penghargaan!=FALSE){
		foreach ($list_penghargaan as $jns_penghargaan) {
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$jns_penghargaan['id_riwayat_penghargaan'];?></td>
			<td align="left" ><?= $jns_penghargaan['nama_penghargaan']; ?></td>
			<td align="left" ><?= $jns_penghargaan['tingkat_penghargaan']; ?></td>
			<td align="left" ><?= $jns_penghargaan['tgl_penghargaan']; ?></td>
			<td align="left" ><?= $jns_penghargaan['ket_riwayat_penghargaan']; ?></td>
			<td align="center" class="colEvn">			
				<a href = "<?=site_url("pegawai/penghargaan/edit/".$jns_penghargaan['id_riwayat_penghargaan']); ?>" class="edit action" >Ubah</a>
				<a href = "<?=site_url("pegawai/penghargaan/delete/".$jns_penghargaan['id_riwayat_penghargaan']); ?>" class="nyroModal delete action" >Hapus</a>
			</td>
		</tr>
		<? } ?>
		<?}
		else {
			echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
		}?>
		</table>
	</div>
</div>

