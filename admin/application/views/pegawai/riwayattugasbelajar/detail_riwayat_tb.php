<?
$this->load->helper('url');
$this->load->library('form_validation');
if ($tb_ib=='tb'){
	$action_url = site_url() . "/pegawai/riwayattugasbelajar/$action/";
}
else
{
	$action_url = site_url() . "/pegawai/riwayatijinbelajar/$action/";
}
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
<!--v3 tambahan tb_ib-->	
<?php 
	if ($tb_ib=='tb'){
?>
<!--end v3 tambahan tb_ib-->	
			<a class="icon" href="<?=site_url('pegawai/riwayattugasbelajar/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
<?php }
else
{ ?>
			<a class="icon" href="<?=site_url('pegawai/riwayatijinbelajar/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>

<?}?>			
			
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
<!--v3 tambahan tb_ib-->	
<?php 
if ($tb_ib=='tb'){
?>
<!--end v3 tambahan tb_ib-->	
	<form name="tbdetail" id="tbdetail" method="POST" action="<?= $action_url; ?>">
	<input type="hidden" name="tb_ib" id="tb_ib" value="<?= $tb_ib?>"/>
		<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
		<table class="general">
			<tr align="left">
				<td width ="291">Kode</td>
				<td> : </td>
				<td >
				<?= $id_riwayat_tb?></td>
			</tr>
			
			<tr align="left">
				<td width ="291">TMT Tugas/Ijin Belajar </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tmt_tb' id='tmt_tb'  value='<?= $tmt_tb?>'  class="required" />
					<?php echo form_error('tmt_tb'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
			</tr>
			<tr align="left">
				<td width ="291">Batas Akhir Tugas/Ijin Belajar </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='batas_akhir_tb' id='batas_akhir_tb'  value='<?= $batas_akhir_tb?>'  class="required"/>
					<?php echo form_error('batas_akhir_tb'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>	</td>
			</tr>
			
			<tr align="left">
				<td width ="291">Tingkat Pendidikan </td>
				<td> : </td>
				<td ><?php
					echo form_dropdown('id_pendidikan_tb',$pendidikan_assoc, $id_pendidikan_tb, 'class="js_required"');
					?>*<? echo form_error('id_pendidikan_tb'); ?>  </td>
			</tr>
			
			<tr align="left">
				<td width ="291">Bidang Studi </td>
				<td> : </td>
				<td >
				
				<input type="text"  size="60" name="bidang_studi_tb" id="bidang_studi_tb" value="<?= $bidang_studi_tb?>"/>
					<?php echo form_error('bidang_studi_tb'); ?></td>
			</tr>
			<tr align="left">
			  <td>Tempat Studi </td>
			  <td> : </td>
			  <td> 
			
				<input type="text"  size="60" name="tempat_studi_tb" id="tempat_studi_tb" value="<?= $tempat_studi_tb?>"/>
					<?php echo form_error('tempat_studi_tb'); ?> </td>	  
				   </td>
		  </tr>
		  <tr align="left">
					<td width ="291">Biaya</td>
					<td> : </td>
					<td >
						<input type="text" name="biaya_tb" id="biaya_tb" value="<?= $biaya_tb?>"/>
					<?php echo form_error('biaya_tb'); ?>	
					</td>
				</tr>
			 <tr align="left">
			   <td>Keterangan</td>
			   <td>:</td>
			   <td>
				<input type="text" size="60" name="keterangan" id="keterangan" value="<?= $keterangan?>"/>
					<?php echo form_error('keterangan'); ?> </td>
	      </tr>
			 <tr align="left">
			   <td width ="291">TMT Pengaktifan Kembali  </td>
			   <td> : </td>
				<td >
					<input type='text' class='datepicker' name='tmt_pk' id='tmt_pk'  value='<?= $tmt_pk?>'  class="required"/>
			   <?php echo form_error('tmt_pk'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
	      </tr>
			 <tr align="left">
				<td width ="291">Status Tugas/Ijin Belajar </td>
				<td> : </td>
				<td width ="200">
				<?php
					echo form_dropdown('aktif',$status_assoc, $aktif);
					echo form_error('aktif'); ?>  </td>
			</tr>
			<tr align="left">
			  <td colspan="3"><strong>Surat Dasar Penerimaan Tugas/Ijin Belajar </strong></td>
		  </tr>
			<tr align="left">
					<td width ="291">Nomor Surat Dasar Penerimaan Tugas Belajar </td>
					<td> : </td>
					<td >
						<input type="text" size="60" name="no_dasar_penerimaan" id="no_dasar_penerimaan" value="<?= $no_dasar_penerimaan?>"/>
					<?php echo form_error('no_dasar_penerimaan'); ?>	</td>
				</tr>
			<tr align="left">
					<td width ="291">Tanggal Surat Dasar Penerimaan Tugas Belajar </td>
					<td> : </td>
			  <td >
						<input type="text" name="tgl_dasar_penerimaan" id="tgl_dasar_penerimaan" value="<?= $tgl_dasar_penerimaan?>"/>
				<?php echo form_error('tgl_dasar_penerimaan'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span></td>
		    </tr>
			<tr align="left">
			  <td colspan="3"><strong>Surat Tugas/Ijin Belajar </strong></td>
		  </tr>
			<tr align="left">
					<td width ="291">Nomor Surat Tugas Belajar </td>
					<td> : </td>
					<td >
						<input type="text" size="60" name="no_SK_tb" id="no_SK_tb" value="<?= $no_SK_tb?>"/>
						
					<?php echo form_error('no_SK_tb'); ?>	</td>
		    </tr>
			<tr align="left">
					<td width ="291">Tanggal Surat Tugas Belajar </td>
					<td> : </td>
			  <td >
						<input type="text" name="tgl_SK_tb" id="tgl_SK_tb" value="<?= $tgl_SK_tb?>"/>
				<?php echo form_error('tgl_SK_tb'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span></td>
		    </tr>
				<tr align="left">
				  <td colspan="3"><strong>SK Pembebasan Sementara dari Tugas-Tugas Jabatan Fungsional Dosen (khusus Tugas Belajar) </strong></td>
		  </tr>
				<tr align="left">
					<td width ="291">Nomor SK Pembebasan Sementara </td>
					<td> : </td>
					<td >
						<input type="text" size="60" name="no_surat_bebas" id="no_surat_bebas" value="<?= $no_surat_bebas?>"/>
					<?php echo form_error('no_surat_bebas'); ?>	</td>
				</tr>
			<tr align="left">
					<td width ="291">Tanggal SK Pembebasan Sementara </td>
					<td> : </td>
			  <td >
						<input type="text" name="tgl_surat_bebas" id="tgl_surat_bebas" value="<?= $tgl_surat_bebas?>"/>
				<?php echo form_error('tgl_surat_bebas'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span></td>
		    </tr>
				
				<tr align="left">
				  <td colspan="3"><strong>SK Tunjangan Tugas Belajar (khusus Tugas Belajar)</strong></td>
		  </tr>
				<tr align="left">
					<td width ="291">Nomor SK Tunjangan Tugas Belajar </td>
					<td> : </td>
					<td >
						<input type="text" size="60" name="no_tunjangan_tb" id="no_tunjangan_tb" value="<?= $no_tunjangan_tb?>"/>
					<?php echo form_error('no_tunjangan_tb'); ?>	</td>
				</tr>
			<tr align="left">
					<td width ="291">Tanggal SK Tunjangan Tugas Belajar </td>
					<td> : </td>
			  <td >
						<input type="text" name="tgl_tunjangan_tb" id="tgl_tunjangan_tb" value="<?= $tgl_tunjangan_tb?>"/>
				<?php echo form_error('tgl_tunjangan_tb'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span></td>
		    </tr>
			  <tr align="left">
			    <td>Besar Tunjangan</td>
			    <td>:</td>
			    <td><input type="text" size="60" name="tunjangan_tb" id="tunjangan_tb" value="<?= $tunjangan_tb?>"/>
					<?php echo form_error('tunjangan_tb'); ?>	</td>
	      </tr>
			  <tr align="left">
			    <td>Status Tunjangan Tugas Belajar</td>
			    <td>:</td>
			    <td><?php
					echo form_dropdown('status_tunjangan',$tunjangan_assoc, $status_tunjangan);
					echo form_error('status_tunjangan'); ?>						</td>
	      </tr>
			  <tr align="left">
			    <td colspan="3"><strong>SK Perpanjangan Tugas Belajar (khusus Tugas Belajar)</strong></td>
	      </tr>
			  <tr align="left">
			    <td>Nomor SK Perpanjangan </td>
			    <td>:</td>
			    <td >
						<input type="text" size="60" name="no_perpanjangan_tb" id="no_perpanjangan_tb" value="<?= $no_perpanjangan_tb?>"/>
					<?php echo form_error('no_perpanjangan_tb'); ?>	</td>
	      </tr>
			  <tr align="left">
			    <td>Tanggal SK Perpanjangan </td>
			    <td>:</td>
			    <td >
						<input type="text" name="tgl_perpanjangan_tb" id="tgl_perpanjangan_tb" value="<?= $tgl_perpanjangan_tb?>"/>
				<?php echo form_error('tgl_perpanjangan_tb'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span></td>
	      </tr>
			  <tr align="left">
			    <td>Status Perpanjangan </td>
			    <td>:</td>
			    <td><?php
					echo form_dropdown('status_perpanjangan',$perpanjangan_assoc, $status_perpanjangan);
					echo form_error('status_perpanjangan'); ?></td>
	      </tr>
			  <tr align="left">
			    <td>Batas Akhir Tugas Belajar setelah Perpanjangan </td>
			    <td>:</td>
			   <td>
					<input type='text' class='datepicker' name='batas_akhir_tb_perpanjangan' id='batas_akhir_tb_perpanjangan'  value='<?= $batas_akhir_tb_perpanjangan?>'  class="required" />
					<?php echo form_error('batas_akhir_tb_perpanjangan'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>	</td>
	      </tr>
		</table>
		<br/>
		<!--<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>-->
		<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
	</form>
	
<!--v3 tambahan tb_ib-->	
<?php 
}
else
{
?>
<form name="tbdetail" id="tbdetail" method="POST" action="<?= $action_url; ?>">
	<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
	<input type="hidden" name="tb_ib" id="tb_ib" value="<?= $tb_ib?>"/>
		<table class="general">
		<tr align="left">
			<td width ="291">Kode</td>
			<td> : </td>
			<td >
			<?= $id_riwayat_tb?></td>
		</tr>
		
		<tr align="left">
			<td width ="291">Tanggal Ijin Belajar </td>
			<td> : </td>
			<td >
			<input type='text' class='datepicker' name='tmt_tb' id='tmt_tb'  value='<?= $tmt_tb?>'  class="required" />
			<?php echo form_error('tmt_tb'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
		</tr>
		<tr align="left">
			<td width ="291">Batas Akhir Ijin Belajar </td>
			<td> : </td>
			<td >
			<input type='text' class='datepicker' name='batas_akhir_tb' id='batas_akhir_tb'  value='<?= $batas_akhir_tb?>'  class="required" />
			</td>
		</tr>
		
		<tr align="left">
			<td width ="291">Tingkat Pendidikan </td>
			<td> : </td>
			<td ><?php
			echo form_dropdown('id_pendidikan_tb',$pendidikan_assoc, $id_pendidikan_tb, 'class="js_required"');
			?>*<? echo form_error('id_pendidikan_tb'); ?>  </td>
		</tr>
		
		<tr align="left">
			<td width ="291">Bidang Studi </td>
			<td> : </td>
			<td >
			
			<input type="text"  size="60" name="bidang_studi_tb" id="bidang_studi_tb" value="<?= $bidang_studi_tb?>"/>
					<?php echo form_error('bidang_studi_tb'); ?>
			
			
			 </td>
		</tr>
		<tr align="left">
			<td>Tempat Studi </td>
			<td> : </td>
			<td> 
			<input type="text"  size="60" name="tempat_studi_tb" id="tempat_studi_tb" value="<?= $tempat_studi_tb?>"/>
			<?php echo form_error('tempat_studi_tb'); ?> </td>	  
			</td>
		</tr>
		<tr align="left">
			<td width ="291">Biaya</td>
			<td> : </td>
			<td >
			<input type="text" name="biaya_tb" id="biaya_tb" value="<?= $biaya_tb?>"/>
			<?php echo form_error('biaya_tb'); ?>	</td>
		</tr>
		<tr align="left">
			<td>Keterangan</td>
			<td>:</td>
			<td><input type="text" size="60" name="keterangan" id="keterangan" value="<?= $keterangan?>"/>
			<?php echo form_error('keterangan'); ?> </td>
		</tr>
		
		<tr align="left">
			<td width ="291">Status Ijin Belajar </td>
			<td> : </td>
			<td width ="200">
			<?php
			echo form_dropdown('aktif',$status_assoc, $aktif);
			echo form_error('aktif'); ?>  </td>
		</tr>
		
		<tr align="left">
			<td colspan="3"><strong>Surat Ijin Belajar </strong></td>
			</tr>
			<tr align="left">
			<td width ="291">Nomor Surat Ijin Belajar </td>
			<td> : </td>
			<td >
				<input type="text" size="60" name="no_SK_tb" id="no_SK_tb" value="<?= $no_SK_tb?>"/>
				<?php echo form_error('no_SK_tb'); ?>	</td>
		</tr>
		<tr align="left">
			<td width ="291">Tanggal Surat Ijin Belajar </td>
			<td> : </td>
			<td >
				<input type="text" name="tgl_SK_tb" id="tgl_SK_tb" value="<?= $tgl_SK_tb?>"/>
				<?php echo form_error('tgl_SK_tb'); ?>	<span style="color:#F00;"><i> yyyy-mm-dd</i></span></td>
		</tr>
	</table>
	<br/>
	<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
	</form>
<?php 
}
?>
<!--end v3 tambahan tb_ib-->			

	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>