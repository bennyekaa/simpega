<?
	$this->load->helper('url');
	$this->load->library('form_validation');
	$action_url = site_url() . "/pegawai/pendidikan/$action/";
	if (!async_request())
	{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/pendidikan/index')?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel-icon.png'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="pendidikandetail" id="pendidikandetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
		<tr align="left">
			<td width ="200px">Kode</td>
			<td> : </td>
			<td width ="200px">
			<?= $id_riwayat_pendidikan?></td>
		</tr>
		<tr align="left">
			<td width ="200px">Nama Sekolah</td>
			<td> : </td>
			<td width ="200px">
			<input type="text" name="nama_instansi" id="nama_instansi" class="js_required" value="<?= $nama_instansi?>"  class="required" />
			<?php echo form_error('nama_instansi'); ?>  </td>
		</tr>
		<tr align="left">
			<td width ="200px">Tingkat Pendidikan</td>
			<td> : </td>
			<td width ="200px">
			<?php
				echo form_dropdown('FK_tingkat_pendidikan',$tingkat_pendidikan_assoc, $FK_tingkat_pendidikan);
				echo form_error('FK_tingkat_pendidikan'); ?>
			</td>
		</tr>
		<tr align="left">
			<td width ="200px">Alamat</td>
			<td> : </td>
			<td width ="200px">
			<input type="text" name="alamat" id="alamat" value="<?= $alamat?>" />
			<?php echo form_error('alamat'); ?>  </td>
		</tr>
		<tr align="left">
			<td width ="200px">Fakultas</td>
			<td> : </td>
			<td width ="200px">
			<?php
				echo form_dropdown('FK_fakultas',$fakultas_assoc, $FK_fakultas);
				echo form_error('FK_fakultas'); ?>
			</td>
		</tr>
		<tr align="left">
			<td width ="200px">Jurusan</td>
			<td> : </td>
			<td width ="200px">
			<input type="text" name="jurusan" id="jurusan" class="js_required" value="<?= $jurusan?>"  class="required"/>
			<?php echo form_error('jurusan'); ?>  </td>
		</tr>
		<tr align="left">
			<td width ="200px">No Ijazah</td>
			<td> : </td>
			<td width ="200px">
			<input type="text" name="no_ijazah" id="no_ijazah" class="js_required" value="<?= $no_ijazah?>"  class="required"/>
			<?php echo form_error('no_ijazah'); ?>  </td>
		</tr>
		<tr align="left">
			<td width ="200px">Tahun Masuk</td>
			<td> : </td>
			<td width ="200px">
			<input type="text" name="thn_masuk" class="js_required required" id="thn_masuk" onkeyup="gsub_tahun(this)" onchange="gsub_tahun(this)" onblur="gsub_tahun(this)" value='<?= $thn_masuk?>'/>
			<?php echo form_error('thn_masuk'); ?>  </td>
		</tr>
		<tr align="left">
			<td width ="200px">Tahun Kelulusan</td>
			<td> : </td>
			<td width ="200px">
			<input type="text" name="thn_lulus" id="thn_lulus" class="js_required required" onkeyup="gsub_tahun(this)" onchange="gsub_tahun(this)" onblur="gsub_tahun(this)" value="<?= $thn_lulus?>"/>
			<?php echo form_error('thn_lulus'); ?>  </td>
		</tr>
		<tr align="left">
			<td width ="200px">Nilai Kumulatif/IPK</td>
			<td> : </td>
			<td width ="200px">
			<input type="text" name="nilai_kumulatif" id="nilai_kumulatif" class="js_required required" onkeyup="gsub_realnum(this)" onchange="gsub_realnum(this)" 
				onblur="gsub_realnum(this)" value="<?= $nilai_kumulatif?>"/>
			<?php echo form_error('nilai_kumulatif'); ?>  </td>
		</tr>
		<tr align="left">
			<td width ="200px">Keterangan</td>
			<td> : </td>
			<td width ="200px">
			<input type="text" name="ket_riwayat_pendidikan" id="ket_riwayat_pendidikan" value="<?= $ket_riwayat_pendidikan?>" />
			<?php echo form_error('ket_riwayat_pendidikan'); ?>  </td>
		</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center">
			<input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " />
		</div>
		</form>
	</div>
</div>

<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>