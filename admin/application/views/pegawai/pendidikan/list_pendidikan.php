<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/pendidikan/add')?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/add-icon.png'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<br/>
	<div class="paging"><?=$page_links?></div>
	<div>
		<table class="table-list">
		<tr class="trhead">
			<th width='50' class="colEvn">Kode</th>
			<th width='100' >Tingkat Pendidikan</th>
			<th width='100' >Fakultas</th>
			<th width='150' >Jurusan</th>
			<th width='75' >Tahun</th>		
			<th class="colEvn">Edit</th>
		</tr>
		<?
		$i = 0;
		if ($list_pendidikan!=FALSE){
		foreach ($list_pendidikan as $jns_pendidikan) {
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$jns_pendidikan['id_riwayat_pendidikan'];?></td>
			<td align="center" ><?= $jns_pendidikan['tingkat_pendidikan']; ?></td>
			<td align="left" ><?= $jns_pendidikan['nama_fakultas']; ?></td>
			<td align="left" ><?= $jns_pendidikan['jurusan']; ?></td>
			<td align="center" ><?= $jns_pendidikan['thn_lulus']; ?></td>
			<td align="center" class="colEvn">			
				<a href = "<?=site_url("pegawai/pendidikan/edit/".$jns_pendidikan['id_riwayat_pendidikan']); ?>" class="edit action" >Ubah</a>
				<a href = "<?=site_url("pegawai/pendidikan/delete/".$jns_pendidikan['id_riwayat_pendidikan']); ?>" class="nyroModal delete action" >Hapus</a>
			</td>
		</tr>
	
		<? } ?>
		<?}
		else {
			echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
		}?>
		</table>
	</div>
</div>

