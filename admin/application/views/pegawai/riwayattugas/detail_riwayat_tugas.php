<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayattugas/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/riwayattugas/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
			
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
	<form name="tugasdetail" id="tugasdetail" method="POST" action="<?= $action_url; ?>">
		<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
		<table class="general">
			<tr align="left">
				<td width ="200">Kode</td>
				<td> : </td>
				<td >
				<?= $id_riwayat_tugas?></td>
			</tr>
			<tr>
					<td  align="left">Unit Kerja</td>
					<td> : </td>
					<td colspan="3"  align="left">
						<?php
							echo form_group_dropdown('kode_unit',$option_unit,$kode_unit);
							echo form_error('kode_unit');
						?>			
					</td>
			</tr>
			<tr align="left">
					<td width ="291">Tugas</td>
					<td> : </td>
					<td >
						<textarea name='nama_tugas' id='nama_tugas' rows="2" cols="40"> <?= $nama_tugas?> </textarea>
					<?php echo form_error('nama_tugas'); ?>	</td>
				</tr>
			<tr align="left">
				<td width ="200">Tanggal Mulai </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tgl_mulai_tugas' id='tgl_mulai_tugas'  value='<?= $tgl_mulai_tugas?>'  class="required"/>
					<?php echo form_error('tgl_mulai_tugas'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>		</td>
			</tr>
			<tr align="left">
				<td width ="200">Tanggal Selesai </td>
				<td> : </td>
				<td >
					<input type='text' class='datepicker' name='tgl_selesai_tugas' id='tgl_selesai_tugas'  value='<?= $tgl_selesai_tugas?>'  class="required"/>
					<?php echo form_error('tgl_selesai_tugas'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>	</td>
			</tr>
			<tr align="left">
				<td width ="200">Keterangan </td>
				<td> : </td>
				<td >
				<textarea name='keterangan' id='keterangan' rows="2" cols="40"> <?= $keterangan?> </textarea>
				<?php echo form_error('keterangan'); ?> </td>
			</tr>
			
			
		  
			
			  <tr align="left">
				<td width ="200px">Aktif</td>
				<td> : </td>
				<td width ="200px">
				<?php
					echo form_dropdown('aktif',$status_assoc, $aktif);
					echo form_error('aktif'); ?>  </td>
			</tr>
		</table>
		<br/>
		<!--<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>-->
		<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
	</form>
	
		
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>