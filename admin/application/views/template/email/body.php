<div><p>Yang terhormat, <?=$name; ?><br/>
Sesuai dengan permintaan Anda, password anda sekarang telah di reset. Detail akun Anda yang baru sebagai berikut:
</p>
<br/>
<div>
	<table>
	<tr>
		<td>Username</td>
		<td>:</td>
		<td><?=$username;?></td>
	</tr>
	<tr>
		<td>Password</td>
		<td>:</td>
		<td><?=$password; ?></td>
	</table>
</div>
<br/>
<p>Untuk mengubah password Anda, gunakan menu Ubah Password yang ada di dalam menu Profil User.</p>
<div class="clear">
<p>Terima kasih,
<br/>Administrator SIMPEG<p>
</div>
</div>