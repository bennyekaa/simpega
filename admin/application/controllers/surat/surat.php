<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Surat extends Member_Controller
{
	function Surat()
	{
		parent::Member_Controller();
		//$this->load->model('surat_model', 'surat');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Surat');
		$data['judul'] 		= "Surat";
		$this->template->display('error',$data);
		//$this->browse();
	}
	
	function browse($group=0)
	{
		$data['list_gaji'] = $this->gaji->findAll();	    
		$data['judul'] 		= "Data Gaji";
		$this->template->display('setup/gaji/list', $data);
	}
	
	function add()
	{
		$this->template->metas('title', 'SIMPEGA | Data Gaji :: Tambah');
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->gaji->add($data);
			set_success('Data gaji berhasil disimpan.');
			redirect('/setup/gaji');
		}
		else
		{
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah gaji';
			$this->template->display('/setup/gaji/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->gaji->update($id, $data);
			set_success('Perubahan data gaji berhasil disimpan');
			redirect('/setup/gaji', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Gaji :: Ubah');
			$data = $this->gaji->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit data gaji';
				$this->template->display('/setup/gaji/detail', $data);
			}
			else
			{
				set_error('Data gaji tidak ditemukan');
				redirect('/setup/gaji', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->gaji->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Gaji :: Hapus');
		confirm("Yakin menghapus data gaji <b>".$data['gaji_pokok']."</b> ?");
		$res = $this->gaji->delete($idField);
		set_success('Data gaji berhasil dihapus');
		redirect('/setup/gaji', 'location');
	}

	function _clear_form()
	{
		$data['kd_gaji']	= '';
		$data['gaji_pokok']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['kd_gaji']	= $this->input->post('kd_gaji', TRUE);
	   	$data['gaji_pokok']		= $this->input->post('gaji_pokok', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('gaji_pokok', 'gaji_pokok', 'required');
		return $this->form_validation->run();
	}
}