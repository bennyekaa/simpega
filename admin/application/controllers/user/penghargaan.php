<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Penghargaan extends Member_Controller
{
	function Penghargaan()
	{
		parent::Member_Controller();
		$this->load->model('penghargaan_model', 'penghargaan');
		$this->load->model('riwayat_penghargaan_model', 'riwayat_penghargaan');
		$this->load->model('user_pegawai_model', 'user_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Riwayat Penghargaan');
		$this->browse();
	}
	
	function browse($group=0)
	{
		$pegawai_id = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
		$data['list_penghargaan'] = $this->riwayat_penghargaan->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= 'Riwayat Penghargaan';
		$this->template->display('pegawai/penghargaan/list', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_pegawai'] = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
			$this->riwayat_penghargaan->add($data);
			set_success('Data riwayat penghargaan berhasil disimpan.');
			redirect('/pegawai/penghargaan');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat Penghargaan :: Add');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Data Riwayat Penghargaan';
			$data['id_riwayat_penghargaan']=$this->riwayat_penghargaan->get_id();
			$data['jenis_penghargaan_assoc'] = array(0=>"-- Pilih Penghargaan --")+$this->penghargaan->get_assoc1("nama_penghargaan");
			$this->template->display('/pegawai/penghargaan/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_penghargaan'] = $id;
			$data['kd_pegawai'] = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
			$this->riwayat_penghargaan->update($id, $data);
			set_success('Perubahan data riwayat penghargaan berhasil disimpan');
			redirect('/pegawai/penghargaan', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat Penghargaan :: Edit');
			$data = $this->riwayat_penghargaan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Riwayat Penghargaan';
				$data['jenis_penghargaan_assoc'] = array(0=>"-- Pilih Penghargaan --")+$this->penghargaan->get_assoc1("nama_penghargaan");
				$this->template->display('/pegawai/penghargaan/detail', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/penghargaan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_penghargaan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Riwayat Penghargaan :: Hapus');
		confirm("Yakin menghapus data riwayat penghargaan <b>".$data['nama_penghargaan']."</b> ?");
		$res = $this->riwayat_penghargaan->delete($idField);
		set_success('Data riwayat penghargaan berhasil dihapus');
		redirect('/pegawai/penghargaan', 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_penghargaan']	= '';
		$data['FK_jenis_penghargaan']	= '';
		$data['kd_pegawai']	= '';
		$data['tgl_penghargaan']	= '';
		$data['ket_riwayat_penghargaan']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_riwayat_penghargaan']		= $this->input->post('id_riwayat_penghargaan', TRUE);
	   	$data['FK_jenis_penghargaan']		= $this->input->post('FK_jenis_penghargaan', TRUE);
		$data['tgl_penghargaan']			= $this->input->post('tgl_penghargaan', TRUE);
		$data['ket_riwayat_penghargaan']	= $this->input->post('ket_riwayat_penghargaan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('FK_jenis_penghargaan', 'FK_jenis_penghargaan', 'required');
		$this->form_validation->set_rules('tgl_penghargaan', 'tgl_penghargaan', 'required');
		return $this->form_validation->run();
	}
}