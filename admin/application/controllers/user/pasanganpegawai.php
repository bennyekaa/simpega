<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pasanganpegawai extends Member_Controller
{
	function Pasanganpegawai()
	{
		parent::Member_Controller();
		$this->load->model('status_keluarga_model', 'status_keluarga');
		$this->load->model('pasangan_pegawai_model', 'pasangan_pegawai');
		$this->load->model('pegawai_model', 'pegawai');
        $this->load->model('lookup_model','lookup');
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Pasangan Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
        $ordby = 'id_pasangan_pegawai';
		
		$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg($kd_pegawai);
		$config['base_url']     = site_url('pegawai/pasanganpegawai/browse/');
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Pasangan dari: " . $data['pegawai']['nama_pegawai'];
		$this->template->display('pegawai/pasanganpegawai/list', $data);
	}

	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
			$data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_pasangan_pegawai'] = $data['id_pasangan_pegawai'];			
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
				$data_status['aktif'] = '0';
				$this->pasangan_pegawai->update_status($kd_pegawai,$data_status);
			}
			$this->pasangan_pegawai->add($data);
			set_success('Data pasangan pegawai berhasil disimpan.');
			redirect('/pegawai/pasanganpegawai/index/' . $data['kd_pegawai']);
		}
		else
		{
            $kd_pegawai = $this->uri->segment(4);
			$this->template->metas('title', 'SIMPEGA | Pasangan Pegawai :: Tambah');
			$data = $this->_clear_form();
            $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['action']='add';
			$data['judul']='Tambah Pasangan dari: '. $data['pegawai']['nama_pegawai'];
            
			$data['id_pasangan_pegawai']=$this->pasangan_pegawai->get_id();
			//menampilkan hanya status istri/sumi berdasarkan jenis kelamin pegawai
			$query = mysql_query("select jns_kelamin from pegawai where kd_pegawai='".$kd_pegawai."'");			
			if ($query) {
				$data_kel=mysql_fetch_array($query); 
				$jk = $data_kel['jns_kelamin'];
				if ($jk=='1') {	
					$data['status_keluarga_assoc'] = $this->status_keluarga->get_pasangan_assoc("status_keluarga",'id_istri');
					}
				else {
					$data['status_keluarga_assoc'] = $this->status_keluarga->get_pasangan_assoc("status_keluarga",'id_suami');
					}
			}
			else
			{
				$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc("status_keluarga");
			}
			$data['status_assoc'] = $this->lookup->status_assoc();
            $this->template->display('/pegawai/pasanganpegawai/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_pasangan_pegawai'] = $id;
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_pasangan_pegawai'] = $data['id_pasangan_pegawai'];			
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
				$data_status['aktif'] = '0';
				$this->pasangan_pegawai->update_status($kd_pegawai,$data_status);
			}
			$this->pasangan_pegawai->update($id, $data);
			set_success('Perubahan data pasangan pegawai berhasil disimpan');
			redirect('/pegawai/pasanganpegawai/index/' . $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Pasangan Pegawai :: Ubah');
			$data = $this->pasangan_pegawai->retrieve_by_pkey($id);
			//menampilkan hanya status istri/sumi berdasarkan jenis kelamin pegawai
			$query = mysql_query("select jns_kelamin from pegawai where kd_pegawai='".$kd_pegawai."'");			
			if ($query) {
				$data_kel=mysql_fetch_array($query); 
				$jk = $data_kel['jns_kelamin'];
				if ($jk=='1') {	
					$data['status_keluarga_assoc'] = $this->status_keluarga->get_pasangan_assoc("status_keluarga",'id_istri');
					}
				else {
					$data['status_keluarga_assoc'] = $this->status_keluarga->get_pasangan_assoc("status_keluarga",'id_suami');
					}
			}
			else
			{
				$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc("status_keluarga");
			}
			$data['status_assoc'] = $this->lookup->status_assoc();
            if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['judul']='Edit Pasangan dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/pasanganpegawai/detail', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/pasanganpegawai', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->pasangan_pegawai->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Pasangan Pegawai :: Hapus');
		confirm("Yakin menghapus data pasangan pegawai <b>".$data['nama_keluarga']."</b> ?");
		$res = $this->pasangan_pegawai->delete($idField);
		set_success('Data pasangan pegawai berhasil dihapus');
		redirect('/pegawai/pasanganpegawai/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_pasangan_pegawai']	= '';
		$data['kd_pegawai']	= '';
		$data['no_urut']	= '';
		$data['nama_pasangan']	= '';
		$data['pekerjaan']	= '';
		$data['tempat_lahir']	= '';
		$data['tanggal_lahir']	= '';
		$data['tanggal_nikah']	= '';
		$data['no_karis_karsu']	= '';
		$data['tgl_karis_karsu']	= '';
		$data['keterangan']	= '';
		$data['aktif']	= '';
		$data['kd_status_keluarga']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   	$data['id_pasangan_pegawai']	= $this->pasangan_pegawai->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['no_urut']	= $this->input->post('no_urut', TRUE);
		$data['nama_pasangan']	= $this->input->post('nama_pasangan', TRUE);
		$data['pekerjaan']	= $this->input->post('pekerjaan', TRUE);
		$data['tempat_lahir']	= $this->input->post('tempat_lahir', TRUE);
		$data['tanggal_lahir']	= $this->input->post('tgl_lahir', TRUE);
		$data['tanggal_nikah']	= $this->input->post('tanggal_nikah', TRUE);
		$data['no_karis_karsu']	= $this->input->post('no_karis_karsu', TRUE);
		$data['tgl_karis_karsu']	= $this->input->post('tgl_karis_karsu', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		$data['kd_status_keluarga']	= $this->input->post('kd_status_keluarga', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_pasangan', 'nama_pasangan', 'required');
		$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}