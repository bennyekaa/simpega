<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pangkat extends Member_Controller
{

	function Pangkat()
	{
		parent::Member_Controller();
		$this->load->model('proses_kenaikan_pangkat_model', 'proses_pangkat');
		$this->load->model('lookup_model', 'lookup');
		$this->load->model('kerja_model', 'kerja');
		$this->load->model('riwayat_pangkat_model', 'riwayat_pangkat');
		$this->load->model('jenis_kenaikan_pangkat_model', 'jenis_pangkat');
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('konfigurasi_model', 'konfig');
		$this->load->model('riwayat_dp3_model', 'dp3');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Kenaikan Pangkat');
		$this->browse();
	}
	
	function browse()
	{	
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'kd_pegawai';
		
		$param = $this->konfig->findAll();
		$group = $this->input->post('group',TRUE);
		$data['list_pegawai'] 	= $this->proses_pangkat->findAll($ordby,$limit_per_page,$start);
		$config['base_url']     = site_url('pegawai/pangkat/browse/');
		$config['total_rows']   = $this->proses_pangkat->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		
		if($group == 1)
		{
			$bln = 4;
		}
		elseif($group == 2)
		{
			$bln = 7;
		}
		elseif($group == 3)
		{
			$bln = 10;
		}
		else
		{
			$bln = 1;
			$group = 0;
		}
		if($data['list_pegawai'] != FALSE)
		{
			foreach($data['list_pegawai'] as $tmp)
			{
				$thn = $this->htg_tahun($tmp['tmt_golongan'],$bln);
				//var_dump($thn);
				$no = $this->dp3->get_max_no_urut($tmp['kd_pegawai']);
				$rata2 = $this->dp3->get_rata2($tmp['kd_pegawai'], $no);
				if($rata2 == FALSE)
				{
					$rata2 = 0;
				}
				if(($thn >= $param[0]['value']) and ($rata2 >= $param[1]['value']))
				{
					$tmp['masa_kerja'] = $thn . " thn";
					$dta[] = $tmp;
				}
			}
		}
		
		$data['list_pegawai'] 	= $dta;
		$data['group']			= $group;
		$data['judul'] 			= "Kenaikan Pangkat";
		$this->template->display('pegawai/pangkat/list', $data);
	}
		
	function rincian()
	{
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$id_peg = $this->uri->segment(4);
		$limit_per_page = 10;
		$ordby = 'id_riwayat_pangkat';
		
		$search ="FK_pegawai = '".$id_peg."'";
		$data['list_pangkat'] 	= $this->riwayat_pangkat->findByFilter($search,$ordby,$limit_per_page,$start);
		$config['base_url']     = site_url('pegawai/pangkat/rincian/'.$id_peg.'/');
		$config['total_rows']   = $this->terminasi->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['id_peg']			= $id_peg;
		$data['judul'] 			= "Riwayat Kenaikan Pangkat";
		//show_error(var_dump($data));
		$this->template->display('pegawai/pangkat/detail-list', $data);
	}
	
	function kenaikan()
	{
		$id = $this->uri->segment(4);
		if ($this->_validate())
		{	
			$data = $this->_get_form_values();
			//show_error($data);
			$this->riwayat_pangkat->add($data);
			$this->kerja->update($data['FK_pegawai'], array('FK_pegawai'=>$data['FK_pegawai'],'id_golpangkat'=>$data['id_golpangkat'],'tmt_golongan'=>$data['tmt_pangkat']));
			set_success('Data kenaikan pangkat berhasil disimpan.');
			redirect('/pegawai/pangkat/rincian/'.$id);
		}
		else
		{
			$gol = $this->golongan->get_assoc("golongan");
			$gol_ = $this->kerja->get_golongan($id);
			$this->template->metas('title', 'SIMPEGA | Kenaikan Pangkat :: Kenaikan');
			$data = $this->_clear_form();
			$data['action']='kenaikan/'.$id;
			$data['judul']='Tambah Kenaikan Pangkat';
			$data['id_riwayat_pangkat']=$this->riwayat_pangkat->get_id();
			$data['FK_pegawai'] = $id;
			$data['id_golpangkat'] = $gol[$gol_['id_golpangkat']];
			$data['no_urut']= 1 + intval($this->riwayat_pangkat->get_max_no_urut($id));
			$data['golongan_assoc'] = array(0=>"-- Pilih Golongan --") + $gol;
			$data['jenis_kenaikan_assoc'] = array(0=>"-- Pilih Jenis Kenaikan --")+$this->jenis_pangkat->get_assoc("jenis_kenaikan_pangkat");
			$this->template->display('/pegawai/pangkat/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_pangkat'] = $id;
			$this->riwayat_pangkat->update($id, $data);
			$this->kerja->update($data['FK_pegawai'], array('FK_pegawai'=>$data['FK_pegawai'],'id_golpangkat'=>$data['id_golpangkat']));
			set_success('Perubahan data kenaikan pangkat berhasil disimpan');
			redirect('/pegawai/pangkat/rincian', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Kenaikan Pangkat :: Edit');
			$data = $this->riwayat_pangkat->retrieve_by_pkey($id);
			$data['id_golpangkat'] = $this->kerja->get_golongan($data['FK_pegawai']);
			$data['action'] = 'edit/'.$id;
			$data['judul']='Edit Kenaikan Pangkat';
			$data['golongan_assoc'] = array(0=>"-- Pilih Golongan --")+$this->golongan->get_assoc("golongan");
			$data['jenis_kenaikan_assoc'] = array(0=>"-- Pilih Jenis Kenaikan --")+$this->jenis_pangkat->get_assoc("jenis_kenaikan_pangkat");
			$this->template->display('/pegawai/pangkat/detail', $data);
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_pangkat->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Kenaikan Pangkat :: Hapus');
		confirm("Yakin menghapus data kenaikan pangkat <b>".$data['no_urut']."</b> ?");
		$res = $this->riwayat_pangkat->delete($idField);
		set_success('Data kenaikan pangkat berhasil dihapus');
		redirect('/pegawai/pangkat/rincian', 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_pangkat']	= '';
		$data['tmt_pangkat']	= '';
		$data['no_sk']	= '';
		$data['no_urut']	= '';
		$data['tgl_sk']	= '';
		$data['id_golpangkat']	= '';
		$data['FK_pegawai']	= '';
		$data['FK_jenis_kenaikan']	= '';
		$data['ket_riwayat_pangkat']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_riwayat_pangkat']	= $this->input->post('id_terminasi', TRUE);
		$data['tmt_pangkat']		= $this->input->post('tmt_pangkat', TRUE);
		$data['no_urut']			= $this->input->post('no_urut', TRUE);
		$data['no_sk']				= $this->input->post('no_sk', TRUE);
		$data['tgl_sk']				= $this->input->post('tgl_sk', TRUE);
		$data['id_golpangkat']		= $this->input->post('FK_gol_baru', TRUE);
	   	$data['FK_pegawai']			= $this->input->post('FK_pegawai', TRUE);
		$data['FK_jenis_kenaikan']	= $this->input->post('FK_jenis_kenaikan', TRUE);
		$data['ket_riwayat_pangkat']= $this->input->post('ket_riwayat_pangkat', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('tmt_pangkat', 'TMT Pangkat', 'required');
		$this->form_validation->set_rules('no_sk', 'No SK', 'required');
		$this->form_validation->set_rules('tgl_sk', 'Tanggal SK', 'required');
		$this->form_validation->set_rules('FK_gol_baru', 'Jenis Golongan', 'required');
		$this->form_validation->set_rules('FK_pegawai', 'ID Pegawai', 'required');
		$this->form_validation->set_rules('FK_jenis_kenaikan', 'Jenis Kenaikan', 'required');
		return $this->form_validation->run();
	}
	
	function htg_tahun($tanggal, $bln)
	{
	    list($year,$month,$day) = explode("-",$tanggal);
		$year_diff  = date("Y") - $year;
		$month_diff = $bln - $month;
		$day_diff   = 1 - $day;
		if ($day_diff < 0 || $month_diff < 0)
			$year_diff--;
		if($year_diff < 0)
			$year_diff = 0;
		return $year_diff;
	}
}