<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Terminasi extends Member_Controller
{

	function Terminasi()
	{
		parent::Member_Controller();
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model', 'lookup');
		$this->load->model('terminasi_model', 'terminasi');
		$this->load->model('jenis_terminasi_model', 'jenis_terminasi');
		$this->load->model('user_pegawai_model', 'user_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Terminasi Pegawai');
		$this->browse();
	}
	
	function browse()
	{	
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'kd_pegawai';
		
		$data['list_pegawai'] = $this->pegawai->getAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('pegawai/terminasi/browse/');
		$config['total_rows']   = $this->pegawai->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
	
		$data['judul'] 			= "Terminasi Pegawai";
		$this->template->display('pegawai/terminasi/list', $data);
	}
		/*
		$pegawai_id = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
		$data['list_terminasi'] = $this->terminasi->retrieve_by_idpeg($pegawai_id);
		*/
		
	function view()
	{
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$id_peg = $this->uri->segment(4);
		$limit_per_page = 10;
		$ordby = 'id_terminasi';
		
		$search ="FK_kd_pegawai = '".$id_peg."'";
		$data['list_terminasi'] 	= $this->terminasi->findByFilter($search,$ordby,$limit_per_page,$start);
		$config['base_url']     = site_url('pegawai/terminasi/view/'.$id_peg.'/');
		$config['total_rows']   = $this->terminasi->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['id_peg']			= $id_peg;
		$data['judul'] 			= "Terminasi Pegawai";
		$this->template->display('pegawai/terminasi/detail-list', $data);
	}
	
	function add()
	{
		$id = $this->uri->segment(4);
		if ($this->_validate())
		{	
			$data = $this->_get_form_values();
			$this->terminasi->add($data);
			$stat = $this->input->post('aktif', TRUE);
			$this->pegawai->modify($id, array('kd_pegawai'=>$id,'aktif'=>$stat));
			set_success('Data riwayat terminasi berhasil disimpan.');
			redirect('/pegawai/terminasi/view/'.$id);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Terminasi Pegawai :: Add');
			$data = $this->_clear_form();
			$data['action']='add/'.$id;
			$data['judul']='Tambah Terminasi';
			$data['id_terminasi']=$this->terminasi->get_id();
			$data['FK_kd_pegawai'] = $id;
			$data['terminasi_assoc'] = array(0=>"-- Pilih Alasan --")+$this->jenis_terminasi->get_assoc("jenis_terminasi");
			$data['status_assoc'] = $this->lookup->status_assoc();
			$this->template->display('/pegawai/terminasi/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_terminasi'] = $id;
			$stat = $this->input->post('aktif', TRUE);
			$this->pegawai->modify($data['FK_kd_pegawai'], array('kd_pegawai'=>$data['FK_kd_pegawai'],'aktif'=>$stat));
			$this->terminasi->update($id, $data);
			set_success('Perubahan data riwayat terminasi berhasil disimpan');
			redirect('/pegawai/terminasi/view/'.$data['FK_kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat Terminasi :: Edit');
			$data = $this->terminasi->retrieve_by_pkey($id);
			$data['aktif'] = $this->pegawai->get_active($data['FK_kd_pegawai']);
			$data['action'] = 'edit/'.$id;
			$data['judul']='Edit Terminasi';
			$data['terminasi_assoc'] = array(0=>"-- Pilih Alasan --")+$this->jenis_terminasi->get_assoc("jenis_terminasi");
			$data['status_assoc'] = $this->lookup->status_assoc();
			$this->template->display('/pegawai/terminasi/detail', $data);
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->terminasi->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Riwayat Terminasi :: Hapus');
		confirm("Yakin menghapus data terminasi <b>".$data['id_terminasi']."</b> ?");
		$res = $this->terminasi->delete($idField);
		set_success('Data terminasi berhasil dihapus');
		redirect('/pegawai/terminasi/view', 'location');
	}

	function _clear_form()
	{
		$data['id_terminasi']	= '';
		$data['tmt_terminasi']	= '';
		$data['no_sk']	= '';
		$data['tgl_sk']	= '';
		$data['FK_kd_pegawai']	= '';
		$data['FK_jenis_terminasi']	= '';
		$data['persetujuan']	= '';
		$data['ket_terminasi']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_terminasi']	= $this->input->post('id_terminasi', TRUE);
		$data['tmt_terminasi']	= $this->input->post('tmt_terminasi', TRUE);
		$data['no_sk']				= $this->input->post('no_sk', TRUE);
		$data['tgl_sk']				= $this->input->post('tgl_sk', TRUE);
	   	$data['FK_kd_pegawai']	= $this->input->post('FK_kd_pegawai', TRUE);
		$data['FK_jenis_terminasi']	= $this->input->post('FK_jenis_terminasi', TRUE);
		$data['persetujuan']		= $this->input->post('persetujuan', TRUE);
		$data['ket_terminasi']		= $this->input->post('ket_terminasi', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('tmt_terminasi', 'tmt_terminasi', 'required');
		$this->form_validation->set_rules('no_sk', 'no_sk', 'required');
		$this->form_validation->set_rules('tgl_sk', 'tgl_sk', 'required');
		$this->form_validation->set_rules('FK_kd_pegawai', 'FK_kd_pegawai', 'required');
		$this->form_validation->set_rules('FK_jenis_terminasi', 'FK_jenis_terminasi', 'required');
		return $this->form_validation->run();
	}
}