<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Anakpegawai extends Member_Controller
{
	function Anakpegawai()
	{
		parent::Member_Controller();
		$this->load->model('status_keluarga_model', 'status_keluarga');
		$this->load->model('anak_pegawai_model', 'anak_pegawai');
		$this->load->model('pegawai_model', 'pegawai');
        $this->load->model('lookup_model', 'lookup');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Anak Pegawai');
        $kd_pegawai = $this->uri->segment(4);
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'kd_anak';
		
		$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($kd_pegawai);
		$config['base_url']     = site_url('pegawai/anakpegawai/browse/');
		$config['total_rows']   = $this->anak_pegawai->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();		    
		$data['pegawai'] 		= $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Anak dari: " . $data['pegawai']['nama_pegawai'];
        $data['gender_assoc'] = array(0=>"-- Pilih Jenis Kelamin --") +$this->lookup->gender_assoc();
		$this->template->display('pegawai/anakpegawai/list', $data);
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
            $kd_pegawai = $this->input->post('kd_pegawai'); 
			$data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			$this->anak_pegawai->add($data);
			set_success('Data anak pegawai berhasil disimpan.');
			redirect('/pegawai/anakpegawai/index/' . $kd_pegawai);
		}
		else
		{
            $kd_pegawai = $this->uri->segment(4, '');
			$this->template->metas('title', 'SIMPEGA | Anak Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Anak dari: ' . $data['pegawai']['nama_pegawai'];
			$data['kd_anak']=$this->anak_pegawai->get_id();
			//$data['status_keluarga_assoc'] = array(0=>"-- Pilih Status --")+$this->status_keluarga->get_assoc("status_keluarga");
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_anak_assoc("status_keluarga");
		    $data['gender_assoc'] = $this->lookup->gender_assoc();
            $data['status_assoc'] = $this->lookup->status_assoc();
            $this->template->display('/pegawai/anakpegawai/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_anak'] = $id;
			$this->anak_pegawai->update($id, $data);
			set_success('Perubahan data anak pegawai berhasil disimpan');
			redirect('/pegawai/anakpegawai/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Anak Pegawai :: Ubah');
			$data = $this->anak_pegawai->retrieve_by_pkey($id);
//			$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc("status_keluarga");
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_anak_assoc("status_keluarga");
			$data['gender_assoc'] = $this->lookup->gender_assoc();
            $data['status_assoc'] = $this->lookup->status_assoc();
            if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['judul']='Edit Anak dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/anakpegawai/detail', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/anakpegawai', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->anak_pegawai->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Anak Pegawai :: Hapus');
		confirm("Yakin menghapus data anak pegawai <b>".$data['nama_anak']."</b> ?");
		$res = $this->anak_pegawai->delete($idField);
		set_success('Data anak pegawai berhasil dihapus');
		redirect('/pegawai/anakpegawai/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['kd_anak']	= '';
		$data['kd_pegawai']	= '';
		$data['nama_anak']	= '';
		$data['tempat_lahir']	= '';
		$data['tgl_lahir']	= '';
		$data['jns_kelamin']	= '';
		$data['keterangan']	= '';
		$data['kd_status_keluarga']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   	$data['kd_anak']	= $this->anak_pegawai->get_id();
	
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['nama_anak']	= $this->input->post('nama_anak', TRUE);
		$data['tempat_lahir']	= $this->input->post('tempat_lahir', TRUE);
		$data['tgl_lahir']	= $this->input->post('tgl_lahir', TRUE);
		$data['jns_kelamin']	= $this->input->post('jns_kelamin', TRUE);
		
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['kd_status_keluarga']	= $this->input->post('kd_status_keluarga', TRUE);
		return $data;
		
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_anak', 'nama_anak', 'required');
		$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}