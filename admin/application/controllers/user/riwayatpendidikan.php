<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatpendidikan extends Member_Controller
{
	function Riwayatpendidikan()
	{
		parent::Member_Controller();
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model('riwayat_pendidikan_model','riwayat_pendidikan');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');
		
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Pendidikan Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		
        $ordby = 'id_riwayat_pendidikan';
		
		
		$data['list_pendidikan'] = $this->riwayat_pendidikan->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
		
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Pendidikan dari: " . $data['pegawai']['nama_pegawai'];
        
		$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
		$this->template->display('pegawai/riwayatpendidikan/list', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_pendidikan_terakhir'] = $data['id_pendidikan'];
				$data_pegawai['ket_pendidikan'] = $data['keterangan'];
				
				
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
				$data_status['aktif'] = '0';
				$this->riwayat_pendidikan->update_status($kd_pegawai,$data_status);
			}
			$this->riwayat_pendidikan->add($data);
			set_success('Data pendidikan pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatpendidikan/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Pendidikan Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Pendidikan dari: ' . $data['pegawai']['nama_pegawai'];
		
			$data['id_riwayat_pendidikan']=$this->riwayat_pendidikan->get_id();
			$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
			$data['pendidikan_assoc'] = array(0=>"-- Pilih Pendidikan --")+$this->pendidikan->get_assoc("nama_pendidikan");
			$data['status_assoc'] = $this->lookup->status_assoc();
			$this->template->display('/pegawai/riwayatpendidikan/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_pendidikan'] = $id;
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_pendidikan_terakhir'] = $data['id_pendidikan'];
				$data_pegawai['ket_pendidikan'] = $data['keterangan'];
			
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
				$data_status['aktif'] = '0';
				$this->riwayat_pendidikan->update_status($kd_pegawai,$data_status);
			}
			$this->riwayat_pendidikan->update($id, $data);
			set_success('Perubahan data pendidikan pegawai berhasil disimpan');
			redirect('/pegawai/riwayatpendidikan/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Pendidikan Pegawai :: Ubah');
			$data = $this->riwayat_pendidikan->retrieve_by_pkey($id);
			$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
			$data['pendidikan_assoc'] = array(0=>"-- Pilih Pendidikan --")+$this->pendidikan->get_assoc("nama_pendidikan");
			$data['status_assoc'] = $this->lookup->status_assoc();
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Pendidikan dari: '. $data['pegawai']['nama_pegawai'];
				
				$this->template->display('/pegawai/riwayatpendidikan/detail', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatpendidikan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_pendidikan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Pendidikan Pegawai :: Hapus');
		confirm("Yakin menghapus data pendidikan pegawai?");
		$res = $this->riwayat_pendidikan->delete($idField);
		set_success('Data pendidikan pegawai berhasil dihapus');
		redirect('/pegawai/riwayatpendidikan/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_pendidikan']	= '';
		$data['kd_pegawai']	= '';
		$data['id_pendidikan']	= '';
		
		//$data['nama_pendidikan']	= '';
		$data['th_lulus']	= '';
		$data['keterangan']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	  
		$data['id_riwayat_pendidikan']	= $this->riwayat_pendidikan->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['id_pendidikan']	= $this->input->post('id_pendidikan', TRUE);
		//$data['nama_pendidikan']	= $this->input->post('nama_pendidikan', TRUE);
		$data['th_lulus']	= $this->input->post('th_lulus', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('id_pendidikan', 'id_pendidikan', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}