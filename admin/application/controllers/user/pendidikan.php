<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan extends Member_Controller
{
	function Pendidikan()
	{
		parent::Member_Controller();
		$this->load->model('fakultas_model', 'fakultas');
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model('riwayat_pendidikan_model', 'riwayat_pendidikan');
		$this->load->model('user_pegawai_model', 'user_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Riwayat Pendidikan');
		$this->browse();
	}
	
	function browse($group=0)
	{
		$pegawai_id = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
		$data['list_pendidikan'] = $this->riwayat_pendidikan->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= " Riwayat Pendidikan";
		$this->template->display('pegawai/pendidikan/list', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['FK_pegawai'] = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
			$this->riwayat_pendidikan->add($data);
			set_success('Data riwayat pendidikan berhasil disimpan.');
			redirect('/pegawai/pendidikan');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat Pendidikan :: Add');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Add Riwayat Pendidikan';
			$data['id_riwayat_pendidikan']=$this->riwayat_pendidikan->get_id();
			$data['tingkat_pendidikan_assoc'] = array(0=>"-- Pilih Pendidikan --")+$this->pendidikan->get_assoc("tingkat_pendidikan");
			$data['fakultas_assoc'] = array(0=>"-- Pilih Fakultas --")+$this->fakultas->get_assoc("nama_fakultas");
			$this->template->display('/pegawai/pendidikan/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_pendidikan'] = $id;
			$data['FK_pegawai'] = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
			$this->riwayat_pendidikan->update($id, $data);
			set_success('Perubahan data riwayat pendidikan berhasil disimpan');
			redirect('/pegawai/pendidikan', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat Pendidikan :: Edit');
			$data = $this->riwayat_pendidikan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Riwayat Pendidikan';
				$data['tingkat_pendidikan_assoc'] = array(0=>"-- Pilih Pendidikan --")+$this->pendidikan->get_assoc("tingkat_pendidikan");
				$data['fakultas_assoc'] = array(0=>"-- Pilih Fakultas --")+$this->fakultas->get_assoc("nama_fakultas");
				$this->template->display('/pegawai/pendidikan/detail', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/pendidikan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_pendidikan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Riwayat Pendidikan :: Hapus');
		confirm("Yakin menghapus data riwayat pendidikan <b>".$data['nama_instansi']."</b> ?");
		$res = $this->riwayat_pendidikan->delete($idField);
		set_success('Data riwayat pendidikan berhasil dihapus');
		redirect('/pegawai/pendidikan', 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_pendidikan']	= '';
		$data['FK_tingkat_pendidikan']	= '';
		$data['nama_instansi']			= '';
		$data['alamat']					= '';
		$data['FK_fakultas']			= '';
		$data['jurusan']				= ''; 
		$data['no_ijazah']				= '';
		$data['nilai_kumulatif']		= '';
		$data['thn_masuk']				= '';
		$data['thn_lulus']				= '';
		$data['ket_riwayat_pendidikan']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_riwayat_pendidikan']	= $this->input->post('id_riwayat_pendidikan', TRUE);
	   	$data['FK_tingkat_pendidikan']	= $this->input->post('FK_tingkat_pendidikan', TRUE);
	   	$data['nama_instansi']			= $this->input->post('nama_instansi', TRUE);
	   	$data['alamat']					= $this->input->post('alamat', TRUE);
		$data['FK_fakultas']			= $this->input->post('FK_fakultas', TRUE);
		$data['jurusan']				= $this->input->post('jurusan', TRUE);
		$data['no_ijazah']				= $this->input->post('no_ijazah', TRUE);
	   	$data['nilai_kumulatif']		= $this->input->post('nilai_kumulatif', TRUE);
	   	$data['thn_masuk']				= $this->input->post('thn_masuk', TRUE);
		$data['thn_lulus']				= $this->input->post('thn_lulus', TRUE);
		$data['ket_riwayat_pendidikan']	= $this->input->post('ket_riwayat_pendidikan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('FK_tingkat_pendidikan', 'FK_tingkat_pendidikan', 'required');
		$this->form_validation->set_rules('nama_instansi', 'nama_instansi', 'required');
		$this->form_validation->set_rules('FK_fakultas', 'FK_fakultas', 'required');
		$this->form_validation->set_rules('jurusan', 'jurusan', 'required');
		$this->form_validation->set_rules('no_ijazah', 'no_ijazah', 'required');
		$this->form_validation->set_rules('thn_lulus', 'thn_lulus', 'required');
		return $this->form_validation->run();
	}
}