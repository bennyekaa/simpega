<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Caripegawai extends Member_Controller
{
	var $pegawai_id;
	var $groupuser;
	function Caripegawai()
	{
		parent::Member_Controller();
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('agama_model', 'agama');
		$this->load->model('lookup_model', 'lookup');
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model('kelurahan_model', 'kelurahan');
		$this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('golongan_model', 'golongan');
        $this->load->model('status_perkawinan_model', 'status_perkawinan');
        $this->load->model('jabatan_model', 'jabatan');
		$this->load->model('riwayat_gol_kepangkatan_model', 'riwayat_gol_kepangkatan');
		$this->load->model('riwayat_jabatan_model', 'riwayat_jabatan');
		$this->load->model('riwayat_pendidikan_model', 'riwayat_pendidikan');
        $this->load->model('lap_pegawai_model','lap_pegawai');
		
		/*
		$this->load->model('gambar_model', 'gambar');
		*/
		$this->load->model('user_pegawai_model', 'user_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data Induk Pegawai');
		$this->cari();
	}
	

	function browse()
	{
       
	
	
	}
    function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
    function view($idField)
    {
        $data = $this->lap_pegawai->retrieve_by_pkey($idField);
		$data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
		$data['nama_peg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama_pegawai');
		$data['tempat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tempat_lahir');
		$data['tanggal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
		$data['alamat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat');
		$data['jns'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'jns_kelamin');
		if ($data['jns']==1)
            $data['jenis'] = 'Laki-Laki';
        else
		    $data['jenis'] = 'Perempuan';
		$data['agama_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_agama');
		$data['agama'] = $this->lookup->get_cari_field('agama','kd_agama',$data['agama_id'],'agama');
		$data['stskawin'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_kawin');	
        if ($data['stskawin']==1)
            $data['stkawin'] = 'Kawin';
        else
		    $data['stkawin'] = 'Belum Kawin';
        $data['jenis_pegawai_id']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jns_pegawai');
        $data['jenis_pegawai']=$this->lookup->get_cari_field('jenis_pegawai','id_jns_pegawai',$data['jenis_pegawai_id'],'jenis_pegawai');
		$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
        if ($data['status_pegawai']==0)
            $data['status_pegawai'] = 'Honorer';
        elseif ($data['status_pegawai']==1)
            $data['status_pegawai'] = 'CPNS';
        else
            $data['status_pegawai'] = 'PNS';
        $data['pangkat_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
        $data['pangkat']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'pangkat');
        
        $data['golongan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
        $data['golongan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['golongan_id'],'golongan');
        
        $data['jabatan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_terakhir');
        $data['jabatan']=$this->lookup->get_cari_field('jabatan','id_jabatan',$data['jabatan_id'],'nama_jabatan');
        
        $data['unit_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit');
        $data['unit_kerja']=$this->lookup->get_cari_field('unit_kerja','kode_unit',$data['unit_id'],'nama_unit');
        
        $tgl = date('Y-m-d');
        $data['tgl_lahir']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
        $usia = $this->datediff($data['tgl_lahir'],$tgl);
        $data['usia'] = $usia['years']." tahun ".$usia['months']." bulan";
        
        $data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
		$masa_kerja = $this->datediff($data['tmt_cpns'],$tgl);
        $data['masa_kerja']=$masa_kerja['years']." tahun ".$masa_kerja['months']." bulan";
        
        $data['jabatan_batas']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_terakhir');
        $data['batas']=$this->lookup->get_cari_field('jabatan','id_jabatan',$data['jabatan_batas'],'batas_maks_pensiun');
        $batas=$data['batas'];
                list($y, $m, $d) = explode('-', $data['tgl_lahir']);
                
                $tahun = $y+ $batas;
                if ($m>=2)
                {
                    $bulan = $m+1;    
                    if ($bulan==13)
                    {
                        $bulan = 1;
                        $tahun = $y+$batas+1;
                    }  
                }
                else
                {
                    $bulan = $m;
                }
                 
    			$data['pensiun'] ='1/'.$bulan.'/'.$tahun;
				$expected_file = PUBLICPATH . 'photo/photo_' . $idField . '.jpg';
				if (file_exists($expected_file)){
						$data['photo'] = base_url() . '/public/photo/photo_' . $idField . '.jpg';
					}
				else{
					$expected_file = PUBLICPATH . 'photo/photo_' . $idField . '.gif';
					if (file_exists($expected_file)){
						$data['photo'] = base_url() . '/public/photo/photo_' . $idField . '.gif';
						}
					else{
						$expected_file = PUBLICPATH . 'photo/photo_' . $idField . '.bmp';
						if (file_exists($expected_file)){
								$data['photo'] = base_url() . '/public/photo/photo_' . $idField . '.bmp';
							}
							else{	
								$data['photo'] = base_url() . '/public/images/image/nophotos.jpg';
						}
					}
				}
        //$data['photo'] = file_exists($expected_file) ? base_url() . '/public/photo/photo_' . $idField . '.jpg' : base_url() . '/public/images/image/nophotos.jpg';

        $data['judul']='Detail Data dari : '.$data['nama_peg'];
		$this->template->display('/pegawai/caripegawai/view', $data);
    }
	
	
	
	
		

	function cari()
	{
		if ($this->_validate_search())
		{
			$val = $this->_get_form_values_search();
			if($val['category']==1)
			{
				$cari=array('NIP'=>$val['search']);
			}
			elseif($val['category']==2)
			{
				$cari=array('nama_pegawai'=>$val['search']);
			}
            elseif($val['category']==3)
            {
                $cari=array('nip_lama'=>$val['search']);
            }
            elseif($val['category']==4)
            {
				$cari=array('tgl_lahir'=>$val['search']);
            }
			elseif($val['category']==5)
            {
				$cari=array('alamat'=>$val['search']);
            }
			$pegawai_list = $this->pegawai->get_all_by($cari);
			
			//show_error(var_dump($data));
            $jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
            if($pegawai_list) foreach($pegawai_list as $pegawai){
			
			
    			$pegawai['batas']=$jabatan[$pegawai['id_jabatan_terakhir']]['batas_maks_pensiun'];
    			$tgl = date('Y-m-d');
                $usia = $this->datediff($pegawai['tgl_lahir'],$tgl);
                $pegawai['usia'] = $usia['years']." tahun ".$usia['months']." bulan";
        		$masa_kerja = $this->datediff($pegawai['tmt_cpns'],$tgl);
                $pegawai['masa_kerja']=$masa_kerja['years']." tahun ".$masa_kerja['months']." bulan";
                //$usia = $this->datediff($pegawai['tgl_lahir'],$tgl);
                $batas=$pegawai['batas'];
                list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
                
                $tahun = $y+ $batas;
                if ($m>=2)
                {
                    $bulan = $m+1;    
                    if ($bulan==13)
                    {
                        $bulan = 1;
                        $tahun = $y+$batas+1;
                    }  
                }
                else
                {
                    $bulan = $m;
                }
                 
    			$pegawai['pensiun'] ='1/'.$bulan.'/'.$tahun;
                
    			$temp[]=$pegawai;
            }
        	$data['pegawai_list']=$temp;
            if($data['pegawai_list']!=false){
				$data['is_new']= false;}
			else{
				$data['is_new']= true;}
			$data['judul']='Hasil Pencarian Pegawai';
            $data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
            //$data['pendidikan_assoc']=$this->pendidikan->get_assoc('nama_pendidikan');
            $data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
            $data['unitkerja_assoc'] =$this->unit_kerja->get_assoc2();
			$this->template->display('/pegawai/caripegawai/list_perunit',$data);
		}
		else
		{
			$this->template->metas('title','SIMPEGA | Pencarian Pegawai');
			$data['judul']='Pencarian Pegawai';
			$data['action']='cari';
			$data['category_assoc']=array(1=>'NIP Baru',2=>'Nama Pegawai',3=>'NIP Lama', 4=>'Tanggal Lahir', 5=>'Alamat');
			$this->template->display('/pegawai/caripegawai/search',$data);
		}
	}
	
	
	
	
	

	
	function _clear_form_search()
	{
		$data['is_new'] = '';
		$data['search']	= '';
		$data['category']	= '';
		return $data;
	}	
	
	function _get_form_values_search()
	{
	   	$data['search']		= $this->input->post('search', TRUE);
	   	$data['category']	= $this->input->post('category', TRUE);
		return $data;
	}
	
	function _validate_search()
	{
		$this->form_validation->set_rules('search', 'search', 'required');
		$this->form_validation->set_rules('category', 'category', 'required');
		return $this->form_validation->run();
	}

	
}