<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pensiun extends Member_Controller
{
	function Pensiun()
	{
		parent::Member_Controller();
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('pensiun_model', 'pensiun');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Pensiun Pegawai');
		$this->browse();
	}
	
	function browse($group=0)
	{
		$data['list_pensiun'] = $this->pensiun->findAll();
		$data['judul'] 		= "Pensiun Pegawai";
		$this->template->display('pegawai/pensiun/list', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->pensiun->add($data);
			set_success('Data pensiun pegawai berhasil disimpan.');
			redirect('/pegawai/pensiun');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Pensiun Pegawai :: Add');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Pensiun Pegawai';
			$data['id_pensiun']=$this->pensiun->get_id();
			$data['pegawai_assoc']=array(0=>"-- Pilih Pegawai --") + $this->pegawai->get_assoc("nama");
			$this->template->display('/pegawai/pensiun/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_pensiun'] = $id;
			$this->pensiun->update($id, $data);
			set_success('Perubahan data riwayat pensiun berhasil disimpan');
			redirect('/pegawai/pensiun', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Pensiun Pegawai :: Edit');
			$data = $this->pensiun->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Pensiun Pegawai';
				$peg = $this->pegawai->get_assoc(array("NIP","nama"));
				$data['pegawai_assoc']=array(0=>"-- Pilih Pegawai --") + $this->pegawai->get_assoc("nama");
				$this->template->display('/pegawai/pensiun/detail', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/pensiun', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->pensiun->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Pensiun Pegawai :: Hapus');
		confirm("Yakin menghapus data pensiun <b>".$data['nama']."</b> ?");
		$res = $this->pensiun->delete($idField);
		set_success('Data pensiun berhasil dihapus');
		redirect('/pegawai/pensiun', 'location');
	}

	function _clear_form()
	{
		$data['id_pensiun']	= '';
		$data['FK_kd_pegawai']	= '';
		$data['no_SK']	= '';
		$data['tgl_pensiun']	= '';
		$data['ket_pensiun']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_pensiun']		= $this->input->post('id_pensiun', TRUE);
		$data['FK_kd_pegawai']	= $this->input->post('FK_kd_pegawai', TRUE);
		$data['no_SK']			= $this->input->post('no_SK', TRUE);
		$data['tgl_pensiun']	= $this->input->post('tgl_pensiun', TRUE);
		$data['ket_pensiun']	= $this->input->post('ket_pensiun', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('FK_kd_pegawai', 'FK_kd_pegawai', 'required');
		$this->form_validation->set_rules('no_SK', 'no_SK', 'required');
		$this->form_validation->set_rules('tgl_pensiun', 'tgl_pensiun', 'required');
		return $this->form_validation->run();
	}
}