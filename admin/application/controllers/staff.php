<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends MY_Controller
{
	function Staff()
	{
		parent::MY_Controller();
		$this->template->metas('title', 'SIMPEGA | Sistem Informasi Kepegawaian');
		$this->load->library('validation');
		$this->load->model('users_model','users');
		$this->load->model('lookup_model','lookup');
		$this->load->model('user_pegawai_model','user_pegawai');
		$this->load->model('pegawai_model', 'pegawai');
	}
	
	function index()
	{
		$this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		//show_error(var_dump($this->user));
		if ($this->user->user_group=='Staff Data')
			$this->template->display('staff/home');
		else if ($this->user->user_group=='Sub Admin')
			$this->template->display('staff/home');
		else
			$this->template->display('staff/home');
	}
	
	function login()
	{
		$this->access->restrict('logged_out');
		$this->load->model('golongan_model', 'golongan');
		$this->load->library('validation');
		$this->load->helper('form');

		$rules = array(
				'username'	=>	'trim|required|strip_tags',
				'password'	=>	'trim|required',
				'token'		=>	'check_login'
		);

		$this->validation->set_rules($rules);

		$fields = array(
				'username'	=>	'username',
				'password'	=>	'password'
		);

		$this->validation->set_fields($fields);
		
		if ($this->validation->run() == FALSE)
		{
			
			$this->template->metas('title', 'SIMPEGA | Login');
			$data['token'] = generate_token();

			$data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
			$data['pegawai']=$this->pegawai->get_list_pegawai();
			$data["ptt"]=$this->pegawai->get_list_ptt();
			$data["cpns"]=$this->pegawai->get_list_cpns();
			
			$this->session->keep_flashdata('referrer');
			$this->load->view('staff/login', $data);
		}
		else
		{
			$uri = $this->session->flashdata('referrer') ? $this->session->flashdata('referrer') : 'staff';
			redirect($uri);
		}
	}
	

	function detail_pegawai($id_pegawai=null){
		$data=null;
		if($id_pegawai==null){
			return NULL;
		}
		$data['detail'] = $this->pegawai->detail_pegawai($id_pegawai);
		$data['detail']['photo'] = file_exists(APPPATH."../public/photo/photo_".$data['detail']['kd_pegawai'].".jpg") ? $data['detail']['kd_pegawai'] : 'none';
		$this->load->view('staff/detail_pegawai', $data);
	}

	function detail_ptt($id_pegawai=null){
		$data=null;
		if($id_pegawai==null){
			return NULL;
		}
		$data['detail']=$this->pegawai->detail_ptt($id_pegawai);
		$this->load->view('staff/detail_ptt', $data);
	}

	function detail_cpns($id_pegawai=null){
		$data=null;
		if($id_pegawai==null){
			return NULL;
		}
		$data['detail']=$this->pegawai->detail_cpns($id_pegawai);
		$this->load->view('staff/detail_cpns', $data);
	}

	function add()
	{
		$this->access->restrict();
		$this->template->metas('title', 'SIMPEGA | Register User :: Tambah');  
		$this->template->load_js('public/js/linkedselect.js');
		$this->template->load_js('public/js/jquery.validate.js');
		$data_peg = $this->lookup->get_datafield('pegawai','NIP','kd_pegawai');
		$kd_pegawai = $data_peg[$this->input->post('NIP')];
		$pwd1 = $this->input->post('password', TRUE);;
		$pwd2 = $this->input->post('password2', TRUE);;
		$nama == '';
		
		
		
			if ($this->_validate('add')) {		
				$query = mysql_query("select nama_pegawai from pegawai where kd_pegawai='".$kd_pegawai."' and tgl_lahir= '".$this->input->post('tgl_lahir')."' "); 					
				if ($query) {
					$data_ada=mysql_fetch_array($query);  
					$nama= $data_ada['nama_pegawai'];	}
					if($pwd1!=$pwd2) {		
						set_error('password yang anda masukkan tidak sama');
						redirect('staff/add');
					}
					elseif  ($nama != ''){
						//set_success('query ada');
	
						$data = $this->_get_form_values();
						$cek = $this->user_pegawai->cek_id_peg($kd_pegawai);
							//show_error(var_dump($cek));
							if($cek!=TRUE) {		
								$this->user_pegawai->add(array('user_id' => $this->input->post('user_id'),'kd_pegawai'=>$kd_pegawai));
								//$this->user_pegawai->add(array('user_id' => $this->input->post('user_id'),'kd_pegawai'=>$kd_pegawai));
								$this->users->add($data);
								set_success('Data user berhasil disimpan');
								redirect('staff/login');
							}
							else {
								set_error('Penyimpanan gagal, pegawai sudah terdaftar');
								redirect('staff/add');
							}
						}
					else{
						set_error('Penyimpanan gagal, NIP dan TGL LAHIR tidak terdaftar');
						redirect('staff/add');
					}
				}
			else{
				$data = $this->_clear_form();
				if($this->input->post('nip')){
					$filterRules=array("NIP"=>$this->input->post('nip'));
					$data['pegawai']=$this->pegawai->find($filterRules);
					// var_dump($data['pegawai']);
					// exit();
					// data = false if pegawai did not found
				}

				
				$data['action']='add';
				$data['judul']='Register User Baru';
				$data['user_id']=$this->users->get_id();
				$data['is_new'] = TRUE;
				//$data['pegawai_options']=array(0=>"-- Pilih Pegawai --")+$this->pegawai->get_assoc("nama");
				$this->template->display('staff/detail_register_user', $data);
			}
			

	}

	function change_password()
	{
		$this->access->restrict();

		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_rules('oldpass', 'Password Lama', 'required|check_password[oldpass]|min_length[1]|max_length[12]');
		$this->form_validation->set_rules('password', 'Password Baru', 'required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Konfirmasi Password ', 'required');
		$data['judul'] = 'Ubah Password User';

		if ($this->form_validation->run() == FALSE)
		{
			$this->template->metas('title', 'SIMPEGA | Ubah Pasword');
			$this->template->display('sistem/ubah-password', $data);
		}
		else
		{
			$this->access->change_password($this->input->post('password',TRUE));
			set_success('Password Sukses diganti');
			redirect('/staff/logout');
		}
	}
	
	
	
	function logout()
	{
		$this->access->restrict();
		$this->user = $this->access->get_user();
		$this->load->model('users_model');
		$this->users_model->update_last_login($this->user->user_id);
		$this->access->logout();
		redirect('');
	}
	function _clear_form()
	{
		$data['user_id']	= '';
		$data['username']	= '';
		$data['password']	= '';
		//$data['password2']	= '';
		$data['FK_group_id']	= '';
		$data['tgl_lahir']	= '';
		$data['NIP']	= '';
		$data['email']	= '';
		$data['name']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['user_id']	= $this->input->post('user_id', TRUE);
		$data['username']	= $this->input->post('username', TRUE);
		$data['password']	= $this->input->post('password', TRUE);;
		//$data['password2']	= $this->input->post('password2', TRUE);;
		$data['FK_group_id']= $this->input->post('FK_group_id', TRUE);;
		$data['email']		= $this->input->post('email', TRUE);;
		$data['name']		= $this->input->post('name', TRUE);;

		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('password2', 'password2', 'required');
		$this->form_validation->set_rules('FK_group_id', 'FK_group_id', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		return $this->form_validation->run();
	}

	function get_datatable() 
	{
		$requestData= $_REQUEST;
		$columns = array( 
			0   =>  'id', 
			1   =>  'NIP',
			2   =>  'gelar_depan, nama_pegawai, gelar_belakang',
			3   =>  'golongan',
			4   =>  'nama_unit',
			5   =>  ''
		);
		if($requestData['status_pegawai'] == 'pns') {
			$data_db = $this->pegawai->get_list_pegawai();
		} 
		else if($requestData['status_pegawai'] == 'ptt'){
			$data_db = $this->pegawai->get_list_ptt();
		}
		else if($requestData['status_pegawai'] == 'cpns'){
			$data_db = $this->pegawai->get_list_cpns();
		}
		else if($requestData['status_pegawai'] == 'dosen'){
			$data_db = $this->pegawai->count_dosen();
		}
		else if($requestData['status_pegawai'] == 'tendik'){
			$data_db = $this->pegawai->count_tendik();
		}
		$totalData = count($data_db);

		$sql = "SELECT
		   pegawai.kd_pegawai, pegawai.NIP, pegawai.gelar_depan, pegawai.nama_pegawai, pegawai.gelar_belakang, pegawai.npwp, pegawai.email, agama.agama, jenis_pegawai.jenis_pegawai, unit_kerja.nama_unit";
		   if($requestData['status_pegawai'] != 'ptt') {
		   	$sql.=", golongan_pangkat.golongan" ;
		   }
	   $sql.=" FROM pegawai"
			." LEFT JOIN agama ON agama.kd_agama = pegawai.kd_agama"
		  ." LEFT JOIN jenis_pegawai ON jenis_pegawai.id_jns_pegawai = pegawai.id_jns_pegawai"
		  ." LEFT JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit";
		  if($requestData['status_pegawai'] != 'ptt') {
		  	$sql.=" LEFT JOIN golongan_pangkat ON golongan_pangkat.id_golpangkat = pegawai.id_golpangkat_terakhir";
		  }

		$sql.=" WHERE NIP NOT LIKE '' AND NIP NOT LIKE '-'";
		if($requestData['status_pegawai'] == 'pns') {
			$sql.=" AND status_pegawai < 3 ";
		} 
		else if($requestData['status_pegawai'] == 'ptt') { 
			$sql.=" AND status_pegawai > 2 ";
		}
		else if($requestData['status_pegawai'] == 'cpns') { 
			$sql.=" AND (status_pegawai = 1 and pegawai.id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen')) ";
		}
		else if($requestData['status_pegawai'] == 'dosen') { 
			$sql.=" AND (status_pegawai >= 1 and pegawai.id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen')) ";
		}
		else if($requestData['status_pegawai'] == 'tendik') { 
			$sql.=" AND ((status_pegawai IN (3,4)) OR (status_pegawai IN (1,2) and pegawai.id_jns_pegawai != (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'))) ";
		}

		if(!empty($requestData['search']['value'])) {
			$sql.=" AND (pegawai.NIP LIKE '%".$requestData['search']['value']."%'";
			$sql.=" OR pegawai.nama_pegawai LIKE '%".$requestData['search']['value']."%'";
			if($requestData['status_pegawai'] != 'ptt') {
				$sql.=" OR golongan_pangkat.golongan LIKE '%".$requestData['search']['value']."%'";
			}
			$sql.=" OR unit_kerja.nama_unit LIKE '%".$requestData['search']['value']."%')";
		}
		$query = $this->pegawai->rawQuery($sql);
		// var_dump($sql); die();
		$totalFiltered = $query->num_rows();

		$sql .= " ORDER BY pegawai.id_jns_pegawai ASC, pegawai.id_golpangkat_terakhir DESC, ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']." "; 
		$query = $this->pegawai->rawQuery($sql);

	

		$data = array(); $i=$requestData['start'];
		foreach ($query->result_array() as $row) {
			$tableData   = array(); 
	    $tableData[] = '<div class="text-center">'.($i + 1).'</div>';
	    $tableData[] = $row["NIP"];
	    $tableData[] = $row["gelar_depan"].' '.$row["nama_pegawai"].' '.$row["gelar_belakang"];
	    $tableData[] = '<div class="text-center">'.(isset($row["golongan"]) ? $row["golongan"] : '').'';
	    $tableData[] = $row["nama_unit"];
	    $tableData[] = '<div class="text-center"><a href="javascript:void(0);" class="btn btn-primary btn-xs" onclick="showDetail('.$row["kd_pegawai"].');" title="Detail pegawai"> <i class="glyphicon glyphicon-user"></i> </a></div>';
	    $data[] = $tableData; $i++;
	  }
	  $totalData = count($data);
	  $json_data = array(
	  	"draw"            => intval( $requestData['draw'] ),
	  	"recordsTotal"    => intval( $totalData ),
	  	"recordsFiltered" => intval( $totalFiltered ),
	  	"data"            => $data
	  );
	  echo json_encode($json_data);
	}
}