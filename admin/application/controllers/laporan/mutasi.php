<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class mutasi extends Member_Controller
{

	function mutasi()
		{
		parent::Member_Controller();
		$this->template->load_js('public/js/common.js');
		$this->load->model('Lap_mutasi_model');
		$this->load->model('lookup_model','lookup');
		$this->load->model('unit_kerja_model', 'unit_kerja');
	}
	// --------------------------------------------------------------------
	function index()
		{
		$this->template->metas('title', 'Mutasi Administration');
		$this->browse();
	}
	function browse($group=0) {
		if ($this->input->post('group'))
			$group= $this->input->post('group');
			$paging_uri=5;
		
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0;
		//$limit_per_page = ;
		//$search_param = array('mutasi.jns_mutasi'=>$group);
	  	if ($this->input->post('nama_search')!='-'){
			$nama_seacrh = $this->input->post('nama_search');
			}
			else{
			$nama_seacrh = '-';
			}
			
		$search_param = array();
		if ($group==0)
			$search_param[] = "(mutasi.no_SK like '%%')";					
		else
			$search_param[] = "(mutasi.jns_mutasi like '".$group."')";		
			
		if ($this->input->post('nama_search')!='-'){
			$search_param[] = "kd_pegawai in(select kd_pegawai from pegawai where 
			((pegawai.nama_pegawai like '%".$this->input->post('nama_search')."%') or 
			(pegawai.NIP like '".$this->input->post('nama_search')."')) or 
			(mutasi.no_SK like '".$this->input->post('nama_search')."'))";	
		}
		else{
			$search_param[] = "(mutasi.no_SK like '%%')";	
		}
		
		//var yg akan dikirim untuk xls dan pdf
		$the_results['group'] = $group;
		$the_results['nama_seacrh'] = $nama_seacrh;
		
		$search_param = implode(" AND ", $search_param);
		$mutasi_list = $this->Lap_mutasi_model->findByFilter($search_param, $limit_per_page,$start);
		
		$the_results['judul'] = "Laporan Mutasi Pegawai";
		$the_results['group'] =$group;

		$this->load->model('Lap_mutasi_model');
		$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$namapeg = $this->lookup->get_datafield('pegawai','kd_pegawai','nama_pegawai');
		$jnsmutasi = $this->lookup->get_datafield('jenis_mutasi','jns_mutasi','jenis_mutasi');
		$SK_mutasi = $this->lookup->get_datafield('mutasi','id_mutasi','no_SK');
		$tglmutasi = $this->lookup->get_datafield('mutasi','id_mutasi','tgl_mutasi');
		
		$this->load->model('Lap_mutasi_model');
		if($mutasi_list) foreach($mutasi_list as $mutasi){
			$mutasi['NIP']=$NIP_peg[$mutasi['kd_pegawai']];
			$mutasi['nama_pegawai']=$namapeg[$mutasi['kd_pegawai']];
			$mutasi['jns_mutasi']=$jnsmutasi[$mutasi['jns_mutasi']];
			$mutasi['no_SK']=$SK_mutasi[$mutasi['id_mutasi']];
			$mutasi['tgl_mutasi']=$tglmutasi[$mutasi['id_mutasi']];
			$temp[]=$mutasi;
		}
		$the_results['nama_seacrh'] = $nama_seacrh;
		$the_results['mutasi_list']=$temp;
		
		$the_results['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
		$the_results['judul'] 	= "Laporan Data Mutasi Pegawai";	
		
		$config['base_url']     = site_url('laporan/mutasi/browse/'.$group);  
		$config['total_rows']   = $this->Lap_kerja_model->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] = 'berikutnya &raquo;';
		$config['prev_link'] = '&laquo; sebelumnya ';
		$this->template->display('laporan/mutasi/list_mutasi', $the_results);  
	}   

	function view($idField){
		//$idField = $this->uri->segment(4);
		$this->load->model('Lap_mutasi_model');
		$data = $this->Lap_mutasi_model->retrieve_by_pkey($idField);
		$data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
		$data['nama_pegawai'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama_pegawai');
		$data['nama_mutasi'] = $this->lookup->get_cari_field('jenis_mutasi','jns_mutasi',$data['jns_mutasi'],'nama_mutasi');
		$data['no_SK'] = $this->lookup->get_cari_field('mutasi','id_mutasi',$data['id_mutasi'],'no_SK');
		$data['tgl_SK'] = $this->lookup->get_cari_field('mutasi','id_mutasi',$data['id_mutasi'],'tgl_SK');
		$data['tmt_SK'] = $this->lookup->get_cari_field('mutasi','id_mutasi',$data['id_mutasi'],'tmt_SK');
		$data['tgl_mutasi'] = $this->lookup->get_cari_field('mutasi','id_mutasi',$data['id_mutasi'],'tgl_mutasi');
		$data['jabatan_asal'] = $this->lookup->get_cari_field('jabatan','id_jabatan',$data['id_jabatan_asal'],'nama_jabatan');
		$data['jabatan_baru'] = $this->lookup->get_cari_field('jabatan','id_jabatan',$data['id_jabatan_baru'],'nama_jabatan');
		$data['unit_asal'] = $this->lookup->get_cari_field('unit_kerja','kode_unit',$data['kode_unit_kerja_asal'],'nama_unit');
		$data['unit_baru'] = $this->lookup->get_cari_field('unit_kerja','kode_unit',$data['kode_unit_kerja_baru'],'nama_unit');
		$data['tgl_mutasi'] = $this->lookup->get_cari_field('mutasi','id_mutasi',$data['id_mutasi'],'ket_mutasi');	
		$data['judul']='Detail Mutasi Pegawai';
		$this->template->display('/laporan/mutasi/view', $data);
	}
	
	function filter($filter_by = '')
		{
		// Whitelist allowed filters
		$filters = array('banned', 'ip_banned');
		
		if ( ! in_array($filter_by, $filters))
		{
			die('illegal filter');
		}
		
		//$this->set_reference();

		$data['kerja'] = $this->kerja->get_all_users($filter_by);
		$this->template->display('laporan/mutasi/list_mutasi', $data);
	}

	function printtopdf($group,$nama_seacrh)
		{
		$this->load->model('Lap_mutasi_model');
		$this->load->model('unit_kerja_model', 'unit_kerja');
		$properties=array();
		$properties['title']='Laporan Mutasi Pegawai';
		$properties['subject']='mutasi';
		$properties['keywords']='mutasi';
		$properties['papersize']="A4";
		$properties['paperlayout']='L';	
		$properties['filename']='Laporan Mutasi';
		
		/*//if ($group==0)
			$mutasi_list = $this->Lap_mutasi_model->findAll( $limit_per_page,$start);
		//else
		//    $mutasi_list = $this->Lap_mutasi_model->findByFilter(array('jns_mutasi'=>$group), $limit_per_page,$start);

		$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$namapeg = $this->lookup->get_datafield('pegawai','kd_pegawai','nama_pegawai');
		$jnsmutasi = $this->lookup->get_datafield('jenis_mutasi','jns_mutasi','jenis_mutasi');
		$SK_mutasi = $this->lookup->get_datafield('mutasi','id_mutasi','no_SK');
		$tglmutasi = $this->lookup->get_datafield('mutasi','id_mutasi','tgl_mutasi');
		
		$this->load->model('Lap_mutasi_model');
		if($mutasi_list) foreach($mutasi_list as $mutasi){
			$mutasi['NIP']=$NIP_peg[$mutasi['kd_pegawai']];
			$mutasi['nama_pegawai']=$namapeg[$mutasi['kd_pegawai']];
			$mutasi['jns_mutasi']=$jnsmutasi[$mutasi['jns_mutasi']];
			$mutasi['no_SK']=$SK_mutasi[$mutasi['id_mutasi']];
			$mutasi['tgl_mutasi']=$tglmutasi[$mutasi['id_mutasi']];
			$temp[]=$mutasi;
		}
		$the_results['mutasi_list']=$temp;*/
		
		$search_param = array();
		if ($group==0)
			$search_param[] = "(mutasi.no_SK like '%%')";					
		else
			$search_param[] = "(mutasi.jns_mutasi like '".$group."')";		
			
		if ($nama_seacrh!='-'){
			$search_param[] = "kd_pegawai in(select kd_pegawai from pegawai 
			where ((pegawai.nama_pegawai like '%".$nama_seacrh."%') or 
			(pegawai.NIP like '".$nama_seacrh."')) or (mutasi.no_SK like '".$nama_seacrh."'))";	
		}
		else{
			$search_param[] = "(mutasi.no_SK like '%%')";	
		}
		
		$search_param = implode(" AND ", $search_param);
		
		$mutasi_list = $this->Lap_mutasi_model->findByFilter($search_param, $limit_per_page,$start);
		
		$the_results['judul'] = "Laporan Mutasi Pegawai";
		$the_results['group'] =$group;

		$this->load->model('Lap_mutasi_model');
		$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$namapeg = $this->lookup->get_datafield('pegawai','kd_pegawai','nama_pegawai');
		$jnsmutasi = $this->lookup->get_datafield('jenis_mutasi','jns_mutasi','jenis_mutasi');
		$SK_mutasi = $this->lookup->get_datafield('mutasi','id_mutasi','no_SK');
		$tglmutasi = $this->lookup->get_datafield('mutasi','id_mutasi','tgl_mutasi');
		
		$this->load->model('Lap_mutasi_model');
		if($mutasi_list) foreach($mutasi_list as $mutasi){
			$mutasi['NIP']=$NIP_peg[$mutasi['kd_pegawai']];
			$mutasi['nama_pegawai']=$namapeg[$mutasi['kd_pegawai']];
			$mutasi['jns_mutasi']=$jnsmutasi[$mutasi['jns_mutasi']];
			$mutasi['no_SK']=$SK_mutasi[$mutasi['id_mutasi']];
			$mutasi['tgl_mutasi']=$tglmutasi[$mutasi['id_mutasi']];
			$temp[]=$mutasi;
		}
		$the_results['nama_seacrh'] = $nama_seacrh;
		$the_results['mutasi_list']=$temp;
		$the_results['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
		
		//echo $mutasi['jns_mutasi'];
		$the_results['judul'] = "Laporan Mutasi Pegawai";
		$html=$this->load->view('laporan/Mutasi/list_mutasipdf', $the_results,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}	
    
    function printtoxls($group,$nama_seacrh)
		{
		$this->load->model('Lap_mutasi_model');
		$this->load->model('unit_kerja_model', 'unit_kerja');
		
		/*//if ($group==0)
			$mutasi_list = $this->Lap_mutasi_model->findAll( $limit_per_page,$start);
		//else
		//    $mutasi_list = $this->Lap_mutasi_model->findByFilter(array('jns_mutasi'=>$group), $limit_per_page,$start);

		$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$namapeg = $this->lookup->get_datafield('pegawai','kd_pegawai','nama_pegawai');
		$jnsmutasi = $this->lookup->get_datafield('jenis_mutasi','jns_mutasi','jenis_mutasi');
		$SK_mutasi = $this->lookup->get_datafield('mutasi','id_mutasi','no_SK');
		$tglmutasi = $this->lookup->get_datafield('mutasi','id_mutasi','tgl_mutasi');
		
		$this->load->model('Lap_mutasi_model');
		if($mutasi_list) foreach($mutasi_list as $mutasi){
			$mutasi['NIP']=$NIP_peg[$mutasi['kd_pegawai']];
			$mutasi['nama_pegawai']=$namapeg[$mutasi['kd_pegawai']];
			$mutasi['jns_mutasi']=$jnsmutasi[$mutasi['jns_mutasi']];
			$mutasi['no_SK']=$SK_mutasi[$mutasi['id_mutasi']];
			$mutasi['tgl_mutasi']=$tglmutasi[$mutasi['id_mutasi']];
			$temp[]=$mutasi;
		}
		$the_results['mutasi_list']=$temp;*/
		
		$search_param = array();
		
		if ($group==0)
			$search_param[] = "(mutasi.no_SK like '%%')";					
		else
			$search_param[] = "(mutasi.jns_mutasi like '".$group."')";		
			
		if ($nama_seacrh!='-'){
			$search_param[] = "kd_pegawai in(select kd_pegawai from pegawai 
			where ((pegawai.nama_pegawai like '%".$nama_seacrh."%') or 
			(pegawai.NIP like '".$nama_seacrh."')) or (mutasi.no_SK like '".$nama_seacrh."'))";	
		}
		else{
			$search_param[] = "(mutasi.no_SK like '%%')";	
		}
		
		$search_param = implode(" AND ", $search_param);
		
		$mutasi_list = $this->Lap_mutasi_model->findByFilter($search_param, $limit_per_page,$start);
		
		$the_results['judul'] = "Laporan Mutasi Pegawai";
		$the_results['group'] =$group;

		$this->load->model('Lap_mutasi_model');
		$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$namapeg = $this->lookup->get_datafield('pegawai','kd_pegawai','nama_pegawai');
		$jnsmutasi = $this->lookup->get_datafield('jenis_mutasi','jns_mutasi','jenis_mutasi');
		$SK_mutasi = $this->lookup->get_datafield('mutasi','id_mutasi','no_SK');
		$tglmutasi = $this->lookup->get_datafield('mutasi','id_mutasi','tgl_mutasi');
		
		$this->load->model('Lap_mutasi_model');
		if($mutasi_list) foreach($mutasi_list as $mutasi){
			$mutasi['NIP']=$NIP_peg[$mutasi['kd_pegawai']];
			$mutasi['nama_pegawai']=$namapeg[$mutasi['kd_pegawai']];
			$mutasi['jns_mutasi']=$jnsmutasi[$mutasi['jns_mutasi']];
			$mutasi['no_SK']=$SK_mutasi[$mutasi['id_mutasi']];
			$mutasi['tgl_mutasi']=$tglmutasi[$mutasi['id_mutasi']];
			$temp[]=$mutasi;
		}
		$the_results['nama_seacrh'] = $nama_seacrh;
		$the_results['mutasi_list']=$temp;
		$the_results['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
		
		//echo $mutasi['jns_mutasi'];
		$the_results['judul'] = "Laporan Mutasi Pegawai";
        
        $the_results['html']=$this->load->view('laporan/Mutasi/list_mutasipdf', $the_results,true);
		$this->load->view('xls.php',$the_results);
	;
	}


	
}
