<?php 
function right($value, $count){
    return substr($value, ($count*-1));
}

function left($string, $count){
    return substr($string, 0, $count);
}
if (!defined('BASEPATH')) exit('No direct script access allowed');

class rekap_sdm_summary extends My_Controller
{

	function rekap_sdm_summary()
	{
		parent::My_Controller();
		 $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('agama_model', 'agama');
        $this->load->model('lookup_model', 'lookup');
        $this->load->model('jenis_pegawai_model', 'jenis_pegawai');
        $this->load->model('pendidikan_model', 'pendidikan');
        $this->load->model('kelurahan_model', 'kelurahan');
        $this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('golongan_model', 'golongan');
        $this->load->model('jabatan_model', 'jabatan');
        $this->load->model('lap_pegawai_model', 'lap_pegawai');

        /* $this->load->model('gambar_model', 'gambar');  */
        $this->load->model('user_pegawai_model', 'user_pegawai');
	}

	// --------------------------------------------------------------------

	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Laporan Daftar Pegawai');
		$this->browse();
	}
	
	function browse()
	{
		
		if ($this->input->post('group'))
		{
            $group = $this->input->post('group');
		}
        else if ($this->uri->segment(4))
            $group = $this->uri->segment(4);
		else 
			$group= '0';
			
			
      	$unit_kerja = 'Semua';
       
		$paging_uri = 6;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
        
		$limit_per_page = 1000;
        $ordby ='id_golpangkat_terakhir desc, eselon , id_jabatan_terakhir asc';
		
        $data['group'] = $group;
		$data['start'] = $start;
		$data['unit_kerja'] = $unit_kerja;
		$data['jenis_pegawai_assoc'] = $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		//$data['adm_akd_assoc'] = $this->lookup->adm_akd_assoc();
		$data['adm_akd_assoc'] = $this->jenis_pegawai->get_jenis_pegawai_assoc();
        $data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$data['pangkat_assoc'] = $this->golongan->get_golpangkat_assoc('pangkat');
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
		$data['jabatans_assoc'] = $this->jabatan->get_jabatan_assocs('nama_jabatan_s');
		
		$config['base_url'] = site_url('laporan/rekap_sdm_summary/browse/' . $group . '/' . $unit_kerja);
        $config['total_rows'] = $this->lap_pegawai->record_count;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();
        
		if (isset($this->user->user_group)){
		$this->template->display('/laporan/rekap_sdm_summary/list_rekap_sdm_summary', $data);  
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}

        
	}   
			
	
	function printtopdf($group,$unit_kerja)
	{
		$properties=array();
		$properties['title']='';
		$properties['subject']='rekap sdm';
		$properties['keywords']='rekap sdm';
		$properties['filename']='rekapsdm'; //.$this->sims->tahun
		$properties['paperlayout']='P';
		$properties['papersize']="A4";
		        

        $data['group'] = $group;
		
		$data['judul'] 				= "REKAP SDM PERJENIS PEGAWAI";
		$html=$this->load->view('laporan/rekap_sdm_summary/list_rekap_sdm_summarypdf', $data,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}	


function printtoxls($group,$unit_kerja)
	{

		$data['group'] = $group;
		$data['judul'] = "rekap sdm";
		$data['html']=$this->load->view('laporan/rekap_sdm_summary/list_rekap_sdm_summarypdf', $data,true);
		$this->load->view('xls.php',$data);
      
	}
	
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}	
	
}
