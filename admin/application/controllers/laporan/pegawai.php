<?php 
function right($value, $count){
    return substr($value, ($count*-1));
}

function left($string, $count){
    return substr($string, 0, $count);
}
if (!defined('BASEPATH')) exit('No direct script access allowed');

class pegawai extends My_Controller
{

	function pegawai()
	{
		parent::My_Controller();
		$this->load->model('lap_unit_model','lap_unit');
		$this->load->model('lookup_model','lookup');
		$this->load->model('unit_kerja_model','unit_kerja');
        $this->load->model('lap_pegawai_model','lap_pegawai');
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model('users_model','users');
	}

	// --------------------------------------------------------------------

	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Laporan Daftar Pegawai');
		$this->browse();
	}
	
	function browse($group='0')
	{
		
		if ($this->input->post('group'))
			$group= $this->input->post('group');
		else if($this->uri->segment(4))
            $group=$this->uri->segment(4);

		$this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		if($this->user->user_group == 'Admin Khusus'){
		$query = mysql_query("select p.kode_unit from user_pegawai u,pegawai 
			p where p.kd_pegawai = u.kd_pegawai and u.user_id='".$this->user->user_id."'");					
			if ($query) {
					$datauser=mysql_fetch_array($query);  
					$unit_user = $datauser['kode_unit']; 
			}	
			$unit_kerja	=$unit_user;
		}
		else
		{        
		//		$unit_kerja = '0';
			if ($this->input->post('unit_kerja'))
			{    
				$unit_kerja = $this->input->post('unit_kerja');
				
			}
			else if ($this->uri->segment(5)) {
				$unit_kerja = $this->uri->segment(5);
			}
		}
         
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		
		$limit_per_page = 20;
	  	$ordby = 'nama_pegawai asc';
		$query = mysql_query("select replace(kode_unit,'0','') as tingkat from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data=mysql_fetch_array($query);  
			$tingkat = strlen($data['tingkat']);}		
			$pembanding=left($unit_kerja,$tingkat);
			
		$search_param = array();
        if ($unit_kerja)
            //$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
			$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
        if ($group=='1')
            $search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
		elseif ($group=='2')
			$search_param[] = "(id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3 )";
			$search_param = implode(" AND ", $search_param);

		$the_results['kerja_list'] = $this->lap_pegawai->findByFilter($search_param,$ordby,$limit_per_page,$start);
		$the_results['group'] = $group;
        $the_results['judul'] 	= "Laporan Daftar Pegawai";
		$the_results['start'] = $start;
		
		$the_results['adm_akd_assoc'] = $this->lookup->adm_akd_assoc();
		$the_results['unit_kerja_assoc'] = array('0'=>"-- Semua Unit Kerja --") + $this->unit_kerja->get_assoc2();
		$the_results['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
		$the_results['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$the_results['unitkerja_assoc'] =  $this->unit_kerja->get_assoc2();
		$the_results['pendidikan_assoc'] = $this->pendidikan->get_pendidikan_assoc();
		$config['base_url']     = site_url('laporan/pegawai/browse/'.$group);  
		$config['total_rows']   = $this->lap_pegawai->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
        $this->pagination->initialize($config);
        $the_results['page_links'] 	= $this->pagination->create_links();
		$this->template->display('laporan/pegawai/list_pegawai', $the_results);  
	}   
			
	function view($idField)
	{
		//$idField = $this->uri->segment(4);
		
		$data = $this->lap_unit->retrieve_by_pkey($idField);
		$data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
		$data['nama_peg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama');
		$data['tempat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tempat_lahir');
		$data['tanggal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
		$data['alamat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat');
		$data['jns'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'JK');
		if ($data['jns']==1)
            $data['jenis'] = 'Laki-Laki';
        else
		    $data['jenis'] = 'Perempuan';
		$data['agama_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_agama');
		$data['agama'] = $this->lookup->get_cari_field('agama','kd_agama',$data['agama_id'],'agama');
		$data['stskawin'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_kawin');	
        if ($data['stskawin']==1)
            $data['stkawin'] = 'Kawin';
        else
		    $data['stkawin'] = 'Belum Kawin';
		$data['judul']='Detail Data Pegawai';
		$this->template->display('/laporan/pegawai/view', $data);
	}

	function printtopdf($group)
	{
		//$properties=array();
		$properties['title']='Buku Pegawai';
		$properties['subject']='kerja';
		$properties['keywords']='pegawai';
		$properties['papersize']="A4";
		$properties['paperlayout']='L';	
		$properties['filename']='Laporan pegawai';
		
				      
		$the_results['group'] = $group;
		$limit_per_page = 20;
	  	$ordby = 'nama_pegawai asc';
		$search_param = array();
        if ($unit_kerja)
            $search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
        if ($group=='1')
            $search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
		elseif ($group=='2')
			$search_param[] = "(id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3 )";
			$search_param = implode(" AND ", $search_param);

		$the_results['kerja_list'] = $this->lap_pegawai->findByFilter($search_param,$ordby,NULL,$start);
		$the_results['group'] = $group;
        $the_results['judul'] 	= "Laporan Daftar Pegawai";
		$the_results['start'] = $start;
		
		$the_results['adm_akd_assoc'] = $this->lookup->adm_akd_assoc();
		$the_results['unit_kerja_assoc'] = array('0'=>"-- Semua Unit Kerja --") + $this->unit_kerja->get_assoc2();
		$the_results['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
		$the_results['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$the_results['unitkerja_assoc'] =  $this->unit_kerja->get_assoc2();
		$the_results['pendidikan_assoc'] = $this->pendidikan->get_pendidikan_assoc();
		
		$the_results['judul'] = "Daftar pegawai";
		$html=$this->load->view('laporan/pegawai/list_pegawaipdf', $the_results,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}	


function printtoxls($group)
	{
				
		$the_results['group']=$group;
		
		$limit_per_page = 20;
	  	$ordby = 'nama_pegawai asc';
		$search_param = array();
        if ($unit_kerja)
            $search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
        if ($group=='1')
            $search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
		elseif ($group=='2')
			$search_param[] = "(id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3 )";
			$search_param = implode(" AND ", $search_param);

		$the_results['kerja_list'] = $this->lap_pegawai->findByFilter($search_param,$ordby,NULL,$start);
		$the_results['group'] = $group;
        $the_results['judul'] 	= "Laporan Daftar Pegawai";
		$the_results['start'] = $start;
		
		$the_results['adm_akd_assoc'] = $this->lookup->adm_akd_assoc();
		$the_results['unit_kerja_assoc'] = array('0'=>"-- Semua Unit Kerja --") + $this->unit_kerja->get_assoc2();
		$the_results['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
		$the_results['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$the_results['unitkerja_assoc'] =  $this->unit_kerja->get_assoc2();
		$the_results['pendidikan_assoc'] = $this->pendidikan->get_pendidikan_assoc();
		
        $the_results['judul'] 	= "Laporan Daftar Pegawai";
		$the_results['start'] = $start;
		$the_results['adm_akd_assoc'] = $this->lookup->adm_akd_assoc();
		$the_results['judul'] = "Daftar pegawai";
		$the_results['html']=$this->load->view('laporan/pegawai/list_pegawaipdf', $the_results,true);
		$this->load->view('xls.php',$the_results);
        
       
	}
	function filter($filter_by = '')
	{
		// Whitelist allowed filters
		$filters = array('banned', 'ip_banned');
		
		if ( ! in_array($filter_by, $filters))
		{
			die('illegal filter');
		}
		
		//$this->set_reference();

		$data['kerja'] = $this->kerja->get_all_users($filter_by);
		$this->template->display('laporan/pegawai/list_pegawai', $data);
	}
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}	
	
}
