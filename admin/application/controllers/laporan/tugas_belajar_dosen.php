<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class tugas_belajar_dosen extends My_Controller
{

	function tugas_belajar_dosen()
	{
		parent::My_Controller();
		$this->load->model('lap_golpangkat_model','lap_golpangkat');
		$this->load->model('lookup_model','lookup');
		$this->load->model('unit_kerja_model','unit_kerja');
        $this->load->model('lap_pegawai_model','lap_pegawai');
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lap_tugas_belajar_model','lap_tugas_belajar');
		$this->load->model('users_model','users');
	}

	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Daftar Dosen yang sedang Studi S2/S3');
		$this->browse();
	}
	
	function browse() {
	
		 
		if ($this->input->post('group'))
			$group= $this->input->post('group');
		$tahun=date('Y');
		
		$tahun = '2010';
		if ($this->input->post('tahun'))
			$tahun= $this->input->post('tahun');
		
		$this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		if($this->user->user_group == 'Admin Khusus'){
		$query = mysql_query("select p.kode_unit from user_pegawai u,pegawai 
			p where p.kd_pegawai = u.kd_pegawai and u.user_id='".$this->user->user_id."'");					
			if ($query) {
					$datauser=mysql_fetch_array($query);  
					$unit_user = $datauser['kode_unit']; 
			}	
			$unit_kerja	=$unit_user;
		}
		else
		{

			if ($this->input->post('unit_kerja'))
				$unit_kerja= $this->input->post('unit_kerja');
			else if($this->uri->segment(4))
            	$jns_pegawai=$this->uri->segment(4);
		}	
		
		
		
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0;
			
		$limit_per_page = 1000;
		$search_param = array();
		
		if ($unit_kerja=='All') 
		{
			if($group==0)
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				$search_param[] = "(month(tmt_tb) <= '$group')";
				
			}
		}
		elseif ($unit_kerja)
		{
			
            $search_param[] = "(a.kode_unit = '$unit_kerja' OR a.kode_unit_induk = '$unit_kerja')";
			if($group==0)
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				$search_param[] = "(month(tmt_tb) <= '$group')";
				
			}
		}
		
        $search_param[]= "aktif like 1";
		$search_param[] = "(id_jns_pegawai like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'))";
		$search_param = implode(" AND ", $search_param);
		$ordby = "tmt_tb asc,a.id_golpangkat_terakhir desc, batas_akhir_tb asc";
		$tb_list = $this->lap_tugas_belajar->find(NULL,$search_param,$ordby,NULL,$start);
		
		$the_results['tb_list']=$tb_list;
		$the_results['tahun'] =$tahun;
		$the_results['group'] =$group; 
		$the_results['unit_kerja'] = $unit_kerja;
		$the_results['judul'] = "Daftar Dosen yang sedang Studi S2/S3";
        $the_results['group_assoc'] = array('0' => '--sepanjang tahun--') + $this->lookup->month_assoc();
        $the_results['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y'), date('Y')+50);
        $the_results['unit_kerja_assoc'] = array('All' => '-- Semua Fakultas --') + $this->unit_kerja->get_fakultas('nama_unit');
     
		$this->template->display('laporan/tugas_belajar/list_tugas_belajar', $the_results);  
	}   

	function printpdf($group,$tahun,$unit_kerja)
	{
		//$properties=array();
		$bln = $this->lookup->longonthname(date("m"));
		//$properties['title']='Daftar Urut Kepangkatan - Periode '.$bln." ". date("Y");
		//$properties['title']='DAFTAR URUT KEPANGKATAN (DUK) PEGAWAI NEGERI SIPIL '.echo "<br>".' TENAGA ADMINISTRASI UNIVERSITAS NEGERI MALANG'. echo"<br>".' KEADAAN : 31 DESEMBER '. $tahun;
		$properties['subject']='kerja';
		$properties['keywords']='pegawai';
		$properties['papersize']="A4";
		$properties['paperlayout']='L';	
		//$properties['filename']='laporan_kepangkatan_'.date("m_Y");
		
		
		
		$the_results['tahun']= $tahun;
		$tgl = ($tahun+1) ."-01-01";
		
		$the_results['start'] = $start;
		$limit_per_page = 1000;
		$search_param = array();
		
		if ($unit_kerja=='All') 
		{
			if($group==0)
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				$search_param[] = "(month(tmt_tb) <= '$group')";
				
			}
		}
		elseif ($unit_kerja)
		{
			
            $search_param[] = "(a.kode_unit = '$unit_kerja' OR a.kode_unit_induk = '$unit_kerja')";
			if($group==0)
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				$search_param[] = "(month(tmt_tb) <= '$group')";
				
			}
		}
		
        $search_param[]= "aktif like 1";
		$search_param = implode(" AND ", $search_param);
		$ordby = "tmt_tb asc";
		$tb_list = $this->lap_tugas_belajar->find(NULL,$search_param,$ordby,NULL,$start);
		
		$the_results['tb_list']=$tb_list;
		$the_results['tahun'] =$tahun;
		$the_results['group'] =$group; 
		$the_results['unit_kerja'] = $unit_kerja;
		$the_results['judul'] = "Daftar Dosen yang sedang Studi S2/S3";
        $the_results['group_assoc'] = array('0' => '--sepanjang tahun--') + $this->lookup->month_assoc();
        $the_results['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y'), date('Y')+50);
        $the_results['unit_kerja_assoc'] = array('All' => '-- Semua Fakultas --') + $this->unit_kerja->get_fakultas('nama_unit');
     
		$this->template->display('laporan/tugas_belajar/list_tugas_belajarpdf', $the_results);
		$this->printtopdf->htmltopdf($properties,$html);
	}
    function printtoxls($group,$tahun,$unit_kerja)
	{
		
		$the_results['tahun'] = $tahun;
		
		$tgl = ($tahun+1) ."-01-01";
		$limit_per_page = 1000;
		$search_param = array();
		
		if ($unit_kerja=='') 
		{
			if($group=='')
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				
			}
			elseif ($group!='')
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				$search_param[] = "(month(tmt_tb) <= '$group')";
				
			}
		}
		elseif ($unit_kerja)
		{
			
            $search_param[] = "(a.kode_unit = '$unit_kerja' OR a.kode_unit_induk = '$unit_kerja')";
			if($group=='')
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				
			}
			elseif ($group!='')
			{
				$search_param[] = "(year(tmt_tb) <= '$tahun')";
				$search_param[] = "(month(tmt_tb) <= '$group')";
				
			}
		}
		
        $search_param[]= "aktif like 1";
		$search_param = implode(" AND ", $search_param);
		$ordby = "tmt_tb asc";
		$tb_list = $this->lap_tugas_belajar->find(NULL,$search_param,$ordby,NULL,$start);
		
		$the_results['tb_list']=$tb_list;
		$the_results['tahun'] =$tahun;
		$the_results['group'] =$group; 
		$the_results['unit_kerja'] = $unit_kerja;
		$the_results['judul'] = "Daftar Dosen yang sedang Studi S2/S3";
        $the_results['group_assoc'] = array('0' => '--sepanjang tahun--') + $this->lookup->month_assoc();
        $the_results['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y'), date('Y')+50);
        $the_results['unit_kerja_assoc'] = array('All' => '-- Semua Fakultas --') + $this->unit_kerja->get_fakultas('nama_unit');
     
		
        $the_results['html']=$this->load->view('laporan/tugas_belajar/list_tugas_belajarpdf', $the_results,true);
		$this->load->view('xls.php',$the_results);
        		
	
	}
    function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}			
}
