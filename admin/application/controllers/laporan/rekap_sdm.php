<?php 
function right($value, $count)
{ 
    return substr($value, ($count*-1));
}

function left($string, $count)
{
    return substr($string, 0, $count);
}
if (!defined('BASEPATH')) exit('No direct script access allowed');

class rekap_sdm extends My_Controller
{

	function rekap_sdm()
	{
		parent::My_Controller();
		$this->load->model('pegawai_model', 'pegawai');
        $this->load->model('agama_model', 'agama');
        $this->load->model('lookup_model', 'lookup');
        $this->load->model('jenis_pegawai_model', 'jenis_pegawai');
        $this->load->model('pendidikan_model', 'pendidikan');
        $this->load->model('kelurahan_model', 'kelurahan');
        $this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('golongan_model', 'golongan');
        $this->load->model('jabatan_model', 'jabatan');
        $this->load->model('lap_pegawai_model', 'lap_pegawai');
		$this->load->model('users_model','users');
        $this->load->model('user_pegawai_model', 'user_pegawai');
	}

	// --------------------------------------------------------------------

	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Laporan Daftar Pegawai');
		$this->browse();
	}
	
	function browse()
	{
		
		if ($this->input->post('group'))
		{
            $group = $this->input->post('group');
		}
        else if ($this->uri->segment(4))
            $group = $this->uri->segment(4);
		else 
			$group= '0';
			
      	$this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		if($this->user->user_group == 'Admin Khusus'){
		$query = mysql_query("select p.kode_unit from user_pegawai u,pegawai 
			p where p.kd_pegawai = u.kd_pegawai and u.user_id='".$this->user->user_id."'");					
			if ($query) {
					$datauser=mysql_fetch_array($query);  
					$unit_user = $datauser['kode_unit']; 
			}	
			$unit_kerja	=$unit_user;
		}
		else
		{
				//$unit_kerja = 'Semua';
			if ($this->input->post('unit_kerja'))
			{    
				$unit_kerja = $this->input->post('unit_kerja');
				
			}
			else
			{
				$unit_kerja = 'Semua';
			}
		} 
		
		$paging_uri = 6;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
        
		$limit_per_page = 1000;
        $ordby ='id_golpangkat_terakhir desc, eselon , id_jabatan_terakhir asc';

        
		$query = mysql_query("select replace(kode_unit,'0','') as tingkat from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data=mysql_fetch_array($query);  
			$tingkat = strlen($data['tingkat']);
			}		
			$pembanding=left($unit_kerja,$tingkat);						
			$search_param = array();
			if ($group=='1')
			{
				//$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen')  and status_pegawai < 3)";
				$search_param[] = "(id_jns_pegawai like '".$group."' and status_pegawai < 3)";
				if ($unit_kerja)
				{   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
					if ($tingkat == 1)
					{
					//jika unit tingkat 1 maka hanya ditampilkan pejabat
					$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
					$search_param[]	= "(eselon not like 'non-eselon')";		
					}
					else
					{
					//jika tingkat > 1 maka hanya ditampilkan staff
					$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
					$search_param[]	= "(eselon like 'non-eselon')";
					}
				}
			}
			else
			{
				//$search_param[] = "(id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3 )";
				$search_param[] = "(id_jns_pegawai like '".$group."' and status_pegawai < 3)";
				if ($unit_kerja)
				{   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
					if ($tingkat == 1)
					{
					//jika unit tingkat 1 maka hanya ditampilkan pejabat
					$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
					$search_param[]	= "(eselon not like 'non-eselon')";		
					}
					else
					{
					//jika tingkat > 1 maka hanya ditampilkan staff
					$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
					$search_param[]	= "(eselon like 'non-eselon')";
					}
				}
			}
			
		$search_param = implode(" AND ", $search_param);
        $ordby ='id_golpangkat_terakhir desc, eselon , id_jabatan_terakhir asc';
		
		$data['kerja_list'] = $this->lap_pegawai->findByFilter($search_param, $ordby, $limit_per_page, $start);
		
        $data['group'] = $group;
		$data['start'] = $start;
		$data['unit_kerja'] = $unit_kerja;
		
		//menampilkan semua unit
		$data['dataunitkerja_assoc'] = $this->unit_kerja->get_assoc2();
		$data['unitkerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_assoc('nama_unit');
		$data['jenis_pegawai_assoc'] = array(0 => "-- Semua Jenis Pegawai --") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		//$data['adm_akd_assoc'] = $this->lookup->adm_akd_assoc();
		$data['adm_akd_assoc'] = $this->jenis_pegawai->get_jenis_pegawai_assoc();
        $data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$data['pangkat_assoc'] = $this->golongan->get_golpangkat_assoc('pangkat');
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
		$data['jabatans_assoc'] = $this->jabatan->get_jabatan_assocs('nama_jabatan_s');
		
		$config['base_url'] = site_url('laporan/rekap_sdm/browse/' . $group . '/' . $unit_kerja);
        $config['total_rows'] = $this->lap_pegawai->record_count;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();
        
        $this->template->display('/laporan/rekap_sdm/list_rekap_sdm', $data);  
	}   
			
	
	function printtopdf($group,$unit_kerja)
	{
		$properties=array();
		$properties['title']='';
		$properties['subject']='statistik pendidikan';
		$properties['keywords']='statistik pendidikan';
		$properties['filename']='statistik_pendidikan'; //.$this->sims->tahun
		$properties['paperlayout']='L';
		$properties['papersize']="A4";
		
		$ordby ='id_golpangkat_terakhir desc, eselon , id_jabatan_terakhir asc';
        
		$query = mysql_query("select replace(kode_unit,'0','') as tingkat from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data=mysql_fetch_array($query);  
			$tingkat = strlen($data['tingkat']);
			}		
			$pembanding=left($unit_kerja,$tingkat);						
			$search_param = array();
			if ($group=='1')
			{
				/*$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen')  and status_pegawai < 3)";*/
				$search_param[] = "(id_jns_pegawai like '".$group."' and status_pegawai < 3)";
				if ($unit_kerja)
				{   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
					if ($tingkat == 1)
					{
					//jika unit tingkat 1 maka hanya ditampilkan pejabat
					$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
					$search_param[]	= "(eselon not like 'non-eselon')";		
					}
					else
					{
					//jika tingkat > 1 maka hanya ditampilkan staff
					$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
					$search_param[]	= "(eselon like 'non-eselon')";
					}
				}
			}
			elseif ($group=='2')
			{
				//$search_param[] = "(id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3 )";
				$search_param[] = "(id_jns_pegawai like '".$group."' and status_pegawai < 3)";
				if ($unit_kerja)
				{   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
					if ($tingkat == 1)
					{
					//jika unit tingkat 1 maka hanya ditampilkan pejabat
					$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
					$search_param[]	= "(eselon not like 'non-eselon')";		
					}
					else
					{
					//jika tingkat > 1 maka hanya ditampilkan staff
					$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
					$search_param[]	= "(eselon like 'non-eselon')";
					}
				}
			}
		$search_param = implode(" AND ", $search_param);
        $ordby ='id_golpangkat_terakhir desc, eselon , id_jabatan_terakhir asc';
		$data['kerja_list'] = $this->lap_pegawai->findByFilter($search_param, $ordby, $limit_per_page, $start);
		
		$data['start']=0;
        $data['group'] = $group;
		$data['unit_kerja'] = $unit_kerja;
		
		$data['dataunitkerja_assoc'] = $this->unit_kerja->get_assoc2();
		$data['unitkerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_assoc('nama_unit');
		$data['jenis_pegawai_assoc'] = array(0 => "-- Semua Jenis Pegawai --") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		//$data['adm_akd_assoc'] = $this->lookup->adm_akd_assoc();
		$data['adm_akd_assoc'] = $this->jenis_pegawai->get_jenis_pegawai_assoc();
        $data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$data['pangkat_assoc'] = $this->golongan->get_golpangkat_assoc('pangkat');
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
		$data['jabatans_assoc'] = $this->jabatan->get_jabatan_assocs('nama_jabatan_s');
		
		$data['judul'] 				= "BUKU PEGAWAI";
		$html=$this->load->view('laporan/rekap_sdm/list_rekap_sdmpdf', $data,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}	


function printtoxls($group,$unit_kerja)
	{
				
		$ordby ='id_golpangkat_terakhir desc, eselon , id_jabatan_terakhir asc';
        
		$query = mysql_query("select replace(kode_unit,'0','') as tingkat from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data=mysql_fetch_array($query);  
			$tingkat = strlen($data['tingkat']);
			}		
			$pembanding=left($unit_kerja,$tingkat);						
			$search_param = array();
			if ($group=='1')
			{
				//$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
				$search_param[] = "(id_jns_pegawai like '".$group."' and status_pegawai < 3)";
				if ($unit_kerja)
				{   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
					if ($tingkat == 1)
					{
					//jika unit tingkat 1 maka hanya ditampilkan pejabat
					$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
					$search_param[]	= "(eselon not like 'non-eselon')";		
					}
					else
					{
					//jika tingkat > 1 maka hanya ditampilkan staff
					$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
					$search_param[]	= "(eselon like 'non-eselon')";
					}
				}
			}
			elseif ($group=='2')
			{
				$search_param[] = "(id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3 )";
				if ($unit_kerja)
				{   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
					if ($tingkat == 1)
					{
					//jika unit tingkat 1 maka hanya ditampilkan pejabat
					$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
					$search_param[]	= "(eselon not like 'non-eselon')";		
					}
					else
					{
					//jika tingkat > 1 maka hanya ditampilkan staff
					$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
					$search_param[]	= "(eselon like 'non-eselon')";
					}
				}
			}
		$search_param = implode(" AND ", $search_param);
        
		$ordby ='id_golpangkat_terakhir desc, eselon , id_jabatan_terakhir asc';
		
		$data['kerja_list'] = $this->lap_pegawai->findByFilter($search_param, $ordby, $limit_per_page, $start);
		
        $data['group'] = $group;
		$data['unit_kerja'] = $unit_kerja;
		$data['dataunitkerja_assoc'] = $this->unit_kerja->get_assoc2();
		$data['unitkerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_assoc('nama_unit');
		$data['jenis_pegawai_assoc'] = array(0 => "-- Semua Jenis Pegawai --") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
	
		$data['adm_akd_assoc'] = $this->jenis_pegawai->get_jenis_pegawai_assoc();
        $data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$data['pangkat_assoc'] = $this->golongan->get_golpangkat_assoc('pangkat');
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
		$data['jabatans_assoc'] = $this->jabatan->get_jabatan_assocs('nama_jabatan_s');
		
		$data['start']=0;
		
		$data['judul'] = "Daftar pegawai";
		$data['html']=$this->load->view('laporan/rekap_sdm/list_rekap_sdmpdf', $data,true);
		$this->load->view('xls.php',$data);
        
       
	}
	
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}	
	
}
