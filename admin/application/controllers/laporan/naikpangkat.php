<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class naikpangkat extends Member_Controller
{

	function naikpangkat()
	{
		parent::Member_Controller();
		$this->load->model('lap_golpangkat_model','lap_golpangkat');
		$this->load->model('lookup_model','lookup');
		$this->load->model('unit_kerja_model','unit_kerja');
        $this->load->model('lap_pegawai_model','lap_pegawai');
		$this->load->model('golongan_model', 'golongan');
		$this->load->model("jenis_pegawai_model");
	}

	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Rencana Kenaikan Pangkat');
		$this->browse();
	}
	
	function browse($group='') {
	//function browse(){
		if ($this->input->post('group'))
			$group= $this->input->post('group');
		else if($this->uri->segment(4))
			$group=$this->uri->segment(4);
		$tahun=date('Y');
		if ($this->input->post('tahun'))
			$tahun= $this->input->post('tahun');

		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0;
		//$limit_per_page = 20;
		list($datestart, $dateend) = $this->get_date_range($group, $tahun); 
		//$ordby = 'id_golpangkat_terakhir desc, tmt_cpns asc, id_pendidikan_terakhir';
		$ordby = 'id_golpangkat desc, pendidikan.id_pendidikan';
		
		$fields = "pegawai.*, DATE_ADD(tmt_golpangkat_terakhir, INTERVAL 4 YEAR ) AS next_year";
		
		$where_val = "DATE_ADD(tmt_golpangkat_terakhir, INTERVAL 4 YEAR) BETWEEN '$datestart' AND '$dateend' 
					  AND pegawai.id_golpangkat_terakhir < pendidikan.id_golpangkat_akhir 
					  AND pegawai.id_jns_pegawai = 1 and status_pegawai < 3 AND pegawai.eselon='non-eselon' ";
		
		$kerja_list = $this->lap_golpangkat->find($fields, $where_val, $ordby, $limit_per_page, $start);
		
		$the_results['start'] = $start;
		$the_results['judul'] = "Rencana Kenaikan Pangkat Reguler";
		$the_results['group'] = $group;
        $the_results['tahun']=$tahun;
		$jabatan = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$golongan = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
		$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		//$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');
		$pendidikan = $this->lookup->get_datafield('riwayat_pendidikan','id_riwayat_pendidikan','keterangan');
		
		if($kerja_list) foreach($kerja_list as $pegawai){
			$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']];
			$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']];
			$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
			$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
			$query = mysql_query("SELECT  CASE WHEN `pendidikan`.`id_pendidikan` < 5 THEN 
								`pendidikan`.`nama_pendidikan`
								ELSE
								CONCAT(`pendidikan`.`nama_pendidikan`,' ',`riwayat_pendidikan`.`prodi`,' ',`riwayat_pendidikan`.`universitas`) END
								as nama_pendidikan
								FROM `pegawai` Inner Join `riwayat_pendidikan` ON `pegawai`.`kd_pegawai` = `riwayat_pendidikan`.`kd_pegawai` 
								Inner Join `pendidikan` ON `riwayat_pendidikan`.`id_pendidikan` = `pendidikan`.`id_pendidikan` AND 
								`pegawai`.`id_pendidikan_terakhir` = `pendidikan`.`id_pendidikan` WHERE  `riwayat_pendidikan`.aktif = '1'
								and `pegawai`.`kd_pegawai`='".$pegawai['kd_pegawai']."'
								GROUP BY `pegawai`.`kd_pegawai`,`pegawai`.`NIP`,`pegawai`.`nama_pegawai`
								");					
			if ($query) 
				{
				$datadidik=mysql_fetch_array($query);  
				$nama_didik = $datadidik['nama_pendidikan'];
				}
			else {$nama_didik='';}
			//$pegawai['nama_pendidikan']=$pendidikan[$pegawai['id_pendidikan_terakhir']];
			$pegawai['nama_pendidikan']= $nama_didik;
			$pegawai['nama_golongan_baru']= $golongan[$pegawai['id_golpangkat_terakhir']+1];
			$pegawai['nama_pangkat_baru']= $pangkat[$pegawai['id_golpangkat_terakhir']+1];
			$pegawai['id_golpangkat_baru']= $pegawai['id_golpangkat_terakhir']+1;
            
            if ($group=='')
            {
                $pegawai['tmt_golpangkat_berikutnya']='-';
				 $pegawai['tmt_golpangkat_baru']='0000-00-00';
            }
            else
            {
                $pegawai['tmt_golpangkat_berikutnya']='1/'.$group.'/'.$tahun;    
				$pegawai['tmt_golpangkat_baru']=$tahun.'-'.$group.'-01';    
            }
            
			$temp[]=$pegawai;
		}
		
		
		
		
		$the_results['kerja_list']=$temp;
		$the_results['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y'), date('Y')+40);
		$the_results['get_jenis_pegawai_all'] =array('All'=>"Administrasi")+ array('nAll'=>"Fungsional");
		/*$config['base_url']     = site_url('laporan/naikpangkat/browse/' . $group . '/');
		$config['total_rows']   = $this->lap_golpangkat->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		
		$this->pagination->initialize($config);
		$the_results['page_links'] 	= $this->pagination->create_links();*/
		$this->template->display('laporan/naikpangkat/list_naikpangkat', $the_results);  
	}
	
	function get_date_range($group, $tahun)
	{
		//echo "Processing \$group = $group  :: \$tahun = $tahun";
		if(!$group)
		{
			$tahun2 = $tahun - 1;
			$bulan1 = '10';
			$bulan2 = '10';
			$date1 = $tahun2 . '-' . $bulan1 . '-' . '02';
			$date2 = $tahun . '-' . $bulan2 . '-' . '01';
			
			return array($date1, $date2);
		}
		else if($group == '04')
		{
			$tahun2 = $tahun - 1;
			$bulan1 = '10';
			$bulan2 = '04';
			$date1 = $tahun2 . '-' . $bulan1 . '-' . '02';
			$date2 = $tahun . '-' . $bulan2 . '-' . '01';
			
			return array($date1, $date2);
		}
		else if($group == '10')
		{
			$tahun2 = $tahun;
			$bulan1 = '04';
			$bulan2 = '10';
			$date1 = $tahun2 . '-' . $bulan1 . '-' . '02';
			$date2 = $tahun . '-' . $bulan2 . '-' . '01';
			
			return array($date1, $date2);
		}
	}

	function printpdf($group,$tahun)
	{
		$properties=array();
		$bln = $this->lookup->longonthname(date("m"));
		$properties['title']='Laporan Rencana Kenaikan Pangkat';
		$properties['subject']='kerja';
		$properties['keywords']='pegawai';
		$properties['papersize']="A4";
		$properties['paperlayout']='L';	
		$properties['filename']='laporan_kepangkatan_'.date("m_Y");
		
		list($datestart, $dateend) = $this->get_date_range($group, $tahun); 
		//$ordby = 'id_golpangkat_terakhir desc, tmt_cpns asc, id_pendidikan_terakhir';
		$ordby = 'id_golpangkat desc, id_pendidikan';
		
		$fields = "pegawai.*, DATE_ADD( tmt_golpangkat_terakhir, INTERVAL 4 YEAR ) AS next_year";
		
			$where_val = "DATE_ADD(tmt_golpangkat_terakhir, INTERVAL 4 YEAR) BETWEEN '$datestart' AND '$dateend' 
AND pegawai.id_golpangkat_terakhir < pendidikan.id_golpangkat_akhir AND pegawai.id_jns_pegawai = 1  and status_pegawai < 3 AND pegawai.eselon='non-eselon'";
		
		$kerja_list = $this->lap_golpangkat->find($fields, $where_val, $ordby, $limit_per_page, $start);
		
		$the_results['start'] = $start;
		$the_results['judul'] = "Rencana Kenaikan Pangkat Reguler";
		$the_results['group'] = $group;
        $the_results['tahun']=$tahun;
		$jabatan = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$golongan = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
		$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		//$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');
		$pendidikan = $this->lookup->get_datafield('pendidikan','id_pendidikan','nama_pendidikan');
		
		if($kerja_list) foreach($kerja_list as $pegawai){
			$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']];
			$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']];
			$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
			$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
			$pegawai['nama_pendidikan']=$pendidikan[$pegawai['id_pendidikan_terakhir']];
			$pegawai['nama_golongan_baru']=$golongan[$pegawai['id_golpangkat_terakhir']+1];
            
            if ($group=='')
            {
                $pegawai['tmt_golpangkat_berikutnya']='-';
            }
            else
            {
                $pegawai['tmt_golpangkat_berikutnya']='1/'.$group.'/'.$tahun;    
            }
            
			$temp[]=$pegawai;
		}
		$the_results['kerja_list']=$temp;	
    
        $the_results['judul'] = "Daftar Kepangkatan Pegawai";		
		$html=$this->load->view('laporan/naikpangkat/list_naikpangkatpdf', $the_results,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}
	
	
    function printtoxls($group,$tahun)
	{
	
		list($datestart, $dateend) = $this->get_date_range($group, $tahun); 
		//$ordby = 'id_golpangkat_terakhir desc, tmt_cpns asc, id_pendidikan_terakhir';
		$ordby = 'id_golpangkat desc, id_pendidikan';
		
		$fields = "pegawai.*, DATE_ADD( tmt_golpangkat_terakhir, INTERVAL 4 YEAR ) AS next_year";
		
			$where_val = "DATE_ADD(tmt_golpangkat_terakhir, INTERVAL 4 YEAR) BETWEEN '$datestart' AND '$dateend' 
AND pegawai.id_golpangkat_terakhir < pendidikan.id_golpangkat_akhir AND pegawai.id_jns_pegawai = 1  and status_pegawai < 3 AND pegawai.eselon='non-eselon'";
		
		$kerja_list = $this->lap_golpangkat->find($fields, $where_val, $ordby, $limit_per_page, $start);
		
		$the_results['start'] = $start;
		$the_results['judul'] = "Rencana Kenaikan Pangkat Reguler";
		$the_results['group'] = $group;
        $the_results['tahun']=$tahun;
		$jabatan = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$golongan = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
		$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		//$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');
		$pendidikan = $this->lookup->get_datafield('pendidikan','id_pendidikan','nama_pendidikan');
		
		if($kerja_list) foreach($kerja_list as $pegawai){
			$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']];
			$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']];
			$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
			$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
			$pegawai['nama_pendidikan']=$pendidikan[$pegawai['id_pendidikan_terakhir']];
			$pegawai['nama_golongan_baru']=$golongan[$pegawai['id_golpangkat_terakhir']+1];
            
            if ($group=='')
            {
                $pegawai['tmt_golpangkat_berikutnya']='-';
            }
            else
            {
                $pegawai['tmt_golpangkat_berikutnya']='1/'.$group.'/'.$tahun;    
            }
            
			$temp[]=$pegawai;
		}
		$the_results['kerja_list']=$temp;	
    
        $the_results['judul'] = "Daftar Kepangkatan Pegawai";	
        $the_results['html']=$this->load->view('laporan/naikpangkat/list_naikpangkatpdf', $the_results,true);
		$this->load->view('xls.php',$the_results);	
	
	}
	
	function printmodel_d($kd_pegawai,$group,$tahun)
	{
		//$properties=array();
		$properties['papersize']="A4";
		$properties['paperlayout']='P';
		$data = $this->lap_pegawai->retrieve_by_pkey($kd_pegawai);
		$data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
		$data['nama_peg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama_pegawai');
		$data['tempat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tempat_lahir');
		$data['tanggal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');	
		$data['karpeg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'karpeg');
		
		$data['pendidikan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_pendidikan_terakhir');
        $data['pendidikan']=$this->lookup->get_cari_field('riwayat_pendidikan','id_pendidikan',$data['pendidikan_id'],'keterangan');
		$data['th_lulus']=$this->lookup->get_cari_field('riwayat_pendidikan','id_pendidikan',$data['pendidikan_id'],'th_lulus');
		
		$data['pangkat_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
        $data['pangkat']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'pangkat');
		$data['golongan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'golongan');
		$data['tmt_pangkat']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_golpangkat_terakhir');
		
		$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
		$data['mk_tambahan_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_th');
		$data['mk_tambahan_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_bl');
		$data['mk_selisih_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_th');
		$data['mk_selisih_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_bl');
		$data['tingkat'] = $this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'tingkat');
		
		$data['group'] = $group;
        $data['tahun'] = $tahun; 
		
		$tgl = $tahun.'-'.$group.'-'.'01';
		$masa_kerja = $this->datediff($data['tmt_cpns'],$tgl);
		$data['masa_kerja_th'] = $masa_kerja['years'] + $data['mk_tambahan_th'] + $data['mk_selisih_th'];
		$data['masa_kerja_bl'] = $masa_kerja['months'] + $data['mk_tambahan_bl'] + $data['mk_selisih_bl'];
		$data['masa_kerja_asli'] = $data['masa_kerja_th']." tahun ".$data['masa_kerja_bl']." bulan";
		$data['masa_kerja_asli_th'] = $data['masa_kerja_th'];
        if (($data['tingkat'] == 2) and (($data['tingkat'] + 1)==3))
			{
				$data['masa_kerja_th']=$data['masa_kerja_th']-5;
			}
			elseif (($data['tingkat'] == 1) and (($data['tingkat'] + 1)==2))
			{
				$data['masa_kerja_th']=$data['masa_kerja_th']-6;
			}
			else
			{
				$data['masa_kerja_th']=$data['masa_kerja_th'];	
			}
		$data['masa_kerja']=$data['masa_kerja_th']." tahun ".$data['masa_kerja_bl']." bulan";		
		
		$data['gaji_pokok']=$this->lookup->get_cari_field2('acuan_gaji_pokok','id_golpangkat','masa_kerja',$data['pangkat_id'],$data['masa_kerja_th'],'gaji_pokok');
		
		$data['eselon']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'eselon');
		if (($data['eselon'] =='non-eselon') and ($data['tingkat']=='1'))
		{
			$data['non_jabatan'] = 'Pembantu Pelaksana';
		}
		elseif (($data['eselon'] =='non-eselon') and ($data['tingkat']=='2'))
		{
			$data['non_jabatan'] = 'Pelaksana';
		}
		elseif (($data['eselon'] =='non-eselon') and ($data['tingkat']=='3'))
		{
			$data['non_jabatan'] = 'Pembantu Pimpinan';
		} 
		
		$data['unit_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit');
        $data['unit_kerja']=$this->lookup->get_cari_field('unit_kerja','kode_unit',$data['unit_id'],'nama_unit');
		
		$data['tmt_golpangkat_baru']=$tahun.'-'.$group.'-01';
		$data['pangkat_baru']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id']+1,'pangkat');
		$data['golongan_baru']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id']+1,'golongan');
		$data['gaji_pokok_baru']=$this->lookup->get_cari_field2('acuan_gaji_pokok','id_golpangkat','masa_kerja',$data['pangkat_id']+1,$data['masa_kerja_th'],'gaji_pokok');
		
		if ($data['eselon']=='non-eselon')
		{
			$data['nama_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_id'],'IV','nama_pegawai');
			$data['gelar_depan_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_id'],'IV','gelar_depan');
			$data['gelar_belakang_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_id'],'IV','gelar_belakang');
			$data['nip_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_id'],'IV','NIP');
			$data['pangkat_id_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_id'],'IV','id_golpangkat_terakhir');
			$data['pangkat_atasan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id_atasan'],'pangkat');
			$data['golongan_atasan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id_atasan'],'golongan');
			$data['tmt_pangkat_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_id'],'IV','tmt_golpangkat_terakhir');
			$data['jabatan_id_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_id'],'IV','id_jabatan_terakhir');
        	$data['jabatan_atasan']=$this->lookup->get_cari_field('jabatan','id_jabatan',$data['jabatan_id_atasan'],'nama_jabatan');
			
		}
		elseif ($data['eselon']=='IV')
		{
			$data['unit_induk_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit_induk');
			$data['nama_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'III','nama_pegawai');
			$data['gelar_depan_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'III','gelar_depan');
			$data['gelar_belakang_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'III','gelar_belakang');
			$data['nip_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'III','NIP');
			$data['pangkat_id_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'III','id_golpangkat_terakhir');
			$data['pangkat_atasan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id_atasan'],'pangkat');
			$data['golongan_atasan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id_atasan'],'golongan');
			$data['tmt_pangkat_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'III','tmt_golpangkat_terakhir');
			$data['jabatan_id_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'III','id_jabatan_terakhir');
        	$data['jabatan_atasan']=$this->lookup->get_cari_field('jabatan','id_jabatan',$data['jabatan_id_atasan'],'nama_jabatan');
			
		}
		elseif ($data['eselon']=='III')
		{
			$data['unit_induk_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit_induk');
			$data['nama_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'II','nama_pegawai');
			$data['gelar_depan_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'II','gelar_depan');
			$data['gelar_belakang_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'II','gelar_belakang');
			$data['nip_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'II','NIP');
			$data['pangkat_id_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'II','id_golpangkat_terakhir');
			$data['pangkat_atasan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id_atasan'],'pangkat');
			$data['golongan_atasan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id_atasan'],'golongan');
			$data['tmt_pangkat_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'II','tmt_golpangkat_terakhir');
			$data['jabatan_id_atasan']=$this->lookup->get_cari_field2('pegawai','kode_unit','eselon',$data['unit_induk_id'],'II','id_jabatan_terakhir');
        	$data['jabatan_atasan']=$this->lookup->get_cari_field('jabatan','id_jabatan',$data['jabatan_id_atasan'],'nama_jabatan');
			
		}
		
		
		$html=$this->load->view('laporan/naikpangkat/cetak_modelD', $data,true);
		
		$this->printtopdf->htmltopdf($properties,$html);
	}
    function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}			
}

