<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


class penghargaan extends Member_Controller {

	function penghargaan()
	{
		parent::Member_Controller();
		$this->template->load_css('public/css/table.css');
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('lookup_model','lookup');
		$this->load->model('penghargaanmodel');
	}

	function index()
	{
		$this->browse();
	}

	function browse($group=0) {
		if ($this->input->post('group'))
			$group= $this->input->post('group');
		$paging_uri=5;
		$this->load->model('lookup_model','lookup');
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
		$start=0;
		$limit_per_page = 20;
		
		$this->load->model('penghargaanmodel');
		$penghargaan_list = $this->penghargaanmodel->findAll( $limit_per_page,$start);
		$the_results['judul'] = "Laporan Penghargaan Pegawai";
		$the_results['group'] =$group;

		$this->load->model('penghargaanmodel');
		$NIP = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$nm_pegawai= $this->lookup->get_datafield('pegawai','kd_pegawai','nama');
		$tgl_penghargaan = $this->lookup->get_datafield('riwayat_penghargaan','id_riwayat_penghargaan','tgl_penghargaan');
		$nm_penghargaan = $this->lookup->get_datafield('master_jenis_penghargaan','kd_jenis_penghargaan','nama_penghargaan');
		
		$this->load->model('penghargaanmodel');
		if($penghargaan_list) foreach($penghargaan_list as $penghargaan){
			$penghargaan['NIPpeg']=$NIP[$penghargaan['kd_pegawai']];
			$penghargaan['nama_peg']=$nm_pegawai[$penghargaan['kd_pegawai']];
			$penghargaan['tglpenghargaan']=$tgl_penghargaan[$penghargaan['id_riwayat_penghargaan']];
			$penghargaan['nama_penghargaan']=$nm_penghargaan[$penghargaan['FK_jenis_penghargaan']];
			$temp[]=$penghargaan;
		}
		$the_results['penghargaan_list']=$temp;
		
		$config['base_url']     = site_url('laporan/penghargaan/browse/'.$ordby.'/');
		$config['total_rows']   = $this->penghargaanmodel->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] = 'berikutnya &raquo;';
		$config['prev_link'] = '&laquo; sebelumnya ';
		$the_results['ordbyid'] = 'laporan/penghargaan/browse/id_riwayat_penghargaan';	
		$this->template->display('laporan/penghargaan/list_penghargaan', $the_results);
	} 	

	function view($idField){
		
		$this->load->model('penghargaanmodel');
		$data = $this->penghargaanmodel->retrieve_by_pkey($idField);
		$data['NIP'] = $this->lookup->get_NIPpegawai($data['kd_pegawai']);
		$data['nama_peg'] = $this->lookup->get_namapegawai($data['kd_pegawai']);
		$data['nama_penghargaan']	= $this->lookup->get_namapenghargaan($data['FK_jenis_penghargaan']);
		$data['judul']='Detail penghargaan';
		$this->template->display('/laporan/penghargaan/view', $data);
	}

	function printtopdf()
	{
		$this->load->model('penghargaanmodel');
		$properties=array();
		$properties['title']='Laporan Penghargaan Pegawai';
		$properties['subject']='pegawai';
		$properties['keywords']='riwayat_penghargaan';
		$properties['papersize']="A4";
		$properties['paperlayout']='P';	
		$properties['filename']='Laporan penghargaan';
		$penghargaan_list = $this->penghargaanmodel->findAll( $limit_per_page,$start);

		$this->load->model('penghargaanmodel');
		$NIP = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$nm_pegawai= $this->lookup->get_datafield('pegawai','kd_pegawai','nama');
		$tgl_penghargaan = $this->lookup->get_datafield('riwayat_penghargaan','id_riwayat_penghargaan','tgl_penghargaan');
		$nm_penghargaan = $this->lookup->get_datafield('master_jenis_penghargaan','kd_jenis_penghargaan','nama_penghargaan');
		$ket_penghargaan= $this->lookup->get_datafield('master_jenis_penghargaan','kd_jenis_penghargaan','ket_jenis_penghargaan');
		
		$this->load->model('penghargaanmodel');
		if($penghargaan_list) foreach($penghargaan_list as $penghargaan){
			$penghargaan['NIPpeg']=$NIP[$penghargaan['kd_pegawai']];
			$penghargaan['nama_peg']=$nm_pegawai[$penghargaan['kd_pegawai']];
			$penghargaan['tglpenghargaan']=$tgl_penghargaan[$penghargaan['id_riwayat_penghargaan']];
			$penghargaan['nama_penghargaan']=$nm_penghargaan[$penghargaan['FK_jenis_penghargaan']];
			$penghargaan['ket']=$ket_penghargaan[$penghargaan['FK_jenis_penghargaan']];
			$temp[$penghargaan['id_riwayat_penghargaan']]=$penghargaan;
		}
		$the_results['penghargaan_list']=$temp;
		
		$the_results['judul'] = "Daftar penghargaan (pegawai)";
		$html=$this->load->view('laporan/penghargaan/list_penghargaanpdf', $the_results,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}
}
?>
