<?php
function right($value, $count){
    return substr($value, ($count*-1));
}

function left($string, $count){
    return substr($string, 0, $count);
}
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class rekapitulasi_pendidikan extends My_Controller {

    var $mutasi_id;
    var $groupuser;

    function rekapitulasi_pendidikan() {
        parent::My_Controller();
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('agama_model', 'agama');
        $this->load->model('lookup_model', 'lookup');
        $this->load->model('jenis_pegawai_model', 'jenis_pegawai');
        $this->load->model('pendidikan_model', 'pendidikan');
        $this->load->model('kelurahan_model', 'kelurahan');
        $this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('golongan_model', 'golongan');
        $this->load->model('jabatan_model', 'jabatan');
        $this->load->model('lap_pegawai_model', 'lap_pegawai');

        /* $this->load->model('gambar_model', 'gambar');  */
		$this->load->model('users_model','users');
        $this->load->model('user_pegawai_model', 'user_pegawai');
    }
    function index() {
        $this->template->metas('title', 'SIMPEGA | Laporan Masa Kerja Unit Pegawai');
        $this->browse();
    }
    function browse() {     
		$this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		if($this->user->user_group == 'Admin Khusus'){
		$query = mysql_query("select p.kode_unit from user_pegawai u,pegawai 
			p where p.kd_pegawai = u.kd_pegawai and u.user_id='".$this->user->user_id."'");					
			if ($query) {
					$datauser=mysql_fetch_array($query);  
					$unit_user = $datauser['kode_unit']; 
			}	
			$unit_kerja	=$unit_user;
		}
		else
		{
			//$unit_kerja = '0';
			if ($this->input->post('unit_kerja'))
			{    
				$unit_kerja = $this->input->post('unit_kerja');
				
			}
			else if ($this->uri->segment(5)) {
				$unit_kerja = $this->uri->segment(5);
			}
		}  
		
		
        if ($this->input->post('id_jns_pegawai')){
			$id_jns_pegawai = $this->input->post('id_jns_pegawai');	}
		else {
			$id_jns_pegawai = 'All';}
			
		if ($this->input->post('nama_search')!=''){
			$nama_seacrh = $this->input->post('nama_search');
			}
			else{
			$nama_seacrh = '';
			}
        $paging_uri = 6;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
        
		$limit_per_page = 1000;
       	$data['id_jns_pegawai'] = $id_jns_pegawai;
		$data['nama_seacrh'] = $nama_seacrh;
		
		$data['judul'] = 'REKAPITULASI PENDIDIKAN PNS ';
		
		if ($unit_kerja=='Semua') {
				 $data['judul_unit'] = 'DI SEMUA UNIT UNIVERSITAS NEGERI MALANG';
			}
			else {
				 $data['judul_unit'] = 'UNIVERSITAS NEGERI MALANG';
			}
        $data['start'] = $start;
		$data['unit_kerja'] = $unit_kerja;
		
		//menampilkan semua unit
		$data['unitkerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_assoc('nama_unit');
		$display_unit =$this->unit_kerja->get_unit('$unit_kerja'); 
		$config['base_url'] = site_url('laporan/rekapitulasi_pendidikan/browse/' . $group . '/' . $unit_kerja);
        $config['total_rows'] = $this->lap_pegawai->record_count;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();
        $data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
		$data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$data['get_jenis_pegawai_all'] =array('All'=>"--Semua--")+ array('nAllUnit'=>"--Tenaga Administratif--") + $this->jenis_pegawai->get_jenis_pegawai_assoc();
		$data['pendidikan_assoc'] =$this->pendidikan->findAll();
		$data['ket_pendidikan_assoc'] = $this->pendidikan->get_assoc('keterangan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
        $this->template->display('/laporan/rekapitulasi_pendidikan/list_rekapitulasi_pendidikan', $data);
    }
	
	
	
	function printtopdf($unit_kerja,$id_jns_pegawai){
		$properties=array();
		$properties['title']='';
		$properties['subject']='rekapitulasi pendidikan';
		$properties['keywords']='rekapitulasi pendidikan';
		$properties['filename']='rekapitulasi_pendidikan'; //.$this->sims->tahun
		$properties['paperlayout']='L';
		$properties['papersize']="A4";
		
		$paging_uri = 6;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
        
		$limit_per_page = 1000;

		$data['id_jns_pegawai'] = $id_jns_pegawai;
		$data['nama_seacrh'] = $nama_seacrh;
		
		$data['judul'] = 'REKAPITULASI PENDIDIKAN PNS ';
		
		if ($unit_kerja=='Semua') {
				 $data['judul_unit'] = 'DI SEMUA UNIT UNIVERSITAS NEGERI MALANG';
			}
			else {
				 $data['judul_unit'] = 'UNIVERSITAS NEGERI MALANG';
			}
        $data['start'] = $start;
		$data['unit_kerja'] = $unit_kerja;
		
        //$data['unitkerja_assoc'] = array('Semua' => "-- Semua Unit Kerja --") + $this->unit_kerja->get_not_tingkat2();
		//menampilkan semua unit
		$data['unitkerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_assoc('nama_unit');
		$display_unit =$this->unit_kerja->get_unit('$unit_kerja');
        
		$config['base_url'] = site_url('laporan/rekapitulasi_pendidikan/browse/' . $group . '/' . $unit_kerja);
        $config['total_rows'] = $this->lap_pegawai->record_count;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();
        $data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
		$data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
        //$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
		$data['pendidikan_assoc'] =$this->pendidikan->findAll();
		$data['ket_pendidikan_assoc'] = $this->pendidikan->get_assoc('keterangan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
		$html=$this->load->view('laporan/rekapitulasi_pendidikan/list_rekapitulasi_pendidikan_pdf', $data,true);
		$this->printtopdf->htmltopdf2($properties,$html);
	}
	function printtoxls($unit_kerja,$id_jns_pegawai){
        $paging_uri = 6;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
        
		$limit_per_page = 1000;
        //$ordby = 'eselon asc,id_golpangkat_terakhir desc';
        //$ordby = $this->uri->segment(4); // pengaturan agar order by sesuai klik kolom
		
		
		/*$query = mysql_query("select replace(kode_unit,'0','') as tingkat from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data=mysql_fetch_array($query);  
			$tingkat = strlen($data['tingkat']);}		
			$pembanding=left($unit_kerja,$tingkat);			
			$search_param = array();
			$search_param[] = "id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen' and status_pegawai < 3)";
		
			if ($unit_kerja)
			{   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
				if ($tingkat == 1)
				{
					//jika unit tingkat 1 maka hanya ditampilkan pejabat
					$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' 
					OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
					$search_param[]	= "(eselon not like 'non-eselon')";		
				}
				else
				{
					//jika tingkat > 1 maka hanya ditampilkan staff
					$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
					$search_param[]	= "(eselon like 'non-eselon')";
				}
			}
		//cek apakah ada pencarian berdasarkan anam, jika ya maka tambahkan filter nama
		if ($this->input->post('nama_search')!=''){
		$search_param[] = "((nama_pegawai like '%".$this->input->post('nama_search')."%') or (NIP like '".$this->input->post('nama_search')."'))";	
		}
		else{
		$search_param[] = "(nama_pegawai like '%%')";	
		}
		
		$search_param = implode(" AND ", $search_param);
        $ordby ='id_golpangkat_terakhir desc';
		//$data['kerja_list'] = $this->lap_pegawai->findByFilter($search_param, $ordby, $limit_per_page, $start);*/
		

		$data['id_jns_pegawai'] = $id_jns_pegawai;
		$data['nama_seacrh'] = $nama_seacrh;
		
		$data['judul'] = 'REKAPITULASI PENDIDIKAN PNS ';
		
		if ($unit_kerja=='Semua') {
				 $data['judul_unit'] = 'DI SEMUA UNIT UNIVERSITAS NEGERI MALANG';
			}
			else {
				 $data['judul_unit'] = 'UNIVERSITAS NEGERI MALANG';
			}
        $data['start'] = $start;
		$data['unit_kerja'] = $unit_kerja;
		
        //$data['unitkerja_assoc'] = array('Semua' => "-- Semua Unit Kerja --") + $this->unit_kerja->get_not_tingkat2();
		//menampilkan semua unit
		$data['unitkerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_assoc('nama_unit');
		$display_unit =$this->unit_kerja->get_unit('$unit_kerja');
        
		$config['base_url'] = site_url('laporan/rekapitulasi_pendidikan/browse/' . $group . '/' . $unit_kerja);
        $config['total_rows'] = $this->lap_pegawai->record_count;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();
        $data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
		$data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
        //$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
		$data['pendidikan_assoc'] =$this->pendidikan->findAll();
		$data['ket_pendidikan_assoc'] = $this->pendidikan->get_assoc('keterangan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
		$data['html']=$this->load->view('laporan/rekapitulasi_pendidikan/list_rekapitulasi_pendidikan_pdf', $data,true);
		$this->load->view('xls.php',$data);
	}
}