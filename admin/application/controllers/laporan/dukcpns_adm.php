<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class dukcpns_adm extends Member_Controller
{

	function dukcpns_adm()
	{
		parent::Member_Controller();
		$this->load->model('lap_golpangkat_model','lap_golpangkat');
		$this->load->model('lookup_model','lookup');
		$this->load->model('unit_kerja_model','unit_kerja');
        $this->load->model('lap_pegawai_model','lap_pegawai');
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
		$this->load->model("jenis_pegawai_model");
		$this->load->model('pegawai_model', 'pegawai');
		
	}

	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Daftar Urut Kepangkatan');
		$this->browse();
	}
	
	function browse() {
	
		 
		
		$tahun = '2010';
		if ($this->input->post('tahun'))
			$tahun= $this->input->post('tahun');
		
		 $tgl = ($tahun+1) ."-01-01";
		
			 
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		
		$limit_per_page = 100;
		$ordby = 'id_golpangkat_terakhir desc, tmt_golpangkat_terakhir asc,tmt_cpns';
        
        $search_param = array();
		
       
        	$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3) ";
			
		
		$search_param[] = "(status_pegawai='1')";	
		$tgl_skr = date('Y-m-d');
		$search_param[] = "(tgl_resign >= '$tgl_skr')";
		$search_param = implode(" AND ", $search_param);
        
		$kerja_list = $this->lap_pegawai->findByFilter($search_param,$ordby,$limit_per_page,$start);
		
		$the_results['start'] = $start;
		$the_results['judul'] = "DAFTAR URUT KEPANGKATAN (DUK) Calon PEGAWAI NEGERI SIPIL <br> Tenaga Kependidikan Universitas Negeri Malang";
		$the_results['group'] = $group;
		$the_results['tahun'] = $tahun;
        $the_results['unit_kerja'] = $unit_kerja;
		$the_results['golongan_assoc'] = array('' => '-- Semua Golongan --') + $this->golongan->get_assoc();
		$the_results['unit_kerja_assoc'] = array('' => '-- Semua Unit Kerja --') + $this->unit_kerja->get_assoc('nama_unit');
		$the_results['jenis_pegawai_assoc'] = array(0=>"-- Semua Jenis Pegawai --") + array(1=>"--Tenaga Administratif--") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
        $the_results['unit_kerja']=$unit_kerja;
		$the_results['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
        $the_results['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
		$the_results['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y'), date('Y')+50);
		$jabatan1 = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$golongan = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
		$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		
		
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');
		$pendidikan1 = $this->lookup->get_datafield('pendidikan','id_pendidikan','nama_pendidikan');
		$jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
        $pendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('mk_tambahan','id_golpangkat_awal'));
		
		$namapendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('nama_pendidikan','id_golpangkat_awal'));
		$mk_tambahan = $this->lookup->get_datafield('pegawai','kd_pegawai','mk_tambahan');
		
		
        if($kerja_list) foreach($kerja_list as $pegawai){
			$pegawai['nama_jabatan']=$jabatan1[$pegawai['id_jabatan_terakhir']];
			$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']];
			$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
			$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
			$pegawai['nama_pendidikan']=$pendidikan1[$pegawai['id_pendidikan_terakhir']];
			$pegawai['mktambahan']=$mk_tambahan[$pegawai['kd_pegawai']];
			
			$masa_kerja = $this->datediff($pegawai['tmt_cpns'],$tgl);
			//$pegawai['masa_kerja'] = $masa_kerja['years']." tahun ".$masa_kerja['months']." bulan";
			$pegawai['mk_tahun'] = $masa_kerja['years'];
			$pegawai['mk_bulan'] = $masa_kerja['months'];	
			
			$mk_tambahan = $pegawai['mktambahan'];
			$mkg = $masa_kerja['years'] + $mk_tambahan;
			$pegawai['mkg_tahun'] = $mkg['years'];
			$pegawai['mkg_bulan'] = $masa_kerja['months'];
			
			$pegawai['nama_pendidikan']=$namapendidikan[$pegawai['id_pendidikan_terakhir']]['nama_pendidikan'];
			
			$temp[]=$pegawai;
		}
		$the_results['kerja_list']=$temp;
		
		$config['base_url']     = site_url('laporan/dukcpns_adm/browse/'.$group);  
		$config['total_rows']   = $this->lap_pegawai->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		
		$this->pagination->initialize($config);
		$the_results['page_links'] 	= $this->pagination->create_links();
		$this->template->display('laporan/dukcpns_adm/list_dukcpns_adm', $the_results);  
	}   

	function printpdf($tahun)
	{
		//$properties=array();
		$bln = $this->lookup->longonthname(date("m"));
		//$properties['title']='Daftar Urut Kepangkatan - Periode '.$bln." ". date("Y");
		//$properties['title']='DAFTAR URUT KEPANGKATAN (DUK) PEGAWAI NEGERI SIPIL '.echo "<br>".' TENAGA ADMINISTRASI UNIVERSITAS NEGERI MALANG'. echo"<br>".' KEADAAN : 31 DESEMBER '. $tahun;
		$properties['subject']='kerja';
		$properties['keywords']='pegawai';
		$properties['papersize']="A4";
		$properties['paperlayout']='L';	
		$properties['filename']='laporan_kepangkatan_'.date("m_Y");
		
		
		
		$the_results['tahun']= $tahun;
		$tgl = ($tahun+1) ."-01-01";
		
		$the_results['start'] = $start;
		$limit_per_page = 1000;
		$ordby = 'id_golpangkat_terakhir desc, tmt_golpangkat_terakhir asc,eselon asc, tmt_jabatan_terakhir';
        
        $search_param = array();
        $search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
		$search_param[] = "(status_pegawai='1')";	
		$search_param = implode(" AND ", $search_param);
        
		$kerja_list = $this->lap_pegawai->findByFilter($search_param,$ordby,$limit_per_page,$start);
		
		
		$the_results['judul'] = "DAFTAR URUT KEPANGKATAN (DUK) Calon PEGAWAI NEGERI SIPIL <br> Tenaga Kependidikan Universitas Negeri Malang";
		$the_results['golongan_assoc'] = array('' => '-- Semua Golongan --') + $this->golongan->get_assoc();
		$the_results['unit_kerja_assoc'] = array('' => '-- Semua Unit Kerja --') + $this->unit_kerja->get_assoc('nama_unit');
		$the_results['jenis_pegawai_assoc'] = array(0=>"-- Semua Jenis Pegawai --") + array(1=>"--Tenaga Administratif--") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
        $the_results['unit_kerja']=$unit_kerja;
		$the_results['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
        $the_results['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
		$the_results['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y'), date('Y')+50);
		$jabatan1 = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$golongan = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
		$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		
		
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');
		$pendidikan1 = $this->lookup->get_datafield('pendidikan','id_pendidikan','nama_pendidikan');
		$jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
        $pendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('mk_tambahan','id_golpangkat_awal'));
		
		$namapendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('nama_pendidikan','id_golpangkat_awal'));
		$mk_tambahan = $this->lookup->get_datafield('pegawai','kd_pegawai','mk_tambahan');
		
		
        if($kerja_list) foreach($kerja_list as $pegawai){
			$pegawai['nama_jabatan']=$jabatan1[$pegawai['id_jabatan_terakhir']];
			$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']];
			$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
			$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
			$pegawai['nama_pendidikan']=$pendidikan1[$pegawai['id_pendidikan_terakhir']];
			$pegawai['mktambahan']=$mk_tambahan[$pegawai['kd_pegawai']];
			
			$masa_kerja = $this->datediff($pegawai['tmt_cpns'],$tgl);
			//$pegawai['masa_kerja'] = $masa_kerja['years']." tahun ".$masa_kerja['months']." bulan";
			$pegawai['mk_tahun'] = $masa_kerja['years'];
			$pegawai['mk_bulan'] = $masa_kerja['months'];	
			
			$mk_tambahan = $pegawai['mktambahan'];
			$mkg = $masa_kerja['years'] + $mk_tambahan;
			$pegawai['mkg_tahun'] = $mkg['years'];
			$pegawai['mkg_bulan'] = $masa_kerja['months'];
			
			$pegawai['nama_pendidikan']=$namapendidikan[$pegawai['id_pendidikan_terakhir']]['nama_pendidikan'];
			
			$temp[]=$pegawai;
		}
		$the_results['kerja_list']=$temp;	
    
        $the_results['judul'] = "Daftar Kepangkatan Pegawai";		
		$html=$this->load->view('laporan/dukcpns_adm/list_dukcpns_admpdf', $the_results,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}
    function printtoxls($tahun)
	{
		
		$the_results['tahun'] = $tahun;
		$limit_per_page = 100;
		$ordby = 'id_golpangkat_terakhir desc, tmt_golpangkat_terakhir asc,tmt_cpns';
        
        $search_param = array();
		
       
        	$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
			
		
		$search_param[] = "(status_pegawai='1')";	
		$tgl_skr = date('Y-m-d');
		$search_param[] = "(tgl_resign >= '$tgl_skr')";
		$search_param = implode(" AND ", $search_param);
        
		$kerja_list = $this->lap_pegawai->findByFilter($search_param,$ordby,$limit_per_page,$start);
		
		$the_results['start'] = $start;
		$the_results['judul'] = "DAFTAR URUT KEPANGKATAN (DUK) Calon PEGAWAI NEGERI SIPIL <br> Tenaga Kependidikan Universitas Negeri Malang";
		$the_results['group'] = $group;
		$the_results['tahun'] = $tahun;
        $the_results['unit_kerja'] = $unit_kerja;
		$the_results['golongan_assoc'] = array('' => '-- Semua Golongan --') + $this->golongan->get_assoc();
		$the_results['unit_kerja_assoc'] = array('' => '-- Semua Unit Kerja --') + $this->unit_kerja->get_assoc('nama_unit');
		$the_results['jenis_pegawai_assoc'] = array(0=>"-- Semua Jenis Pegawai --") + array(1=>"--Tenaga Administratif--") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
        $the_results['unit_kerja']=$unit_kerja;
		$the_results['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
        $the_results['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
		$the_results['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y'), date('Y')+50);
		$jabatan1 = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$golongan = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
		$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		
		
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');
		$pendidikan1 = $this->lookup->get_datafield('pendidikan','id_pendidikan','nama_pendidikan');
		$jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
        $pendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('mk_tambahan','id_golpangkat_awal'));
		
		$namapendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('nama_pendidikan','id_golpangkat_awal'));
		$mk_tambahan = $this->lookup->get_datafield('pegawai','kd_pegawai','mk_tambahan');
		
		
        if($kerja_list) foreach($kerja_list as $pegawai){
			$pegawai['nama_jabatan']=$jabatan1[$pegawai['id_jabatan_terakhir']];
			$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']];
			$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
			$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
			$pegawai['nama_pendidikan']=$pendidikan1[$pegawai['id_pendidikan_terakhir']];
			$pegawai['mktambahan']=$mk_tambahan[$pegawai['kd_pegawai']];
			
			$masa_kerja = $this->datediff($pegawai['tmt_cpns'],$tgl);
			//$pegawai['masa_kerja'] = $masa_kerja['years']." tahun ".$masa_kerja['months']." bulan";
			$pegawai['mk_tahun'] = $masa_kerja['years'];
			$pegawai['mk_bulan'] = $masa_kerja['months'];	
			
			$mk_tambahan = $pegawai['mktambahan'];
			$mkg = $masa_kerja['years'] + $mk_tambahan;
			$pegawai['mkg_tahun'] = $mkg['years'];
			$pegawai['mkg_bulan'] = $masa_kerja['months'];
			
			$pegawai['nama_pendidikan']=$namapendidikan[$pegawai['id_pendidikan_terakhir']]['nama_pendidikan'];
			
			$temp[]=$pegawai;
		}
		$the_results['kerja_list']=$temp;	
    
        $the_results['judul'] = "Daftar Kepangkatan Pegawai";
        $the_results['html']=$this->load->view('laporan/dukcpns_adm/list_dukcpns_admpdf', $the_results,true);
		$this->load->view('xls.php',$the_results);
        		
	
	}
    function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}			
}
