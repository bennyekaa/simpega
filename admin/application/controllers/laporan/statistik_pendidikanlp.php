<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class statistik_pendidikanlp extends MY_Controller
{
	function statistik_pendidikanlp()
	{
		parent::MY_Controller();
		$this->load->model('group_unit_model', 'group_unit');
		$this->load->model('unit_kerja_model', 'unit_kerja');
		$this->load->model("group_unit_model");
		$this->load->model('lookup_model','lookup');
		$this->load->model("pendidikan_model");
		$this->load->model("jenis_pegawai_model");
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data sub unit');
		$this->statistik();
	}		
	
	function statistik() {
		$paging_uri=6;
		//end penambahan pengecekan arsip		
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 100;
		$ordby = 'id_group,kode_unit,tingkat';
		
		if ($this->input->post('id_jns_pegawai')){
			$id_jns_pegawai = $this->input->post('id_jns_pegawai');	}
		else {
			$id_jns_pegawai = 'All';}
						
		if ($this->input->post('kd_unit')){
			$kd_unit = $this->input->post('kd_unit');}
		else {
			$kd_unit = 'Semua';}
			
		//penambahan pengecekan arsip
		if ($this->input->post('tahun')){
			$tahun = $this->input->post('tahun');	}
		else {
			$tahun = date('Y');}
			
		if ($this->input->post('group'))
		{
            $group = $this->input->post('group');
		}
        else if ($this->uri->segment(4))
            $group = $this->uri->segment(4);
		else 
			$group=date('m');
			$tahunskr = date('Y');
			$groupskr = date('m');
			$data['periodeskr'] = $tahunskr.$groupskr;
			$data['tahun'] = $tahun;
			$data['group'] = $group;
			$data['periode'] = $tahun.$group;
		//end tambahan
		
		
		// query untuk memfilter unit kerja 
		$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'");					
		if ($query)	{
			$dataku=mysql_fetch_array($query);  
			$id_tenaga_dosen= $dataku['id_var'];
			}
			if ($id_jns_pegawai == $id_tenaga_dosen)
				$param_unit[] = "(unit_kerja.id_group='2')";
			else
				$param_unit[] = "";
				
			$param_unit = implode(" AND ", $param_unit);


		if ($id_jns_pegawai == 'nAll'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai(1);}
		elseif ($id_jns_pegawai == 'nAllUnit' and $kd_unit != 'Semua'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai($id_tenaga_dosen);}  	
		elseif ($kd_unit != 'Semua' and $id_jns_pegawai != 'All'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawaiIdem($id_jns_pegawai);}
		elseif ($kd_unit != 'Semua'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai(0);}
		else{
			$data['list_unit_kerja'] = $this->unit_kerja->find($param_unit,array("tingkat"=>'1'),$limit_per_page,$start,$ordby);
			}			
			
		$data['kd_unit'] = $kd_unit;
		$data['id_jns_pegawai'] = $id_jns_pegawai;		
		
		$data['GD_assoc'] = $this->lookup->GD_assoc();
		
		$data['group_assoc'] =  array(date('m') => $this->lookup->longonthname(date('m'))) + $this->lookup->month_assoc();
        $data['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y')-10, date('Y')+50);
		
		$data['pendidikan_assoc'] =$this->pendidikan_model->findAll();
		$data['unit_kerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_unit_assoc();
		$data['get_jenis_pegawai_all'] =array('All'=>"--Semua--")+ array('nAll'=>"--Fungsional--") + array('nAllUnit'=>"--Tenaga Administratif--") + $this->jenis_pegawai_model->get_jenis_pegawai_assoc();
		$data['judul'] 			= "REKAPITULASI JUMLAH PEGAWAI BERDASARKAN TINGKAT PENDIDIKAN DI UNIVERSITAS NEGERI MALANG";
		$config['base_url']     = site_url('laporan/statistik_pendidikanlp/statistik/'.$id_group.'/'.$ordby.'/');
		$config['total_rows']   = $this->unit_kerja->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$this->template->display('laporan/statistik/list_statistik_pendidikanlp', $data);
	}
	
	
	function statistiktopdf($kd_unit,$id_jns_pegawai,$tahun,$group,$periode,$periodeskr){
		$properties=array();
		$properties['title']='REKAPITULASI JUMLAH PEGAWAI BERDASARKAN TINGKAT PENDIDIKAN DI UNIVERSITAS NEGERI MALANG';
		$properties['subject']='statistik pendidikan';
		$properties['keywords']='statistik pendidikan';
		$properties['filename']='statistik_pendidikanlp'; //.$this->sims->tahun
		$properties['paperlayout']='L';
		$properties['papersize']="A4";
		
		$this->load->model('group_unit_model', 'group_unit');
		$this->load->model('unit_kerja_model', 'unit_kerja');
		$this->load->model("group_unit_model");
		$this->load->model('lookup_model','lookup');
		$this->load->model("pendidikan_model");
		$this->load->model("jenis_pegawai_model");
		
		$start=0 ;
		$limit_per_page = 100;		
		$ordby = 'id_group,kode_unit,tingkat';
				
		$data['kd_unit'] = $kd_unit;
		$data['id_jns_pegawai'] = $id_jns_pegawai;	
		$data['periodeskr'] = $periodeskr;
		$data['tahun'] = $tahun;
		$data['group'] = $group;
		$data['periode'] = $periode;
		// query untuk memfilter unit kerja 
		$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'");					
		if ($query)	{
			$dataku=mysql_fetch_array($query);  
			$id_tenaga_dosen= $dataku['id_var'];
			}
			if ($id_jns_pegawai == $id_tenaga_dosen)
				$param_unit[] = "(unit_kerja.id_group='2')";
			else
				$param_unit[] = "";
				
			$param_unit = implode(" AND ", $param_unit);
			
		if ($id_jns_pegawai == 'nAll'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai(1);}
		elseif ($id_jns_pegawai == 'nAllUnit' and $kd_unit != 'Semua'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai($id_tenaga_dosen);}  	
		elseif ($kd_unit != 'Semua' and $id_jns_pegawai != 'All'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawaiIdem($id_jns_pegawai);}
		elseif ($kd_unit != 'Semua'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai(0);}
		else{
			$data['list_unit_kerja'] = $this->unit_kerja->find($param_unit,array("tingkat"=>'1'), $limit_per_page,$start,$ordby);}

		$data['GD_assoc'] = $this->lookup->GD_assoc();
		$data['pendidikan_assoc'] =$this->pendidikan_model->findAll();
		$data['unit_kerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_unit_assoc();

		$data['judul'] 				= "REKAPITULASI JUMLAH PEGAWAI BERDASARKAN TINGKAT PENDIDIKAN DI UNIVERSITAS NEGERI MALANG";
		$html=$this->load->view('laporan/statistik/list_statistik_pendidikanlp_pdf', $data,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}
	
	function printtoxls($kd_unit,$id_jns_pegawai,$tahun,$group,$periode,$periodeskr){
		$this->load->model('group_unit_model', 'group_unit');
		$this->load->model('unit_kerja_model', 'unit_kerja');
		$this->load->model("group_unit_model");
		$this->load->model('lookup_model','lookup');
		$this->load->model("pendidikan_model");
		$this->load->model("jenis_pegawai_model");

		$start=0 ;
		$limit_per_page = 100;		
		$ordby = 'id_group,kode_unit,tingkat';
				
		$data['kd_unit'] = $kd_unit;
		$data['id_jns_pegawai'] = $id_jns_pegawai;	
		$data['periodeskr'] = $periodeskr;
		$data['tahun'] = $tahun;
		$data['group'] = $group;
		$data['periode'] = $periode;
		
		// query untuk memfilter unit kerja 
		$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'");					
		if ($query)	{
			$dataku=mysql_fetch_array($query);  
			$id_tenaga_dosen= $dataku['id_var'];
			}
			if ($id_jns_pegawai == $id_tenaga_dosen)
				$param_unit[] = "(unit_kerja.id_group='2')";
			else
				$param_unit[] = "";
				
			$param_unit = implode(" AND ", $param_unit);
			
		if ($id_jns_pegawai == 'nAll'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai(1);}
		elseif ($id_jns_pegawai == 'nAllUnit' and $kd_unit != 'Semua'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai($id_tenaga_dosen);}  	
		elseif ($kd_unit != 'Semua' and $id_jns_pegawai != 'All'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawaiIdem($id_jns_pegawai);}
		elseif ($kd_unit != 'Semua'){
			$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai(0);}
		else{
			$data['list_unit_kerja'] = $this->unit_kerja->find($param_unit,array("tingkat"=>'1'), $limit_per_page,$start,$ordby);}

		//getting golpangkat
		$data['GD_assoc'] = $this->lookup->GD_assoc();
		$data['pendidikan_assoc'] =$this->pendidikan_model->findAll();
		$data['unit_kerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_unit_assoc();
		
		$data['html']=$this->load->view('laporan/statistik/list_statistik_pendidikanlp_pdf', $data,true);
		$this->load->view('xls.php',$data);
	}

}

