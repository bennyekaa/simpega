<?php 
function right($value, $count){
    return substr($value, ($count*-1));
}

function left($string, $count){
    return substr($string, 0, $count);
}
if (!defined('BASEPATH')) exit('No direct script access allowed');

class pegawai_unit extends My_Controller
{

	function pegawai_unit()
	{
		parent::My_Controller();
		 $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('agama_model', 'agama');
        $this->load->model('lookup_model', 'lookup');
        $this->load->model('jenis_pegawai_model', 'jenis_pegawai');
        $this->load->model('pendidikan_model', 'pendidikan');
        $this->load->model('kelurahan_model', 'kelurahan');
        $this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('golongan_model', 'golongan');
        $this->load->model('jabatan_model', 'jabatan');
        $this->load->model('lap_pegawai_model', 'lap_pegawai');

        /* $this->load->model('gambar_model', 'gambar');  */
        $this->load->model('user_pegawai_model', 'user_pegawai');
	}

	// --------------------------------------------------------------------

	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Laporan Daftar Pegawai');
		$this->browse();
	}
	
	function browse()
	{
		
		if ($this->input->post('group'))
		{
            $group = $this->input->post('group');
		}
        else if ($this->uri->segment(4))
            $group = $this->uri->segment(4);
		else 
			$group= '0';
			
		$this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		if($this->user->user_group == 'Admin Khusus'){
		$query = mysql_query("select p.kode_unit from user_pegawai u,pegawai 
			p where p.kd_pegawai = u.kd_pegawai and u.user_id='".$this->user->user_id."'");					
			if ($query) {
					$datauser=mysql_fetch_array($query);  
					$unit_user = $datauser['kode_unit']; 
			}	
			$unit_kerja	=$unit_user;
		}
		else
		{			
		//       $unit_kerja = '0';
			if ($this->input->post('unit_kerja'))
			{    
				$unit_kerja = $this->input->post('unit_kerja');
				
			}
			else if ($this->uri->segment(5)) {
				$unit_kerja = $this->uri->segment(5);
			}
		
		}		
		$paging_uri = 6;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
        
		$limit_per_page = 1000;
        $ordby = 'eselon asc,id_golpangkat_terakhir desc';
        
		$query = mysql_query("select replace(kode_unit,'0','') as tingkat from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data=mysql_fetch_array($query);  
			$tingkat = strlen($data['tingkat']);
			}		
			$pembanding=left($unit_kerja,$tingkat);						
			$search_param = array();
			if ($group=='1')
			{
				$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
				if ($unit_kerja)
				{   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
					if ($tingkat == 1)
					{
					//jika unit tingkat 1 maka hanya ditampilkan pejabat
					$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
					$search_param[]	= "(eselon not like 'non-eselon')";		
					}
					else
					{
					//jika tingkat > 1 maka hanya ditampilkan staff
					$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
					$search_param[]	= "(eselon like 'non-eselon')";
					}
				}
			}
			elseif ($group=='2')
			{
				$search_param[] = "(id_jns_pegawai = (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3 )";
				if ($unit_kerja)
				{   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
					if ($tingkat == 1)
					{
					//jika unit tingkat 1 maka hanya ditampilkan pejabat
					$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
					$search_param[]	= "(eselon not like 'non-eselon')";		
					}
					else
					{
					//jika tingkat > 1 maka hanya ditampilkan staff
					$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
					$search_param[]	= "(eselon like 'non-eselon')";
					}
				}
			}
		$search_param = implode(" AND ", $search_param);
        $ordby ='id_golpangkat_terakhir desc, eselon asc';
		$data['kerja_list'] = $this->lap_pegawai->findByFilter($search_param, $ordby, $limit_per_page, $start);
		
        $data['group'] = $group;
		$data['start'] = $start;
		$data['unit_kerja'] = $unit_kerja;
		
		//menampilkan semua unit
		$data['unitkerja_assoc'] = array('Semua'=>"--Semua Unit--")+ $this->unit_kerja->get_assoc('nama_unit');
		$data['jenis_pegawai_assoc'] = array(0 => "-- Semua Jenis Pegawai --") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		$data['adm_akd_assoc'] = $this->lookup->adm_akd_assoc();
        $data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
		
		$config['base_url'] = site_url('laporan/pegawai_unit/browse/' . $group . '/' . $unit_kerja);
        $config['total_rows'] = $this->lap_pegawai->record_count;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();
        
        $this->template->display('/laporan/pegawai_unit/list_pegawai_unit', $data);  
	}   
			
	
	function printtopdf($group,$unit_kerja)
	{
		$properties=array();
		$properties['title']='Buku Pegawai';
		$properties['subject']='kerja';
		$properties['keywords']='pegawai';
		$properties['papersize']="A4";
		$properties['paperlayout']='L';	
		$properties['filename']='Laporan pegawai';
		
		$the_results['group']=$group;
		$the_results['unit_kerja']=$unit_kerja;
		      
		$limit_per_page = 20;
	  	$ordby = 'nama_pegawai asc';
		
		
        
		if (($unit_kerja=='0') and ($group=='All'))
		{
			$kerja_list = $this->lap_pegawai->findAll($ordby,$start,$limit_per_page);
		}
		elseif ($group=='All')
		{
			$search_param = array();
			if ($unit_kerja)
				$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
        	$search_param = implode(" AND ", $search_param);
			$kerja_list = $this->lap_pegawai->findByFilter($search_param,$ordby,$limit_per_page,$start);
		}
		else
		{
			$search_param = array();
			if ($unit_kerja)
				$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
        	if ($group != 'nAll') 
            	$search_param[] = "(id_jns_pegawai = '$group' and status_pegawai < 3 )";
			elseif ($group=='nAll')
        		$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen')  and status_pegawai < 3)";
			$search_param = implode(" AND ", $search_param);
			$kerja_list = $this->lap_pegawai->findByFilter($search_param,$ordby,$limit_per_page,$start);
		}
		
		$the_results['group'] = $group;
        $the_results['judul'] 	= "Laporan Daftar Pegawai";
		$the_results['start'] = $start;
		$the_results['unit_kerja_assoc'] = array('0'=>"-- Semua Unit Kerja --") + $this->unit_kerja->get_assoc2();
		$the_results['jenis_pegawai_assoc'] = array('All'=>"-- Semua Jenis Pegawai --") + array('nAll'=>"--Tenaga Administratif--")+ $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		$the_results['unit_kerja']= $unit_kerja;
		$the_results['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit($unit_kerja);
		$the_results['nama_jenis'] = $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		//$jabatan = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
        $golongan = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
		$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		
		
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');		
		
		if($kerja_list) foreach($kerja_list as $pegawai){
			
			$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']]['nama_jabatan'];
			$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']];
			$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
			
			
			$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
			$pegawai['batas']=$jabatan[$pegawai['id_jabatan_terakhir']]['batas_maks_pensiun'];
			
			$temp[]=$pegawai;
		}
		$the_results['kerja_list']=$temp;
		$the_results['judul'] = "Daftar pegawai";
		$the_results['html']=$this->load->view('laporan/pegawai_unit/list_pegawai_unitpdf', $the_results,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}	


function printtoxls($group,$unit_kerja)
	{
				
		$the_results['group']=$group;
		$the_results['unit_kerja']=$unit_kerja;
		      
		$limit_per_page = 20;
	  	$ordby = 'nama_pegawai asc';
		$search_param = array();
		if ($unit_kerja)
			$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
        if($group)
            $search_param[] = "(id_jns_pegawai = '$group' and status_pegawai < 3 )";
		
        $search_param = implode(" AND ", $search_param);
		
        if (($unit_kerja=='0') and ($group=='0'))
		{
			$kerja_list = $this->lap_pegawai->findAll($ordby,$start,$limit_per_page);
		}
		else
		{
			$kerja_list = $this->lap_pegawai->findByFilter($search_param,$ordby,$limit_per_page,$start);
		}
		
		
		$jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
        $golongan = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
		$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');		
		$tgl = date('Y-m-d');
		if($kerja_list) foreach($kerja_list as $pegawai){
			
			$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']]['nama_jabatan'];
			$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']];
			$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
			$pegawai['NIP']=$NIP_peg[$pegawai['kd_pegawai']];
			
			$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
			$pegawai['batas']=$jabatan[$pegawai['id_jabatan_terakhir']]['batas_maks_pensiun'];
			
            //$usia = $this->datediff($pegawai['tgl_lahir'],$tgl);
            $batas=$pegawai['batas'];
            list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
            
            $tahun = $y+ $batas;
            if ($m>=2)
            {
                $bulan = $m+1;    
            }
            else
            {
                $bulan = $m;
            }
             
			$pegawai['pensiun'] ='1/'.$bulan.'/'.$tahun;
            
			$temp[]=$pegawai;
		}
		
		
		$the_results['kerja_list']=$temp;
		$the_results['judul'] = "Daftar pegawai";
		$the_results['html']=$this->load->view('laporan/pegawai_unit/list_pegawai_unitpdf', $the_results,true);
		$this->load->view('xls.php',$the_results);
        
       
	}
	
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}	
	
}
