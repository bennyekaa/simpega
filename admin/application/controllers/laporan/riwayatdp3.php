<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatdp3 extends Member_Controller
{
	function Riwayatdp3()
	{
		parent::Member_Controller();
		
		$this->load->model('riwayat_dp3_model','riwayat_dp3');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');

		
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Riwayat DP3');
		$this->browse($kd_pegawai);
	}
	


	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  		'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	function printtopdf($id)
	{
			$properties=array();
			//$properties['title']="LAPORAN DATA ABSENSI PERIODE ".strtoupper($this->lookup->longonthname($group)).' TAHUN : '.$tahun;
			$properties['subject']='Riwayat DP3';
			$properties['keywords']='Riwayat DP3';
			$properties['filename']='detail_riwayat_dp3'; 
			$properties['paperlayout']='L';
			$properties['papersize']="Folio";
			$properties['ImageScale']="4";
			
			
			$this->template->metas('title', 'SIMPEGA | Riwayat DP3 :: Ubah');
			$data = $this->riwayat_dp3->retrieve_by_pkey($id);
			
			
			
			
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['judul']='Edit DP3 dari: '. $data['pegawai']['nama_pegawai'];
				$data['photo'] = base_url() . '/public/images/garuda.jpg';
				//$this->template->display('/pegawai/riwayatdp3/detail_riwayat_dp3_pdf', $data);
				$html=$this->load->view('pegawai/riwayatdp3/detail_riwayat_dp3_pdf', $data,true);
				$this->printtopdf->htmltopdf($properties,$html);
			}
			
			
	}
		
	
}