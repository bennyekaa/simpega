<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatpelatihan extends Member_Controller
{
	function Riwayatpelatihan()
	{
		parent::Member_Controller();
		
		$this->load->model('riwayat_pelatihan_model','riwayat_pelatihan');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model', 'lookup');
		
	}
	
	function index()
	{
		$kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Pelatihan Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		/*$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'id_riwayat_pelatihan';
		*/
		$data['list_pelatihan'] = $this->riwayat_pelatihan->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
		$data['jenis_pelatihan_assoc'] = $this->lookup->jenis_pelatihan_assoc();
		/*$config['base_url']     = site_url('pegawai/riwayatpelatihan/browse/');
		$config['total_rows']   = $this->riwayat_pelatihan->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
        */
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Pelatihan dari: " . $data['pegawai']['nama_pegawai'];		    
	
		
		if (isset($this->user->user_group)){
		$this->template->display('pegawai/riwayatpelatihan/list_riwayatpelatihan', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			$this->riwayat_pelatihan->add($data);
			set_success('Data pelatihan pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatpelatihan/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Pelatihan Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
             $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Pelatihan dari: ' . $data['pegawai']['nama_pegawai'];
            
			$data['id_riwayat_pelatihan']=$this->riwayat_pelatihan->get_id();
			$data['jenis_pelatihan_assoc'] = $this->lookup->jenis_pelatihan_assoc();
			$data['semester_assoc'] = $this->lookup->semester_assoc();
			$data['capaian_assoc'] = $this->lookup->capaian_assoc();
			//$data['golongan_pangkat_assoc'] = array(0=>"-- Pilih Golongan/Pangkat --")+$this->golongan->get_assoc("id_golpangkat");
			$this->template->display('/pegawai/riwayatpelatihan/detail_riwayatpelatihan', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_pelatihan'] = $id;
			$this->riwayat_pelatihan->update($id, $data);
			set_success('Perubahan data pasangan pegawai berhasil disimpan');
			redirect('/pegawai/riwayatpelatihan/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Pelatihan Pegawai :: Ubah');
			$data = $this->riwayat_pelatihan->retrieve_by_pkey($id);
			//$data['golongan_pangkat_assoc'] = array(0=>"-- Pilih Golongan/Pangkat --")+$this->golongan->get_assoc("id_golpangkat");
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                 $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['jenis_pelatihan_assoc'] = $this->lookup->jenis_pelatihan_assoc();
				$data['semester_assoc'] = $this->lookup->semester_assoc();
				$data['capaian_assoc'] = $this->lookup->capaian_assoc();
				$data['judul']='Edit Pelatihan dari: '. $data['pegawai']['nama_pegawai'];			
				$this->template->display('/pegawai/riwayatpelatihan/detail_riwayatpelatihan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatpelatihan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_pelatihan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Pelatihan Pegawai :: Hapus');
		confirm("Yakin menghapus data pelatihan pegawai?");
		$res = $this->riwayat_pelatihan->delete($idField);
		set_success('Data pelatihan pegawai berhasil dihapus');
		redirect('/pegawai/riwayatpelatihan/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_pelatihan']	= '';
		$data['kd_pegawai']	= '';
		$data['nama_pelatihan']	= '';
		$data['jns_pelatihan'] = '';
		$data['nomor_surat_tugas'] = '';
		$data['tahun'] = '';
		$data['tgl_mulai']	= '';
		$data['tgl_selesai']	= '';
		$data['lama_hari']	= '';
		$data['tempat_pelatihan']	= '';
		$data['keterangan']	= '';
		$data['semester' ]	= '';
		$data['tahun_ajaran'] = '';
		$data['bukti_penugasan' ] = '';
		$data['capaian'] = '';
		$data['jml_jam'] = '';
		$data['sks'] = '';
		$data['dn_ln'] = '';
		$data['penyelenggara'] = '';
		$data['angkatan'] = '';
		$data['predikat'] = '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   
		
		
		$data['id_riwayat_pelatihan']	= $this->riwayat_pelatihan->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['nama_pelatihan']	= $this->input->post('nama_pelatihan', TRUE);
		$data['jenis_pelatihan']	= $this->input->post('jns_pelatihan', TRUE);
		$data['nomor_surat_tugas']	= $this->input->post('nomor_surat_tugas', TRUE);
		$data['tahun']	= $this->input->post('tahun', TRUE);
		$data['tgl_mulai']	= $this->input->post('tgl_mulai', TRUE);
		$data['tgl_selesai']	= $this->input->post('tgl_selesai', TRUE);
		$data['lama_hari']	= $this->input->post('lama_hari', TRUE);
		$data['tempat_pelatihan']	= $this->input->post('tempat_pelatihan', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['semester' ]	= $this->input->post('semester', TRUE);
		$data['tahun_ajaran'] = $this->input->post('tahun_ajaran', TRUE);
		$data['bukti_penugasan' ] = $this->input->post('bukti_penugasan', TRUE);
		$data['capaian'] = $this->input->post('capaian', TRUE);
		$data['jml_jam'] = $this->input->post('jml_jam', TRUE);
		$data['sks'] = $this->input->post('sks', TRUE);		
		$data['dn_ln'] = $this->input->post('dn_ln', TRUE);
		$data['penyelenggara'] = $this->input->post('penyelenggara', TRUE);
		$data['angkatan'] = $this->input->post('angkatan', TRUE);
		$data['predikat'] = $this->input->post('predikat', TRUE);

		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_pelatihan', 'nama_pelatihan', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}