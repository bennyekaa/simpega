<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CariPegawai extends Member_Controller
{
	var $pegawai_id;
	var $groupuser;
	function CariPegawai()
	{
		parent::Member_Controller();
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('agama_model', 'agama');
		$this->load->model('lookup_model', 'lookup');
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model('kelurahan_model', 'kelurahan');
		$this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('golongan_model', 'golongan');
        $this->load->model('status_perkawinan_model', 'status_perkawinan');
		$this->load->model('status_keluarga_model', 'status_keluarga');
        $this->load->model('jabatan_model', 'jabatan');
		$this->load->model('riwayat_gol_kepangkatan_model', 'riwayat_gol_kepangkatan');
		$this->load->model('riwayat_jabatan_model', 'riwayat_jabatan');
		$this->load->model('riwayat_jabatanstruktural_model','riwayat_jabatanstruktural');
		$this->load->model('riwayat_karyailmiah_model','riwayat_karyailmiah');
		$this->load->model('jenis_karya_model','jenis_karya');
		$this->load->model('riwayat_pendidikan_model', 'riwayat_pendidikan');
		$this->load->model('riwayat_tugas_belajar_model','riwayat_tugas_belajar');
		$this->load->model('riwayat_pelatihan_model', 'riwayat_pelatihan');
		$this->load->model('riwayat_angka_kredit_model','riwayat_angka_kredit');
		$this->load->model('riwayat_dp3_model','riwayat_dp3');
		$this->load->model('riwayat_penghargaan_model', 'riwayat_penghargaan');
		$this->load->model('jenis_penghargaan_model','jenis_penghargaan');
		$this->load->model('jenis_mutasi_model', 'jenis_mutasi');
		$this->load->model('riwayat_mutasi_model','riwayat_mutasi');
        $this->load->model('lap_pegawai_model','lap_pegawai');
		$this->load->model('anak_pegawai_model','anak_pegawai');
		$this->load->model('pasangan_pegawai_model','pasangan_pegawai');
		
		/*
		$this->load->model('gambar_model', 'gambar');
		*/
		$this->load->model('user_pegawai_model', 'user_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data Induk Pegawai');
		$this->cari();
	}
	

	function browse()
	{
       
	
	
	}
    function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	
    function view($idField = '')
    {
			//die( print_r($this->user, true) );
			if( '' == $idField )
			{
				$idField = $this->user_pegawai->get_idpegawai_by_userid( $this->user->user_id );
				
				//die("Id Pegawai is: {$idField}");
			}
        $data = $this->lap_pegawai->retrieve_by_pkey($idField);
		$data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
		$data['no_KTP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'no_KTP');
		$data['nama_peg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama_pegawai');
		$data['tempat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tempat_lahir');
		$data['tanggal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
		$data['alamat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat');   
		$data['alamat_asal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat_asal');
		$data['hp'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hp');
		$data['email'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'email');
		$data['rt'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'rt');
		$data['rw'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'rw');
		$data['id_pendidikan_terakhir'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_pendidikan_terakhir');
		$data['drh_tinggi'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_tinggi');
		$data['drh_bb'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_bb');
		$data['drh_rambut'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_rambut');
		$data['drh_bentuk_muka'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_bentuk_muka');
		$data['drh_warna_kulit'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_warna_kulit');
		$data['drh_ciri_ciri'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_ciri_ciri');
		$data['drh_cacat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_cacat');
		$data['hobi_kesenian'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_kesenian');
		$data['hobi_olahraga'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_olahraga');
		$data['hobi_lain'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_lain');
		$data['drh_nama_kecil'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_nama_kecil');
		
		$data['jns'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'jns_kelamin');
		if ($data['jns']==1)
            $data['jenis'] = 'Laki-Laki';
        else
		    $data['jenis'] = 'Perempuan';
		$data['agama_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_agama');
		$data['agama'] = $this->lookup->get_cari_field('agama','kd_agama',$data['agama_id'],'agama');
		
		$data['stskawin'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_kawin');	
        if ($data['stskawin']==1)
            $data['stkawin'] = 'Kawin';
        else
		    $data['stkawin'] = 'Belum Kawin';
        $data['jenis_pegawai_id']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jns_pegawai');
        $data['jenis_pegawai']=$this->lookup->get_cari_field('jenis_pegawai','id_jns_pegawai',$data['jenis_pegawai_id'],'jenis_pegawai');
		$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
        if ($data['status_pegawai']==0)
            $data['status_pegawai'] = 'Honorer';
        elseif ($data['status_pegawai']==1)
            $data['status_pegawai'] = 'CPNS';
        else
            $data['status_pegawai'] = 'PNS';
        $data['pangkat_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
        $data['pangkat']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'pangkat');
        
        $data['golongan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
        $data['golongan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['golongan_id'],'golongan');
        
        $data['jabatan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_terakhir');
        $data['jabatan']=$this->lookup->get_cari_field('jabatan','id_jabatan',$data['jabatan_id'],'nama_jabatan');
        
        $data['unit_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit');
        $data['unit_kerja']=$this->lookup->get_cari_field('unit_kerja','kode_unit',$data['unit_id'],'nama_unit');
        
        $tgl = date('Y-m-d');
        $data['tgl_lahir']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
        $usia = $this->datediff($data['tgl_lahir'],$tgl);
        $data['usia'] = $usia['years']." tahun ".$usia['months']." bulan";
        
        $data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
		$masa_kerja = $this->datediff($data['tmt_cpns'],$tgl);
		$data['mk_tahun']=$masa_kerja['years'];
		$data['mk_bulan']=$masa_kerja['months'];
		$data['mk_tambahan_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_th');
		$data['mk_tambahan_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_bl');
		$data['mk_selisih_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_th');
		$data['mk_selisih_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_bl');
		
		
		$mkk_th = $data['mk_tahun'] + $data['mk_tambahan_th'] + $data['mk_selisih_th'];
		$mkk_bl = $data['mk_bulan'] + $data['mk_tambahan_bl'] + $data['mk_selisih_bl'];
		//$mkg_th = $mkk_th + $data['mk_selisih_th'];
		//$mkg_bl = $mkk_bl + $data['mk_selisih_bl'];
        /*$data['masa_kerja']=$mkg_th['years']." tahun ".$mkg_bl['months']." bulan";*/
		$data['masa_kerja']= $mkk_th ." tahun ".$mkk_bl." bulan";
		
        
        $ordby1='id_pendidikan, th_lulus';
		$data['list_pendidikan'] = $this->riwayat_pendidikan->find(NULL, array('kd_pegawai' => $idField), $ordby1, $start, null);
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
		$data['agama_assoc'] = array(0 => "-- Pilih Agama --") + $this->agama->get_assoc('agama');
		$data['option_kecamatan']=$this->kelurahan->get_kelurahan();
		$data['gol_darah_assoc'] = array(0 => "-") + $this->lookup->gol_darah_assoc();
		 
		$data['list_tb'] = $this->riwayat_tugas_belajar->find(NULL, array('kd_pegawai' => $idField), null, $limit_per_page,$start,$ordby);
		
		$ordby2 = 'riwayat_gol_kepangkatan.aktif ,riwayat_gol_kepangkatan.status_pegawai, riwayat_gol_kepangkatan.id_golpangkat, riwayat_gol_kepangkatan.tmt_pangkat';
		$data['list_pangkat'] = $this->riwayat_gol_kepangkatan->find( NULL,array('kd_pegawai' => $idField), $ordby2, $limit_per_page,$start,NULL);
		
		
		$data['golongan_assoc'] = $this->golongan->get_assoc();
		
		$ordbyjabatan = 'b.id_jabatan,riwayat_jabatan.tmt_jabatan asc';
		$data['list_jabatan'] = $this->riwayat_jabatan->find(NULL, array('kd_pegawai' => $idField), $ordbyjabatan,$start,NULL);
			
		//$data['list_jabatan'] = $this->riwayat_jabatan->find(NULL, array('kd_pegawai' => $idField), $ordby, $limit_per_page,$start,NULL);
		$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
		$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
		
		$data['list_ak'] = $this->riwayat_angka_kredit->find(NULL, array('kd_pegawai' => $idField), null, $limit_per_page,$start,$ordby);
		
		//$ordby = 'id_riwayat_karya';
		$data['list_karya_ilmiah'] = $this->riwayat_karyailmiah->find(NULL, array('kd_pegawai' => $idField), null, $limit_per_page,$start,$ordby);
		$data['jenis_karya_assoc']=$this->jenis_karya->get_assoc('nama_karya');
		
		$data['list_pelatihan'] = $this->riwayat_pelatihan->find(NULL, array('kd_pegawai' => $idField), $ordby, $limit_per_page,$start,NULL);
		
		$data['list_dp3'] = $this->riwayat_dp3->find(NULL, array('kd_pegawai' => $idField), $ordby,$start, $limit_per_page);
		
		$data['list_penghargaan'] = $this->riwayat_penghargaan->find(NULL, array('kd_pegawai' => $idField), null, $limit_per_page,$start,$ordby);
		$data['jenis_penghargaan_assoc'] = $this->jenis_penghargaan->get_assoc('nama_penghargaan');
		
		$data['list_mutasi'] = $this->riwayat_mutasi->find(NULL, array('mutasi.kd_pegawai' => $idField), $ordby, $start, $limit_per_page);		
        $data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
		$data['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
		$data['gol_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$data['option_unit']=$this->unit_kerja->get_unit('1');
		$data['golongan_assoc'] = $this->golongan->get_assoc();

		$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg($idField);
		$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc('status_keluarga');
		
		$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($idField);
		
				$expected_file = PUBLICPATH . 'photo/photo_' . $idField . '.jpg';
				if (file_exists($expected_file)){
						$data['photo'] = base_url() . '/public/photo/photo_' . $idField . '.jpg';
					}
					else{
						$expected_file = PUBLICPATH . 'photo/photo_' . $idField . '.gif';
						if (file_exists($expected_file)){
							$data['photo'] = base_url() . '/public/photo/photo_' . $idField . '.gif';
							}
						else{
							$expected_file = PUBLICPATH . 'photo/photo_' . $idField . '.bmp';
							if (file_exists($expected_file)){
									$data['photo'] = base_url() . '/public/photo/photo_' . $idField . '.bmp';
								}
								else{	
									$data['photo'] = base_url() . '/public/images/image/nophotos.jpg';
							}
						}
					}
        //$data['photo'] = file_exists($expected_file) ? base_url() . '/public/photo/photo_' . $idField . '.jpg' : base_url() . '/public/images/image/nophotos.jpg';

        $data['judul']='Detail Data dari : '.$data['nama_peg'];
		$data['action'] = 'view/' . $id;
		

		if (isset($this->user->user_group)){
			$this->template->display('/pegawai/caripegawai/view_caripegawai', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}

    }
	
	
	
	
		

	function cari()
	{
		//print_r($this->user);
		//$this->output->enable_profiler(true);
		if('Sub Admin' == $this->user->user_group)
		{
			$idPegawai = $this->user_pegawai->get_idpegawai_by_userid( $this->user->id );
			$thisPegawai = $this->pegawai->findById( $idPegawai );
			//print_r($thisPegawai[0]);
			$cari=array('kode_unit'=> $thisPegawai[0]['kode_unit']);
				
			$pegawai_list = $this->pegawai->get_all_by($cari);
			//show_error(var_dump($data));
			$jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
			if($pegawai_list)
				foreach($pegawai_list as $pegawai)
				{
				

					$pegawai['batas']=$jabatan[$pegawai['id_jabatan_terakhir']]['batas_maks_pensiun'];
					$tgl = date('Y-m-d');
					$usia = $this->datediff($pegawai['tgl_lahir'],$tgl);
					$pegawai['usia'] = $usia['years']." tahun ".$usia['months']." bulan";
					$masa_kerja = $this->datediff($pegawai['tmt_cpns'],$tgl);
					$pegawai['masa_kerja']=$masa_kerja['years']." tahun ".$masa_kerja['months']." bulan";
					//$usia = $this->datediff($pegawai['tgl_lahir'],$tgl);
					$batas=$pegawai['batas'];
					list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
									
					$tahun = $y+ $batas;
					if ($m>=2)
					{
						$bulan = $m+1;    
						if ($bulan==13)
						{
							$bulan = 1;
							$tahun = $y+$batas+1;
						}  
					}
					else
					{
						$bulan = $m;
					}
									 
					$pegawai['pensiun'] ='1/'.$bulan.'/'.$tahun;
								
					$temp[]=$pegawai;
				}
				$data['pegawai_list']=$temp;
				if($data['pegawai_list']!=false)
				{
					$data['is_new']= false;
				}
				else
				{
					$data['is_new']= true;
				}
				$data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
				$data['pendidikan_assoc']=$this->pendidikan->get_assoc('nama_pendidikan');
				$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
				$data['unitkerja_assoc'] =$this->unit_kerja->get_assoc2();
				$data['judul']='Daftar Pegawai Di Unit ' . $data['unitkerja_assoc'][ $thisPegawai[0]['kode_unit'] ];

				if (isset($this->user->user_group)){
					$this->template->display('/pegawai/caripegawai/list_caripegawai_perunit',$data);
				} else {
					redirect('/pegawai/pegawai/', 'location');
				}
			}
		else
		{
			if ($this->_validate_search())
			{
				$val = $this->_get_form_values_search();
				if($val['category']==1)
				{
					$cari=array('NIP'=>$val['search']);
				}
				elseif($val['category']==2)
				{
					$cari=array('nama_pegawai'=>$val['search']);
				}
							elseif($val['category']==3)
							{
									$cari=array('nip_lama'=>$val['search']);
							}
							elseif($val['category']==4)
							{
									//$cari=array('CONCAT(right(tgl_lahir,2),'-',mid(tgl_lahir,6,2),'-',left(tgl_lahir,4))'=>$val['search']);
					$cari=array('tgl_lahir'=>$val['search']);
							}
				elseif($val['category']==5)
							{
					$cari=array('alamat'=>$val['search']);
							}
				
				$pegawai_list = $this->pegawai->get_all_by($cari);
				//show_error(var_dump($data));
							$jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
							if($pegawai_list) foreach($pegawai_list as $pegawai){
				
				
						$pegawai['batas']=$jabatan[$pegawai['id_jabatan_terakhir']]['batas_maks_pensiun'];
						$tgl = date('Y-m-d');
									$usia = $this->datediff($pegawai['tgl_lahir'],$tgl);
									$pegawai['usia'] = $usia['years']." tahun ".$usia['months']." bulan";
							$masa_kerja = $this->datediff($pegawai['tmt_cpns'],$tgl);
									$pegawai['masa_kerja']=$masa_kerja['years']." tahun ".$masa_kerja['months']." bulan";
									//$usia = $this->datediff($pegawai['tgl_lahir'],$tgl);
									$batas=$pegawai['batas'];
									list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
									
									$tahun = $y+ $batas;
									if ($m>=2)
									{
											$bulan = $m+1;    
											if ($bulan==13)
											{
													$bulan = 1;
													$tahun = $y+$batas+1;
											}  
									}
									else
									{
											$bulan = $m;
									}
									 
						$pegawai['pensiun'] ='1/'.$bulan.'/'.$tahun;
									
						$temp[]=$pegawai;
							}
						$data['pegawai_list']=$temp;
							if($data['pegawai_list']!=false){
					$data['is_new']= false;}
				else{
					$data['is_new']= true;}
				$data['judul']='Data Pegawai';
							$data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
							//$data['pendidikan_assoc']=$this->pendidikan->get_assoc('nama_pendidikan');
							$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
							$data['unitkerja_assoc'] =$this->unit_kerja->get_assoc2();
				//$this->template->display('/pegawai/caripegawai/list_caripegawai_perunit',$data);
				
				if (isset($this->user->user_group)){
					$this->template->display('/pegawai/caripegawai/list_caripegawai_perunit',$data);
				} else {
					redirect('/pegawai/pegawai/', 'location');
				}

			}
			else
			{
				$this->template->metas('title','SIMPEGA | Pencarian Pegawai');
				$data['judul']='Pencarian Pegawai';
				$data['action']='cari';
				$data['category_assoc']=array(1=>'NIP Baru',2=>'Nama Pegawai',3=>'NIP Lama', 4=>'Tanggal Lahir', 5=>'Alamat');
				//$this->template->display('/pegawai/caripegawai/search_caripegawai',$data);
				if (isset($this->user->user_group)){
					$this->template->display('/pegawai/caripegawai/list_caripegawai_perunit',$data);
				} else {
					redirect('/pegawai/pegawai/', 'location');
				}
			}
		}
	}
	
	function _clear_form_search()
	{
		$data['is_new'] = '';
		$data['search']	= '';
		$data['category']	= '';
		return $data;
	}	
	
	function _get_form_values_search()
	{
	   	$data['search']		= $this->input->post('search', TRUE);
	   	$data['category']	= $this->input->post('category', TRUE);
		return $data;
	}
	
	function _validate_search()
	{
		$this->form_validation->set_rules('search', 'search', 'required');
		$this->form_validation->set_rules('category', 'category', 'required');
		return $this->form_validation->run();
	}


	// return list nama pegawai for autocomplete

	function get_list_nama_pegawai(){

		header("Content-Type: text/javascript");
		$result="var nama = [";
		$list_nama_pegawai=$this->pegawai->get_list_nama_pegawai();

		// $result="var nama = [{ value: 'Andorra', data: 'AD' },{ value: 'Zimbabwe', data: 'ZZ' },];";

		foreach ($list_nama_pegawai as $nama_pegawai) {
			$nama_pegawai->keterangan=substr($nama_pegawai->keterangan, 4,4);
			$result.='	{	value	: "'.$nama_pegawai->gelar_depan.$nama_pegawai->nama_pegawai.$nama_pegawai->gelar_belakang.'",
							data 	:{
										kd_pegawai        : "'.$nama_pegawai->kd_pegawai.'",
										id_golpangkat     : "'.$nama_pegawai->id_golpangkat_terakhir.'",
										jabatan_lama      : "'.$nama_pegawai->nama_jabatan_uj.'",
										otk				: "'.$nama_pegawai->keterangan.'",
										unit_kerja_lama   : "'.$nama_pegawai->nama_unit.'",
										id_unit_kerja_lama: "'.$nama_pegawai->kode_unit.'",
										id_jabatan_lama   : "'.$nama_pegawai->id_jabatan_uj.'"
									}
						},';
		}

		$result.="];";
		echo $result;
	}
}
