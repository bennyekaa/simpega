<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatdp3 extends Member_Controller 
{ 
	function Riwayatdp3()
	{
		parent::Member_Controller();
		
		$this->load->model('riwayat_dp3_model','riwayat_dp3');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');

		
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Riwayat DP3');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{

		$ordby = 'tahun' ;
		
		$data['list_dp3'] = $this->riwayat_dp3->find(NULL, array('kd_pegawai' => $kd_pegawai), $ordby,$start, $limit_per_page);
		
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Riwayat DP3 dari: " . $data['pegawai']['nama_pegawai'];
		
		$data['photo'] = base_url() . '/public/images/garuda.jpg';
		
		$this->template->display('pegawai/riwayatdp3/list_riwayat_dp3', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			$this->riwayat_dp3->add($data);
            
			set_success('Data DP3 pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatdp3/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | DP3 Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data DP3 dari: ' . $data['pegawai']['nama_pegawai'];
			$data['id_riwayat_dp3']=$this->riwayat_dp3->get_id();
			
			$this->template->display('/pegawai/riwayatdp3/detail_riwayat_dp3', $data);
		}
	}
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  		'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	function edit($id)
	{
	
	


		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_dp3'] = $id;
			$this->riwayat_dp3->update($id, $data);
			set_success('Perubahan data DP3 pegawai berhasil disimpan');
			redirect('/pegawai/riwayatdp3/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat DP3 :: Ubah');
			$data = $this->riwayat_dp3->retrieve_by_pkey($id);
			
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit DP3 dari: '. $data['pegawai']['nama_pegawai'];
				
				$data['photo'] = base_url() . '/public/images/garuda.jpg';
				
				$this->template->display('/pegawai/riwayatdp3/detail_riwayat_dp3', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatdp3', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_dp3->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Riwayat DP3 :: Hapus');
		confirm("Yakin menghapus data DP3?");
		$res = $this->riwayat_dp3->delete($idField);
		set_success('Data DP3 pegawai berhasil dihapus');
		redirect('/pegawai/riwayatdp3/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_dp3']	= '';
		$data['kd_pegawai']	= '';
		
		$data['unsur1']	= '0';
		$data['unsur2']	= '0';
		$data['unsur3']	= '0';
		$data['unsur4']	= '0';
		$data['unsur5']	= '0';
		$data['unsur6']	= '0';
		$data['unsur7']	= '0';
		$data['unsur8']	= '0';
		$data['unsur9']	= '0';
		$data['unsur10']	= '0';
		$data['nilai_dp3']	= '0';
		$data['tahun']	= '';
		$data['keterangan']	= '';
		/*$data['pp_nip']= ''; 
		$data['pp_nama']= ''; 
		$data['pp_golpangkat']= '';
		$data['pp_unit_kerja']= ''; 
		$data['pp_jabatan']= '';
		$data['app_nip']= ''; 
		$data['app_nama']= '';
		$data['app_golpangkat']= ''; 
		$data['app_unit_kerja']= '';
		$data['app_jabatan']= ''; */
		$data['keberatan']	= '-';
		$data['tanggapan']	= '-';
		$data['keputusan']	= '-';
		$data['user_id']	= '';
		$data['tgl_keberatan']	= '';
		$data['tgl_tanggapan']	= '';
		$data['tgl_keputusan']	= '';
		$data['tgl_buat']	= '';
		$data['tgl_terima_dinilai'] = '';
		$data['tgl_terima_penilai'] = '';
		$data['dinilai_jabatan'] = '-';
		$data['dinilai_unit'] = '-';
		return $data;
	}	
	
	function _get_form_values()
	{
		
		$data['id_riwayat_dp3']	= $this->riwayat_dp3->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		
		$data['unsur1']	= $this->input->post('unsur1', TRUE);
		$data['unsur2']	= $this->input->post('unsur2', TRUE);
		$data['unsur3']	= $this->input->post('unsur3', TRUE);
		$data['unsur4']	= $this->input->post('unsur4', TRUE);
		$data['unsur5']	= $this->input->post('unsur5', TRUE);
		$data['unsur6']	= $this->input->post('unsur6', TRUE);
		$data['unsur7']	= $this->input->post('unsur7', TRUE);
		$data['unsur8']	= $this->input->post('unsur8', TRUE);
		$data['unsur9']	= $this->input->post('unsur9', TRUE);
		$data['unsur10']	= $this->input->post('unsur10', TRUE);
		$data['nilai_total']	= $this->input->post('nilai_total', TRUE);
		$data['nilai_dp3']	= $this->input->post('nilai_dp3', TRUE);
		
		$data['tahun']	= $this->input->post('tahun', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['pp_nip']	= $this->input->post('pp_nip', TRUE); 
		$data['pp_nama'] = $this->input->post('pp_nama', TRUE); 
		$data['pp_golpangkat'] = $this->input->post('pp_golpangkat', TRUE);
		$data['pp_unit_kerja'] = $this->input->post('pp_unit_kerja', TRUE);
		$data['pp_jabatan'] = $this->input->post('pp_jabatan', TRUE);
		$data['app_nip'] = $this->input->post('app_nip', TRUE);
		$data['app_nama'] = $this->input->post('app_nama', TRUE);
		$data['app_golpangkat']	= $this->input->post('app_golpangkat', TRUE);
		$data['app_unit_kerja']	= $this->input->post('app_unit_kerja', TRUE);
		$data['app_jabatan']	= $this->input->post('app_jabatan', TRUE);
		$data['dinilai_jabatan'] = $this->input->post('dinilai_jabatan', TRUE);
		$data['dinilai_unit'] = $this->input->post('dinilai_unit', TRUE);
		
		$data['keberatan']	= $this->input->post('keberatan', TRUE);
		$data['tanggapan']	= $this->input->post('tanggapan', TRUE);
		$data['keputusan']	= $this->input->post('keputusan', TRUE);
		$data['user_id'] = $this->input->post('user_id', TRUE);
		$data['tgl_keberatan'] = $this->input->post('tgl_keberatan', TRUE);
		$data['tgl_tanggapan'] = $this->input->post('tgl_tanggapan', TRUE);
		$data['tgl_keputusan'] = $this->input->post('tgl_keputusan', TRUE);
		$data['tgl_buat'] = $this->input->post('tgl_buat', TRUE);
		$data['tgl_terima_dinilai'] = $this->input->post('tgl_terima_dinilai', TRUE);
		$data['tgl_terima_penilai'] = $this->input->post('tgl_terima_penilai', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('tahun', 'tahun', 'required');
		$this->form_validation->set_rules('nilai_dp3', 'nilai_dp3', 'required');
		if ($this->input->post('app_jabatan', TRUE)=="")
		{
			$this->form_validation->set_rules('bolehsimpan', 'bolehsimpan', 'required');}
			return $this->form_validation->run();
		}
	}