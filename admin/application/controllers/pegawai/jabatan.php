<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jabatan extends Member_Controller
{
	function Jabatan()
	{
		parent::Member_Controller();
		$this->load->model('jabatan_struktur_model', 'jabatan');
		$this->load->model('riwayat_jabatan_model', 'riwayat_jabatan');
		$this->load->model('user_pegawai_model', 'user_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Riwayat Jabatan');
		$this->browse();
	}
	
	function browse($group=0)
	{
		$pegawai_id = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
		$data['list_jabatan'] = $this->riwayat_jabatan->findAll_bykd_pegawai($pegawai_id);
		$data['judul'] 		= "Riwayat Jabatan";
		
		if (isset($this->user->user_group)){
			$this->template->display('pegawai/jabatan/list_jabatan', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_pegawai'] = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
			$this->riwayat_jabatan->add($data);
			set_success('Data riwayat jabatan berhasil disimpan.');
			redirect('/pegawai/jabatan');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat Jabatan :: Add');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Riwayat Jabatan';
			$data['id_riwayat_jabatan']=$this->riwayat_jabatan->get_id();
			$data['jabatan_assoc'] = array(0=>"-- Pilih Jabatan --")+$this->jabatan->get_assoc("jabatan");
			$this->template->display('/pegawai/jabatan/detail_jabatan', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_jabatan'] = $id;
			$data['kd_pegawai'] = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
			$this->riwayat_jabatan->update($id, $data);
			set_success('Perubahan data riwayat jabatan berhasil disimpan');
			redirect('/pegawai/jabatan', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat Jabatan :: Edit');
			$data = $this->riwayat_jabatan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Riwayat Jabatan';
				$data['jabatan_assoc'] = array(0=>"-- Pilih Jabatan --")+$this->jabatan->get_assoc("jabatan");
				$this->template->display('/pegawai/jabatan/detail_jabatan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/jabatan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_jabatan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Riwayat Jabatan :: Hapus');
		confirm("Yakin menghapus data riwayat jabatan <b>".$data['jabatan']."</b> ?");
		$res = $this->riwayat_jabatan->delete($idField);
		set_success('Data riwayat jabatan berhasil dihapus');
		redirect('/pegawai/jabatan', 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_jabatan']	= '';
		$data['id_jabatan']	= '';
		$data['kd_pegawai']	= '';
		$data['no_SK']	= '';
		$data['tgl_SK']	= '';
		$data['periode']	= '';
		$data['instansi_jabat']	= '';
		$data['ket_riwayat_jabatan']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_riwayat_jabatan']	= $this->input->post('id_riwayat_jabatan', TRUE);
	   	$data['id_jabatan']		= $this->input->post('id_jabatan', TRUE);
		$data['no_SK']	= $this->input->post('no_SK', TRUE);
		$data['tgl_SK']	= $this->input->post('tgl_SK', TRUE);
		$data['periode']	= $this->input->post('periode', TRUE);
		$data['instansi_jabat']	= $this->input->post('instansi_jabat', TRUE);
		$data['ket_riwayat_jabatan']	= $this->input->post('ket_riwayat_jabatan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('id_jabatan', 'id_jabatan', 'required');
		$this->form_validation->set_rules('no_SK', 'no_SK', 'required');
		$this->form_validation->set_rules('tgl_SK', 'tgl_SK', 'required');
		$this->form_validation->set_rules('periode', 'periode', 'required');
		$this->form_validation->set_rules('instansi_jabat', 'instansi_jabat', 'required');
		return $this->form_validation->run();
	}
}