<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayattridharma extends Member_Controller
{
	function Riwayattridharma()
	{
		parent::Member_Controller();
		
		$this->load->model('riwayat_tridharma_model','riwayat_tridharma');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model', 'lookup');
		
	}
	
	function index()
	{
		$kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | TRI DHARMA DOSEN');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		/*$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'id_riwayat_tridharma';
		*/
		$data['list_tridharma'] = $this->riwayat_tridharma->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
		/*$config['base_url']     = site_url('pegawai/riwayattridharma/browse/');
		$config['total_rows']   = $this->riwayat_tridharma->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
        */
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Penunjang Tri Dharma dari: " . $data['pegawai']['nama_pegawai'];		    
		$data['semester_assoc'] = $this->lookup->semester_assoc();
		$data['capaian_assoc'] = $this->lookup->capaian_assoc();
		if (isset($this->user->user_group)){
				$this->template->display('pegawai/riwayattridharma/list_riwayattridharma', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}

		
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			$this->riwayat_tridharma->add($data);
			set_success('Data penungjang tri dharma pegawai berhasil disimpan.');
			redirect('/pegawai/riwayattridharma/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | TRIDHARMA DOSEN :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
             $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Penunjang Tri Dharma dari: ' . $data['pegawai']['nama_pegawai'];
            
			$data['id_riwayat_tridharma']=$this->riwayat_tridharma->get_id();
			$data['semester_assoc'] = $this->lookup->semester_assoc();
			$data['capaian_assoc'] = $this->lookup->capaian_assoc();
			//$data['golongan_pangkat_assoc'] = array(0=>"-- Pilih Golongan/Pangkat --")+$this->golongan->get_assoc("id_golpangkat");
			$this->template->display('/pegawai/riwayattridharma/detail_riwayattridharma', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_tridharma'] = $id;
			$this->riwayat_tridharma->update($id, $data);
			set_success('Perubahan data pasangan pegawai berhasil disimpan');
			redirect('/pegawai/riwayattridharma/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | PENUNJANG TRI DHARMA DOSEN :: Ubah');
			$data = $this->riwayat_tridharma->retrieve_by_pkey($id);
			//$data['golongan_pangkat_assoc'] = array(0=>"-- Pilih Golongan/Pangkat --")+$this->golongan->get_assoc("id_golpangkat");
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                 $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['semester_assoc'] = $this->lookup->semester_assoc();
				$data['capaian_assoc'] = $this->lookup->capaian_assoc();
				$data['judul']='Edit Penunjang Tri Dharma dari: '. $data['pegawai']['nama_pegawai'];			
				$this->template->display('/pegawai/riwayattridharma/detail_riwayattridharma', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayattridharma', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_tridharma->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | PENUNJANG TRI DHARMA DOSEN :: Hapus');
		confirm("Yakin menghapus data tridharma pegawai?");
		$res = $this->riwayat_tridharma->delete($idField);
		set_success('Data Tri Dharma dosen berhasil dihapus');
		redirect('/pegawai/riwayattridharma/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_tridharma']	= '';
		$data['kd_pegawai']	= '';
		$data['nama_tridharma']	= '';
		$data['jns_tridharma'] = '';
		$data['nomor_surat_tugas'] = '';
		$data['tahun'] = '';
		$data['tgl_mulai']	= '';
		$data['tgl_selesai']	= '';
		$data['lama_hari']	= '';
		$data['tempat_tridharma']	= '';
		$data['keterangan']	= '';
		$data['semester' ]	= '';
		$data['tahun_ajaran'] = '';
		$data['bukti_penugasan' ] = '';
		$data['capaian'] = '';
		$data['jml_jam'] = '';
		$data['sks'] = '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   
		
		
		$data['id_riwayat_tridharma']	= $this->riwayat_tridharma->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['nama_tridharma']	= $this->input->post('nama_tridharma', TRUE);
		$data['jenis_tridharma']	= $this->input->post('jns_tridharma', TRUE);
		$data['nomor_surat_tugas']	= $this->input->post('nomor_surat_tugas', TRUE);
		$data['tahun']	= $this->input->post('tahun', TRUE);
		$data['tgl_mulai']	= $this->input->post('tgl_mulai', TRUE);
		$data['tgl_selesai']	= $this->input->post('tgl_selesai', TRUE);
		$data['lama_hari']	= $this->input->post('lama_hari', TRUE);
		$data['tempat_tridharma']	= $this->input->post('tempat_tridharma', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['semester' ]	= $this->input->post('semester', TRUE);
		$data['tahun_ajaran'] = $this->input->post('tahun_ajaran', TRUE);
		$data['bukti_penugasan' ] = $this->input->post('bukti_penugasan', TRUE);
		$data['capaian'] = $this->input->post('capaian', TRUE);
		$data['jml_jam'] = $this->input->post('jml_jam', TRUE);
		$data['sks'] = $this->input->post('sks', TRUE);		

		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_tridharma', 'nama_tridharma', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}