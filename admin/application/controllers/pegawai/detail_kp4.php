<?
$this->load->helper('form');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/pegawai/$action/";
if (!async_request()) {
?>


  
        <div id="content-header">
            <div id="module-title"><?=$judul?> <?=$pegawai['nama_pegawai']?></div>
			
				<div id="module-menu">
					<div id="tab-title"> 
<!--					<a class="icon" href="
					<?=site_url('pegawai/pegawai/cetak_kp4/'.$pegawai['kd_pegawai'])?>" title="Cetak KP4">
					<img class="noborder" src="<?=base_url().'public/images/print_pdf_kp4.jpg'?>"></a> -->
						<?php if($this->user->user_group=="Administrator") { ?>
							<a class="icon" href="<?=site_url('pegawai/pegawai/') ?>" title="Batal">
							<img class="noborder" src="<?=base_url() . 'public/images/cancel2.png' ?>"></a>
						<? } else {?>
							<a class="icon" href="<?=site_url('pegawai/caripegawai/view') ?>" title="Batal">
							<img class="noborder" src="<?=base_url() . 'public/images/cancel2.png' ?>"></a>
						<? } ?>
            		</div>
        		</div>
			
        	<div class="clear"></div>
    	</div>

    <br/>
<?php
	$centang = $_POST['centang'];
	if(empty($centang)){
?>	
        <form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?= $action_url; ?>">
         
			<br />
			<table width="723" class="general">
				<tr align="left">				 	
                	<td colspan="7" align="center">
				  
					<h1><span style="font:Arial, Helvetica, sans-serif; font-size:20px">SURAT KETERANGAN</span><br />					
					<span style="font:Arial, Helvetica, sans-serif; font-size:18px">
					UNTUK MENDAPATKAN PEMBAYARAN TUNJANGAN KELUARGA</span></h1>			   		</td>
				</tr>
                <tr align="left">				 	
                  <td colspan="7" align="left"><div align="left">Saya yang bertanda tangan di bawah ini: </div></td>
                </tr>
                <tr>
					<td width="20">1.</td>
					<td colspan="2">Nama Lengkap </td>
                    <td width="23">&nbsp;</td>
                    <td width="9"> : </td>
                   <td><?=$gelar_depan.' '.$nama_peg.' '.$gelar_belakang?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP.&nbsp;<?=$NIP;?></td>
                </tr>
                <tr>
					<td width="20">2.</td>
					<td colspan="2">Tempat / Tanggal Lahir </td>
               	    <td>&nbsp;</td>
           	      <td>:</td>
                  	<td align="left"><?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$pegawai['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
							
							if ($tanggal=='0000-00-00') {
							$str_tgl_lahir = '-';
							}
							else {
								list($y, $m, $d) = explode('-', $tanggal);
								$str_tgl_lahir = $d . " " . lookup_model::longonthname($m) . " " . $y;
							}
					?>
					<?=$tempat_lahir?>, <?=$str_tgl_lahir?></td>
                </tr>
                <tr>
					<td width="20">3.</td>
					<td colspan="2">Jenis Kelamin </td>
                    <td>&nbsp;</td>
                  <td>:</td>
                  <td align="left"><?=$jenis;?></td>
                </tr>
                <tr>
					<td width="20">4.</td>
					<td colspan="2">Agama</td>
                    <td>&nbsp;</td>
                  <td>:</td>
                  <td align="left"><?=$agama?></td>
                </tr>
                <tr>
					<td width="20">5.</td>
					<td colspan="2">Status Kepegawaian </td>
                    <td>&nbsp;</td>
                  <td>:</td>
                  <td align="left"><?=$status_pegawai_s?></td>
                </tr>
                <tr>
					<td width="20">6.</td>
					<td colspan="2">Jabatan Struktural / Fungsional </td>
                    <td>&nbsp;</td>
                  <td>:</td>
                  <td align="left"><?=$jabatan?></td>
                </tr>
                <tr>
					<td width="20">7.</td>
					<td colspan="2">Pangkat / Golongan </td>
                    <td>&nbsp;</td>
                  <td>:</td>
                  <td align="left"><?=$pangkat?> , <?=$golongan?></td>
                </tr>
                <tr>
					<td width="20">8.</td>
					<td colspan="2">Pada Instansi, Dep. / Lembaga </td>
                    <td>&nbsp;</td>
                  <td>:</td>
                  <td align="left">Universitas Negeri Malang </td>
                </tr>
                <tr>
					<td width="20">9.</td>
					<td colspan="2">Masa Kerja Golongan </td>
                    <td>&nbsp;</td>
                  <td>:</td>
                  <td align="left"><?=$masa_kerja_golongan?> &nbsp;&nbsp;&nbsp;Masa Kerja Tambahan: <?=$masa_kerja_tambahan?></td>
                </tr>
				 <tr>
				 <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="206">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td align="left">Masa Kerja Seluruhnya: <?=$masa_kerja_seluruhnya?></td>
                </tr>
                <tr>
				<td width="20">10.</td>
				<td colspan="5">Di Gaji menurut PP No 30 Tahun 2015 dengan gaji pokok Rp. 
			      <?php
			        $sql = "select * from acuan_gaji_pokok where masa_kerja<='".$data_tahun."' and id_golpangkat='".$pangkat_id."' and tahun='2015'";
			        echo $sql;
					$query = mysql_query($sql);										
					
					$datasen=mysql_fetch_array($query); 
					if ($datasen!=FALSE){
							$gaji_pokok_asli = $datasen['gaji_pokok'];
						}
					if ($status_pegawai_s=='PNS') {
					echo number_format($gaji_pokok_asli,0,',','.'); 
					}
					else {
					//$gaji_pokok_asli_cpns = 1.25 * $gaji_pokok_asli;
					$gaji_pokok_asli_cpns = $gaji_pokok_asli;
					echo "80% x ".number_format($gaji_pokok_asli_cpns,0,',','.').",- = Rp. ".number_format($gaji_pokok_asli,0,',','.');
					}
					?>,-
                </tr>
                <tr>
                  <td>11.</td>
				  <td colspan="2">Alamat / tempat tinggal </td>
                  <td>&nbsp;</td>
                  <td  align="left">:</td>
                  <td  align="left"><?=$alamat?> , Telp. <?=$no_telp?></td>
                </tr>
				<?php
					$query = mysql_query("select nama_kelurahan,nama_kecamatan,nama_kabupaten,nama_propinsi from 
					kelurahan,kecamatan,kabupaten where kelurahan.kd_kecamatan=kecamatan.kd_kecamatan and
					kecamatan.kd_kabupaten=kabupaten.kd_kabupaten and kd_kelurahan='".$kelurahan_id."'");										
					$datakel=mysql_fetch_array($query); 
					if ($datakel!=FALSE){
							$namakel = $datakel['nama_kelurahan'];
							$namakec = $datakel['nama_kecamatan'];
							$namakab = $datakel['nama_kabupaten'];
						}
					?>
				 <tr>
   				  <td width="20">&nbsp;</td>
                  <td colspan="6" width="620"  align="left">
				  <table border="0" cellpadding="2" cellspacing="2">
						  <tr>
						  	<td width="130" align="center">Kode Pos</td>
							<td width="130" align="center">Kota/Kab</td>
							<td width="130" align="center">Provinsi</td>
							<td width="190" align="center">Email</td>
						  </tr>
					  </table>
				  </td>
                </tr>
				<tr>
                  <td width="20">&nbsp;</td>
                  <td colspan="6" width="620"  align="left">
					  <table border="1" cellpadding="2" cellspacing="2">
						  <tr>
						  	<td width="130"><?=$kode_pos?></td>
							<td width="130"><?=$namakab?></td>
							<td width="130">Jawa Timur</td>
							<td width="190"><?=$email?></td>
						  </tr>
					  </table>
				  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
				  <td colspan="5">Menerangkan dengan sesungguhnya bahwa saya: </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
				  <td width="17">a.</td>
                  <td colspan="4"  align="left">disamping jabatan utama tersebut, bekerja pula sebagai : 
				  <input size="40" type="text" name="kp4_11a" id="kp4_11a" onchange="gsub_test(this)" onblur="gsub_test(this)" value="<?= set_value('kp4_11a', $pegawai['kp4_11a']); ?>" class="js_required required"/></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="4"  align="left">dengan mendapat penghasilan sebesar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp.
                  <input size="30" type="text" name="kp4_11b" id="kp4_11b" onchange="gsub_test(this)" onblur="gsub_test(this)" value="<?= set_value('kp4_11b', $pegawai['kp4_11b']); ?>" class="js_required required"/>                    &nbsp;sebulan</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>b.</td>
                  <td colspan="4"  align="left">mempunyai pensiun / pensiun janda sebesar Rp.
                    <input size="30" type="text" name="kp4_11c" id="kp4_11c" onchange="gsub_test(this)" onblur="gsub_test(this)" value="<?= set_value('kp4_11c', $pegawai['kp4_11c']); ?>" class="js_required required"/>                    &nbsp;sebulan</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>c.</td>
                  <td colspan="4"  align="left">mempunyai susunan keluarga sbb: </td>
                </tr>
                <tr>
                  <td align="left">&nbsp;</td>
				  <td align="left">&nbsp;</td>
				  <td colspan="4" align="left">
				  <table width="90%" class="table-list" align="left">
				  <tr class="trhead">
					<th width="39" rowspan="2" align="center" class="trhead">No</th>
					<th width="218" rowspan="2" class="trhead">Nama istri / suami / anak<br />
					  Tanggungan</th>
					<th colspan="2" align="center" class="trhead">Tanggal</th>
					<th width="167" rowspan="2" align="center" class="trhead">Pekerjaan / sekolah </th>
					<th width="167" rowspan="2" align="center" class="trhead">Keterangan<br />
					  (AK, AT, AA) </th>
				  </tr>
				  <tr class="trhead">
				    <th align="center" class="trhead">Kelahiran</th>
				    <th align="center" class="trhead">Perkawinan</th>
				  </tr>
				  <?
						$i = 0;
						if ($list_pasangan!=FALSE){
						foreach ($list_pasangan as $jns_keluarga) {
						$i++;
						if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
						if ($jns_keluarga['tanggal_lahir']=='0000-00-00') {
						$str_tanggal_lahir = '-';
						}
						else {
							list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_lahir']);
							$str_tanggal_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
						}
						$jns_keluarga['tanggal_lahir']=$str_tanggal_lahir;
						
						if ($jns_keluarga['tanggal_nikah']=='0000-00-00') {
							$str_tanggal_nikah = '-';
						}
						else {
							list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_nikah']);
							$str_tanggal_nikah = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
						}
						$jns_keluarga['tanggal_nikah']=$str_tanggal_nikah;
				  ?>
				  <tr class="<?=$bgColor?>">
				    <td class="trhead" align="left"><?=$i;?></td>
				    <td class="trhead"><?= $jns_keluarga['nama_pasangan']; ?></td>
				    <td width="143" align="center" class="trhead"><?= $jns_keluarga['tanggal_lahir']; ?></td>
				    <td width="124" align="center" class="trhead"><?= $jns_keluarga['tanggal_nikah']; ?></td>
				    <td class="trhead" align="center"><?= $jns_keluarga['pekerjaan']; ?></td>
				    <td class="trhead" align="center"><?= $status_keluarga_assoc[$jns_keluarga['kd_status_keluarga']]; ?>
					</td>
				    </tr>
					 
				  <? 	
				  		}
						}
						else {?>
						<tr class="<?=$bgColor?>">
							<td class="trhead" align="center" width="18"><strong>1</strong></td>
							<td width="140" class="trhead">&nbsp;<strong></strong></td>
							<td width="75" align="center" class="trhead"><strong></strong></td>
							<td width="75" align="center" class="trhead"><strong></strong></td>
							<td width="70" class="trhead" align="center"><strong></strong></td>
							<td width="76" class="trhead" align="center"></td>
						</tr>		
							
						<?
						}?>
						<?
			$j = $i;
			$x=0;
			if ($list_anak!=FALSE){
		foreach ($list_anak as $jns_anak) {
		   $j++;
		   $x++;
		   if (($j%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			if ($jns_anak['tgl_lahir']=='0000-00-00') {
                $str_tgl_lahir = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_anak['tgl_lahir']);
                $str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
            }
            $jns_anak['tgl_lahir']=$str_tgl_lahir;
		
		 ?>
          <tr class="<?=$bgColor?>">
            <td align="left" class="colEvn"><?=$j;?></td>
            <td><?= $jns_anak['nama_anak']; ?></td>
            <td align="center"><?= $jns_anak['tgl_lahir']; ?></td>
            <td align="center">-</td>
            <td align="center"><?= $jns_anak['pekerjaan']; ?> <?= $pendidikan_assoc[$jns_anak['id_pendidikan_anak']]; ?></td>
            <td align="center"><?= $status_keluarga_assoc[$jns_anak['kd_status_keluarga']]; ?> <input type="checkbox" name="centang[<?= $x; ?>]" value="<?=$jns_anak['kd_anak']?>" <? if ($jns_anak['aktif']==1){?>  checked="checked" /> <? } ?> </td>
          </tr>
          <? } ?>
          <?}
	else {
		//echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
				  </table>				  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>d.</td>
				  
				 
                  <td colspan="4"  align="left">Jumlah Anak : <?=$jumlah_anak_masuk_daftar?> orang &nbsp;&nbsp; <strong>(yang menjadi tanggungan dan yang masuk dalam daftar gaji diberi centang)</strong> </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>e.</td>
                  <td colspan="4"  align="left">Jumlah Anak : 
                  <?=$jumlah_anak?> orang &nbsp;&nbsp; <strong>(yang menjadi tanggungan termasuk yang tidak masuk dalam daftar gaji).</strong></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="4"  align="left">Keterangan ini saya buat dengan sesungguhnya dan apabila&nbsp; keterangan ini ternyata tidak benar (palsu) saya<br /> bersediadituntut dimuka pengadilan berdasarkan Undang-undang yang berlaku, dan bersedia mengembalikan<br /> semua penghasilan yang telah saya terima yang seharusnya bukan menjadi hak saya. </td>
                </tr>
		  </table>
            <br>
            <div class="submit" style="text-align:center">
			<input id="submit" size="100px" name="submitkp4" type="submit" value="  Cetak " /></div>
        </form>
   <br />
   <br />
<? }
}
//untuk centang gaji
if(is_array($centang)){
	//echo '<b>centang yang anda pilih : <br />';				
	$centang=$_POST['centang'];
	$s=0;
	$search_param = array();
	if ($centang){
		foreach ($centang as $id_anakgaji){	
			//echo $id_anakgaji;
				$sql="UPDATE  `anak_pegawai` SET aktif = '1' where kd_anak='".$id_anakgaji."'"; 
			//echo $sql.' <br />';
			$query = mysql_query($sql);	
			//set_success('Data isian tangguangan anak berhasil disimpan');
			//redirect('pegawai/detiltugas/browse/'.$id_riwayat_tugas.'/'.$kd_pegawai);
            $search_param[] = "kd_anak not like '".$id_anakgaji."'";
		}
		$search_param = implode(" AND ", $search_param);
	}	
	$sql2="UPDATE  `anak_pegawai` SET aktif = '0' where $search_param"; 
	$query2 = mysql_query($sql2);	
	//echo $sql2;
}
?>

<?php
				
if((isset($_POST['submitkp4'])))	
{
$sql="UPDATE `pegawai`  SET  kp4_11a='".$_POST['kp4_11a']."', kp4_11b='".$_POST['kp4_11b']."', kp4_11c='".$_POST['kp4_11c']."'  WHERE kd_pegawai = '".$kd_pegawai."'";
	$query = mysql_query($sql);
	redirect('pegawai/pegawai/cetak_KP4/'.$kd_pegawai);
}
?>



		
