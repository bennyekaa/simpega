<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Arsipstatistik extends Member_Controller
{
	var $pegawai_id;
	var $groupuser;
	function Arsipstatistik()
	{
		parent::Member_Controller();
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model', 'lookup');
		$this->load->model('user_pegawai_model', 'user_pegawai');
		$this->load->model('arsip_statistik_model', 'arsip_statistik');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Arsip Data Pegawai');
		$this->cari();
	}
	
	function cari()
	{
		if ($this->input->post('group')){
			$group = $this->input->post('group');
		}
		else {
			$group =  $this->lookup->longonthname(date('m'));
		}
		
		if ($this->input->post('tahun')){
			$tahun = $this->input->post('tahun');
		}
		else {
			$tahun =  date('Y');
		}
		
		if ($this->input->post('id_pengarsip')){
			$id_pengarsip = $this->input->post('id_pengarsip');
		}
		else {
			$id_pengarsip =  date('Y');
		}
		
		//apakah sudah ditekan tombol cari?
		if ($this->_validate()){	
			$query_pegawai = mysql_query("select  * from pegawai where 
			kd_pegawai is not NULL order by kd_pegawai");
			$row_count = mysql_num_rows($query_pegawai);
			if ($row_count>0){	
				//lakukan pengecekan apakah data periode tersebut sudah diarsipkan
				$query_arsip = mysql_query("select  * from arsip_statistik where 
				periode='".$tahun.$group."'");
				$row_count = mysql_num_rows($query_arsip);
				if ($row_count>0){					
					$query_not_arsip = mysql_query("select  * from arsip_statistik where 
					kd_pegawai not in (select kd_pegawai from pegawai) and periode='".$tahun.$group."'");
					$row_count = mysql_num_rows($query_not_arsip);
					//1.lakukan looping untuk menghapus data pegawai tersebut dalam tabel arsip_statistik
					//  yang tidak ada lama tabel pegawai	
					if ($row_count>0){
						while ($data_not_arsip=mysql_fetch_array($query_not_arsip))
						{
							$idField=$data_not_arsip['id_statistik'];
							//$data = $this->agama->retrieve_by_pkey($idField);		
							$this->template->metas('title', 'SIMPEGA | Data Statistik :: Hapus');
							confirm("Yakin menghapus arsip statistik NIP. <b>".$data_not_arsip['NIP']."</b> ?");
							$this->arsip_statistik->delete($idField);
						}
					}
					else {
					//id_statistik, Periode, kd_pegawai, NIP, jns_kelamin, id_jns_pegawai, status_pegawai, kode_unit, 
					//kode_unit_induk, id_golpangkat_terakhir, id_pendidikan_terakhir, id_pengarsip
						$query_in_arsip = mysql_query("select   kd_pegawai, NIP, jns_kelamin, id_jns_pegawai, 
						status_pegawai, kode_unit, kode_unit_induk, id_golpangkat_terakhir, id_pendidikan_terakhir 
						from pegawai where kd_pegawai in (select kd_pegawai from arsip_statistik where periode='".$tahun.$group."')");
						$row_count = mysql_num_rows($query_in_arsip);
						if ($row_count>0){
							while ($data_in_arsip=mysql_fetch_array($query_in_arsip))
							{
							 $query = mysql_query("update arsip_statistik 
								set NIP='".$data_in_arsip['NIP']."', jns_kelamin='".$data_in_arsip['jns_kelamin']."', 
								id_jns_pegawai='".$data_in_arsip['id_jns_pegawai']."', status_pegawai='".$data_in_arsip['status_pegawai']."', 
								kode_unit='".$data_in_arsip['kode_unit']."', kode_unit_induk='".$data_in_arsip['kode_unit_induk']."', 
								id_golpangkat_terakhir='".$data_in_arsip['id_golpangkat_terakhir']."', 
								id_pendidikan_terakhir ='".$data_in_arsip['id_pendidikan_terakhir']."', id_pengarsip='".$id_pengarsip."'
								where kd_pegawai = '".$data_in_arsip['kd_pegawai']."' and periode='".$tahun.$group."'");	
							}
						}
						
					}
					//2.lakukan looping untuk menyimpan data pegawai yg belum masuk ke dalam tabel arsip_statistik
					$query_notin_arsip = mysql_query("select  * from pegawai where 
					kd_pegawai not in (select kd_pegawai from arsip_statistik where periode='".$tahun.$group."')");
					$row_count = mysql_num_rows($query_notin_arsip);
					if ($row_count>0){	
						while ($data_notin_arsip=mysql_fetch_array($query_notin_arsip))
						{
							$data_isian['kd_pegawai']=$data_notin_arsip['kd_pegawai']; 
							$data_isian['NIP']=$data_notin_arsip['NIP']; 
							$data_isian['jns_kelamin']=$data_notin_arsip['jns_kelamin'];
							$data_isian['id_jns_pegawai']=$data_notin_arsip['id_jns_pegawai'];
							$data_isian['status_pegawai']=$data_notin_arsip['status_pegawai'];
							$data_isian['kode_unit']=$data_notin_arsip['kode_unit'];
							$data_isian['kode_unit_induk']=$data_notin_arsip['kode_unit_induk'];
							$data_isian['id_golpangkat_terakhir']=$data_notin_arsip['id_golpangkat_terakhir'];
							$data_isian['id_pendidikan_terakhir']=$data_notin_arsip['id_pendidikan_terakhir'];
							$data_isian['periode']=$tahun.$group;
							$data_isian['id_pengarsip']=$id_pengarsip;
							
							//$data_akhir=$data_notin_arsip;	
							$data_akhir=$data_notin_arsip;	
							$this->arsip_statistik->add($data_isian);
						}
					}
					$data['ada_arsip'] = 'Data statistik pegawai periode '.$tahun.$group.' telah sukses diperbarui!';
				}
				else{
					//lakukan looping untuk menyimpan data pegawai tersebut dalam tabel arsip_statistik							
					while ($data_pegawai=mysql_fetch_array($query_pegawai))
					{
						$data_isian['kd_pegawai']=$data_pegawai['kd_pegawai']; 
						$data_isian['NIP']=$data_pegawai['NIP']; 
						$data_isian['jns_kelamin']=$data_pegawai['jns_kelamin'];
						$data_isian['id_jns_pegawai']=$data_pegawai['id_jns_pegawai'];
						$data_isian['status_pegawai']=$data_pegawai['status_pegawai'];
						$data_isian['kode_unit']=$data_pegawai['kode_unit'];
						$data_isian['kode_unit_induk']=$data_pegawai['kode_unit_induk'];
						$data_isian['id_golpangkat_terakhir']=$data_pegawai['id_golpangkat_terakhir'];
						$data_isian['id_pendidikan_terakhir']=$data_pegawai['id_pendidikan_terakhir'];
						$data_isian['periode']=$tahun.$group;
						$data_isian['id_pengarsip']=$id_pengarsip;
						
						$data_akhir=$data_isian;	
						$this->arsip_statistik->add($data_akhir);
					}
					$data['ada_arsip'] = 'Data statistik pegawai periode '.$tahun.$group.' telah tersimpan!';
				}
			}
			else
			{
			$data['ada_arsip'] = 'Data statistik pegawai periode '.$tahun.$group.' kosong!';
			}
			$data['judul']='Pengarsipan Data Statistik Pegawai';
			$this->template->display('/pegawai/arsipstatistik/view_arsipstatistik',$data);
		}	
		else {
			$this->template->metas('title','SIMPEGA | Pengarsipan Data Statistik');
			$data['judul']='Pengarsipan Data Statistik Pegawai';
			$data['action']='cari';
			$data['group_assoc'] = array(date('m') => $this->lookup->longonthname(date('m')));
			$data['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y'), date('Y'));
			$this->template->display('/pegawai/arsipstatistik/search_arsipstatistik',$data);
		}
	}
	
	function _clear_form()
	{
		$data['is_new'] = '';
		$data['id_pengarsip']	= '';
		$data['group']	= '';
		$data['tahun']	= '';
		return $data;
	}
	
	function _get_form_values()
	{
	   	$data['id_pengarsip']		= $this->input->post('id_pengarsip', TRUE);
	   	$data['periode']	= $this->input->post('tahun', TRUE).$this->input->post('group', TRUE);
		$data['group']	= $this->input->post('group', TRUE);
		$data['tahun']	= $this->input->post('tahun', TRUE);
		return $data;
	}
	
	
	function _validate()
	{
		$this->form_validation->set_rules('id_pengarsip', 'id_pengarsip', 'required');
		$this->form_validation->set_rules('group', 'group', 'required');
		$this->form_validation->set_rules('tahun', 'tahun', 'required');
		return $this->form_validation->run();
	}
	
}
