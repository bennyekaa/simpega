<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatpenghargaan extends Member_Controller
{
	function Riwayatpenghargaan()
	{
		parent::Member_Controller();
		
		$this->load->model('riwayat_penghargaan_model','riwayat_penghargaan');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');
		$this->load->model('jenis_penghargaan_model','jenis_penghargaan');	
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Riwayat Penghargaan');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		$ordby = 'tahun, id_riwayat_penghargaan';
		$data['list_penghargaan'] = $this->riwayat_penghargaan->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Penghargaan dari: " . $data['pegawai']['nama_pegawai'];
		$data['jenis_penghargaan_assoc'] = $this->jenis_penghargaan->get_assoc('nama_penghargaan');
		
		if (isset($this->user->user_group)){
		$this->template->display('pegawai/riwayatpenghargaan/list_riwayat_penghargaan', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}

	}
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			$this->riwayat_penghargaan->add($data);
            
			set_success('Data penghargaan pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatpenghargaan/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Penghargaan Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Penghargaan dari: ' . $data['pegawai']['nama_pegawai'];
			$data['id_riwayat_penghargaan']=$this->riwayat_penghargaan->get_id();
			$data['jenis_penghargaan_assoc'] = $this->jenis_penghargaan->get_assoc('nama_penghargaan');
			$this->template->display('/pegawai/riwayatpenghargaan/detail_riwayat_penghargaan', $data);
		}
	}
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  		'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_penghargaan'] = $id;
			$this->riwayat_penghargaan->update($id, $data);
			set_success('Perubahan data penghargaan pegawai berhasil disimpan');
			redirect('/pegawai/riwayatpenghargaan/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Penghargaan :: Ubah');
			$data = $this->riwayat_penghargaan->retrieve_by_pkey($id);
			$data['jenis_penghargaan_assoc'] = $this->jenis_penghargaan->get_assoc('nama_penghargaan');
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Penghargaan dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/riwayatpenghargaan/detail_riwayat_penghargaan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatpenghargaan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_penghargaan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Penghargaan :: Hapus');
		confirm("Yakin menghapus data penghargaan?");
		$res = $this->riwayat_penghargaan->delete($idField);
		set_success('Data Penghargaan pegawai berhasil dihapus');
		redirect('/pegawai/riwayatpenghargaan/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_penghargaan']	= '';
		$data['kd_pegawai']	= '';
		$data['jenis_penghargaan']	= '';
		$data['tahun']	= '';
		$data['keterangan']	= '';
	
		return $data;
	}	
	
	function _get_form_values()
	{
		
		$data['id_riwayat_penghargaan']	= $this->riwayat_penghargaan->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['jenis_penghargaan']	= $this->input->post('jenis_penghargaan', TRUE);
		$data['tahun']	= $this->input->post('tahun', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('tahun', 'tahun', 'required');
		return $this->form_validation->run();
	}
}