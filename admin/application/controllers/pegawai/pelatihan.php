<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pelatihan extends Member_Controller
{
	function Pelatihan()
	{
		parent::Member_Controller();
		$this->load->model('jenis_pelatihan_model', 'jenis_pelatihan');
		$this->load->model('riwayat_pelatihan_model', 'riwayat_pelatihan');
		$this->load->model('user_pegawai_model', 'user_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Riwayat Pelatihan');
		$this->browse();
	}
	
	function browse($group=0)
	{
		$pegawai_id = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
		$data['list_pelatihan'] = $this->riwayat_pelatihan->retrieve_by_idpeg($pegawai_id);
		//show_error(var_dump($data));
		$data['judul'] 		= "Riwayat Pelatihan";
		
		if (isset($this->user->user_group)){
			$this->template->display('pegawai/pelatihan/list_pelatihan', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_pegawai'] = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
			//show_error(var_dump($data));
			$this->riwayat_pelatihan->add($data);
			set_success('Data riwayat pelatihan berhasil disimpan.');
			redirect('/pegawai/pelatihan');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat Pelatihan :: Add');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Riwayat Pelatihan';
			$data['jenis_pelatihan_assoc'] = array(0=>'-- Pilih Pelatihan --')+$this->jenis_pelatihan->get_assoc("jenis_pelatihan");
			$data['id_riwayat_pelatihan']=$this->riwayat_pelatihan->get_id();
			$this->template->display('/pegawai/pelatihan/detail_pelatihan', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_pelatihan'] = $id;
			$data['kd_pegawai'] = $this->user_pegawai->get_idpegawai_by_userid($this->user->user_id);
			//show_error(var_dump($data));
			$this->riwayat_pelatihan->update($id, $data);
			set_success('Perubahan data riwayat pelatihan berhasil disimpan');
			redirect('/pegawai/pelatihan', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat Pelatihan :: Edit');
			$data = $this->riwayat_pelatihan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Riwayat Pelatihan';
				$data['jenis_pelatihan_assoc'] = array(0=>'-- Pilih Pelatihan --')+$this->jenis_pelatihan->get_assoc("jenis_pelatihan");
				$this->template->display('/pegawai/pelatihan/detail_pelatihan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/pelatihan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_pelatihan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Riwayat Pelatihan :: Hapus');
		confirm("Yakin menghapus data riwayat pelatihan <b>".$data['nama_pelatihan']."</b> ?");
		$res = $this->riwayat_pelatihan->delete($idField);
		set_success('Data riwayat pelatihan berhasil dihapus');
		redirect('/pegawai/pelatihan', 'location');
	}

	function _clear_form()
	{
	   	$data['id_riwayat_pelatihan']	= '';
	   	$data['tgl_mulai']		= '';
		$data['tgl_akhir']		= '';
		$data['no_sk']		= '';
		$data['tgl_sk']		= '';
		$data['FK_jns_pelatihan']		= '';
		$data['nama_pelatihan']	= '';
		$data['nama_lembaga']	= '';
		$data['ket_riwayat_pelatihan']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_riwayat_pelatihan']	= $this->input->post('id_riwayat_pelatihan', TRUE);
	   	$data['tgl_mulai']		= $this->input->post('tgl_mulai', TRUE);
		$data['tgl_akhir']		= $this->input->post('tgl_akhir', TRUE);
		$data['no_sk']		= $this->input->post('no_sk', TRUE);
		$data['tgl_sk']		= $this->input->post('tgl_sk', TRUE);
		$data['FK_jns_pelatihan']		= $this->input->post('FK_jns_pelatihan', TRUE);
		$data['nama_pelatihan']	= $this->input->post('nama_pelatihan', TRUE);
		$data['nama_lembaga']	= $this->input->post('nama_lembaga', TRUE);
		$data['ket_riwayat_pelatihan']	= $this->input->post('ket_riwayat_pelatihan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('no_sk', 'no_sk', 'required');
		$this->form_validation->set_rules('tgl_sk', 'tgl_sk', 'required');
		$this->form_validation->set_rules('FK_jns_pelatihan', 'FK_jns_pelatihan', 'required');
		$this->form_validation->set_rules('nama_pelatihan', 'nama_pelatihan', 'required');
		return $this->form_validation->run();
	}
}