<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatangkakredit extends Member_Controller
{
	function Riwayatangkakredit()
	{
		parent::Member_Controller();
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('riwayat_angka_kredit_model','riwayat_angka_kredit');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');
		$this->load->model('lap_gaji_model','lap_gaji');
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Angka Kredit Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		$ordby = 'id_rincian_ak';
		$data['list_ak'] = $this->riwayat_angka_kredit->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Angka Kredit dari: " . $data['pegawai']['nama_pegawai'];
		$data['golongan_assoc'] = $this->golongan->get_assoc();
		$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
		$this->template->display('pegawai/riwayatangkakredit/list_riwayat_ak', $data);
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			
				//$data_pegawai['angka_kredit']= $data['jumlah'];
				
				//$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
			
			$this->riwayat_angka_kredit->add($data);
            
			set_success('Data angka kredit pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatangkakredit/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Angka Kredit Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Angka Kredit dari: ' . $data['pegawai']['nama_pegawai'];
			$data['id_rincian_ak']=$this->riwayat_angka_kredit->get_id();
			$data['status_assoc'] = $this->lookup->status_assoc();
			$this->template->display('/pegawai/riwayatangkakredit/detail_riwayat_ak', $data);
		}
	}
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  		'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_gol'] = $id;
			$kd_pegawai = $this->input->post('kd_pegawai');
			if ($data['aktif']=='1')
            {
				
				$data_pegawai['angka_kredit']= $data['jumlah'];
				
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				$data_status['aktif'] = '0';
				
				$this->riwayat_angka_kredit->update_status($kd_pegawai,$data_status);
				
			}
				$this->riwayat_angka_kredit->update($id, $data);
			set_success('Perubahan data angka kredit pegawai berhasil disimpan');
			redirect('/pegawai/riwayatangkakredit/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Angka Kredit Pegawai :: Ubah');
			$data = $this->riwayat_angka_kredit->retrieve_by_pkey($id);
			$data['golongan_assoc'] = $this->golongan->get_assoc();
            $data['status_assoc'] = $this->lookup->status_assoc();
			$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
			
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Kepangkatan dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/riwayatangkakredit/detail_riwayat_ak', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatangkakredit', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_angka_kredit->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Angka Kredit Pegawai :: Hapus');
		confirm("Yakin menghapus data angka kredit pegawai?");
		$res = $this->riwayat_angka_kredit->delete($idField);
		set_success('Data Angka Kredit pegawai berhasil dihapus');
		redirect('/pegawai/riwayatangkakredit/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_rincian_ak']	= '';
		$data['kd_pegawai']	= '';
		$data['tmt_ak']	= '';
		$data['angka_kredit']	= '';
	
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   
		$data['id_rincian_ak']	= $this->riwayat_angka_kredit->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		
		$data['tmt_ak']	= $this->input->post('tmt_ak', TRUE);
		
		$data['angka_kredit']	= $this->input->post('angka_kredit', TRUE);
		return $data;
	}
	
	function _validate()
	{
		//$this->form_validation->set_rules('id_golpangkat', 'id_golpangkat', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}