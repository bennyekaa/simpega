<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DP3 extends Member_Controller
{

	function DP3()
	{
		parent::Member_Controller();
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('riwayat_dp3_model', 'riwayat_dp3');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | DP3 Pegawai');
		$this->browse();
	}
	
	function browse()
	{	
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'kd_pegawai';
		
		$data['list_pegawai'] = $this->pegawai->getAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('pegawai/dp3/browse/');
		$config['total_rows']   = $this->pegawai->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
	
		$data['judul'] 			= "DP3 Pegawai";
		$this->template->display('pegawai/dp3/list', $data);
	}
		
	function view()
	{
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$id_peg = $this->uri->segment(4);
		$limit_per_page = 10;
		$ordby = 'no_urut';
		
		$pegawai = $this->pegawai->get_assoc("nama");
		$search ="FK_pegawai = '".$id_peg."'";
		$data['list_dp3'] 		= $this->riwayat_dp3->findByFilter($search,$ordby,$limit_per_page,$start);
		
		if ($data['list_dp3'])
		{
			foreach($data['list_dp3'] as $tmp)
			{
				$tmp['pejabat']		= $pegawai[$tmp['FK_pejabat']];
				$tmp['atasan']		= $pegawai[$tmp['FK_atasan_pejabat']];
				$dat[] = $tmp; 
			}
		}
		$data['list_dp3'] = $dat;
		//show_error(var_dump($data));
		$config['base_url']     = site_url('pegawai/dp3/view/'.$id_peg.'/');
		$config['total_rows']   = $this->riwayat_dp3->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['id_peg']			= $id_peg;
		$data['judul'] 			= "DP3 Pegawai";
		$this->template->display('pegawai/dp3/detail-list', $data);
	}
	
	function add()
	{
		$id = $this->uri->segment(4);
		if ($this->_validate())
		{	
			$data = $this->_get_form_values();
			$jum = 0;
			$jum = intval($data['kesetiaan'])+intval($data['prestasi'])+intval($data['tanggungjawab'])+
					intval($data['taat'])+intval($data['jujur'])+intval($data['kerjasama'])+intval($data['prakarsa'])+intval($data['pemimpin']);
			if($data['pemimpin'] =='' or $data['pemimpin'] ==0 or $data['pemimpin'] ==FALSE)
			{
				$rata = $jum / 7;
			}
			else
			{	
				$rata = $jum / 8;
			}
			$data['jmlh'] = $jum;
			$data['rata_rata'] = $rata;
			//show_error(var_dump($data));
			$this->riwayat_dp3->add($data);
			set_success('Data riwayat dp3 berhasil disimpan.');
			redirect('/pegawai/dp3/view/'.$id);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | DP3 Pegawai :: Add');
			$data = $this->_clear_form();
			$data['action']='add/'.$id;
			$data['judul']='Tambah DP3';
			$data['id_dp3']= $this->riwayat_dp3->get_id();
			$data['FK_pegawai'] = $id;
			$data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
			$data['pejabat_assoc'] = $this->pegawai->get_assoc("nama");
			$data['a_pejabat_assoc'] = $this->pegawai->get_assoc("nama");
			$data['no_urut']= 1 + intval($this->riwayat_dp3->get_max_no_urut($id));
			$this->template->display('/pegawai/dp3/detail', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_dp3'] = $id;
			$jum = 0;
			$jum = intval($data['kesetiaan'])+intval($data['prestasi'])+intval($data['tanggungjawab'])+
					intval($data['taat'])+intval($data['jujur'])+intval($data['kerjasama'])+intval($data['prakarsa'])+intval($data['pemimpin']);
			if($data['pemimpin'] =='' or $data['pemimpin'] ==0 or $data['pemimpin'] ==FALSE)
			{
				$rata = $jum / 7;
			}
			else
			{	
				$rata = $jum / 8;
			}
			$data['jmlh'] = $jum;
			$data['rata_rata'] = $rata;
			$this->riwayat_dp3->update($id, $data);
			set_success('Perubahan data riwayat DP3 berhasil disimpan');
			redirect('/pegawai/dp3/view/'.$data['FK_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Riwayat riwayat_dp3 :: Edit');
			$data = $this->riwayat_dp3->retrieve_by_pkey($id);
			$data['action'] = 'edit/'.$id;
			$data['judul']='Edit DP3';
			$data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['FK_pegawai']);
			$data['pejabat_assoc'] = $this->pegawai->get_assoc("nama");
			$data['a_pejabat_assoc'] = $this->pegawai->get_assoc("nama");
			$this->template->display('/pegawai/dp3/detail', $data);
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_dp3->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Riwayat riwayat_dp3 :: Hapus');
		confirm("Yakin menghapus data dp3 <b>".$data['no_urut']."</b> ?");
		$res = $this->riwayat_dp3->delete($idField);
		set_success('Data dp3 pegawai berhasil dihapus');
		redirect('/pegawai/dp3/view', 'location');
	}

	function _clear_form()
	{
	   	$data['id_dp3']				= '';
		$data['FK_pegawai']			= '';
		$data['no_urut']			= '';
		$data['FK_pejabat']			= '';
	   	$data['FK_atasan_pejabat']	= '';
	   	$data['tmt_dp3']			= '';
		$data['kesetiaan']			= '';
		$data['prestasi']			= '';
		$data['tanggungjawab']		= '';
		$data['taat']				= '';
	   	$data['jujur']				= '';
		$data['kerjasama']			= '';
		$data['prakarsa']			= '';
		$data['pemimpin']			= '';
		$data['jmlh']				= '';
	   	$data['rata_rata']			= '';
		$data['keberatan']			= '';
		$data['tanggapan']			= '';
		$data['putusan']			= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_dp3']				= $this->input->post('id_dp3', TRUE);
		$data['FK_pegawai']			= $this->input->post('FK_pegawai', TRUE);
		$data['no_urut']			= $this->input->post('no_urut', TRUE);
		$data['FK_pejabat']			= $this->input->post('FK_pejabat', TRUE);
	   	$data['FK_atasan_pejabat']	= $this->input->post('FK_atasan_pejabat', TRUE);
	   	$data['tmt_dp3']			= $this->input->post('tmt_dp3', TRUE);
		$data['kesetiaan']			= $this->input->post('kesetiaan', TRUE);
		$data['prestasi']			= $this->input->post('prestasi', TRUE);
		$data['tanggungjawab']		= $this->input->post('tanggungjawab', TRUE);
		$data['taat']				= $this->input->post('taat', TRUE);
	   	$data['jujur']				= $this->input->post('jujur', TRUE);
		$data['kerjasama']			= $this->input->post('kerjasama', TRUE);
		$data['prakarsa']			= $this->input->post('prakarsa', TRUE);
		$data['pemimpin']			= $this->input->post('pemimpin', TRUE);
		$data['keberatan']			= $this->input->post('keberatan', TRUE);
		$data['tanggapan']			= $this->input->post('tanggapan', TRUE);
		$data['putusan']			= $this->input->post('putusan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('FK_pegawai', 'FK_pegawai', 'required');
		$this->form_validation->set_rules('FK_pejabat', 'FK_pejabat', 'required');
		$this->form_validation->set_rules('FK_atasan_pejabat', 'FK_atasan_pejabat', 'required');
		$this->form_validation->set_rules('tmt_dp3', 'tmt_dp3', 'required');
		$this->form_validation->set_rules('kesetiaan', 'kesetiaan', 'required');
		$this->form_validation->set_rules('prestasi', 'prestasi', 'required');
		$this->form_validation->set_rules('tanggungjawab', 'tanggungjawab', 'required');
		$this->form_validation->set_rules('taat', 'taat', 'required');
		$this->form_validation->set_rules('jujur', 'jujur', 'required');
		$this->form_validation->set_rules('kerjasama', 'kerjasama', 'required');
		$this->form_validation->set_rules('prakarsa', 'prakarsa', 'required');
		return $this->form_validation->run();
	}
}