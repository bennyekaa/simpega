<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class detiltugas extends Member_Controller
{
	function detiltugas()
	{
		parent::Member_Controller();
		$this->load->model('detil_tugas_model', 'detil_tugas');
		$this->load->model('riwayat_tugas_model', 'riwayat_tugas');
		$this->load->model("jabatan_model");
		$this->load->model("lookup_model");
		$this->load->model("golongan_model");
		$this->load->model('utp_model', 'utp');
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Detil Tugas');
		$this->browse($id_riwayat_tugas,$kd_pegawai);
	}
	
		
	function browse($id_riwayat_tugas,$kd_pegawai) {
		
		$search_param = array();
		$search_param[] = "(id_riwayat_tugas = '$id_riwayat_tugas')";
//		$search_param[] = "(kd_pegawai = '$kd_pegawai')";
		$search_param = implode(" AND ", $search_param);
		
		
		$data['list_detil_tugas'] = $this->detil_tugas->findByFilter($search_param, NULL, NULL);
		$data['id_riwayat_tugas'] = $id_riwayat_tugas;
		$data['id_detil_riwayat_tugas'] = $id_detil_riwayat_tugas;
		$data['riwayat_tugas']=$this->riwayat_tugas->retrieve_by_pkey($id_riwayat_tugas);
		$data['judul']='Data Detil Tugas: ' . $data['riwayat_tugas']['nama_tugas'];
		$data['kd_pegawai'] = $data['riwayat_tugas']['kd_pegawai'];	
		$kd_pegawai = $data['kd_pegawai'];
		
			
		$this->template->display('pegawai/detiltugas/list_detil_tugas', $data);
	}
	
	
	function add($id_riwayat_tugas)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$id_riwayat_tugas = $this->input->post('id_riwayat_tugas');
			$this->detil_tugas->add($data);
			set_success('Data Detil Tugas berhasil disimpan.');
			$data['riwayat_tugas']=$this->riwayat_tugas->retrieve_by_pkey($id_riwayat_tugas);
			$data['kd_pegawai'] = $data['riwayat_tugas']['kd_pegawai'];	
			$data['id_riwayat_tugas'] = $data['riwayat_tugas']['id_riwayat_tugas'];	
			redirect('/pegawai/detiltugas/browse/'.$data['id_riwayat_tugas'].'/'.$data['kd_pegawai']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Detil Tugas :: Tambah');
			$data = $this->_clear_form();
			$data['id_riwayat_tugas'] = $id_riwayat_tugas;
			$data['riwayat_tugas']=$this->riwayat_tugas->retrieve_by_pkey($id_riwayat_tugas);
			$data['kd_pegawai'] = $data['riwayat_tugas']['kd_pegawai'];	
			$data['id_riwayat_tugas'] = $data['riwayat_tugas']['id_riwayat_tugas'];
			$data['action']='add/'.$data['id_riwayat_tugas'];
			$data['action_list'] = 'browse/'.$data['id_riwayat_tugas'].'/'.$data['kd_pegawai'];
			
			$ordby = 'id_utp';
			
			//$data['list_utp'] 	= $this->utp->findAll($limit_per_page,$start,$ordby);
			$data['list_utp'] 	= $this->utp->find_not($id_riwayat_tugas,$limit_per_page,$start,$ordby);
			
			$data['id_detil_riwayat_tugas']=$this->detil_tugas->get_id();
			$this->template->display('/pegawai/detiltugas/detail_tugas', $data);
		}
	}
	
	function edit($id_riwayat_tugas)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_detil_riwayat_tugas']=$id;
			$this->detil_tugas->update($id, $data);
			set_success('Perubahan Data Detil Tugas berhasil disimpan');
			$data['riwayat_tugas']=$this->riwayat_tugas->retrieve_by_pkey($id);
			$data['kd_pegawai'] = $data['riwayat_tugas']['kd_pegawai'];	
			redirect('/pegawai/detiltugas/browse/'.$data['id_riwayat_tugas'].'/'.$data['kd_pegawai']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Detil Tugas :: Ubah');
			$data = $this->detil_tugas->retrieve_by_pkey($id);
			if ($data)
			{
				$data['riwayat_tugas']=$this->riwayat_tugas->retrieve_by_pkey($id);
				$data['kd_pegawai'] = $data['riwayat_tugas']['kd_pegawai'];	
				$data['id_riwayat_tugas'] = $data['riwayat_tugas']['id_riwayat_tugas'];	
			
				$data['action']='edit/'.$id;
				$data['action_list'] = 'browse/'.$data['id_riwayat_tugas'].'/'.$data['kd_pegawai'];
							
							
				$data['judul']='Edit Detil Tugas';
				$this->template->display('/pegawai/detiltugas/detail_tugas', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/detiltugas/browse/'.$data['id_riwayat_tugas'].'/'.$data['kd_pegawai']);
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->detil_tugas->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Detil Tugas :: Hapus');
		confirm("Yakin menghapus data ?");
		$res = $this->detil_tugas->delete($idField);
		set_success('Data Detil Tugas berhasil dihapus');
		redirect('/pegawai/detiltugas/browse/'.$data['id_riwayat_tugas'].'/'.$data['kd_peg']);
		//redirect('/pegawai/detiltugas/', 'location');
	}

	function _clear_form()
	{
		
		$data['id_detil_riwayat_tugas']	= '';
		$data['id_riwayat_tugas']	= '';
		$data['rincian_tugas']	= '';
		
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_detil_riwayat_tugas']	= $this->detil_tugas->get_id();
		$data['id_riwayat_tugas']			= $this->input->post('id_riwayat_tugas', TRUE);
	   	$data['rincian_tugas']				= $this->input->post('rincian_tugas', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('rincian_tugas', 'rincian_tugas', 'required');
		//$this->form_validation->set_rules('id_jabatan', 'id_jabatan', 'required');
		return $this->form_validation->run();
	}
}
