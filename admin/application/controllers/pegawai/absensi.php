<?php
function right($value, $count){
    return substr($value, ($count*-1));
}

function left($string, $count){
    return substr($string, 0, $count);
}
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Absensi extends My_Controller {

    var $absensi_id;
    var $groupuser;

    function Absensi() {
        parent::My_Controller();
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('agama_model', 'agama');
        $this->load->model('lookup_model', 'lookup');
        $this->load->model('jenis_pegawai_model', 'jenis_pegawai');
        $this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('lap_pegawai_model', 'lap_pegawai');
		$this->load->model('users_model','users');
        /* $this->load->model('gambar_model', 'gambar');  */
        $this->load->model('user_pegawai_model', 'user_pegawai');
    }

    function index() {
        $this->template->metas('title', 'SIMPEGA | Data Absensi Pegawai');
        $this->browse(0);
    }
    
    function browse($group) {
        if ($this->input->post('group'))
		{
            $group = $this->input->post('group');
		}
        else if ($this->uri->segment(4))
            $group = $this->uri->segment(4);
		else 
			$group=array(date('m'));
			
		
		
		if ($this->input->post('tahun'))
			$tahun= $this->input->post('tahun');
		else 
			$tahun=date('Y');
			
			
		
		$this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		if($this->user->user_group == 'Admin Khusus'){
		$query = mysql_query("select p.kode_unit from user_pegawai u,pegawai 
			p where p.kd_pegawai = u.kd_pegawai and u.user_id='".$this->user->user_id."'");					
			if ($query) {
					$datauser=mysql_fetch_array($query);  
					$unit_user = $datauser['kode_unit']; 
			}	
			$unit_kerja	=$unit_user;
		}
		else
		{
		//	$unit_kerja = '0';
			if ($this->input->post('unit_kerja'))
			{    
				$unit_kerja = $this->input->post('unit_kerja');
				
			}
			else if ($this->uri->segment(5)) {
				$unit_kerja = $this->uri->segment(5);
			}
		}        
		if ($this->input->post('nama_search')!=''){
			$nama_seacrh = $this->input->post('nama_search');
			}
			else{
			$nama_seacrh = '';
			}
			
        $paging_uri = 6;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
        
		$limit_per_page = 1000;
        //$ordby = 'eselon asc,id_golpangkat_terakhir desc';
		$ordby = 'eselon asc,id_golpangkat_terakhir desc';
		if (($unit_kerja=='Semua') and ($this->input->post('nama_search')!='')){
			$query = mysql_query("select kode_unit from pegawai where NIP='".$this->input->post('nama_search')."'");					
			if ($query) {
				$data=mysql_fetch_array($query);  
				$unit_kerja=$kode_unit;
			}
		}
		
		$query = mysql_query("select replace(kode_unit,'0','') as tingkat from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data=mysql_fetch_array($query);  
			$tingkat = strlen($data['tingkat']);		
			$pembanding=left($unit_kerja,$tingkat);}
			
        $search_param = array();
        $search_param[] = "id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') 
						   and status_pegawai < 3 ";
		
		if ($unit_kerja)
        {   $tingkat=$this->lookup->get_cari_field('unit_kerja','kode_unit',$unit_kerja,'tingkat');
			if ($tingkat == 1)
			{
				 //LENGTH(replace(nip,'1','')) 
				$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '".
				$unit_kerja."' OR pegawai.kode_unit_induk = '".$unit_kerja."')"; 
				$search_param[]	= "(eselon not like 'non-eselon')";		
			}
			else
			{
				$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";	
				$search_param[]	= "(eselon like 'non-eselon')";
			}
		
		}
        
		//cek apakah ada pencarian berdasarkan anam, jika ya maka tambahkan filter nama
		if ($this->input->post('nama_search')!=''){
		$search_param[] = "((nama_pegawai like '%".$this->input->post('nama_search')."%') or (NIP like '".$this->input->post('nama_search')."'))";	
		}
		else{
		$search_param[] = "(nama_pegawai like '%%')";	
		}
		
		$search_param = implode(" AND ", $search_param);
        $ordby ='id_golpangkat_terakhir desc';
		$data['kerja_list'] = $this->lap_pegawai->findByFilter($search_param, $ordby, $limit_per_page, $start);
		
        $data['group'] = $group;
		$data['tahun'] = $tahun;
		$data['nama_seacrh'] = $nama_seacrh;
		if ($group=='0') {
				 $data['judul'] = 'DATA ABSENSI PERIODE TAHUN : '.$tahun;
			}
			else {
				 $data['judul'] = 'DATA ABSENSI PERIODE BULAN : '. strtoupper($this->lookup->longonthname($group)).' TAHUN : '.$tahun;
			}
		if ($unit_kerja=='Semua') {
				 $data['judul_unit'] = 'DI SEMUA UNIT UNIVERSITAS NEGERI MALANG';
			}
			else {
				 $data['judul_unit'] = 'UNIVERSITAS NEGERI MALANG';
			}
        $data['start'] = $start;
		$data['unit_kerja'] = $unit_kerja;
        $data['start'] = $start;
		$data['unit_kerja'] = $unit_kerja;
		//menampilkan semua unit
		$data['unitkerja_assoc'] = array('Semua'=>"--Pilih Unit--")+ $this->unit_kerja->get_assoc('nama_unit');
		$data['group_assoc'] = array(date('m') => $this->lookup->longonthname(date('m'))) + $this->lookup->month_assoc();
        $data['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y')-1, date('Y')+50);
        $data['option_unit']=$this->unit_kerja->get_unit('1');
		$data['jenis_pegawai_assoc'] = array(0 => "-- Semua Jenis Pegawai --") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		$data['nama_jenis'] = $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		$data['id_absensi']=$this->pegawai->get_id_absensi();
		$display_unit =$this->unit_kerja->get_unit('$unit_kerja');
        
		$config['base_url'] = site_url('pegawai/absensi/browse/' . $group . '/' . $unit_kerja);
        $config['total_rows'] = $this->lap_pegawai->record_count;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();
        $this->template->display('/pegawai/absensi/list_perunit_absensi', $data);
    }

    function view() {
        $data['judul'] = "Detail Data dari: ";
        $id = $this->uri->segment(4);
        $this->edit($id);
    }

}