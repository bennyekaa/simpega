<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatgolpangkat extends Member_Controller
{
	function Riwayatgolpangkat()
	{
		parent::Member_Controller();
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('riwayat_gol_kepangkatan_model','riwayat_gol_kepangkatan');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');
		$this->load->model('lap_gaji_model','lap_gaji');
		$this->load->model('users_model','users');
		include "application/libraries/lib/nusoap.php";
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Kepangkatan Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		$ordby = 'riwayat_gol_kepangkatan.aktif ,riwayat_gol_kepangkatan.status_pegawai, riwayat_gol_kepangkatan.id_golpangkat, riwayat_gol_kepangkatan.tmt_pangkat ';
		$data['list_pangkat'] = $this->riwayat_gol_kepangkatan->find(NULL, array('kd_pegawai' => $kd_pegawai), $ordby, $limit_per_page,$start,NULL);
		
		$data['golongan_assoc'] = $this->golongan->get_assoc();
		$data['gol_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Kepangkatan dari: " . $data['pegawai']['nama_pegawai'];
		$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
		if (isset($this->user->user_group)){
		$this->template->display('pegawai/riwayatgolpangkat/list_riwayatgolpangkat', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
			$NIP = $this->input->post('NIP');
            $data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_golpangkat_terakhir'] = $data['id_golpangkat'];
				$data_pegawai['status_pegawai'] = $data['status_pegawai'];
				$data_pegawai['tmt_golpangkat_terakhir']= $data['tmt_pangkat'];
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				$data_status['aktif'] = '0';			
				$this->riwayat_gol_kepangkatan->update_status($kd_pegawai,$data_status);
				
//akses webservice	
	
				$param=array('id_app' =>'3','key'=>'123','kd_pegawai'=>$NIP,'id_golpangkat_terakhir'=>$data['id_golpangkat']);
				$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
				$client=new nusoap_client($wsdl,true);
				$cek=$client->call("checkConnection",array("id_app"=>"3"));
				if($cek[item][0]=='200'):
					$message=$client->call("updatePangkatPegawai",$param);
		/*			print_r($message);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  */
				else:
					print_r($cek);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  
				endif; 
//end	
			}
			$this->riwayat_gol_kepangkatan->add($data);
            
			set_success('Data kepangkatan pegawai berhasil disimpan.');
//			redirect('/pegawai/riwayatgolpangkat/add/' . $kd_pegawai);
			redirect('/pegawai/riwayatgolpangkat/index/' . $kd_pegawai);
			

		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Kepangkatan Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Kepangkatan dari: ' . $data['pegawai']['nama_pegawai'];
			$data['id_riwayat_gol']=$this->riwayat_gol_kepangkatan->get_id();
			//ambil dan tampilkan pertama golongan pegawai bersangkutan
			$gol_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','id_golpangkat_terakhir');
			$data['keterangan_assoc']= $this->lookup->ket_kp_assoc();
			$idgol_peg = $gol_peg[$kd_pegawai];
			//id_golpeg di defaultkan + 1
			$idgol_peg_now = $idgol_peg + 1;
			$gol = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
			$pkt = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
			$golongan = $gol[$idgol_peg_now];
			$pangkat = $pkt[$idgol_peg_now];
			if ($golongan=='') {
						$golongan = $gol[$idgol_peg];
						$pangkat = $pkt[$idgol_peg];					
						$data['golongan_assoc'] =  array( $idgol_peg => $pangkat.', '.$golongan) + $this->golongan->get_assoc();
						$idgol_peg_now = $idgol_peg;
				}
			else{
				$data['golongan_assoc'] =  array( $idgol_peg_now => $pangkat.', '.$golongan) + $this->golongan->get_assoc();
			}
			
			//ambil dan tampilkan masa kerja
			$golongan = $this->lookup->get_datafield2('golongan_pangkat','id_golpangkat',array('golongan','tingkat'));
			$pendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('mk_tambahan','id_golpangkat_awal'));
			$tgl= date('Y-m-d');
			$cari = array('kd_pegawai' => $kd_pegawai);
			$data_pegawai = $this->pegawai->get_all_by($cari); //ambil semua data pegawai
			// berikut coding untuk memberikan default nilai
			// pada form <supaya keluar gaji pokok secara otomatis>
			if($data_pegawai) foreach($data_pegawai as $pegawai){
				$tglkgb = $data_pegawai['tmt_kgb'];
				$mk_tambahan = $pendidikan[$pegawai['id_pendidikan_terakhir']]['mk_tambahan'];
				$tingkat = $golongan[$idgol_peg_now]['tingkat'];
				$tingkat_awal = $golongan[$golpendidikanawal[$pegawai['id_pendidikan_terakhir']]]['tingkat']; 
				if ($tingkat_awal==3)
					{
						$tingkat_akhir=0;
					}
					else
					{
						$tingkat_akhir= $tingkat - $tingkat_awal ;
					}
					if (($idgol_peg_now >= 9))
					{
						 $masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						 $masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						 
						 $masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
						 $masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
						 
						 if ($tingkat_akhir==1){
							$masa_kerja_golongan = $masa_kerja-5;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-5;
						 }
						 elseif ($tingkat_akhir==2){
							$masa_kerja_golongan = $masa_kerja-11;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;
							}
						 else{
							$masa_kerja_golongan = $masa_kerja;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb;
						 }  
					}
					elseif (($idgol_peg_now >=5)){
						$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						
						$masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
						$masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
						 
						 if ($tingkat_akhir==1){
							$masa_kerja_golongan = $masa_kerja-6;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-6;
						 }
						 elseif ($tingkat_akhir==2){
							$masa_kerja_golongan = $masa_kerja-11;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;}
						 else{
							$masa_kerja_golongan = $masa_kerja;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb;
						 }
					}
					else{
						$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						$masa_kerja_golongan = $masa_kerja['years'];
						$masa_kerja_golongan_kgb = $masa_kerja_kgb['years'];
					}
					$golpangkat_terakhir=$idgol_peg_now;
					$gajilama = $this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan_kgb);
					
					if ($gajilama == 0 ){
						//jika gaji lama 0 maka pangkat - 1
						$gajilama= $this->lap_gaji->getgajipegawai($golpangkat_terakhir-1,$masa_kerja_golongan_kgb);  
					}
					else {
						$gajilama= $gajilama;
					}
					//$pegawai['gajilama']= $gajilama;  
					$gajibaru=$this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan);
					if ($masa_kerja_awal['months'] == '') {
						$masa_kerja_golongan= $masa_kerja_golongan." th" ; 
						
						$masa_kerja=$masa_kerja." th";
					   }
				   else{
						 $masa_kerja_golongan = $masa_kerja_golongan." th ".$masa_kerja_awal['months']." bln" ; 
						 $masa_kerja=$masa_kerja." th ".$masa_kerja_awal['months']." bln";
				   }
			}
			
			$data['masa_kerja'] = $masa_kerja_golongan; 
			//$data['gaji_pokok'] = $gajibaru; 
			//$data['golongan_assoc'] = $this->golongan->get_assoc();
			//return array(0=>"Honorer",1=>"CPNS",2=>"PNS");
			//ambil dan tampilkan pertama status pegawai bersangkutan
			$status_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','status_pegawai');
			$idstatus_peg =$status_peg[$kd_pegawai];
			if ($idstatus_peg=='0'){
				$status_pegawai = 'Honorer';
				}
			elseif ($idstatus_peg=='1'){
				$status_pegawai = 'CPNS';
				}
			else {
				$status_pegawai = 'PNS';
				}
			$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
			
            $data['status_assoc'] = $this->lookup->status_assoc();
            // echo "<pre>";var_dump($data);exit();
			if (isset($this->user->user_group)){
			$this->template->display('/pegawai/riwayatgolpangkat/detail_riwayatgolpangkat', $data);
			} else {
			redirect('/pegawai/pegawai/', 'location');
			}
			
		}
	}
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  		'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_gol'] = $id;
			$kd_pegawai = $this->input->post('kd_pegawai');
			$NIP = $this->input->post('NIP');
			if ($data['aktif']=='0')
			{
				if ($data['aktif']=='1')
				{
					
					$data_pegawai['id_golpangkat_terakhir'] = $data['id_golpangkat'];
					$data_pegawai['status_pegawai'] = $data['status_pegawai'];
					$data_pegawai['tmt_golpangkat_terakhir']= $data['tmt_pangkat'];
					
					$this->pegawai->modify($kd_pegawai, $data_pegawai);
					$data_status['aktif'] = '0';
					
//akses webservice	
	
				$param=array('id_app' =>'3','key'=>'123','kd_pegawai'=>$NIP,'id_golpangkat_terakhir'=>$data['id_golpangkat']);
				$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
				$client=new nusoap_client($wsdl,true);
				$cek=$client->call("checkConnection",array("id_app"=>"3"));
				if($cek[item][0]=='200'):
					$message=$client->call("updatePangkatPegawai",$param);
//update status					
$this->riwayat_gol_kepangkatan->update_status($kd_pegawai,$data_status);		
//end		
		/*			print_r($message);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  */
				else:
					print_r($cek);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  
				endif; 
//end	
				}
			}
			else
			{
				if ($data['aktif']=='1')
				{
					
					$data_pegawai['id_golpangkat_terakhir'] = $data['id_golpangkat'];
					$data_pegawai['status_pegawai'] = $data['status_pegawai'];
					$data_pegawai['tmt_golpangkat_terakhir']= $data['tmt_pangkat'];
					$this->pegawai->modify($kd_pegawai, $data_pegawai);
					
					$data_status['aktif'] = '0';
					
//akses webservice	
				$param=array('id_app' =>'3','key'=>'123','kd_pegawai'=>$NIP,'id_golpangkat_terakhir'=>$data['id_golpangkat']);
				$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
				$client=new nusoap_client($wsdl,true);
				$cek=$client->call("checkConnection",array("id_app"=>"3"));
				if($cek[item][0]=='200'):
					$message=$client->call("updatePangkatPegawai",$param);
//begin update
$this->riwayat_gol_kepangkatan->update_status($kd_pegawai,$data_status);
//end 					
		/*			print_r($message);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  */
				else:
					print_r($cek);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  
				endif; 
//end					
					
				}
			}
			$this->riwayat_gol_kepangkatan->update($id, $data);
			set_success('Perubahan data kepangkatan pegawai berhasil disimpan');
			redirect('/pegawai/riwayatgolpangkat/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Kepangkatan Pegawai :: Ubah');
			$data = $this->riwayat_gol_kepangkatan->retrieve_by_pkey($id);
			$data['golongan_assoc'] = $this->golongan->get_assoc();
            $data['status_assoc'] = $this->lookup->status_assoc();
			$data['keterangan_assoc']= $this->lookup->ket_kp_assoc();
			$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
			
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Kepangkatan dari: '. $data['pegawai']['nama_pegawai'];

				// echo "<pre>";var_dump($data["pegawai"]);exit();
				if (isset($this->user->user_group)){
                $this->template->display('/pegawai/riwayatgolpangkat/detail_riwayatgolpangkat', $data);
				} else {
				redirect('/pegawai/pegawai/', 'location');
				}
				
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatgolpangkat', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_gol_kepangkatan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Kepangkatan Pegawai :: Hapus');
		confirm("Yakin menghapus data kepangkatan pegawai?");
		$res = $this->riwayat_gol_kepangkatan->delete($idField);
		set_success('Data Kepangkatan pegawai berhasil dihapus');
		redirect('/pegawai/riwayatgolpangkat/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
	//nilai default sementara selama input
		$data['id_riwayat_gol']	= '';
		$data['kd_pegawai']	= $this->uri->segment(4, '');
		$data['id_golpangkat']	= $this->uri->segment(5, '')+1;
		
		/*$data['no_SK_pangkat']	= '/H32.II/KP/2010';
		$data['tgl_SK_pangkat']	= '2011-27-01';
		$data['tmt_pangkat']	= '2011-01-01';
		$data['pejabat']	= 'PR II';*/
		
		
		$data['no_SK_pangkat']	= '';
		$data['tgl_SK_pangkat']	= '';
		$data['tmt_pangkat']	= '';
		$data['pejabat']	= '';
		$data['keterangan']	= '';
		$data['status_pegawai']	= '4';
		$data['masa_kerja']	= '';
		$data['mk_tahun']	= '0';
		$data['mk_bulan']	= '0';
		$data['gaji_pokok']	= '0';
		$data['aktif']	= '';
		$data['tambahan']	= '-';
		return $data;
	}	
	
	function _get_form_values()
	{
		$data['id_riwayat_gol']	= $this->riwayat_gol_kepangkatan->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['id_golpangkat']	= $this->input->post('id_golpangkat', TRUE);
		$data['no_SK_pangkat']	= $this->input->post('no_SK_pangkat', TRUE);
		$data['tgl_SK_pangkat']	= $this->input->post('tgl_SK_pangkat', TRUE);
		$data['tmt_pangkat']	= $this->input->post('tmt_pangkat', TRUE);
		$data['pejabat']	= $this->input->post('pejabat', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['status_pegawai']	= $this->input->post('status_pegawai', TRUE);
		$data['mk_tahun']	= $this->input->post('mk_tahun', TRUE);
		$data['mk_bulan']	= $this->input->post('mk_bulan', TRUE);
		$data['masa_kerja']	= $this->input->post('masa_kerja', TRUE);
		$data['gaji_pokok']	= $this->input->post('gaji_pokok', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		$data['tambahan']	= $this->input->post('tambahan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('id_golpangkat', 'id_golpangkat', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}