<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class group_unit extends Member_Controller
{
	function group_unit()
	{
		parent::Member_Controller();
		$this->load->model('group_unit_model', 'group_unit');
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Unit Kerja');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'id_group';
		
		$data['list_group_unit'] = $this->group_unit->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/group_unit/browse/');
		$config['total_rows']   = $this->group_unit->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();	    
		$data['judul'] 		= "Data Unit Kerja";
		//show_error(var_dump($data));
		$this->template->display('setup/group_unit/list_group_unit', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->group_unit->add($data);
			set_success('Data Unit Kerja berhasil disimpan.');
			redirect('/setup/group_unit');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Unit Kerja :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Unit Kerja';
			$data['id_group']=$this->group_unit->get_id();
			$this->template->display('/setup/group_unit/detail_group_unit', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_group']=$id;
			$this->group_unit->update($id, $data);
			set_success('Perubahan data kelompok unit berhasil disimpan');
			redirect('/setup/group_unit', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Unit Kerja :: Ubah');
			$data = $this->group_unit->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Unit Kerja';
				$this->template->display('/setup/group_unit/detail_group_unit', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/group_unit', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->group_unit->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Unit Kerja :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['group_unit']."</b> ?");
		$res = $this->group_unit->delete($idField);
		set_success('Data kelompok unit berhasil dihapus');
		redirect('/setup/group_unit', 'location');
	}

	function _clear_form()
	{
		$data['id_group']	= '';
		$data['group_unit']	= '';
		$data['keterangan']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_group']	= $this->input->post('id_group', TRUE);
	   	$data['nama_group']		= $this->input->post('nama_group', TRUE);
		$data['keterangan']		= $this->input->post('keterangan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_group', 'nama_group', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		return $this->form_validation->run();
	}
}