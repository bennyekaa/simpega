<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Unit extends Member_Controller
{
	function Unit()
	{
		parent::Member_Controller();
		$this->load->model('group_unit_model', 'group_unit');
		$this->load->model('unit_kerja_model', 'unit_kerja');
		$this->load->model("group_unit_model");
		$this->load->model('lookup_model','lookup');
		$this->load->model("golongan_model");
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data sub unit');
		$this->browse();
	}
	
	function index2()
	{
		$this->template->metas('title', 'SIMPEGA | Data Statistik Pegawai');
		$this->statistik();
	}
	
	function browse() {
		$id_group=$this->uri->segment(4);
		$data['option_group_unit'] = $this->group_unit->get_assoc();
		if(!$id_group)
			redirect('setup/group_unit');
		$paging_uri=6;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 100;
		//$ordby = $this->uri->segment(5);
		$ordby = 'id_group,kode_unit,tingkat';
		
		
		if ($this->input->post('tingkat')){
			$tingkat = $this->input->post('tingkat');}
		else {
			$tingkat = 'semua';}
			
		$data['tingkat'] = $tingkat;
		$data['id_group'] = $id_group;
		
		if ($tingkat=='semua')
			$data['list_unit_kerja'] = $this->unit_kerja->find(array("id_group"=>$id_group),'', $limit_per_page,$start,$ordby);
		else
			$data['list_unit_kerja'] = $this->unit_kerja->find(array("id_group"=>$id_group),array("tingkat"=>$tingkat) , $limit_per_page,$start,$ordby);
		
		$data['nama_group'] = $this->group_unit->get_assoc2();
		$data['unit_kerja_assoc'] = $this->unit_kerja->get_unit_assoc();
		$data['GD_assoc'] = $this->lookup->GD_assoc();
		$data['get_tingkat_all'] =array('semua'=>"--Semua--") + $this->lookup->get_tingkat_all();
		$data['judul'] 		= "Data sub unit";
		$config['base_url']     = site_url('setup/unit/browse/'.$id_group.'/'.$ordby.'/');
		$config['total_rows']   = $this->unit_kerja->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['ordbynama'] 	= 'setup/unit/browse/'.$id_group.'/nama_unit';
		$data['ordbyno_urut'] 	= 'setup/unit/browse/'.$id_group.'/kd_unit';
		$this->template->display('setup/unit/list_unit', $data);
	}
	
	function add($id_group)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->unit_kerja->add($data);
			set_success('Data Unit Keja berhasil disimpan.');
			redirect('/setup/unit/browse/'.$data['id_group']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data sub unit :: Tambah');
			$data = $this->_clear_form();
			$data['id_group'] = $id_group;
			$data['nama_group'] = $this->group_unit->get_assoc2();
			$data['action']='add/'.$id_group;
			$data['action_list'] = 'browse/'.$id_group;
			$data['unit_kerja_assoc'] =array(0=>"-- Pilih Parent --") + $this->unit_kerja->get_unit_assoc();
			$data['judul']='Tambah sub unit';
			$data['kode_unit']=$this->unit_kerja->get_id();
			//$data['gd_assoc']=array(D=>"-- Pilih GD --")+$this->lookup->gd_assoc();
			$data['GD_assoc'] = $this->lookup->GD_assoc();
			$this->template->display('/setup/unit/detail_unit', $data);
			
		}
	}
	
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->unit_kerja->update($id, $data);
			set_success('Perubahan data sub unit berhasil disimpan');
			
			/*$data = $this->_get_form_values();
			$data['kode_unit']=$id;
			$this->unit_kerja->update($id, $data);*/
			//$data['gd_assoc']=array(D=>$data['GD'])+$this->lookup->gd_assoc();
			$data['GD_assoc'] = $this->lookup->GD_assoc();
			set_success('Perubahan data sub unit berhasil disimpan');
			redirect('/setup/unit/browse/'.$data['id_group']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data sub unit :: Ubah');
			$data = $this->unit_kerja->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action_list'] = 'browse/'.$data['id_group'];
				$data['id_group'] = $data['id_group'];
				$data['nama_group'] = $this->group_unit->get_assoc2();
				$data['action'] = 'edit/'.$id;
				$data['unit_kerja_assoc'] =array(0=>"-- Pilih Unit Utama --") + $this->unit_kerja->get_unit_assoc_by_key($data['id_group']);
				$data['judul']='Edit unit';
				//$data['gd_assoc']=array(D=>$data['GD'])+$this->lookup->gd_assoc();
				$data['GD_assoc'] = array(D=>$data['GD'])+ $this->lookup->GD_assoc();
				$this->template->display('/setup/unit/detail_unit', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/unit/browse/'.$data['id_group']);
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->unit_kerja->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data unit :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['nama_unit']."</b> ?");
		$res = $this->unit_kerja->delete($idField);
		set_success('Data unit berhasil dihapus');
		redirect('/setup/unit/browse/'.$data['id_group']);
	}
	
	

	function _clear_form()
	{
		$data['kode_unit']	= '';
		$data['nama_unit']	= '';
		$data['alamat']		= '';
		$data['id_group']	= '';
		$data['kode_unit_general']	= '';
		$data['GD']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['kode_unit']	= $this->input->post('kode_unit', TRUE);
	   	$data['nama_unit']		= $this->input->post('nama_unit', TRUE);
		$data['alamat']		= $this->input->post('alamat', TRUE);
		$data['id_group']	= $this->input->post('id_group', TRUE);
		$data['kode_unit_general']	= $this->input->post('kode_unit_general', TRUE);
		$data['GD']	= $this->input->post('GD', TRUE);
		$data['tingkat']	= $this->input->post('tingkat', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('kode_unit', 'kode_unit', 'required');
		$this->form_validation->set_rules('nama_unit', 'nama_unit', 'required');
		$this->form_validation->set_rules('kode_unit_general', 'kode_unit_general', 'required');
		$this->form_validation->set_rules('id_group', 'id_group', 'required');
		$this->form_validation->set_rules('kode_unit_general', 'kode_unit_general', 'required');
		$this->form_validation->set_rules('tingkat', 'tingkat', 'required');
		return $this->form_validation->run();
	}
	
	function cek_duplicate_kode_unit($kode_unit=""){
		$data = $this->unit_kerja->retrieve_by_filters(array('kode_unit'=>$kode_unit));
		if($data) echo json_encode(1);
		else echo json_encode(0);
	}
	/*
	
	function statistik() {
		$id_group=$this->uri->segment(4);
		
		$data['option_group_unit'] = $this->group_unit->get_assoc();
		if(!$id_group)
			redirect('setup/group_unit');
		$paging_uri=6;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 100;
		//$ordby = $this->uri->segment(5);
		$ordby = 'id_group,kode_unit,tingkat';
		$tingkat = $this->input->post('tingkat');
		$data['tingkat'] = $tingkat;
		$data['id_group'] = $id_group;
		if ($tingkat=='semua')
			$data['list_unit_kerja'] = $this->unit_kerja->find(array("id_group"=>$id_group),'', $limit_per_page,$start,$ordby);
		else
			$data['list_unit_kerja'] = $this->unit_kerja->find(array("id_group"=>$id_group),array("tingkat"=>$tingkat) , $limit_per_page,$start,$ordby);
		
		//getting golpangkat
		$data['golongan_pangkat_assoc'] =$this->golongan_model->findAll();
		

		//$data['jumlah_pegawai_assoc'] =$this->unit_kerja->Hitung('2320',$tingkat,'P');
		
		$data['nama_group'] = $this->group_unit->get_assoc2();
		$data['unit_kerja_assoc'] = $this->unit_kerja->get_unit_assoc();
		$data['GD_assoc'] = $this->lookup->GD_assoc();
		$data['get_tingkat_all'] =array('semua'=>"--Semua--") + $this->lookup->get_tingkat_all();
		$data['judul'] 		= "REKAPITULASI JUMLAH PEGAWAI ADMINISTRASI, PUSTAKAWAN, DAN PRANATA HUMAS<BR>UNIVERSITAS NEGERI MALANG";
		$config['base_url']     = site_url('setup/unit/statistik/'.$id_group.'/'.$ordby.'/');
		$config['total_rows']   = $this->unit_kerja->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['ordbynama'] 	= 'setup/unit/statistik/'.$id_group.'/nama_unit';
		$data['ordbyno_urut'] 	= 'setup/unit/statistik/'.$id_group.'/kd_unit';
		$this->template->display('setup/unit/list_statistik', $data);
	}*/

}

