<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Keluarga extends Member_Controller
{
	function Keluarga()
	{
		parent::Member_Controller();
		$this->load->model('status_keluarga_model', 'status_keluarga');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data Anak');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'kd_status_keluarga';
		
		$data['list_keluarga'] = $this->status_keluarga->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/keluarga/browse/');
		$config['total_rows']   = $this->golongan->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['judul'] 		= "Data Status Keluarga";
		$this->template->display('setup/keluarga/list_keluarga', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->status_keluarga->add($data);
			set_success('Data status keluarga berhasil disimpan.');
			redirect('/setup/keluarga');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Anak :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Status Keluarga';
			$data['kd_status_keluarga']=$this->status_keluarga->get_id();
			$this->template->display('/setup/keluarga/detail_keluarga', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_status_keluarga'] = $id;
			$this->status_keluarga->update($id, $data);
			set_success('Perubahan data status keluarga berhasil disimpan');
			redirect('/setup/keluarga', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Anak :: Ubah');
			$data = $this->status_keluarga->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Status Keluarga';
				$this->template->display('/setup/keluarga/detail_keluarga', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/keluarga', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->status_keluarga->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Anak :: Hapus');
		confirm("Yakin menghapus data status keluarga <b>".$data['status_keluarga']."</b> ?");
		$res = $this->status_keluarga->delete($idField);
		set_success('Data status keluarga berhasil dihapus');
		redirect('/setup/keluarga', 'location');
	}

	function _clear_form()
	{
		$data['kd_status_keluarga']	= '';
		$data['status_keluarga']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['kd_status_keluarga']	= $this->input->post('kd_status_keluarga', TRUE);
	   	$data['status_keluarga']	= $this->input->post('status_keluarga', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('status_keluarga', 'status keluarga', 'required');
		return $this->form_validation->run();
	}
}