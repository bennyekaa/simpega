<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Golongan extends Member_Controller
{
	function Golongan()
	{
		parent::Member_Controller();
		$this->load->model('golongan_model', 'golongan');
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Golongan');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'id_golpangkat';
		
		$data['list_golongan'] = $this->golongan->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/golongan/browse/');
		$config['total_rows']   = $this->golongan->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();	    
		$data['judul'] 		= "Data Golongan";
		//show_error(var_dump($data));
		$this->template->display('setup/golongan/list_golongan', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->golongan->add($data);
			set_success('Data golongan berhasil disimpan.');
			redirect('/setup/golongan');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Golongan :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Golongan';
			$data['id_golpangkat']=$this->golongan->get_id();
			$this->template->display('/setup/golongan/detail_golongan', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_golpangkat']=$id;
			$this->golongan->update($id, $data);
			set_success('Perubahan data golongan berhasil disimpan');
			redirect('/setup/golongan', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Golongan :: Ubah');
			$data = $this->golongan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Golongan';
				$this->template->display('/setup/golongan/detail_golongan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/golongan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->golongan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Golongan :: Hapus');
		confirm("Yakin menghapus data golongan <b>".$data['golongan']."</b> ?");
		$res = $this->golongan->delete($idField);
		set_success('Data golongan berhasil dihapus');
		redirect('/setup/golongan', 'location');
	}

	function _clear_form()
	{
		$data['id_golpangkat']	= '';
		$data['golongan']	= '';
		$data['pangkat']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_golpangkat']	= $this->input->post('id_golpangkat', TRUE);
	   	$data['golongan']		= $this->input->post('golongan', TRUE);
		$data['pangkat']		= $this->input->post('pangkat', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('golongan', 'golongan', 'required');
		$this->form_validation->set_rules('pangkat', 'pangkat', 'required');
		return $this->form_validation->run();
	}
}