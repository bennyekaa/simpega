<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kepegawaian extends Member_Controller
{
	var $swith = '';
	
	function Kepegawaian()
	{
		parent::Member_Controller();
		$this->load->model('status_pegawai_model', 'status_pegawai');
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
	}
	
	function index()
	{
		redirect('/index.php');
	}
	
	function jenis()
	{
		$this->swith = 'jenis';
		$this->template->metas('title', 'SIMPEGA | Data Jenis Pegawai');
		$this->browse();
	}
	
	function status()
	{
		$this->swith = 'status';
		$this->template->metas('title', 'SIMPEGA | Data Status Pegawai');
		$this->browse();
	}
	
	function browse($group=0)
	{
		$data['cat'] = $this->swith;
		if($this->swith == 'jenis')
		{
			$data['list_pegawai'] = $this->jenis_pegawai->findAll();	    
			$data['judul'] 		= "Data Jenis Kepegawaian";
			$this->template->display('setup/pegawai/list-jenis', $data);
		}
		elseif($this->swith == 'status')
		{
			$data['list_pegawai'] = $this->status_pegawai->findAll();	    
			$data['judul'] 		= "Data Status Kepegawaian";
			$this->template->display('setup/pegawai/list-status', $data);
		}
		else
		{
			redirect('/index.php');
		}
	}
	
	function add()
	{
		$catt = $this->uri->segment(4);
		if($catt=="jenis")
		{
			if ($this->_validate())
			{
				$data = $this->_get_form_values();
				$this->jenis_pegawai->add($data);
				set_success('Data Jenis Kepegawaian berhasil disimpan.');
				redirect('/setup/kepegawaian/jenis');
			}
			else
			{
				$this->template->metas('title', 'SIMPEGA | Data Jenis Pegawai :: Tambah');
				$data = $this->_clear_form();
				$data['action']='add/jenis';
				$data['judul']='Tambah Jenis Kepegawaian';
				$data['kd_jenis_pegawai'] = $this->jenis_pegawai->get_id();
				$this->template->display('/setup/pegawai/detail-jenis', $data);
			}
		}
		elseif($catt=="status")
		{
			if ($this->_validate1())
			{
				$data = $this->_get_form_values1();
				$this->status_pegawai->add($data);
				set_success('Data Status Kepegawaian berhasil disimpan.');
				redirect('/setup/kepegawaian/status');
			}
			else
			{
				$this->template->metas('title', 'SIMPEGA | Data Status Pegawai :: Tambah');
				$data = $this->_clear_form1();
				$data['action']='add/status';
				$data['judul']='Tambah Status Kepegawaian';
				$data['kd_status_pegawai'] = $this->status_pegawai->get_id();
				$this->template->display('/setup/pegawai/detail-status', $data);
			}
		}
		else
		{
			redirect('/index.php');
		}
	}
	
	function edit()
	{
		$catt = $this->uri->segment(4);
		$idField = $this->uri->segment(5);
		if($catt == "jenis")
		{
			if ($this->_validate())
			{
				$data = $this->_get_form_values();
				$data['kd_jenis_pegawai'] = $idField;
				$this->jenis_pegawai->update($idField, $data);
				set_success('Perubahan data jenis pegawai berhasil disimpan');
				redirect('/setup/kepegawaian/jenis', 'location');
			}
			else
			{
				$this->template->metas('title', 'SIMPEGA | Data Jenis Pegawai :: Ubah');
				$data = $this->jenis_pegawai->retrieve_by_pkey($idField);
				$data['action'] = 'edit/jenis/'.$idField;
				$data['judul']='Edit Jenis Kepegawaian';
				$this->template->display('/setup/pegawai/detail-jenis', $data);
			}
		}
		elseif($catt == "status")
		{
			if ($this->_validate1())
			{
				$data = $this->_get_form_values1();
				$data['kd_status_pegawai'] = $idField;
				$this->status_pegawai->update($idField, $data);
				set_success('Perubahan data status kepegawaian berhasil disimpan');
				redirect('/setup/kepegawaian/status', 'location');
			}
			else
			{
				$this->template->metas('title', 'SIMPEGA | Data Status Pegawai :: Ubah');
				$data = $this->status_pegawai->retrieve_by_pkey($idField);
				$data['action'] = 'edit/status/'.$idField;
				$data['judul']='Edit Status Kepegawaian';
				$data['status_kepegawaian_assoc'] = array(0=>"-- Status Kepegawaian --")+ $this->status_pegawai->get_assoc("status_pegawai");
				$this->template->display('/setup/pegawai/detail-status', $data);
			}
		}
		else
		{
			redirect('/index.php');
		}
	}
	
	function delete()
	{
		$catt = $this->uri->segment(4);
		$idField = $this->uri->segment(5);
		if($catt == "jenis")        
		{
			$data = $this->jenis_pegawai->retrieve_by_pkey($idField);
			
			$this->template->metas('title', 'SIMPEGA | Data Jenis Pegawai :: Hapus');
			confirm("Yakin menghapus data jenis kepegawaian <b>".$data['jenis_pegawai']."</b> ?");
			$res = $this->jenis_pegawai->delete($idField);
			set_success('Data jenis kepegawaian berhasil dihapus');
			redirect('/setup/kepegawaian/jenis', 'location');
		}
		elseif($catt == "status")        
		{
			$data = $this->status_pegawai->retrieve_by_pkey($idField);	
			
			$this->template->metas('title', 'SIMPEGA | Data Status Pegawai :: Hapus');
			confirm("Yakin menghapus data status kepegawaian <b>".$data['status_pegawai']."</b> ?");
			$res = $this->status_pegawai->delete($idField);
			set_success('Data status kepegawaian berhasil dihapus');
			redirect('/setup/kepegawaian/status', 'location');
		}
		else
		{
			redirect('/index.php');
		}
	}

	function _clear_form()
	{
		$data['kd_jenis_pegawai']	= '';
		$data['jenis_pegawai']	= '';
		$data['ket_jenis_pegawai'] = '';
		return $data;
	}
	
	function _clear_form1()
	{
		$data['kd_status_pegawai']	= '';
		$data['status_pegawai']	= '';
		$data['ket_status_pegawai'] = '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['kd_jenis_pegawai']	= $this->input->post('kd_jenis_pegawai', TRUE);
	   	$data['jenis_pegawai']		= $this->input->post('jenis_pegawai', TRUE);
		$data['ket_jenis_pegawai'] = $this->input->post('ket_jenis_pegawai', TRUE);
		return $data;
	}
	
	function _get_form_values1()
	{
	   	$data['kd_status_pegawai']	= $this->input->post('kd_status_pegawai', TRUE);
	   	$data['status_pegawai']		= $this->input->post('status_pegawai', TRUE);
		$data['ket_status_pegawai'] = $this->input->post('ket_status_pegawai', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('jenis_pegawai', 'jenis pegawai', 'required');
		return $this->form_validation->run();
	}
	
	function _validate1()
	{
		$this->form_validation->set_rules('status_pegawai', 'status pegawai', 'required');
		return $this->form_validation->run();
	}
}