<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class kecamatan extends Member_Controller
{
	function kecamatan()
	{
		parent::Member_Controller();
		$this->load->model('kecamatan_model', 'kecamatan');
		$this->load->model("kabupaten_model");
		$this->load->model("lookup_model");
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Kecamatan');
		$this->browse();
	}
	
		
	function browse() {
		$kd_kabupaten=$this->uri->segment(4);
		$data['option_kabupaten'] = $this->kabupaten_model->get_assoc();
		if(!$kd_kabupaten)
			redirect('setup/kabupaten');
		$paging_uri=6;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
			$limit_per_page = 15;
			$ordby = $this->uri->segment(5);
			$data['list_kecamatan'] = $this->kecamatan->findByFilter(array("kd_kabupaten"=>$kd_kabupaten),$limit_per_page,$start,$ordby);
			$data['kd_kabupaten'] = $kd_kabupaten;
			$data['judul'] 		= "Data Kecamatan";
			$data['nama_kabupaten'] = $this->kabupaten_model->get_assoc2();
			$config['base_url']     = site_url('setup/kecamatan/browse/'.$kd_kabupaten.'/'.$ordby.'/');
			$config['total_rows']   = $this->kecamatan->record_count;
			$config['per_page']     = $limit_per_page;
			$config['uri_segment'] 	= $paging_uri;
			$config['next_link'] 	= 'berikutnya &raquo;';
			$config['prev_link'] 	= '&laquo; sebelumnya ';
			$this->pagination->initialize($config);
			$data['page_links'] 	= $this->pagination->create_links();
			$data['ordbynama'] 	= 'setup/kecamatan/browse/'.$kd_kabupaten.'/nama_kecamatan';
			$data['ordbyno_urut'] 	= 'setup/kecamatan/browse/'.$kd_kabupaten.'/kd_kecamatan';
			$this->template->display('setup/kecamatan/list_kecamatan', $data);
	}

	
	function add($id_kabupaten)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->kecamatan->add($data);
			set_success('Data Kecamatan berhasil disimpan.');
			redirect('/setup/kecamatan/browse/'.$data['kd_kabupaten']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Kecamatan :: Tambah');
			$data = $this->_clear_form();
			$data['kd_kabupaten'] = $id_kabupaten;
			$data['nama_kabupaten'] = $this->kabupaten_model->get_assoc2();
			$data['action']='add/'.$id_kabupaten;
			$data['action_list'] = 'browse/'.$id_kabupaten;
			$data['judul']='Tambah Kecamatan';
			$data['kd_kecamatan']=$this->kecamatan->get_id();
			$this->template->display('/setup/kecamatan/detail_kecamatan', $data);
		}
	}
	
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_kecamatan']=$id;
			$this->kecamatan->update($id, $data);
			set_success('Perubahan data Kecamatan berhasil disimpan');
			redirect('/setup/kecamatan/browse/'.$data['kd_kabupaten']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Kecamatan :: Ubah');
			$data = $this->kecamatan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action_list'] = 'browse/'.$data['kd_kabupaten'];
				$data['kd_kabupaten'] = $data['kd_kabupaten'];
				$data['nama_kabupaten'] = $this->kabupaten_model->get_assoc2();
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Kecamatan';
				$this->template->display('/setup/kecamatan/detail_kecamatan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/kecamatan/browse/'.$data['kd_kabupaten']);
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->kecamatan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Kecamatan :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['nama_kecamatan']."</b> ?");
		$res = $this->kecamatan->delete($idField);
		set_success('Data Kecamatan berhasil dihapus');
		redirect('/setup/kecamatan/browse/'.$data['kd_kabupaten']);
	}

	function _clear_form()
	{
		$data['kd_kecamatan']	= '';
		$data['nama_kecamatan']	= '';
		$data['kd_kabupaten']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['kd_kecamatan']	= $this->input->post('kd_kecamatan', TRUE);
	   	$data['nama_kecamatan']		= $this->input->post('nama_kecamatan', TRUE);
		$data['kd_kabupaten']		= $this->input->post('kd_kabupaten', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_kecamatan', 'nama_kecamatan', 'required');
		$this->form_validation->set_rules('kd_kabupaten', 'kd_kabupaten', 'required');
		return $this->form_validation->run();
	}
}