<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class peta_situs extends Member_Controller
{
	function peta_situs()
	{
		parent::Member_Controller();
		//$this->load->model('agama_model', 'agama');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Peta Situs');
		$this->browse();
	}
	
	function browse()
	{
		$config['base_url']     = site_url('setup/peta_situs/browse/');
		$data['judul'] 		= "Data Peta Situs";
		$this->template->display('setup/peta_situs/list_peta_situs', $data);
	}
}