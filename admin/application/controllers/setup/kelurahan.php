<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class kelurahan extends Member_Controller
{
	function kelurahan()
	{
		parent::Member_Controller();
		$this->load->model('kelurahan_model', 'kelurahan');
		$this->load->model("kecamatan_model");
		$this->load->model("lookup_model");
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data kelurahan');
		$this->browse();
	}
	
	
	function browse() {
		$kd_kecamatan=$this->uri->segment(4);
		$data['option_kecamatan'] = $this->kecamatan_model->get_assoc();
		if(!$kd_kecamatan)
			redirect('setup/kecamatan');
		$paging_uri=6;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
			
		$limit_per_page = 15;
		$ordby = $this->uri->segment(5);
		$data['list_kelurahan'] = $this->kelurahan->findByFilter(array("kelurahan.kd_kecamatan"=>$kd_kecamatan),$limit_per_page,$start,$ordby);
		$data['kd_kecamatan'] = $kd_kecamatan;
		$data['judul'] 		= "Data kelurahan";
		$data['nama_kecamatan'] = $this->kecamatan_model->get_assoc2();
		$config['base_url']     = site_url('setup/kelurahan/browse/'.$kd_kecamatan.'/'.$ordby.'/');
		$config['total_rows']   = $this->kelurahan->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['ordbynama'] 	= 'setup/kelurahan/browse/'.$kd_kecamatan.'/nama_kelurahan';
		$data['ordbyno_urut'] 	= 'setup/kelurahan/browse/'.$kd_kecamatan.'/kd_kelurahan';
		$this->template->display('setup/kelurahan/list_kelurahan', $data);
	}
	
/*	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->kelurahan->add($data);
			set_success('Data kelurahan berhasil disimpan.');
			redirect('/setup/kelurahan');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data kelurahan :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah kelurahan';
			$data['kd_kelurahan']=$this->kelurahan->get_id();
			$this->template->display('/setup/kelurahan/detail_kelurahan', $data);
		}
	}
	*/
	
	function add($id_kecamatan)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->kelurahan->add($data);
			set_success('Data kelurahan berhasil disimpan.');
			redirect('/setup/kelurahan/browse/'.$data['kd_kecamatan']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data kelurahan :: Tambah');
			$data = $this->_clear_form();
			$data['kd_kecamatan'] = $id_kecamatan;
			$data['action']='add/'.$id_kecamatan;
			$data['action_list'] = 'browse/'.$id_kecamatan;
			$data['judul']='Tambah kelurahan';
			$data['kd_kelurahan']=$this->kelurahan->get_id();
			$this->template->display('/setup/kelurahan/detail_kelurahan', $data);
		}
	}
	
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_kelurahan']=$id;
			$this->kelurahan->update($id, $data);
			set_success('Perubahan data kelurahan berhasil disimpan');
			redirect('/setup/kelurahan/browse/'.$data['kd_kecamatan']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data kelurahan :: Ubah');
			$data = $this->kelurahan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action_list'] = 'browse/'.$data['kd_kecamatan'];
				$data['kd_kecamatan'] = $data['kd_kecamatan'];
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit kelurahan';
				$this->template->display('/setup/kelurahan/detail_kelurahan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/kelurahan/browse/'.$data['kd_kecamatan']);
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->kelurahan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data kelurahan :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['nama_kelurahan']."</b> ?");
		$res = $this->kelurahan->delete($idField);
		set_success('Data kelurahan berhasil dihapus');
		redirect('/setup/kelurahan/browse/'.$data['kd_kecamatan']);
	}

	function _clear_form()
	{
		$data['kd_kelurahan']	= '';
		$data['nama_kelurahan']	= '';
		$data['kd_kecamatan']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['kd_kelurahan']	= $this->input->post('kd_kelurahan', TRUE);
	   	$data['nama_kelurahan']		= $this->input->post('nama_kelurahan', TRUE);
		$data['kd_kecamatan']		= $this->input->post('kd_kecamatan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_kelurahan', 'nama_kelurahan', 'required');
		$this->form_validation->set_rules('kd_kecamatan', 'kd_kecamatan', 'required');
		return $this->form_validation->run();
	}
}