<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class parameter extends Member_Controller
{
	function parameter()
	{
		parent::Member_Controller();
		$this->load->model('parameter_model', 'parameter');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data Parameter Kenaikan Pangkat');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'id_parameter';
		$data['list_parameter'] 	= $this->parameter->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/parameter/browse/');
		$config['total_rows']   = $this->parameter->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();   
		$data['judul'] 		= "Data Parameter Kenaikan Pangkat";
		//show_error(var_dump($data));
		$this->template->display('setup/parameter/list_parameter', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->parameter->add($data);
			set_success('Data Parameter Kenaikan Pangkat Berhasil Disimpan.');
			redirect('/setup/parameter');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Parameter Kenaikan Pangkat :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Parameter Kenaikan Pangkat';
			$data['id_parameter']=$this->parameter->get_id();
			$this->template->display('/setup/parameter/detail_parameter', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_parameter'] = $id;
			
			$this->parameter->update($id, $data);
			set_success('Perubahan data parameter kenaikan pangkat berhasil disimpan');
			redirect('/setup/parameter', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Parameter Kenaikan Pangkat :: Ubah');
			$data = $this->parameter->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Parameter Kenaikan Pangkat';
				$this->template->display('/setup/parameter/detail_parameter', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/parameter', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->parameter->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Parameter Kenaikan Pangkat :: Hapus');
		confirm("Yakin menghapus data parameter kenaikan pangkat <b>".$data['parameter']."</b> ?");
		$res = $this->parameter->delete($idField);
		set_success('Data parameter kenaikan pangkat berhasil dihapus');
		redirect('/setup/parameter', 'location');
	}

	function _clear_form()
	{
		$data['id_parameter']	= '';
		$data['parameter']	= '';
		$data['periode_minimal'] = '';
		$data['DP3_minimal'] = '';
		$data['AK_minimal'] = '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_parameter']		= $this->input->post('id_parameter', TRUE);
	   	$data['parameter']		= $this->input->post('parameter', TRUE);
		$data['periode_minimal']	= $this->input->post('periode_minimal', TRUE);
		$data['DP3_minimal']	= $this->input->post('DP3_minimal', TRUE);
		$data['AK_minimal']	= $this->input->post('AK_minimal', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('parameter', 'parameter', 'required');
		$this->form_validation->set_rules('periode_minimal', 'periode_minimal', 'required');
		return $this->form_validation->run();
	}
}