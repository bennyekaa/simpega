<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pangkat extends Member_Controller
{
	function Pangkat()
	{
		parent::Member_Controller();
		$this->load->model('konfigurasi_model', 'konfig');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Parameter Kenaikan Pangkat');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'id_konfig';
		
		$data['list_pangkat'] 	= $this->konfig->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/pangkat/browse/');
		$config['total_rows']   = $this->konfig->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();	
		$data['judul'] 			= "Konfigurasi Kenaikan Pangkat";
		$this->template->display('setup/pangkat/list_pangkat', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->konfig->add($data);
			set_success('Konfigurasi kenaikan pangkat berhasil disimpan.');
			redirect('/setup/pangkat');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Parameter Kenaikan Pangkat :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Konfigurasi';
			$data['id_konfig']=$this->konfig->get_id();
			$this->template->display('/setup/pangkat/detail_pangkat', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_konfig'] = $id;
			$this->konfig->update($id, $data);
			set_success('Perubahan konfigurasi kenaikan pangkat berhasil disimpan');
			redirect('/setup/pangkat', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Parameter Kenaikan Pangkat :: Ubah');
			$data = $this->konfig->retrieve_by_pkey($id);
			$data['action'] = 'edit/'.$id;
			$data['judul']='Edit Konfigurasi';
			$this->template->display('/setup/pangkat/detail_pangkat', $data);
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->konfig->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Parameter Kenaikan Pangkat :: Hapus');
		confirm("Yakin menghapus Parameter kenaikan pangkat <b>".$data['jenis_konfig']."</b> ?");
		$res = $this->konfig->delete($idField);
		set_success('Parameter berhasil dihapus');
		redirect('/setup/pangkat', 'location');
	}

	function _clear_form()
	{
		$data['id_konfig']		= '';
		$data['jenis_konfig']	= '';
		$data['value']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_konfig']		= $this->input->post('id_konfig', TRUE);
	   	$data['jenis_konfig']	= $this->input->post('jenis_konfig', TRUE);
	   	$data['value']			= $this->input->post('value', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('jenis_konfig', 'Nama Konfigurasi', 'required');
		$this->form_validation->set_rules('value', 'Nilai Konfigurasi', 'required');
		return $this->form_validation->run();
	}
}