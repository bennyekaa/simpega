<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan extends Member_Controller
{
	function Pendidikan()
	{
		parent::Member_Controller();
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model("golongan_model");
	}
	
	function index()
	{
		$this->template->metas('title','SIMPEGA | Data Tingkat Pendidikan');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 15;
		$ordby = 'id_pendidikan';
		$data['golongan_pangkat_assoc'] = $this->golongan_model->get_assoc();
		$data['golongan_assoc'] = $this->golongan_model->get_gol_assoc();
		$data['list_pendidikan'] = $this->pendidikan->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/pendidikan/browse/');
		$config['total_rows']   = $this->pendidikan->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();	  
		$data['judul'] 		= "Dasar Golongan Pengangkatan dan Pangkat/Golongan Terakhir";
		$this->template->display('setup/pendidikan/list_pendidikan', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->pendidikan->add($data);
			set_success('Data pendidikan berhasil disimpan.');
			redirect('/setup/pendidikan');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Tingkat Pendidikan :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
			$data['judul']='Tambah Tingkat Pendidikan';
			$data['id_pendidikan']=$this->pendidikan->get_id();
			$this->template->display('/setup/pendidikan/detail_pendidikan', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_pendidikan']= $id;
			$this->pendidikan->update($id, $data);
			set_success('Perubahan data pendidikan berhasil disimpan');
			redirect('/setup/pendidikan', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Tingkat Pendidikan :: Ubah');
			$data = $this->pendidikan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
				$data['judul']='Edit Tingkat Pendidikan';
				$this->template->display('/setup/pendidikan/detail_pendidikan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/pendidikan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->pendidikan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Tingkat Pendidikan :: Hapus');
		confirm("Yakin menghapus data pendidikan <b>".$data['nama_pendidikan']."</b> ?");
		$res = $this->pendidikan->delete($idField);
		set_success('Data tingkat pendidikan berhasil dihapus');
		redirect('/setup/pendidikan', 'location');
	}

	function _clear_form()
	{
		$data['id_pendidikan']	= '';
		$data['nama_pendidikan']	= '';
		$data['jenis_pendidikan']	= '';
		$data['keterangan']	= '';
		$data['id_golpangkat_awal']	= '';
		$data['id_golpangkat_akhir']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_pendidikan']	= $this->input->post('id_pendidikan', TRUE);
	   	$data['nama_pendidikan']		= $this->input->post('nama_pendidikan', TRUE);
		$data['jenis_pendidikan']		= $this->input->post('jenis_pendidikan', TRUE);
		$data['id_golpangkat_awal']		= $this->input->post('id_golpangkat_awal', TRUE);
		$data['id_golpangkat_akhir']		= $this->input->post('id_golpangkat_akhir', TRUE);
		$data['keterangan']		= $this->input->post('keterangan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_pendidikan', 'tingkat pendidikan', 'required');
		/*$this->form_validation->set_rules('id_pendidikan', 'id_pendidikan', 'required');*/
		return $this->form_validation->run();
	}
}