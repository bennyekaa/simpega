<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jenis_pegawai extends Member_Controller
{

	function jenis_pegawai()
	{
		parent::Member_Controller();
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
		$this->load->model('parameter_model', 'parameter');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data Jenis Pegawai');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'id_jns_pegawai';
		$data['parameter_assoc'] = $this->parameter->get_assoc();
		$data['list_jenis_pegawai'] 	= $this->jenis_pegawai->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/jenis_pegawai/browse/');
		$config['total_rows']   = $this->jenis_pegawai->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();   
		$data['judul'] 		= "Data Jenis Pegawai";
		//show_error(var_dump($data));
		$this->template->display('setup/jenis_pegawai/list_jenis_pegawai', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->jenis_pegawai->add($data);
			set_success('Data Jenis Pegawai Berhasil Disimpan.');
			redirect('/setup/jenis_pegawai');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Jenis Pegawai';
			$data['id_jns_pegawai']=$this->jenis_pegawai->get_id();
			$data['parameter_assoc'] = $this->parameter->get_assoc();
			$this->template->display('/setup/jenis_pegawai/detail_jenis_pegawai', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_jns_pegawai'] = $id;
			
			$this->jenis_pegawai->update($id, $data);
			set_success('Perubahan data jenis pegawai berhasil disimpan');
			redirect('/setup/jenis_pegawai', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Pegawai :: Ubah');
			$data = $this->jenis_pegawai->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Jenis Pegawai';
				$data['parameter_assoc'] = $this->parameter->get_assoc();
				$this->template->display('/setup/jenis_pegawai/detail_jenis_pegawai', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/jenis_pegawai', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->jenis_pegawai->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Jenis Pegawai :: Hapus');
		confirm("Yakin menghapus data jenis pegawai <b>".$data['jenis_pegawai']."</b> ?");
		$res = $this->jenis_pegawai->delete($idField);
		set_success('Data jenis pegawai berhasil dihapus');
		redirect('/setup/jenis_pegawai', 'location');
	}

	function _clear_form()
	{
		$data['id_jns_pegawai']	= '';
		$data['jenis_pegawai']	= '';
		$data['usia_pensiun'] 	= '';
		$data['id_parameter'] 	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_jns_pegawai']	= $this->input->post('id_jns_pegawai', TRUE);
	   	$data['jenis_pegawai']	= $this->input->post('jenis_pegawai', TRUE);
		$data['usia_pensiun']	= $this->input->post('usia_pensiun', TRUE);
		$data['id_parameter']	= $this->input->post('id_parameter', TRUE);
		
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('jenis_pegawai', 'jenis pegawai', 'required');
		return $this->form_validation->run();
	}
}