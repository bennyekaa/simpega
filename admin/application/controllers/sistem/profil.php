<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profil extends My_Controller
{
	function Profil()
	{
		parent::My_Controller();
		$this->load->library('form_validation');
		$this->load->model('upload_model');
		$this->load->model('users_model');
		$this->load->helper('form');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Profil User');
		$this->browse();
	}
	
	function browse()
	{
		$data = $this->users_model->retrieve_by_pkey($this->user->user_id);
		$data['judul'] = 'Profil User';
		$this->template->display('sistem/profil',$data);
	}
	
	function ubah()
	{
		$this->access->restrict();
		$this->form_validation->set_rules('oldpass', 'Password Lama', 'required|min_length[1]|max_length[12]');
		$this->form_validation->set_rules('password', 'Password Baru', 'required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Konfirmasi Password', 'required');
		$data['judul'] = 'Ubah Password';

		if ($this->form_validation->run() == FALSE)
		{
			$this->template->metas('title', 'SIMPEGA | Ubah Password');
			$this->template->display('sistem/ubah-password', $data);
		}
		else
		{
			$this->access->change_password($this->input->post('password',TRUE));
			set_success('Password berhasil dirubah');
			redirect('staff/logout');
		}
	}
	
	function upload()
	{
		if ($this->_validate())
		{	
			$id=$this->user->user_id;
			$data = $this->_get_form_values();
			$data['FK_pegawai']=$id;
			if($_FILES['gambar']['name']){
				//upload gambar
				$config['upload_path'] = './public/images/user-image/';
				$config['allowed_types'] = 'gif';
				$config['max_size'] = '100';
				$config['max_width']  = '200';
				$config['max_height']  = '200';
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('gambar')){
					set_error('Upload error, File Gambar tidak valid!!');
					redirect('staf/info/upload');
				}   
				else{
					$gambar = array('upload_data' => $this->upload->data());
					
					$gambar = $this->input->xss_clean($gambar);
					$tags = $_POST['tags'];
					unset($_POST['tags']);
					$photo_id = $gambar['upload_data']['file_name'];
					list($width, $height, $type, $attr) = getimagesize($config['upload_path'] .$photo_id);
				
					$this->load->library('image_lib');
					if ($width > 30 or $height>24){
						//resize image
						$config['image_library'] 	= 'GD2';
						$config['source_image'] 	= $config['upload_path'] . $photo_id ;
						$config['new_image'] 		= $config['upload_path'] . $photo_id ;
						$config['create_thumb'] 	= FALSE;
						$config['maintain_ratio'] 	= TRUE;
						$config['width'] = 30;
						$config['height'] = 24;
						$config['master_dim']=(abs($width-$config['width'])>=abs($height-$config['height']))? 'width':'height';
						$this->image_lib->initialize($config);
						$this->load->library('image_lib', $config);
						if(!$this->image_lib->resize())
							echo $this->image_lib->display_errors();
					}
					$data['gambar'] =  $photo_id;
				}
			}
			$this->upload_model->delete($id);
			$this->upload_model->add($data);
			set_success('Gambar berhasil diupload');
			redirect('/staf/profil/index');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Upload Image');
			$data = $this->_clear_form();
			$data['action']='upload/'.($id);
			$data['judul']='Upload image user';
			$this->template->display('/sistem/upload-foto', $data);
		}
	}
	
	function _clear_form()
	{
		$data['id_gambar']		= '';
		$data['gambar']			= '';
		return $data;
	}

	function _get_form_values()
	{
	   	$data['id_gambar']		= $this->input->post('id_gambar', TRUE);
		$data['gambar']			= $this->input->post('gambar', TRUE);
		return $data;
	}

	function _validate()
	{
		$this->form_validation->set_rules('gambar', 'gambar', 'required');
		return $this->form_validation->run();
	}
}