<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ubah extends Member_Controller
{
	function Ubah()
	{
		parent::Member_Controller();
		$this->load->library('form_validation');
		$this->load->helper('form');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Ubah Password');
		$this->browse();
	}
	
	function browse()
	{
		$this->access->restrict();
		$this->template->metas('title', 'SIMPEGA | Ubah Password');
		$this->form_validation->set_rules('oldpass', 'oldpass', 'required|min_length[1]|max_length[12]');
		$this->form_validation->set_rules('password', 'password', 'required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'passconf', 'required');
		$data['judul'] = 'Ubah Password';

		if ($this->form_validation->run() == FALSE)
		{
			$this->template->display('sistem/ubah-password', $data);
		}
		else
		{
			$this->access->change_password($this->input->post('password',TRUE));
			set_success('Password berhasil dirubah');
			redirect('staff/logout');
		}
	}
}