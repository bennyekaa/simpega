<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Group extends Member_Controller
{
	function Group()
	{
		parent::Member_Controller();
		$this->load->model('Usergroup_model','usergroup');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Grup User');
		$this->browse();
	}
	
	function browse($group=0)
	{
		$data['list_group'] = $this->usergroup->findAll();
		$data['judul'] = 'Daftar Grup User';
		$this->template->display('setup/user/user-group', $data);
	}
}