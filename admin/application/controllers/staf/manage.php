<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends Member_Controller
{
	function Manage()
	{
		parent::Member_Controller();
		//		parent::Member_Controller();
		$this->load->library('validation');
		$this->load->model('users_model','users');
		$this->load->model('pegawai_model','pegawai');
		$this->load->model('usergroup_model');
		$this->load->model('user_pegawai_model','user_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Pengaturan User');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		
		if($this->input->post('search'))
        {    
			$search = $this->input->post('search');
		}
		else
		{
			$search = '';
		}
		
		 $search_param = array();
        if($search)
            $search_param[] = "(username like '%$search%')";
			$search_param[] = "(name like '%$search%')";
			$search_param = implode(" OR ", $search_param);
			
		$limit_per_page = 10;
		$data['start'] 		= $start;
		$data['list_user'] 		= $this->users->findAll($limit_per_page,$start,$search_param);
		$config['base_url']     = site_url('staf/manage/browse/');
		$config['total_rows']   = $this->users->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['judul'] = 'Pengaturan User';
		$this->template->display('setup/user/manage-user', $data);
	}

	function add()
	{
   		$this->template->metas('title', 'SIMPEGA | Pengaturan User :: Tambah');  
		$this->template->load_js('public/js/linkedselect.js');
		$this->template->load_js('public/js/jquery.validate.js');
		if ($this->_validate('add')) {
			$data = $this->_get_form_values();
			$cek = $this->user_pegawai->cek_id_peg($this->input->post('kd_pegawai'));
			//show_error(var_dump($cek));
			if($cek!=TRUE) {		
				$this->user_pegawai->add(array('user_id' => $this->input->post('user_id'),'kd_pegawai'=>$this->input->post('kd_pegawai')));
				$this->users->add($data);
				set_success('Data user berhasil disimpan');
				redirect('staf/manage');
			}
			else {
				set_error('Penyimpanan gagal, pegawai sudah terdaftar');
				redirect('staf/manage');
			}
		}
		else{
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah User';
			$data['user_id']=$this->users->get_id();
			$data['is_new'] = TRUE;
			$data['pegawai_options']=array(0=>"-- Pilih Pegawai --")+$this->pegawai->get_assoc("NIP");
			$this->template->display('setup/user/detail-manage-user', $data);
		}
	}
	
	
	function edit($id)
	{
		$this->template->metas('title', 'SIMPEGA | Pengaturan User :: Ubah');  
		$this->template->load_js('public/js/linkedselect.js');
		$this->template->load_js('public/js/jquery.validate.js');	
		$id_peg = $this->user_pegawai->get_idpegawai_by_userid($id);
		if ($this->_validate())
		{
			$cek_id = $this->input->post('kd_pegawai', TRUE);
			if($cek_id == $id_peg)
			{
				$data = array(
							'user_id'	=> $id,
							'FK_group_id' => $this->input->post('FK_group_id', TRUE),
							'name'		=> $this->input->post('name', TRUE),
							'username'	=> $this->input->post('username', TRUE),
							'email'		=> $this->input->post('email', TRUE));
				$data1 = array('user_id'	=> $id,
							'kd_pegawai'	=> $cek_id);
				$this->users->update_user($id, $data);
				$this->user_pegawai->update_user($id, $data1);
				//show_error(var_dump($data+$data1));
				set_success('Data user berhasil disimpan');
				redirect('staf/manage');
			}
			else
			{
				set_error('Penyimpanan gagal, pegawai sudah terdaftar dengan User lain');
				redirect('staf/manage');
			}
		}
		else
		{
			$data = $this->users->retrieve_by_pkey($id);
			$data['action'] = 'edit/'.$id;
			$data['judul'] = 'Ubah User';
			$data['is_new'] = FALSE;
			$data['pegawai_options'] = array(0=>"-- Pilih Pegawai --") + $this->pegawai->get_assoc("nama_pegawai");
			$data['kd_pegawai'] = $id_peg;
			$this->template->display('setup/user/detail-manage-user', $data);
		}
	}
	
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->users->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Pengaturan User :: Hapus');
		confirm("Yakin menghapus user <b>".$data['username']."</b> ?");
		$res = $this->users->delete($idField);
		$res2 = $this->user_pegawai->delete($data['user_id']);
		set_success('Data user berhasil dihapus');
		redirect('staf/manage', 'location');
	}

	function _clear_form()
	{
		$data['user_id']	= '';
		$data['username']	= '';
		$data['password']	= '';
		$data['FK_group_id']	= '';
		$data['email']	= '';
		$data['name']	= '';
		 $data['search'] = '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['user_id']	= $this->input->post('user_id', TRUE);
		$data['username']	= $this->input->post('username', TRUE);
		$data['password']	= $this->input->post('password', TRUE);;
		$data['FK_group_id']= $this->input->post('FK_group_id', TRUE);;
		$data['email']		= $this->input->post('email', TRUE);;
		$data['name']		= $this->input->post('name', TRUE);;
		 //$data['search'] = $this->input->post('search', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('username', 'username', 'required');
		
		//$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('FK_group_id', 'FK_group_id', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		return $this->form_validation->run();
	}
}