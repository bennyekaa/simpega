<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Home extends Member_Controller
{
	function Home()
	{
		parent::Member_Controller();
	}
	
	function index()
	{
		$this->template->display('staff/home');
	}
}