<?php  if (!defined('BASEPATH')) exit('No direct script rule allowed');

class Menu extends Member_Controller
{
	function __construct() 	{
		parent::Member_Controller();
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('menus_model');
		$this->load->model('rule_model');
		$this->load->model('pegawai_model');
	}

	function index()
	{
		$this->browse();
	}

	function browse($id_menu="")
	{
		$menu = $this->menus_model->retrieve_by_pkey($id_menu);
		$the_results['parent'] = $menu['parent'];
		$the_results['name'] = $menu['name'];
		$the_results['parent'] = $menu['parent'];
		$the_results['id_menu'] = $id_menu;
		$the_results['menu_list'] = $this->menus_model->FindAll();
		$the_results['judul'] = "Daftar Menu ".$menu['name'];
		$this->template->display('setup/user/manage-menu', $the_results);
	}

	function add($parent=0)
	{
		if ($this->_validate())
		{
			$parent=$this->uri->segment(4);
			$data = $this->_get_form_values();
			$data['parent'] = $parent;
			if(!$data['ordering'])
				$data['ordering']=$this->menus_model->get_newordering($parent)+1;
			$this->menus_model->add($data);
			set_success('Data menu berhasil disimpan');
			redirect('/staf/menu/browse/'.$parent);
		}
		else
		{
			$data = $this->_clear_form();
			$data['action']='add'.($parent?"/".$parent:"");
			$data['judul']='Tambah Menu';
			$data['parent']=$parent;
			$data['option_rule']=$this->rule_model->get_rule();
			$data['option_rule'] = array(""=>array(0=>"-- Pilihan Rule --"))+$data['option_rule'];
			$this->template->display('/setup/user/detail-manage-menu', $data);
		}
	}
	

	function edit($id)
	{
		if ($this->_validate2())
		{
			$data = $this->_get_form_values();
			$data['id_menu']=$id;
			$this->menus_model->modify($id, $data);
			set_success('Perubahan data menu berhasil disimpan');
			redirect('/staf/menu', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Menu :: Ubah');
			$data = $this->menus_model->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Ubah Menu';
				$query = mysql_query("select r.id_rule, r.rule_name,m.parent from menus m, rule r 
									  where r.id_rule = m.id_menu and m.id_menu='".$id."'");					
				if ($query) {
						$rule = mysql_fetch_array($query);  
						$id_rule = $rule['id_rule'];
						$nama_rule =  $rule['rule_name'];
						$id_parent =  $rule['parent'];}	
						
				$query = mysql_query("select name from menus where id_menu='".$id_parent."'");					
				if ($query) {
						$menu=mysql_fetch_array($query);  
						$nama_parent =  $menu['name'];}	
				
								
				$data['option_rule']=array($id_rule=>$nama_rule)+$this->rule_model->get_rule();
				$data['option_menu']=array($id_parent=>$nama_parent)+$this->menus_model->get_menu();
				$this->template->display('/setup/user/detail-manage-menu', $data);
			}
			/*else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/user', 'location');
			}*/
		}
	}
	
	
	function delete() {
		$idField = $this->uri->segment(4);
		$data = $this->menus_model->retrieve_by_pkey($idField);
		confirm("Yakin menghapus menu <b>".$data['name']."</b> ?");
		$res = $this->menus_model->delete_by_pkey($idField);
		set_success('Data menu berhasil dihapus');
		redirect('/staf/menu/browse/'.$data['parent'], 'location');
	}
	
	function delete_sel(){
		$idField = $this->uri->segment(4);
		if($ids=$this->input->post('id',true)){
			confirm2("Yakin menghapus menu yang terpilih ?",array('id'=>$ids));
			$ids = explode(';',$ids);
			
			$res = $this->menus_model->delete_any($ids);
			set_success('Data menu berhasil dihapus');
			redirect('/staf/menu/browse/'.$idField, 'location');
		}
		else{
			set_error('Tidak ada data yang dipilih untuk dihapus');
			redirect('/staf/menu/browse/'.$idField, 'location');
		};
	}
	
	function saveorder(){
		$idField = $this->uri->segment(4);
		if(($ids=$this->input->post('id_orders',true)) && 
				($orders=$this->input->post('orders',true))){
			$ids 	= explode(';',$ids);
			$orders = explode(';',$orders);
			
			$rules	= array_combine($ids,$orders);
			if($rules) foreach($rules as $id=>$value){
				$this->menus_model->modify($id, array('ordering'=>$value));
			}
			set_success('Data urutan menu berhasil diubah');
			redirect('staf/menu/browse/'.$idField, 'location');
		}else{
			set_error('Tidak ada urutan menu yang diubah');
			redirect('/staf/menu/browse'.$idField, 'location');
		};
	}
	
	function _clear_form(){
		$data['id_menu']			= '';
		$data['id_rule']			= '';
		$data['name']				= '';
		$data['parent']				= '';
		$data['ordering']			= '';
		return $data;
	}

	function _get_form_values(){
	   	$data['id_menu']			= $this->input->post('id_menu', TRUE);
		$data['id_rule']			= $this->input->post('id_rule', TRUE);
		$data['parent']				= $this->input->post('parent', TRUE);
		$data['name']				= $this->input->post('name', TRUE);
		$data['ordering']			= $this->input->post('ordering', TRUE);
		return $data;
	}

	function _validate(){
		$this->form_validation->set_rules('name', 'name', 'required|callback__nama_check');
		return $this->form_validation->run();
	}
	
	function _validate2(){
		$this->form_validation->set_rules('name', 'name', 'required|callback__nama_check');
		$this->form_validation->set_rules('ordering', 'ordering', 'required|callback__ordering_check');
		$this->form_validation->set_rules('id_menu', 'id_menu', 'required|callback__id_menu_check');
		$this->form_validation->set_rules('id_rule', 'id_rule', 'required|callback__id_rule_check');
		$this->form_validation->set_rules('parent', 'parent', 'required|callback__parent_check');
		return $this->form_validation->run();
	}
}
?>
