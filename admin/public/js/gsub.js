function gsub_num(el)
	{
		var str=el.value;
		var i, num='';
		
		myRegExp = /[0-9]/ ;
		for (i=0;i<str.length;i++) {
			if (myRegExp(str[i])) 
				num = num + str[i];
		}
		el.value = num;
	}
	
	function gsub_onlychar(el)
	{
		var str=el.value;
		var i, num='';
		myRegExp = /[0-9a-zA-Z]/ ;
		if (str[0] && myRegExp.test(str[0]))
			num=str[0];
		el.value = num;
	}
	
	function gsub_tahun(el)
	{
		var str=el.value;
		var i, num='';
		
		myRegExp = /[0-9]/ ;
		for (i=0;i<4;i++) {
			if (myRegExp(str[i])) 
				num = num + str[i];
		}
		el.value = num;
	}
	
	function gsub_realnum(el){
		var str=el.value;
		while (str && !((str+1)/2)) {
			if (str.length==1)
				str='';
			else
				str = str.substring(0,str.length-1);
		};
		el.value = str;
	}
	
	function gsub_realnum_unsigned(el){
		if(el.value == "-") el.value="";
		if(el.value < 0) el.value*=-1;
		var str=el.value;
		while (str && !((str+1)/2)) {
			if (str.length==1) str='';
			else
			str = str.substring(0,str.length-1);
		};
		el.value = str;
	}
	
	function gsub_alpha_num(el)
	{
		var str=el.value;
		var i, num='';
		
		myRegExp = /[a-z\ \_\-A-Z0-9]/ ;
		for (i=0;i<str.length;i++) {
			if (myRegExp(str[i])) 
				num = num + str[i];
		}
		el.value = num;
	}
	
	function gsub_test(el)
	{
		var str=el.value;
		var i, num='';
		
		myRegExp = /[0-9\ ]/ ;
		for (i=0;i<str.length;i++) {
			if (myRegExp(str[i])) 
				num = num + str[i];
		}
		el.value = num;
	}
	
	function gsub_input(el)
	{
		var str=el.value;
		var i, num='';
		
		myRegExp = /[0-9]/ ;
		for (i=0;i<str.length;i++) {
			if (myRegExp(str[i])) 
				num = num + str[i];
		}
		if(num > 100)
			num = 1;
		el.value = num;
	}