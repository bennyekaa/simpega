<?php 	

	include "../config/conf.php";
	
	$actionData = anti_injection("act");
	
	if ($actionData=='read') {	

		$dataYangDiCari = anti_injection("dataYangDiCari");
		$halaman = anti_injection("page");

		$dataPerPage = 25;	
		if(isset($halaman)) {
			$noPage = $halaman;
		} 
		else $noPage = 1;
		$offset = ($noPage - 1) * $dataPerPage;
				
		$queryjml 	= "SELECT COUNT(*) AS jumData from pegawai, golongan_pangkat, unit_kerja
WHERE pegawai.kode_unit = unit_kerja.kode_unit
AND pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat AND (pegawai.nama_pegawai like '%".$dataYangDiCari."%' or pegawai.nip like '%".$dataYangDiCari."%')";
		$hasil  = mysql_query($queryjml);
		$data   = mysql_fetch_array($hasil);
	
		$jumData = $data['jumData'];
		 
		$jumPage = ceil($jumData/$dataPerPage);
	
		$query = mysql_query("SELECT pegawai.nip, pegawai.nama_pegawai, golongan_pangkat.golongan, unit_kerja.nama_unit, pegawai.npwp
FROM pegawai, golongan_pangkat, unit_kerja
WHERE pegawai.kode_unit = unit_kerja.kode_unit
AND pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat AND (pegawai.nama_pegawai like '%".$dataYangDiCari."%' or pegawai.nip like '%".$dataYangDiCari."%') order by pegawai.id_golpangkat_terakhir, pegawai.nip LIMIT ".$offset.", ". $dataPerPage);
		$jmlquery = mysql_num_rows($query);
?>
<legend>Data Pegawai</legend>
    	<p align="justify">Terdapat <b><?php echo $jumData; ?></b> data pegawai.</p>
    <table class="table table-hover table-striped" style="font-size:14px">
        <thead>
            <tr>
                <th width="30">No.</th>
                <th width="50">NIP</th>
                <th>Nama Pegawai</th>
                <th width="50">Golongan</th>
                <th>Unit&nbsp;Kerja</th>
                <th width="150">NPWP</th>
            </tr>
        </thead>
        <tbody class="table-condensed">
<?php
	$x = 1;
	$bgcol="warning";
	if ($query) {
		while ($dataquery=mysql_fetch_array($query)) {
?>
            <tr>
                <td style="text-align:left;vertical-align:top"><?php echo $x; ?>.</td>
                <td style="text-align:left"><?php echo $dataquery['nip']; ?></td>
                <td style="text-align:justify"><?php echo str_replace(' ','&nbsp;',$dataquery['nama_pegawai']); ?></td>
                <td style="text-align:center"><?php echo $dataquery['golongan']; ?></td>
                <td style="text-align:left"><?php echo str_replace(' ','&nbsp;',$dataquery['nama_unit']); ?></td>
                <td style="text-align:left"><?php echo $dataquery['npwp']; ?></td>
            </tr>			
<?php			
			if ($x%2==1){$bgcol="error";}
            else {$bgcol="warning";} 
			$x++;		
		}
	}
?>
	    </tbody>
    </table>
    <div class="pagination  pagination-right">
        <ul>
			<?php
                if ($noPage > 1) {
            ?>										      
		            <li><a href="#" onclick="ShowSoal(<?php echo $noPage-1; ?>,document.getElementById('frminputcaridata').value)"  class="active">&laquo;</a></li>
        	<?php
				}
				
				for($page = 1; $page <= $jumPage; $page++) {
					if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) {   
						if (($showPage == 1) && ($page != 2))  echo "<li><a href='#' class=\"active\">..</a></li>"; 
						if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  echo "<li><a href='#' class=\"active\">..</a></li>";
						if ($page == $noPage) {
							?>
      						<li class="disabled"><a href="#"><?php echo $page; ?></a></li>
			<?php
						} else {
			?>
      						<li><a href="#" onclick="ShowSoal(<?php echo $page; ?>,document.getElementById('frminputcaridata').value)" class="active"><?php echo $page; ?></a></li>
			<?php
						}
						$showPage = $page;
					}
				}
				
				if ($noPage < $jumPage) {
			?>
            		<li><a href="#" onclick="ShowSoal(<?php echo $noPage+1; ?>,document.getElementById('frminputcaridata').value)" class="active">&raquo;</a></li>
            <?php
				}
			?>
        </ul>
    </div>
<?php
	}
?>