      <div class="jumbotron">
        <div id="myCarousel" class="carousel slide">
          <div class="carousel-inner">
            <div class="item active">
              <img src="img/header/1.jpg" alt="">
            </div>
            <div class="item">
              <img src="img/header/2.jpg" alt="">
            </div>
            <div class="item">
              <img src="img/header/3.jpg" alt="">
            </div>
          </div>
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div><!-- /.carousel -->
      </div>

<h3>Selamat Datang di Sistem Informasi Kepegawaian</h3>
          <p align="justify">
		Aplikasi SIMPEGA (Sistem Informasi Kepegawaian) Universitas Negeri Malang adalah aplikasi berbasis web yang digunakan untuk menunjang proses administrasi kepegawaian di Universitas Negeri Malang. 
		Untuk menggunakan SIMPEGA terlebih dahulu harus memiliki NIP dan account SIMPEGA. Untuk mendapatkan Account, hubungi bagian Kepegawaian 
		UM. <br>
		Kantor Pusat UM, Gedung A2 lantai 2 Bagian Kepegawaian<br>
		Kritik, saran, pertanyaan dapat juga dilayangkan ke email:<br>
		kepegawaian@um.ac.id
	 </p>
      		
<legend>Berita Terbaru</legend>
      <div class="row-fluid">
<?php
	$query = mysql_query("select * from berita where status_aktif='1' order by tgl_publis desc limit 0,3");
	if ($query) {
		while ($data=mysql_fetch_array($query)) {
?>
        <div class="span4">
          <?php

		  		$pnjjudul = strlen($data['judul_berita']);
				if ($pnjjudul >= 15) {
					$judul = substr($data['judul_berita'], 0, 15)."...";
				} else {
					$judul = $data['judul_berita'];	
				}

		  		$pnjisi = strlen($data['isi_berita']);
				if ($pnjisi >= 221) {
					$isi = substr($data['isi_berita'], 0, 215)."...";
				} else {
					$isi = $data['isi_berita'];	
				}
				
		  ?>
          <h2><?php echo $judul; ?></h2>
          <p><?php echo $isi; ?></p>
          <p><a class="btn" href="?lp=<?php echo encrypt_url($data['id_berita'].'#detail_berita'); ?>">Selanjutnya &raquo;</a></p>
        </div>
<?php
		}
	}
?>

      </div>

<a href="?lp=<?php echo encrypt_url('berita_index'); ?>">&raquo; Berita Lainnya</a> 
