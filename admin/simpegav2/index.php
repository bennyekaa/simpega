<?php
	session_start();

	include("config/conf.php");

    $loadpage = $_REQUEST['lp'];
	$hsldes = decrypt_url($loadpage);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $webname; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/icon.ico">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="ico/favicon.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="?lp=<?php echo encrypt_url('home'); ?>"><img src="img/icon.JPG" align="absmiddle" width="16" height="16">&nbsp;<?php echo $webdesc; ?></a>
          <div class="navbar-text pull-left">
            <ul class="nav">
              <li><a href="?lp=<?php echo encrypt_url('daftar'); ?>">Buku&nbsp;Tamu</a></li>
              <li><a href="?lp=<?php echo encrypt_url('daftar'); ?>">Download</a></li>
              <li><a href="?lp=<?php echo encrypt_url('daftar'); ?>">Hubungi&nbsp;Admin&nbsp;Simpega</a></li>
              <li><a href="https://simpega.um.ac.id/admin/index.php/staff/login">Login&nbsp;Simpega</a></li>
            </ul>
          </div>                    
          <div class="nav-collapse collapse">
            <form class="form-search navbar-form pull-right">
              <div class="input-append">
                <input type="text" class="span2 search-query"  placeholder="NIP atau Nama Pegawai"  onkeyup="ShowSoal(1,document.getElementById('frminputcaridata').value)" id="frminputcaridata" >
                <button type="submit" class="btn" onclick="ShowSoal(1,document.getElementById('frminputcaridata').value)">Cari</button>
              </div>
            </form>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
		<div id="TmpDataShowSoal">
      <!-- Main hero unit for a primary marketing message or call to action -->       
		<?php
            if ($loadpage=="") {
                $loadpage = encrypt_url("home");
            } else {
                $loadpage = $loadpage;
            } 
            
            $hasildes = decrypt_url($loadpage);
			
			$ekstrakloadpage = explode("#",$hasildes);
			$jmlekstrakloadpage = count($ekstrakloadpage);
			
			if ($jmlekstrakloadpage==1) {		
				$_SESSION['idberita'] = "";
				
				include("application/".$ekstrakloadpage[0].".php");
			} else if ($jmlekstrakloadpage==2) {	
				$_SESSION['idberita'] = "";
				$_SESSION['idberita'] = $ekstrakloadpage[0];
				
				include("application/".$ekstrakloadpage[1].".php");
			}
        ?>          

      <hr />

    <div id="footer">
      <div class="container">
        <p class="muted credit">&copy; IT-BUK 2013 | Buku Tamu | Download | Hubungi Admin Simpega | <a href="https://simpega.um.ac.id/admin/index.php/staff/login">Login SIMPEGA</a></p>
      </div>
    </div>
    	
        </div>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap-fileinput.js"></script>    
    <script src="js/bootstrap-datepicker.js"></script>    
    <script src="js/action.js"></script>
    
    <script src="application/home.js"></script>
  </body>
</html>